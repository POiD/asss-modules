
#define I_POINTS_GOALS "points_goals-1"

typedef struct Ipointsgoals
{
        INTERFACE_HEAD_DECL

        void (*ResetGame)(Arena *arena, Player *player);
	int (*GetScores)(Arena *arena, int *scores, int freqs);
	void (*SetScores)(Arena *arena, int *scores);
	void (*ResetScores)(Arena *arena);

} Ipointsgoals;

/* called when a player scores a goal
 * @threading called from main
 */
#define CB_PGGOAL "pggoal"
typedef void (*PgGoalFunc)(Arena *arena, Player *p, int bid, int x, int y);
/* pycb: arena_not_none, player_not_none, int, int, int */
