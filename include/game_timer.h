
#ifndef __GAME_TIMER_H
#define __GAME_TIMER_H


#define CB_TIMEPAUSE 	"timepause"
#define CB_TIMERESUME	"timeresume"
#define CB_TIMESET	"timeset"

typedef void (*GameTimerChangeFunc)(Arena *arena, ticks_t time);

/** the interface id for Igametimer */
#define I_GAMETIMER "gametimer-1"

typedef struct Igametimer
{
	INTERFACE_HEAD_DECL

	bool (*SetInGameTimer)(Arena* arena, int timeSeconds);

} Igametimer;

#endif