#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "pbfreeplay.h"
#include "balls.h"
#include "../scoring/points_goal.h"
#include "teams.h"
#include "pb_utils.c"
#include "signups.h"
#include "pbstats.h"
#include "pbextras.h"

// Interfaces
local Imodman *			mm;
local Ichat *			chat;
local Iarenaman *		aman;
local Iplayerdata *		pd;	//x
local Iballs *			balls;	//x
local Imainloop * 		ml;	//x
local Igame *			game;	//x
local Icmdman *			cmd;
local Ilogman *			lm;
local Ipointsgoals *	pg;
local Iconfig *			cfg;
local Iteams *			teams;
local Icapman *			capman;
local Isignups *		signups;
local Ipbstats *		pbstats;
local Ipbextras *		pbextras;


local int 		arenaKey	= -1;
local int		playerKey	= -1;


local void HelpCommands(Player *player);

local Ipbfreeplay _int =
{
	INTERFACE_HEAD_INIT(I_PBFREEPLAY, "pbfreeplay")
	HelpCommands
};

EXPORT const char info_pbfreeplay[] = CORE_MOD_INFO("pbfreeplay");


/****************************************************************
 *		INITIALIZATION SECTION				*
 ****************************************************************/
local void InitializeArenaData(Arena *arena)
{
	FreeArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->roundStart		= FALSE;
	arenaData->randomizeTeamsOnEnd	= TRUE;

	Target target;
	target.type=T_ARENA;
	target.u.arena=arena;

	arenaData->arenaTarget			= target;
}

local void ReleaseArenaData(Arena *arena)
{
	//MyArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);
}

		/************************************************************************************************
		 *					COMMANDS SECTION					*
		 ************************************************************************************************/

/****************************************************************
 *			HELP COMMANDS				*
 ****************************************************************/
local helptext_t pbhelp_help =
"Module: pbfreeplay\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module available commands.";

local void CPbHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);
}

local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;
	//FreeArenaData *arenaData		= P_ARENA_DATA(player->arena, arenaKey);

	chat->SendMessage(player, "----------------------------------------------------");
	chat->SendMessage(player, "The following PB Module commands are available:");
	chat->SendMessage(player, "----------------------------------------------------");

	DisplayCommand(chat, player, "?pbhelp",		"Display this help list");
	DisplayCommand(chat, player, "?pbversion",	"Display the PB Module version information");

	DisplayCommand(chat, player, "?randomize",		"Log your vote for randomizing the teams");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_randomnow",		"?randomnow",		"Randomize the teams and restart the game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_togglerandom",	"?togglerandom [ON/OFF]","Display or Enabled/Disable if teams are randomized at the end of a game.");
}

/****************************************************************
 *			VERSION					*
 ****************************************************************/
local helptext_t pbversion_help =
"Module: pbfreeplay\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module version information.";

local void CPbVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS PB Module -- Version: 1.2    Date: 10 October 2021    Author: POiD");
}

/****************************************************************
 *			RANDOMIZE				*
 ****************************************************************/
local helptext_t randomize_help =
"Module: pbfreeplay\n"
"Targets: none\n"
"Args: none\n"
"Randomize teams with players in game.";

local void RandomizeTeams(Arena *arena);

local void CRandomize(const char *command, const char *params, Player *player, const Target *target)
{
	//FreeArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	int total		= 0;
	int yesVote		= 0;
	Player *currentPlayer	= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(currentPlayer, player->arena)
	{
		if (currentPlayer->status != S_PLAYING)
			continue;

		if (currentPlayer->p_ship != SHIP_SPEC)
		{
			total++;
			PBPlayerVoting *playerData = PPDATA(currentPlayer, playerKey);

			if (currentPlayer == player)
			{
				playerData->randomizeVote = !playerData->randomizeVote;
				chat->SendMessage(player, "Your vote to random teams has been set to %s", playerData->randomizeVote ? "Yes" : "No" );
			}

			if (playerData->randomizeVote)
				yesVote++;
		}
	}
	pd->Unlock();

	if (yesVote > (total / 2))
	{
		RandomizeTeams(player->arena);
		pg->ResetGame(player->arena, player);
	}
	else
	{
		chat->SendMessage(player, "Current votes for randomizing:  For: %d  Against: %d", yesVote, (total-yesVote));
	}
}

local void RandomizeTeams(Arena *arena)
{
	//FreeArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

	int freqCounts[4]	= { 0, 0, 0, 0};

	int random		= 0;
	int total		= 0;
	int extra		= 0;

	Target target;
	target.type		= T_PLAYER;

	Player *player	= NULL;
	Link *link		= NULL;

	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		PBPlayerVoting *playerData	= PPDATA(player, playerKey);
		if (playerData)
			playerData->randomizeVote	= FALSE;

		if ((player->status == S_PLAYING) && (player->p_ship != SHIP_SPEC))
		{
			total++;
		}
	}
	pd->Unlock();

	extra	= total % 2;
	total	= total / 2;

	if (total != 0)
	{
		pd->Lock();
		player	= NULL;
		link	= NULL;
		FOR_EACH_PLAYER_IN_ARENA(player, arena)
		{
			if (player->status != S_PLAYING)
				continue;

			if (player->p_ship != SHIP_SPEC)
			{
				if (freqCounts[1] == total)
				{
					if (player->p_freq != 0)
						game->SetShipAndFreq(player, 0, 0);
				}
				else
				{
					//warp
					target.u.p	= player;
					game->GivePrize(&target, PRIZE_WARP , 1);
				}
			}
			else if (freqCounts[0] == total + extra)
			{
				if (player->p_freq != 1)
					game->SetShipAndFreq(player, 1, 1);
				else
				{
					//warp
					target.u.p	= player;
					game->GivePrize(&target, PRIZE_WARP , 1);
				}
			}
			else
			{
				random = rand() % 2;
				freqCounts[random]++;

				if (player->p_freq != random)
				{
					game->SetShipAndFreq(player, random, random);
				}
				else
				{
					//warp
					target.u.p	= player;
					game->GivePrize(&target, PRIZE_WARP , 1);
				}
			}
		}
		pd->Unlock();
		chat->SendArenaMessage(arena, "Teams Randomized!");
	}
}


/****************************************************************
 *			RANDOM NOW				*
 ****************************************************************/
local helptext_t randomnow_help =
"Module: pbfreeplay\n"
"Targets: none\n"
"Args: none\n"
"Op Command to randomize teams and restart the game.";

local void CRandomNow(const char *command, const char *params, Player *player, const Target *target)
{
	//FreeArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	RandomizeTeams(player->arena);
	pg->ResetGame(player->arena, player);
}

/****************************************************************
 *			TOGGLE RANDOM				*
 ****************************************************************/
local helptext_t togglerandom_help =
"Module: pbfreeplay\n"
"Targets: none\n"
"Args: none\n"
"Toggle whether teams are randomized at the end of a game.";

local void CToggleRandom(const char *command, const char *params, Player *player, const Target *target)
{
	FreeArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Randomize currently set as %s", arenaData->randomizeTeamsOnEnd ? "ON" : "OFF");
		return;
	}

	if (strncasecmp("ON",params,2) == 0)
	{
		if (arenaData->randomizeTeamsOnEnd)
		{
			chat->SendMessage(player, "Randomize is already set to ON");
			return;
		}

		arenaData->randomizeTeamsOnEnd = TRUE;
		chat->SendMessage(player, "Randomize set to ON");
	}
	else if (strncasecmp("OFF",params,3) == 0)
	{
		if (!arenaData->randomizeTeamsOnEnd)
		{
			chat->SendMessage(player, "Randomize is already set to OFF");
			return;
		}

		arenaData->randomizeTeamsOnEnd = FALSE;
		chat->SendMessage(player, "Randomize set to OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			SHIP FREQ CHANGE			*
 ****************************************************************/
local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq)
{
	lm->Log(L_INFO, "<pbfreeplay> %s freq ship change %d:%d %d:%d", player->name, oldShip, newShip, oldFreq, newFreq );

	FreeArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->roundStart)
		return;

	int random;
	//player specced so update playingTime
	if (newShip != SHIP_SPEC)
	{
		/********************************
		 *	New Ship is Not SPEC	*
		 ********************************/
		if ((newShip > 1) || (newFreq > 1))
		{
			random = rand() % 2;
			game->SetShipAndFreq(player, random, random);
			return;
		}
		if (player->p_freq != player->p_ship)
		{
			game->SetShipAndFreq(player, player->p_freq, player->p_freq);
			return;
		}
	}

	//check balance if someone specced or someone entered
	//if (arenaData->balanceKey == NULL)
	//	BalanceFreqs(player->arena, arenaData, PB_FIRST_WARNING);
}

/****************************************************************
 *			BALL PICKUP				*
 ****************************************************************/
local void RoundStart(Arena *arena, FreeArenaData *arenaData);

local void BallPickup(Arena *arena, Player *player, int ballID)
{
	//lm->Log(L_INFO, "<pbfreeplay> %s ball pickup", player->name );

	FreeArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
	{
		RoundStart(arena, arenaData);
	}

	arenaData->roundStart = TRUE;
}

/****************************************************************
 *			PLAYER ACTION				*
 ****************************************************************/
local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<pbfreeplay> %s Player Action %d", player->name, action );

	switch (action)
	{
		case PA_ENTERARENA:
			chat->SendMessage(player, "Welcome to PowerBall! ");
			chat->SendMessage(player, "This arena is powered by PB Module!");
			chat->SendMessage(player, "Type ?pbhelp for available commands.");

			//FreeArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);
		break;
	}
}

/****************************************************************
 *			GAME START				*
 ****************************************************************/
local void GameStart(Arena *arena)
{
	lm->Log(L_INFO, "<pbfreeplay> Game Start" );

	FreeArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	RoundStart(arena, arenaData);
}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local void GameOver(Arena *arena)
{
	FreeArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	lm->Log(L_INFO, "<pbfreeplay> Game Over");

	arenaData->roundStart		= FALSE;

	if (!arenaData->randomizeTeamsOnEnd)
		return;

	RandomizeTeams(arena);
}


/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);
		return;
	}
}
/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras= NULL;
		return;
	}
}

		/************************************************************************************************
		 *					TIMERS SECTION						*
		 ************************************************************************************************/
/********************************************************
 *			ROUND START			*
 ********************************************************/
local void RoundStart(Arena *arena, FreeArenaData *arenaData)
{
	Player *player	= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->status != S_PLAYING)
			continue;

		if (player->p_ship != SHIP_SPEC)
		{
			int random = rand() % 2;
			game->SetShipAndFreq(player, random, random);
		}
	}
	pd->Unlock();
}


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(pg);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(teams);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(signups);
	mm->ReleaseInterface(pbstats);
	mm->ReleaseInterface(pbextras);
}

/* The entry point: */
EXPORT int MM_pbfreeplay(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,			ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			pd	= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			balls	= mm->GetInterface(I_BALLS,			ALLARENAS);
			ml	= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			game	= mm->GetInterface(I_GAME,			ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			pg	= mm->GetInterface(I_POINTS_GOALS,	ALLARENAS);
			cfg	= mm->GetInterface(I_CONFIG,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			teams	= mm->GetInterface(I_TEAMS,			ALLARENAS);
			signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);

			if (!chat || !aman || !pd || !balls || !ml || !game || !cmd || !lm || !pg || !cfg || !capman)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface aman");
					if (!pd)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface pd");
					if (!balls)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface balls");
					if (!ml)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface ml");
					if (!game)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface game");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface cmd");
					if (!pg)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface pg");
					if (!cfg)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface cfg");
					if (!capman)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface capman");
				}

				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey	= aman->AllocateArenaData(sizeof(FreeArenaData));
				playerKey	= pd->AllocatePlayerData(sizeof(PBPlayerVoting));

				if ((arenaKey == -1) || (playerKey == -1))	//check for valid memory allocation
				{
					if (arenaKey != -1)
						aman->FreeArenaData(arenaKey);
					if (playerKey != -1)
						pd->FreePlayerData(playerKey);

					ReleaseInterfaces();

					rv = MM_FAIL;
				}
				else
				{
					mm->RegInterface(&_int, ALLARENAS);

					rv = MM_OK;

					if (!signups)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface signups");
					if (!teams)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface teams");
					if (!pbstats)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface pbstats");
					if (!pbextras)
						lm->LogA(L_DRIVEL, "pbfreeplay", arena, "Unable to load interface pbextras");
				}
			}
		break;

		case MM_ATTACH:

			if (!signups)
				signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			if (!teams)
				teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
			if (!pbstats)
				pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);

			mm->RegCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->RegCallback(CB_BALLPICKUP,		BallPickup,			arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,			arena);
			mm->RegCallback(CB_GAMESTART,		GameStart,			arena);
			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			cmd->AddCommand("pbfreeplayhelp",	CPbHelp,		arena,	pbhelp_help);
			cmd->AddCommand("pbversion",	CPbVersion,		arena,	pbversion_help);
			cmd->AddCommand("randomize",	CRandomize,		arena,	randomize_help);
			cmd->AddCommand("randomnow",	CRandomNow,		arena,	randomnow_help);
			cmd->AddCommand("togglerandom",	CToggleRandom,		arena,	togglerandom_help);

			InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("pbfreeplayhelp",		CPbHelp,		arena);
			cmd->RemoveCommand("pbversion",		CPbVersion,		arena);
			cmd->RemoveCommand("randomize",		CRandomize,		arena);
			cmd->RemoveCommand("randomnow",		CRandomNow,		arena);
			cmd->RemoveCommand("togglerandom",	CToggleRandom,		arena);

			mm->UnregCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->UnregCallback(CB_BALLPICKUP,		BallPickup,			arena);
			mm->UnregCallback(CB_GAMEOVER,			GameOver,			arena);
			mm->UnregCallback(CB_GAMESTART,			GameStart,			arena);
			mm->UnregCallback(CB_PLAYERACTION,		PlayerAction,		arena);
			mm->UnregCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->UnregCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			if (arenaKey != -1)
				ReleaseArenaData(arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (playerKey != -1)
				pd->FreePlayerData(playerKey);

			if (arenaKey != -1)
				aman->FreeArenaData(arenaKey);

			if ((playerKey != -1) && (arenaKey != -1))
				mm->UnregInterface(&_int, ALLARENAS);

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}

