#ifndef __PB_EXTRAS_H__
#define __PB_EXTRAS_H__

#define I_PBEXTRAS "pbextras-1"

typedef struct Ipbextras
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
} Ipbextras;

#endif
