#ifndef __PB_STATS_H__
#define __PB_STATS_H__

#define MVPNAMELEN		200
#define NEARBALLSQRDISTANCE	250000	//500 pixels
#define STEAL_TIMEOUT		400
#define ASSIST_TIMEOUT		1000
#define STATS_MAX_TEAMS		2

typedef struct PBTimer
{
	Arena *			arena;
} PBTimer;

typedef struct PlayerStatsDataStruct
{
	char *		name;			//X
	int		rating;			//X
	float		rpm;			//X
	int		spawns;			//X
	int		kills;			//X
	int		ballKills;		//X
	int		teamKills;		//X
	int		farKills;		//X
	int		deaths;			//X
	int		goals;			//X
	int		assists;		//X
	int		steals;			//X
	int		turnover;		//X
	int		saves;
	int		ballDeaths;		//X
	int		carries;		//X
	
	bool		hasBall;
	unsigned long	carriesTime;		//X
	unsigned long	playingTime;		//X
	unsigned long	nearTime;		//In Seconds

	bool		nearBall;		//X
	bool		inShip;			//X
	ticks_t		enteredTick;		//X
	ticks_t		pickupTick;		//X

} PlayerStatsData;


typedef struct FreqStatsStruct
{
	int		rating;
	int		goals;
	int		assists;
	int		kills;
	int	 	ballKills;
	int		teamKills;
	int		deaths;
	int		ballDeaths;
	int		steals;
	int		turnover;
	int		saves;
	int		spawns;
	int		carries;
} FreqStats;

typedef struct GameMVPsStruct
{
	int		goals;
	char		goalsNames[MVPNAMELEN];
	int		assists;
	char		assistsNames[MVPNAMELEN];
	int		kills;
	char		killsNames[MVPNAMELEN];
	int		deaths;
	char		deathsNames[MVPNAMELEN];
	int		steals;
	char		stealsNames[MVPNAMELEN];
	int		spawns;
	char		spawnsNames[MVPNAMELEN];
	int		carries;
	char		carriesNames[MVPNAMELEN];
	int		time;
	char		timeNames[MVPNAMELEN];
	int		near;
	char		nearNames[MVPNAMELEN];
	int		rating;
	char		ratingNames[MVPNAMELEN];

} GameMVPs;

typedef struct StatsArenaDataStruct
{
	//used in end of game report
	LinkedList *		gameStats[STATS_MAX_TEAMS];

	Player *		playerWithBall;
	Player *		lastWithBall;
	bool			possibleChoke;
	Player *		ballKilledPlayer;

	int			assistFrequency;
	int			numAssists;
	LinkedList *		assistPlayers;

	bool			roundStart;
	bool			goalScored;
	int			activeBallID;
	unsigned long		gameTime;		//In Seconds

	ticks_t			lastReleasedTick;

	Region *		goal0;
	Region *		goal1;

} StatsArenaData;


typedef struct AssistPlayerStruct
{
	char *		name;
	int		frequency;
	ticks_t		passedTick;

} AssistPlayer;


#define I_PBSTATS "pbstats-1"

typedef struct Ipbstats
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
	void (*ResetStats)(Arena *arena);

} Ipbstats;

#endif
