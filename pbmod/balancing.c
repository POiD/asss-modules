#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "asss.h"
#include "teams.h"


// Interfaces
local Imodman *		mm;
local Ichat *		chat;
//arenadata
local Iarenaman *	aman;
local Iplayerdata *	pd;
//timers
local Imainloop * 	ml;
local Igame *		game;
local Icmdman *		cmd;
local Ilogman *		lm;
//for teamMax amd alternatePicking
local Iconfig *		cfg;
local Icapman *		capman;


local int 		arenaKey	= -1;

/* prototypes */
//team balancing
local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq);
//local PB_warningType BalanceFreqs(Arena *arena, TeamsArenaData *arenaData, PB_warningType warningStage);
//local void SetBalanceTimer(Arena *arena, TeamsArenaData *arenaData, PB_warningType warningStage);
//local void ChangeTeamPlayers(Arena *arena, TeamsArenaData *arenaData, int fromFreq, int toFreq, int count);


/* MISC */
//local void RoundStart(Arena *arena, TeamsArenaData *arenaData);

//arena data
local void InitializeArenaData(Arena *arena);
local void ReleaseArenaData(Arena *arena);

/* CALLBACKS */
local void PlayerAction(Player *player, int action, Arena *arena);

/* TIMER */
local int Balance(void *vp);
local void CleanUpBalancing(void *vp);

/****************************************************************
 *			Command Functions			*
 ****************************************************************/
local void CBalancingHelp(const char *command, const char *params, Player *player, const Target *target);
local void CBalancingVersion(const char *command, const char *params, Player *player, const Target *target);


local Ibalancing _int =
{
	INTERFACE_HEAD_INIT(I_BALANCING, "balancing")
	ToggleBalancing
};

EXPORT const char info_balancing[] = CORE_MOD_INFO("balancing");

/************************************************
 * 		BALANCING TEAM SECTION		*
 ***********************************************/

local void InitializeArenaData(Arena *arena)
{
	TeamsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->numberOfTeams	= 0;
	arenaData->choosingLocked	= FALSE;
	arenaData->alternatePicking	= FALSE;
	arenaData->repopulateSignups	= TRUE;
	arenaData->useSignupList	= FALSE;
	arenaData->isDraft		= FALSE;

	arenaData->currentPick		= -1;
	arenaData->teamMax		= INT_MAX;
	arenaData->teams		= LLAlloc();
	arenaData->signups		= LLAlloc();


	Target target;
	target.type=T_ARENA;
	target.u.arena=arena;

	arenaData->arenaTarget		= target;
	arenaData->balanceKey		= NULL;


	game->LockArena(arena, 1, 0, 0, 1);
}

local void ReleaseArenaData(Arena *arena)
{
	BalanceData *arenaData = P_ARENA_DATA(arena, arenaKey);

	Link *link;
	FreqDetails *freqDetails;
	FOR_EACH(arenaData->freqDetails, freqDetails, link)
	{
		LLClear(freqDetails->players);
		LLFree(freqDetails->players);
	}
	LLClear(arenaData->freqDetails);
	LLFree(arenaData->freqDetails);

	game->UnlockArena(arena, 1, 0);
}


local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq)
{
	lm->Log(L_INFO, "<teams> %s freq ship change %d:%d %d:%d", player->name, oldShip, newShip, oldFreq, newFreq );

	//TeamsArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	//lm->LogA(L_DRIVEL, "teams", player->arena, "SHIP FREQ CHANGE: %s: %d->%d  %d->%d", player->name, oldShip, newShip, oldFreq, newFreq);

	//if (!arenaData->roundStart)
	//	return;

        int random;
        //player specced so update playingTime
        if (newShip == SHIP_SPEC)
        {
                if (oldShip != SHIP_SPEC)
                {
                        if (ValidGameFreq(arenaData->gameType, oldFreq))
                        {
                                if (arenaData->freqPlayers[oldFreq] != NULL)
                                {
                                        //find old reference to remove it.
                                        if (LLRemove( arenaData->freqPlayers[oldFreq], player))
                                        {
                                                arenaData->freqCounts[oldFreq]--;
                                        }
                                }
                        }
                }
                else
                //don't redo balancing if player was already in spec
                        return;
        }
        else
        {
                /********************************
                 *      New Ship is Not SPEC    *
                 ********************************/
                if (!ValidGameFreq(arenaData->gameType, player->p_freq))
                {
                        switch (arenaData->gameType)
                        {
                                case PB_GAME_ANY:
                                case PB_GAME_3H:
                                case PB_GAME_PUB:
                                case PB_GAME_PRO:
                                case PB_GAME_MINI:
                                case PB_GAME_SCRAMBLE:
                                        random = rand() % 2;
                                        game->SetShipAndFreq(player, random, random);
                                break;
                                //case 4 player freqs
                                //      random = rand() % 4;
                                //      game->SetShipAndFreq(player, random, random);
                                //break;
                        }
                        return;
                }
                if (player->p_freq != player->p_ship)
                {
                        game->SetShipAndFreq(player, player->p_freq, player->p_freq);
                        return;
                }

                //ship is on a valid freq and aligned with the ship.
                if (oldShip == SHIP_SPEC)
                {
                        //add player to new freq counts
                        if (arenaData->freqPlayers[newFreq] == NULL)
                                arenaData->freqPlayers[newFreq] = LLAlloc();
                        LLAdd( arenaData->freqPlayers[newFreq], player);
                        arenaData->freqCounts[newFreq]++;
                }
                else
                {
                        if (newFreq != oldFreq)
                        {
                                if (ValidGameFreq(arenaData->gameType, oldFreq))
                                {
                                        if (arenaData->freqPlayers[oldFreq] != NULL)
                                        {
                                                //find old reference to remove it.
                                                if (LLRemove( arenaData->freqPlayers[oldFreq], player))
                                                {
                                                        arenaData->freqCounts[oldFreq]--;
                                                }
                                        }
                                }

                                //add player to new freq counts
                                if (arenaData->freqPlayers[newFreq] == NULL)
                                        arenaData->freqPlayers[newFreq] = LLAlloc();
                                LLAdd( arenaData->freqPlayers[newFreq], player);
                                arenaData->freqCounts[newFreq]++;
                        }
                }
        }


	//check balance if someone specced or someone entered
	//if (arenaData->balanceKey == NULL)
	//	BalanceFreqs(player->arena, arenaData, PB_FIRST_WARNING);
}

local int Balance(void *vp)
{
	BalanceData *bd = (BalanceData *)vp;

	//bd->warningStage = BalanceFreqs(bd->arena, bd->arenaData, bd->warningStage);

	if (bd->warningStage != PB_ALLOKAY)
		return TRUE;

	//lm->LogA(L_DRIVEL, "teams", bd->arena, "Balance exiting. %d - %d - %d = %d", (int)bd->arenaData->balanceKey, bd->arenaData->freqCounts[0], bd->arenaData->freqCounts[1], bd->warningStage);

	bd->arenaData->balanceKey	= NULL;
	afree(bd);

	return FALSE;
}

/*

local PB_warningType BalanceFreqs(Arena *arena, TeamsArenaData *arenaData, PB_warningType warningStage)
{
	return PB_ALLOKAY;

	//lm->LogA(L_DRIVEL, "teams", arena, "Balance Freqs. %d - %d", arenaData->freqCounts[0], arenaData->freqCounts[1]);

	switch (arenaData->gameType)
	{
		case PB_GAME_ANY:
		case PB_GAME_3H:
		case PB_GAME_PUB:
		case PB_GAME_MINI:
		case PB_GAME_PRO:
			if (abs(arenaData->freqCounts[0] - arenaData->freqCounts[1]) > 1)
			{
				if (warningStage == PB_FIRST_WARNING)
				{
					chat->SendArenaMessage(arena, "Unbalanced Teams! %i vs %i. First warning ...",
								arenaData->freqCounts[0],
								arenaData->freqCounts[1]);
					//Start timer here
					SetBalanceTimer(arena, arenaData, PB_LAST_WARNING);
					return PB_LAST_WARNING;
				}
				else if (warningStage == PB_LAST_WARNING)
				{
					chat->SendArenaMessage(arena, "Unbalanced Teams! %i vs %i. LAST warning ...",
								arenaData->freqCounts[0],
								arenaData->freqCounts[1]);
					//Start timer here
					return PB_TOOLATE;
				}
				else
				{
					lm->LogA(L_DRIVEL, "teams", arena, "CHANGE EM: Balance Freqs. %d - %d", arenaData->freqCounts[0], arenaData->freqCounts[1]);

					if (arenaData->freqCounts[0] > arenaData->freqCounts[1])
					{
						int diff = (arenaData->freqCounts[0] - arenaData->freqCounts[1]) / 2;
						ChangeTeamPlayers(arena, arenaData, 0, 1, diff);
					}
					else
					{
						int diff = (arenaData->freqCounts[1] - arenaData->freqCounts[0]) / 2;
						ChangeTeamPlayers(arena, arenaData, 1, 0, diff);
					}
				}
			}
		break;
		case PB_GAME_SCRAMBLE:
		break;
	}
	return PB_ALLOKAY;
	return 0;
}
*/

/*
local void SetBalanceTimer(Arena *arena, TeamsArenaData *arenaData, PB_warningType warningStage)
{
	lm->LogA(L_DRIVEL, "teams", arena, "balance Key. %d", (int)arenaData->balanceKey);

	if ((arenaData->balanceKey == NULL) && (warningStage != PB_ALLOKAY))
	{
		BalanceData * bd	= amalloc(sizeof(BalanceData));
		bd->arena		= arena;
		bd->arenaData		= arenaData;
		bd->warningStage	= warningStage;

		arenaData->balanceKey	= arena;

		ml->SetTimer(Balance, TEAM_WARNING, TEAM_WARNING, bd, arenaData->balanceKey);
	}
}
*/
/*
local void ChangeTeamPlayers(Arena *arena, TeamsArenaData * arenaData, int fromFreq, int toFreq, int count)
{
	int vals[count];
	for (int j = 0; j < count; j++)
		vals[j] = -1;

	for (int i = 0; i < count; i++)
	{
		int num;
		bool found = FALSE;
		do
		{
			num	= rand() % arenaData->freqCounts[fromFreq];
			for (int j = 0; j < i; j++)
			{
				if (vals[j] == num)
					found = TRUE;
			}
		}
		while (found);
		vals[i] = num;

		//Link *last	= NULL;
		Link *next	= LLGetHead(arenaData->freqPlayers[fromFreq]);
		while ((num > 0) && (next))
		{
			//last = next;
			next = next->next;
			num--;
		}
		if (next)
		{
			Player * movePlayer = (Player *)next->data;

			//remove the player from this team so we don't try select them again if moving multiple players
			//arenaData->freqCounts[fromFreq]--;
			//if (last)
			//	last->next = next->next;

			if (movePlayer != NULL)
			{
				game->SetShipAndFreq(movePlayer, toFreq, toFreq);
				chat->SendArenaMessage(arena, "Switched %s!", movePlayer->name);
			}
		}
		else
			i--;
	}
}
*/

local void CleanUpBalancing(void *vp)
{
	BalanceData * bd = (BalanceData *)vp;
	afree(bd);
}


local void RemovePlayer(Arena *arena, Player *player)
{
        MyArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

        if (arenaData->freqPlayers[player->p_freq] != NULL)
        {
                if (LLRemove(arenaData->freqPlayers[player->p_freq], player))
                {
                        arenaData->freqCounts[player->p_freq]--;
                }
        }
}

/****************************************************************
 *                      NEW PLAYER                              *
 ****************************************************************/
local void NewPlayer(Player *player, int isnew)
{
        lm->Log(L_INFO, "<pbmod> %s new player %d", player->name, isnew );

        if (!isnew)
        {
                RemovePlayer(player->arena, player);
        }
}



/************************************************
 * 	END BALANCING TEAM SECTION		*
 ***********************************************/

/************************************************
 * 		PLAYER ACTION SECTION		*
 ***********************************************/

local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<teams> %s Player Action %d", player->name, action );

	switch (action)
	{
		case PA_DISCONNECT:
		case PA_LEAVEARENA:
			RemovePlayer(arena, player);
		break;

		case PA_ENTERARENA:
			chat->SendMessage(player, "Type ?teamshelp for available commands.");
		break;
	}
}

/*
local void RoundStart(Arena *arena, TeamsArenaData *arenaData)
{
	Player *player		= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->p_ship != SHIP_SPEC)
		{
			if (!ValidGameFreq(arenaData->gameType, player->p_freq))
			{
				int random = rand() % 2;
				game->SetShipAndFreq(player, random, random);
			}
			else if (player->p_freq != player->p_ship)
			{
				game->SetShipAndFreq(player, player->p_freq, player->p_freq);
			}
			else
			{
				//ensure player is setup on this freq for stats and balancing
				//GetPlayerStatsRecord(player, arenaData);
			}
		}
	}
	pd->Unlock();

	BalanceFreqs(arena, arenaData, PB_FIRST_WARNING);
}
*/

/****************************************
 * 	COMMANDS SECTION		*
 ****************************************/

local void CBalancingVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS Balancing Module -- Version: 1.0    Date: 07 September 2021    Author: POiD");
}
local void CBalancing(const char *command, const char *params, Player *player, const Target *target)
{
	//if params == "ON" vs "OFF"
	chat->SendMessage(player, "ASSS Balancing Module -- Version: 1.0    Date: 07 September 2021    Author: POiD");
}

///////////////////////////////////////////////////////////////////////////////////

/************************************************
 *		CallBack functions		*
 ************************************************/

//option for callback to turn balancing on and off between games etc

///////-------------------------------------------------------------------------------------

local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(capman);
}

/* The entry point: */
EXPORT int MM_balancing(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			pd	= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			ml	= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			game	= mm->GetInterface(I_GAME,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			cfg	= mm->GetInterface(I_CONFIG,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);


			if (!chat || !aman || !pd || !ml || !game || !cmd || !lm || !cfg || !capman)
			{
				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey = aman->AllocateArenaData(sizeof(TeamsArenaData));

				if (arenaKey == -1)	//check for valid memory allocation
				{
					ReleaseInterfaces();
					rv = MM_FAIL;
				}
				else
				{
					mm->RegInterface(&_int, ALLARENAS);

					rv = MM_OK;
				}
			}
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->RegCallback(CB_NEWPLAYER,		NewPlayer,		arena);

			cmd->AddCommand("balancinghelp",	CBalancingHelp,	arena,	balancinghelp_help);
			cmd->AddCommand("balancingversion",	CBalancingVersion,	arena,	balancingversion_help);

			InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("balancinghelp",	CBalancingHelp,		arena);
			cmd->RemoveCommand("balancingversion",	CBalancingVersion,	arena);

			ml->CleanupTimer(Balance,		NULL,	CleanUpBalancing);

			ReleaseArenaData(arena);

			mm->UnregCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->UnregCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->UnregCallback(CB_NEWPLAYER,		NewPlayer,		arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (arenaKey != -1)
			{
				aman->FreeArenaData(arenaKey);
				mm->UnregInterface(&_int, ALLARENAS);
			}

			mm->ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}
