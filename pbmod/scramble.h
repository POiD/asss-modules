#ifndef __SCRAMBLE_H__
#define __SCRAMBLE_H__

#define LOCK_STATUS(arena) \
        pthread_mutex_lock((pthread_mutex_t*)P_ARENA_DATA(arena, mtxkey))
#define UNLOCK_STATUS(arena) \
        pthread_mutex_unlock((pthread_mutex_t*)P_ARENA_DATA(arena, mtxkey))

typedef struct ScrambleData
{
        int	gamestate;     // 0 = pre-game, 1-5 = countdown, 10 = active, 11 = SDOt
        int	goalvalue;     // value of goal
        int	ballfreq;  // -1 = spawned, 0 = birds, 1 = javs
        int	leaguearena;  // 1 = yes (let bot handle start of game)
        int	isscramble;
	int	timerStartTick;
} ScrambleData;

#endif
