#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "pbleague.h"
#include "scoring/points_goal.h"
#include "balls.h"
#include "pb_utils.c"
#include "teams.h"
#include "objects.h"
#include "mapdata.h"
#include "pblvzs.h"
#include "game_timer.h"

// Interfaces
local Imodman *		mm;
local Ichat *		chat;
local Iarenaman *	aman;
local Iballs *		balls;	//x
local Igame *		game;
local Icmdman *		cmd;
local Ilogman *		lm;
local Ipointsgoals *	pg;
local Icapman *		capman;
local Iplayerdata *	pd;
local Imainloop *	ml;	//x
local Iobjects *	lvz;
local Imapdata *	mapdata;
local Iconfig *		cfg;
local Ipblvzs *		pblvzs;
local Igametimer *	gametimer;
local Istats *		stats;

local int 		arenaKey	= -1;

local void HelpCommands(Player *player);

local Ipbleague _int =
{
	INTERFACE_HEAD_INIT(I_PBLEAGUE, "pbleague")
	HelpCommands
};

EXPORT const char info_pbleague[] = CORE_MOD_INFO("pbleague");

		/****************************************************************************************
		 *			 		COMMANDS SECTION				*
		 ****************************************************************************************/
/********************************************************
 *			HELP COMMANDS			*
 ********************************************************/
local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;

	chat->SendMessage(player, "-------------------------------------------------------");
	chat->SendMessage(player, "The following PB Leagues Module commands are available:");
	chat->SendMessage(player, "-------------------------------------------------------");
	//DisplayCommand(chat, player, "?authhelp [player]", "Send the auth help text to [player] or yourself if no name provided.");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_settime",	"?settime [minutes][:seconds]",	"Display or set the game time to minutes:seconds");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_resettime",	"?resettime",			"Reset the game time to the arena default (Teams:GameTime)");
}

/********************************************************
 *			INITIALIZE ARENA DATA		*
 ********************************************************/
local void InitializeArenaData(Arena *arena)
{
	PbLeagueArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->goal0		= mapdata->FindRegionByName(arena, "Goal0");
	arenaData->goal1		= mapdata->FindRegionByName(arena, "Goal1");

	arenaData->start0		= mapdata->FindRegionByName(arena, "Start0");
	arenaData->start1		= mapdata->FindRegionByName(arena, "Start1");

	arenaData->timerSeconds		= cfg->GetInt(arena->cfg, "Teams", "GameTime", 0);

	//causes zone to hang ;)
	//balls->StopGame(arena);
}

local void GetWarpPoint(Region *region, Region *regionDefault, int *x, int *y)
{
	if (region)
	{
		mapdata->FindRandomPoint(region, x, y);
		return;
	}
	if (regionDefault)
	{
		mapdata->FindRandomPoint(regionDefault, x, y);
		return;
	}
	*x = -1;
	*y = -1;
}

local void ShipResetAndWarpAll(PbLeagueArenaData *arenaData, Arena *arena)
{
	lm->Log(L_INFO, "<pbleague> Ship Reset And Warp All" );
	int x, y;
	Target target;

	Link *link;
	Player *player;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->p_ship != SHIP_SPEC)
		{
			if (player->p_freq % 2)
				GetWarpPoint(arenaData->start1, arenaData->goal1, &x, &y);
			else
				GetWarpPoint(arenaData->start0, arenaData->goal0, &x, &y);

			target.type = T_PLAYER;
			target.u.p = player;
			game->ShipReset(&target);
			stats->ScoreReset(player, INTERVAL_RESET);

			if ((x != -1) && (y != -1))
				game->WarpTo(&target, x, y);
			else
				game->GivePrize(&target, PRIZE_WARP, 1);
		}
	}
	pd->Unlock();
	stats->SendUpdates(NULL);
}

/********************************************************
 *			SET TIME			*
 ********************************************************/
local helptext_t settime_help =
"Module: pbleague\n"
"Targets: none\n"
"Args: none or [minutes][:seconds]\n"
"Display or Set the in arena timer for this game.";

local void CSetTime(const char *command, const char *params, Player *player, const Target *target)
{
	int min, sec;
	char *minutes;
	char *seconds;

	PbLeagueArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Timer currently set to %d:%02d", arenaData->timerSeconds / 60, arenaData->timerSeconds % 60 );
		return;
	}

	if (TwoSplitUserInput(':', params, &minutes, &seconds))
	{
		min = satoi(minutes);
		sec = satoi(seconds);

		sec += (min * 60);

		TwoSplitRelease(&minutes, &seconds);
	}
	else
	{
		sec = satoi(params);
	}

	if ((!sec) & (params[0] != '0'))
	{
		chat->SendMessage(player, "Please specify a new time in the form of <seconds> or <min>:<seconds>");
		return;
	}

	arenaData->timerSeconds	= sec;
	chat->SendMessage(player, "Timer now set to %d:%02d", arenaData->timerSeconds / 60, arenaData->timerSeconds % 60);
}

/********************************************************
 *			RESET TIME			*
 ********************************************************/
local helptext_t resettime_help =
"Module: pbleague\n"
"Targets: none\n"
"Args: none\n"
"Reset arena time to the arena setting default (Teams:GameTime)";

local void CResetTime(const char *command, const char *params, Player *player, const Target *target)
{
	PbLeagueArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	arenaData->timerSeconds	= cfg->GetInt(player->arena->cfg, "Teams", "GameTime", 0);

	chat->SendMessage(player, "Timer now reset to %d:%02d", arenaData->timerSeconds / 60, arenaData->timerSeconds % 60);
}

		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/

typedef struct CountDownStruct
{
	int			countdown;
	Arena *			arena;
	Target			target;
	PbLeagueArenaData *	arenaData;
} CountDown;

local int Countdown(void *vp);

local void HandleTeamsReady(Arena *arena)
{
	lm->Log(L_INFO, "<pbleague> Handle Teams Ready" );
	PbLeagueArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

	CountDown * countdown		= amalloc(sizeof(CountDown));
	countdown->arena		= arena;
	countdown->countdown		= 3;
	countdown->target.type		= T_ARENA;
	countdown->target.u.arena	= arena;
	countdown->arenaData		= arenaData;

	chat->SendArenaMessage(arena, "Game Starts in 30 seconds ...");

	ml->SetTimer(Countdown, 3000, 300, countdown, arena);
}

local int Countdown(void *vp)
{
	CountDown * countdown	= (CountDown *)vp;
	lm->Log(L_INFO, "<pbleague> Countdown %d", countdown->countdown );

	switch (countdown->countdown)
	{
		case 3:
			//ready
			ShipResetAndWarpAll(countdown->arenaData, countdown->arena);
			lvz->Toggle(&countdown->target, LVZ_READY, 1);
		break;
		case 2:
			//set
			lvz->Toggle(&countdown->target, LVZ_READY, 0);
			lvz->Toggle(&countdown->target, LVZ_SET, 1);
		break;
		case 1:
			//go
			lvz->Toggle(&countdown->target, LVZ_SET, 0);
			lvz->Toggle(&countdown->target, LVZ_GO, 1);
			ShipResetAndWarpAll(countdown->arenaData, countdown->arena);
			chat->SendArenaSoundMessage(countdown->arena, 104, "Go Go Go !!!");
			balls->StartGame(countdown->arena);
		break;
		default:
			lvz->Toggle(&countdown->target, LVZ_GO, 0);
		break;
	}

	if (countdown->countdown > 0)
	{
		countdown->countdown--;
		return TRUE;
	}
	else
	{
		afree(countdown);
		return FALSE;
	}
}

/****************************************************************
 *			GAME START				*
 ****************************************************************/
local void GameStart(Arena *arena)
{
	lm->Log(L_INFO, "<pbleague> Game Start" );
	PbLeagueArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	arenaData->isGoldenGoal		= FALSE;
	arenaData->goldenGoalRule	= cfg->GetInt(arena->cfg, "Soccer", "GoldenGoalRule", 0);

	if (arenaData->timerSeconds)
	{
		if (pblvzs)
			pblvzs->StartGameTimer(arena, arenaData->timerSeconds);
	
		lm->Log(L_INFO, "<pbleague> Game Start: Calling Set In Game Timer %d", arenaData->timerSeconds );
		gametimer->SetInGameTimer(arena, arenaData->timerSeconds);
	}

	pg->ResetScores(arena);
}

/****************************************************************
 *			TIMER FINISH				*
 ****************************************************************/
local void TimerFinish(Arena *arena)
{
	lm->Log(L_INFO, "<pbleague> Timer Finish" );
	PbLeagueArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	int currentScores[CFG_SOCCER_MAXFREQ];
	pg->GetScores(arena, currentScores, CFG_SOCCER_MAXFREQ);

	if ((arenaData->goldenGoalRule) && (currentScores[0] == currentScores[1]))
	{
		if (arenaData->goldenGoalRule < 0)
		{
			int time = arenaData->goldenGoalRule * -1;
			chat->SendArenaSoundMessage(arena, 1, "Scores tied. %d:%02d additional time added", time / 60, time % 60);

			lm->Log(L_INFO, "<pbleague> Timer Finish: Calling Set In Game Timer %d", time );
			if (!gametimer->SetInGameTimer(arena, time))
				lm->Log(L_INFO, "<pbleague> Timer Finish - Error setting new time %d", time );
			DO_CBS(CB_TIMESET, arena, GameTimerChangeFunc, (arena, time));
		}
		else
		{
			chat->SendArenaSoundMessage(arena, 1, "Scores tied. Golden Goal in effect!");
			arenaData->isGoldenGoal = TRUE;
		}
	}
	else
		//game finished.
		balls->EndGame(arena);
}

/****************************************************************
 *			GOAL SCORED				*
 ****************************************************************/
local void GoalScored(Arena *arena, Player *player, int ballID, int x, int y)
{
	lm->Log(L_INFO, "<pblvzs> %s scored goal", player->name );

	PbLeagueArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (arenaData->isGoldenGoal)
	{
		arenaData->isGoldenGoal = FALSE;
		balls->EndGame(arena);
	}
}

/****************************************************************
 *			TIMER PAUSE				*
 ****************************************************************/
//local void TimerPause(Arena *arena, ticks_t time)
//{
//	lm->Log(L_INFO, "<pbleague> Timer Finish" );
//}

/****************************************************************
 *			TIMER RESUME				*
 ****************************************************************/
//local void TimerResume(Arena *arena, ticks_t time)
//{
//	lm->Log(L_INFO, "<pbleague> Timer Finish" );
//}

/****************************************************************
 *			TIMER SET				*
 ****************************************************************/
//local void TimerSet(Arena *arena, ticks_t time)
//{
//	lm->Log(L_INFO, "<pbleague> Timer Finish" );
//}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local void GameOver(Arena *arena)
{
	lm->Log(L_INFO, "<pbleague> Game Over" );
	PbLeagueArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (arenaData->timerSeconds)
	{
		lm->Log(L_INFO, "<pbleague> Game Over: Calling Set In Game Timer %d", 0 );
		gametimer->SetInGameTimer(arena, 0);
	}
}

/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "pblvzs"))
	{
		pblvzs	= mm->GetInterface(I_PBLVZS,		ALLARENAS);
		return;
	}
}
/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "pblvzs"))
	{
		pblvzs	= NULL;
		return;
	}
}


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(pg);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(pblvzs);
	mm->ReleaseInterface(gametimer);
	mm->ReleaseInterface(mapdata);
	mm->ReleaseInterface(lvz);
	mm->ReleaseInterface(stats);
}

/* The entry point: */
EXPORT int MM_pbleague(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm		= mm_;
			chat		= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman		= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			balls		= mm->GetInterface(I_BALLS,		ALLARENAS);
			game		= mm->GetInterface(I_GAME,		ALLARENAS);
			cmd		= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm		= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			pg		= mm->GetInterface(I_POINTS_GOALS,	ALLARENAS);
			capman		= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			pd		= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			ml		= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			cfg		= mm->GetInterface(I_CONFIG,		ALLARENAS);
			pblvzs		= mm->GetInterface(I_PBLVZS,		ALLARENAS);
			gametimer	= mm->GetInterface(I_GAMETIMER,		ALLARENAS);
			mapdata		= mm->GetInterface(I_MAPDATA,		ALLARENAS);
			lvz		= mm->GetInterface(I_OBJECTS,		ALLARENAS);
			stats		= mm->GetInterface(I_STATS,		ALLARENAS);

			if (!chat || !aman || !balls || !cmd || !lm || !pg || !capman || !pd || !ml || !cfg || !gametimer || !mapdata || !lvz || !game || !stats)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface aman");
					if (!balls)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface balls");
					if (!game)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface game");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface cmd");
					if (!pg)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface pg");
					if (!capman)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface capman");
					if (!pd)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface pd");
					if (!ml)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface ml");
					if (!cfg)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface cfg");
					if (!gametimer)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface gametimer");
					if (!mapdata)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface mapdata");
					if (!lvz)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface lvz");
					if (!stats)
						lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface stats");
				}

				ReleaseInterfaces();

				arenaKey = -1;

				rv = MM_FAIL;
			}
			else
			{
				if (!pblvzs)
					lm->LogA(L_DRIVEL, "pbleague", arena, "Unable to load interface pblvzs");

				//allocate data
				arenaKey = aman->AllocateArenaData(sizeof(PbLeagueArenaData));
				rv = MM_OK;

				if (arenaKey == -1)	//check for valid memory allocation
				{
					ReleaseInterfaces();
					rv = MM_FAIL;
				}
				else
					mm->RegInterface(&_int, ALLARENAS);
			}
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);
			mm->RegCallback(CB_TEAMSREADY,		HandleTeamsReady,	arena);
			mm->RegCallback(CB_GAMESTART,		GameStart,		arena);
			mm->RegCallback(CB_TIMESUP,		TimerFinish,		arena);
			//mm->RegCallback(CB_TIMEPAUSE,		TimerPause,		arena);
			//mm->RegCallback(CB_TIMERESUME,		TimerResume,		arena);
			//mm->RegCallback(CB_TIMESET,		TimerSet,		arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->RegCallback(CB_PGGOAL,		GoalScored,		arena);

			cmd->AddCommand("settime",	CSetTime,	arena,	settime_help);
			cmd->AddCommand("resettime",	CResetTime,	arena,	resettime_help);

			InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("settime",		CSetTime,	arena);
			cmd->RemoveCommand("resettime",		CResetTime,	arena);

			mm->UnregCallback(CB_TEAMSREADY,	HandleTeamsReady,	arena);
			mm->UnregCallback(CB_GAMESTART,		GameStart,		arena);
			mm->UnregCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->UnregCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);
			mm->UnregCallback(CB_TIMESUP,		TimerFinish,		arena);
			//mm->UnregCallback(CB_TIMEPAUSE,		TimerPause,		arena);
			//mm->UnregCallback(CB_TIMERESUME,	TimerResume,		arena);
			//mm->UnregCallback(CB_TIMESET,		TimerSet,		arena);
			mm->UnregCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->UnregCallback(CB_PGGOAL,		GoalScored,		arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (arenaKey != -1)
			{
				aman->FreeArenaData(arenaKey);
				mm->UnregInterface(&_int, ALLARENAS);
			}

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}
