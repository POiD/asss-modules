#ifndef __PB_LVZS_H__
#define __PB_LVZS_H__

typedef enum
{
	PB_GOTBALL,
	PB_LOSTBALL,
	PB_LEAD,
	PB_LEADLOST,
	PB_GOAL,
	PB_GOALGP,
	PB_GAMEPOINT,
	PB_GAMEPOINTLOST,
	PB_WIN,
	PB_GAMEOVER,
	PB_GAMESTART,
	PB_TIMER,
	PB_GAMETIME

} PB_lvzAction;

//#define ANNOUNCE_GAMEOVER	300
#define ANNOUNCE_GAMEOVER	400
#define ANNOUNCE_WINNER		300
#define ANNOUNCE_GAMEPOINT	700
#define ANNOUNCE_JAV_SCORE	400
#define ANNOUNCE_WB_SCORE	400
//#define ANNOUNCE_WIN		200
#define ANNOUNCE_WIN		50

//PB LVZs
#define LVZ_GAMEOVER		50	//timed
#define LVZ_NEWGAME		53	//timed

#define LVZ_WB_BALL		40	//timed
#define LVZ_JAV_BALL		41	//timed
#define LVZ_WB_LEADING		44
#define LVZ_JAV_LEADING		45
#define LVZ_WB_GAMEPOINT	46
#define LVZ_JAV_GAMEPOINT	47
#define LVZ_WB_SCORE_SPARKLE	48	//timed
#define LVZ_JAV_SCORE_SPARKLE	49	//timed
#define LVZ_WB_WIN		51	//timed
#define LVZ_JAV_WIN		52	//timed

//PB Specific Songs
#define BONG_GAMEOVER		30	//X
#define BONG_LOCKDOWN		31	//X
#define BONG_WB_SCORE		50	//X
#define BONG_WB_GAMEPOINT	51	//X
#define BONG_WB_WIN		52	//X
#define BONG_JAV_SCORE		70	//X
#define BONG_JAV_GAMEPOINT	71	//X
#define BONG_JAV_WIN		72	//X


//Game Timer Related
#define LVZ_TIMER		57
#define LVZ_SEC_ZERO		58
#define LVZ_SEC_COUNTDOWN	59
#define LVZ_10SEC_ZERO		60
#define LVZ_MIN_ZERO		70
#define LVZ_10MIN_ZERO		80


typedef struct GamePointDataStruct
{
	Arena *		arena;
	int		freq;
} GamePointData;

typedef struct DelayedSoundStruct
{
	Arena *		arena;
	int		bong;
} DelayedSound;

typedef struct EnabledLVZStruct
{
	bool WB_BALL;
	bool WB_LEADING;
	bool WB_GAMEPOINT;

	bool JAV_BALL;
	bool JAV_LEADING;
	bool JAV_GAMEPOINT;

	int WB_10s;
	int WB_1s;
	int JAV_10s;
	int JAV_1s;

	bool timerDisplay;
	int gameTimeMinutes10s;
	int gameTimeMinutes;
	int gameTimeSeconds10s;
	//int gameTimeSeconds;

} EnabledLVZs;

typedef struct GameOverDataStruct
{
	Arena *		arena;
	int		winner;
	Target		target;
	EnabledLVZs *	lvzs;
} GameOverData;


typedef struct PbLVZsArenaDataStruct
{
	Target			arenaTarget;

	bool			roundStart;
	int			lastScoringFreq;
	int			leadingFreq;
	int			gamePointFreq;
	int			scores[CFG_SOCCER_MAXFREQ];
	int			hasLead[CFG_SOCCER_MAXFREQ];
	int			hasGamePoint[CFG_SOCCER_MAXFREQ];
	int			hasBall[CFG_SOCCER_MAXFREQ];

	EnabledLVZs		lvzs;

	bool			isLeagueGame;
	bool			timerPaused;

	int			timerSeconds;

	int			capturePoints;
	int			winBy;

} PbLVZsArenaData;


#define I_PBLVZS "pblvzs-1"

typedef struct Ipblvzs
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
	void (*StartGameTimer)(Arena *arena, int time);

} Ipblvzs;

#endif
