#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "pblvzs.h"
#include "objects.h"
#include "../scoring/points_goal.h"
#include "pb_utils.c"
#include "game_timer.h"


// Interfaces
local Imodman *		mm;	//x
local Ichat *		chat;	//x
local Iarenaman *	aman;	//x
local Imainloop * 	ml;	//x
local Icmdman *		cmd;	//x
local Ilogman *		lm;
local Ipointsgoals *	pg;	//x
local Iobjects *	lvz;
local Iconfig *		cfg;

local int 		arenaKey	= -1;

local void HelpCommands(Player *player);
local void StartGameTimer(Arena *arena, int time);

local Ipblvzs _int =
{
	INTERFACE_HEAD_INIT(I_PBLVZS, "pblvzs")
	HelpCommands, StartGameTimer
};

EXPORT const char info_pblvzs[] = CORE_MOD_INFO("pblvzs");


/****************************************************************
 *		INITIALIZATION SECTION				*
 ****************************************************************/
local void InitializeArenaData(Arena *arena)
{
	PbLVZsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->roundStart		= FALSE;

	Target target;
	target.type=T_ARENA;
	target.u.arena=arena;

	arenaData->arenaTarget		= target;

	//arenaData->gameTimerKey		= amalloc(sizeof(bool));


	arenaData->lvzs.gameTimeMinutes10s	= -1;
	arenaData->lvzs.gameTimeMinutes		= -1;
	arenaData->lvzs.gameTimeSeconds10s	= -1;
	arenaData->lvzs.WB_1s			= -1;
	arenaData->lvzs.WB_10s			= -1;
	arenaData->lvzs.JAV_1s			= -1;
	arenaData->lvzs.JAV_10s			= -1;
}

local void ReleaseArenaData(Arena *arena)
{
	//PbLVZsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	//if (arenaData->gameTimerKey)
	//	afree(arenaData->gameTimerKey);
}

		/************************************************************************************************
		 *					HELPER COMMAND SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			PLAY DELAYED SOUND			*
 ****************************************************************/
local int PlayDelayed(void *vp);
local void CleanupDelayed(void *vp);

local void PlayDelayedSound(Arena *arena, int delay, int bong)
{
	DelayedSound *delayed	= amalloc(sizeof(DelayedSound));
	delayed->bong		= bong;
	delayed->arena		= arena;
	ml->SetTimer(PlayDelayed, delay, 0, delayed, arena);
}
local int PlayDelayed(void *vp)
{
	DelayedSound *delayed	= (DelayedSound *)vp;
	chat->SendArenaSoundMessage(delayed->arena, delayed->bong, "");

	afree(delayed);
	return FALSE;
}
local void CleanupDelayed(void *vp)
{
	DelayedSound *delayed = (DelayedSound *)vp;
	if (delayed)
		afree(delayed);
}

		/************************************************************************************************
		 *					COMMANDS SECTION					*
		 ************************************************************************************************/

local void HelpCommands(Player *player)
{
	//bool displayedMod = FALSE;

	chat->SendMessage(player, "----------------------------------------------------");
	chat->SendMessage(player, "The following PB LVZs Module commands are available:");
	chat->SendMessage(player, "----------------------------------------------------");

	DisplayCommand(chat, player, "?pblvzsversion",	"Display the PB LVZs Module version information");
}

/****************************************************************
 *			VERSION					*
 ****************************************************************/
local helptext_t pblvzsversion_help =
"Module: pblvzs\n"
"Targets: none\n"
"Args: none\n"
"Display PB LVZs Module version information.";

local void CPblvzsVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS PB LVZs Module -- Version: 1.0    Date: 16 October 2021    Author: POiD");
}


/****************************************************************
 *			START GAME TIMER			*
 ****************************************************************/
local void SetLVZs(Arena *arena, Target *target, PB_lvzAction action, int freq, int score, EnabledLVZs *lvzs);
local int GameDisplayTimer(void *vp);
local int CalculateLVZTimerValues(PbLVZsArenaData *arenaData, int time);
local void RoundStart(Arena *arena, PbLVZsArenaData *arenaData);
local void ResetGameTimerLVZs(PbLVZsArenaData *arenaData, int activate, Target *target);

local void StartGameTimer(Arena *arena, int time)
{
	if (!time)
		return;
	
	PbLVZsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
		RoundStart(arena, arenaData);

	arenaData->isLeagueGame = TRUE;

	SetLVZs(arena, &arenaData->arenaTarget, PB_TIMER, 1, 0, &arenaData->lvzs);

	int startDelay = CalculateLVZTimerValues(arenaData, time);
	
	/* timers */
	ml->SetTimer(GameDisplayTimer, startDelay, 1000, arena, arena);
}

local int CalculateLVZTimerValues(PbLVZsArenaData *arenaData, int time)
{
	arenaData->timerSeconds	= time;

	int minutes = arenaData->timerSeconds / 60;
	int seconds = arenaData->timerSeconds % 60;

	arenaData->lvzs.gameTimeMinutes10s	= minutes / 10;
	arenaData->lvzs.gameTimeMinutes		= minutes % 10;
	arenaData->lvzs.gameTimeSeconds10s	= seconds / 10;

	arenaData->timerSeconds -= (seconds % 10);

	lm->Log(L_INFO, "<pblvzs> Calculate LVZ Timer Values %d %d %d %d", time,
															arenaData->lvzs.gameTimeMinutes10s,
															arenaData->lvzs.gameTimeMinutes,
															arenaData->lvzs.gameTimeSeconds10s
	);

	ResetGameTimerLVZs(arenaData, 1, &arenaData->arenaTarget);

	return (seconds % 10) * 100;
}

/****************************************
 * 	Base Timer SECTION		*
 ****************************************/
local int GameDisplayTimer(void *vp)
{
	Arena *arena = (Arena *)vp;
	PbLVZsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	if (arenaData->timerPaused)
		return TRUE;

	SetLVZs(arena, &arenaData->arenaTarget, PB_GAMETIME, 1, 0, &arenaData->lvzs);

	if (arenaData->timerSeconds >= 0)
	{
		arenaData->timerSeconds -= 10;
		return TRUE;
	}

	//recognise end of timer
	return FALSE;
}
local void CleanupGameDisplayTimer(void *vp)
{
}


		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			BALL PICKUP				*
 ****************************************************************/
local void BallPickup(Arena *arena, Player *player, int ballID)
{
	//lm->Log(L_INFO, "<pblvzs> %s ball pickup", player->name );

	PbLVZsArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

	//if (!arenaData->roundStart)
	//{
	//	RoundStart(arena, arenaData);
	//}

	for (int i = 0; i < CFG_SOCCER_MAXFREQ; i++)
	{
		if (i == player->p_freq)
		{
			arenaData->hasBall[i] = 1;
			SetLVZs(arena, &arenaData->arenaTarget, PB_GOTBALL, i, 0, &arenaData->lvzs);
		}
		else if (arenaData->hasBall[i])
		{
			arenaData->hasBall[i] = 0;
			SetLVZs(arena, &arenaData->arenaTarget, PB_LOSTBALL, i, 0, &arenaData->lvzs);
		}
	}

	//arenaData->roundStart = TRUE;
}

/****************************************************************
 *			BALL FIRE				*
 ****************************************************************/
local void BallFire(Arena *arena, Player *player, int ballID)
{
	lm->Log(L_INFO, "<pblvzs> %s fired ball", player->name );

	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	SetLVZs(arena, &arenaData->arenaTarget, PB_LOSTBALL, player->p_freq, 0, &arenaData->lvzs);

}

/****************************************************************
 *			BALL HOLDER KILLED			*
 ****************************************************************/
local void BallHolderKilled(Arena *arena, Player *killer, Player *killed, int ballID)
{
	lm->Log(L_INFO, "<pblvzs> %s ball holder killed by %s", killed->name, killer->name );

	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
		return;

	SetLVZs(arena, &arenaData->arenaTarget, PB_LOSTBALL, killed->p_freq, 0, &arenaData->lvzs);
}

/****************************************************************
 *			GOAL SCORED				*
 ****************************************************************/
local int SendGamePoint(void *vp);

local void GoalScored(Arena *arena, Player *player, int ballID, int x, int y)
{
	lm->Log(L_INFO, "<pblvzs> %s scored goal", player->name );

	PbLVZsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	int currentScores[CFG_SOCCER_MAXFREQ];
	pg->GetScores(arena, currentScores, CFG_SOCCER_MAXFREQ);

	SetLVZs(arena, &arenaData->arenaTarget, arenaData->hasGamePoint[player->p_freq] ? PB_GOALGP : PB_GOAL, player->p_freq, currentScores[player->p_freq], &arenaData->lvzs);

	int otherFreq = (player->p_freq + 1) % 2;

	if (currentScores[player->p_freq] > currentScores[otherFreq])
	{
		//player freq has the lead

		//reset indicators for other freqs
		for (int i = 0; i < CFG_SOCCER_MAXFREQ; i++)
		{
			if (i == player->p_freq)
				continue;

			if (arenaData->hasGamePoint[i])
			{
				SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINTLOST, i, 0, &arenaData->lvzs);
				arenaData->hasGamePoint[i]		= 0;
			}
			if (arenaData->hasLead[i])
			{
				SetLVZs(arena, &arenaData->arenaTarget, PB_LEADLOST, i, 0, &arenaData->lvzs);
				arenaData->hasLead[i]			= 0;
			}
		}

		//score difference is 1 less than winby AND
		//score is at least capturepoints - 1
		if (	((currentScores[player->p_freq] - currentScores[otherFreq]) >= (arenaData->winBy - 1)) &&
			(currentScores[player->p_freq] > (arenaData->capturePoints - 2))
		   )
		{
			//this could be Game point
			if (arenaData->hasLead[player->p_freq])
			{
				arenaData->hasLead[player->p_freq]	= 0;
				SetLVZs(arena, &arenaData->arenaTarget, PB_LEADLOST, player->p_freq, 0, &arenaData->lvzs);
			}

			if (arenaData->hasGamePoint[player->p_freq] != 1)
			{
				arenaData->hasGamePoint[player->p_freq]		= 1;

				SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINT, player->p_freq, 0, &arenaData->lvzs);

				GamePointData *data	= amalloc(sizeof(GamePointData));
				data->arena		= arena;
				data->freq		= player->p_freq;
				ml->SetTimer(SendGamePoint, ANNOUNCE_GAMEPOINT, 0, data, arena);
			}
		}
		else
		{
			if (!arenaData->hasLead[player->p_freq])
			{
				arenaData->hasLead[player->p_freq]	= 1;
				SetLVZs(arena, &arenaData->arenaTarget, PB_LEAD, player->p_freq, 0, &arenaData->lvzs);
			}
		}
	}
	else if (currentScores[player->p_freq] == currentScores[otherFreq])
	{
		//freqs are tied.

		for (int i = 0; i < CFG_SOCCER_MAXFREQ; i++)
		{
			if (arenaData->hasGamePoint[i])
			{
				SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINTLOST, i, 0, &arenaData->lvzs);
				arenaData->hasGamePoint[i] = 0;
			}
			if (arenaData->hasLead[i])
			{
				SetLVZs(arena, &arenaData->arenaTarget, PB_LEADLOST, i, 0, &arenaData->lvzs);
				arenaData->hasLead[i] = 0;
			}
		}
	}
	else
	{
		//player freq is losing

		//score difference is 1 less than winby AND
		//score is at least capturepoints - 1
		if (	((currentScores[otherFreq] - currentScores[player->p_freq]) >= (arenaData->winBy - 1)) &&
			(currentScores[otherFreq] > (arenaData->capturePoints - 2))
		   )
		{
			//this could be Game point
			SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINT, otherFreq, 0, &arenaData->lvzs);

			GamePointData * pointData	= amalloc(sizeof(GamePointData));
			pointData->arena		= arena;
			pointData->freq			= otherFreq;
			ml->SetTimer(SendGamePoint, ANNOUNCE_GAMEPOINT, 0, pointData, arena);
		}
	}

	memcpy(arenaData->scores, currentScores, sizeof(currentScores));
	arenaData->lastScoringFreq	= player->p_freq;
}

local int SendGamePoint(void *vp)
{
	GamePointData *pointData = (GamePointData *)vp;

	if (pointData->freq == 0)
		chat->SendArenaSoundMessage(pointData->arena, BONG_WB_GAMEPOINT, "");
	else
		chat->SendArenaSoundMessage(pointData->arena, BONG_JAV_GAMEPOINT, "");

	afree(pointData);

	return FALSE;
}
local void CleanUpGamePoint(void *vp)
{
	GamePointData *pointData = (GamePointData *)vp;
	if (pointData)
		afree(pointData);
}


/****************************************************************
 *			PLAYER ACTION				*
 ****************************************************************/
local void SetEnterLVZs(Arena *arena, Target *target, PbLVZsArenaData *arenaData);

local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<pblvzs> %s Player Action %d", player->name, action );

	Target target;
	PbLVZsArenaData *arenaData;

	switch (action)
	{
		case PA_ENTERARENA:
			arenaData		= P_ARENA_DATA(arena, arenaKey);

			target.type     = T_PLAYER;
			target.u.p      = player;

			SetEnterLVZs(arena, &target, arenaData);
		break;
	}
}

local void ResetGameTimerLVZs(PbLVZsArenaData *arenaData, int activate, Target *target)
{
	lm->Log(L_INFO, "<pblvzs> Set Enter LVZs %d %d %d %d", activate,
															arenaData->lvzs.gameTimeMinutes10s,
															arenaData->lvzs.gameTimeMinutes,
															arenaData->lvzs.gameTimeSeconds10s
	);

	//only deactivate count down seconds as it will activate at the proper time.
	if (!activate)
		lvz->Toggle(target, LVZ_SEC_COUNTDOWN, activate);

	if (arenaData->lvzs.gameTimeMinutes10s != -1)
		lvz->Toggle(target, arenaData->lvzs.gameTimeMinutes10s + LVZ_10MIN_ZERO, activate);
	if (arenaData->lvzs.gameTimeMinutes != -1)
		lvz->Toggle(target, arenaData->lvzs.gameTimeMinutes + LVZ_MIN_ZERO, activate);
	if (arenaData->lvzs.gameTimeSeconds10s != -1)
		lvz->Toggle(target, arenaData->lvzs.gameTimeSeconds10s + LVZ_10SEC_ZERO, activate);
}

local void SetEnterLVZs(Arena *arena, Target *target, PbLVZsArenaData *arenaData)
{
	lm->Log(L_INFO, "<pblvzs> Set Enter LVZs");

	if (arenaData->lvzs.WB_BALL)
		SetLVZs(arena, target, PB_GOTBALL, 0, 0, NULL);
	if (arenaData->lvzs.JAV_BALL)
		SetLVZs(arena, target, PB_GOTBALL, 1, 0, NULL);

	if (arenaData->lvzs.WB_LEADING)
		SetLVZs(arena, target, PB_LEAD, 0, 0, NULL);
	if (arenaData->lvzs.JAV_LEADING)
		SetLVZs(arena, target, PB_LEAD, 1, 0, NULL);

	if (arenaData->lvzs.WB_GAMEPOINT)
		SetLVZs(arena, target, PB_GAMEPOINT, 0, 0, NULL);
	if (arenaData->lvzs.JAV_GAMEPOINT)
		SetLVZs(arena, target, PB_GAMEPOINT, 1, 0, NULL);

	if (arenaData->lvzs.WB_10s != -1)
		lvz->Toggle(target, arenaData->lvzs.WB_10s, 1);

	if (arenaData->lvzs.WB_1s != -1)
		lvz->Toggle(target, arenaData->lvzs.WB_1s, 1);

	if (arenaData->lvzs.JAV_10s != -1)
		lvz->Toggle(target, arenaData->lvzs.JAV_10s, 1);
	if (arenaData->lvzs.JAV_1s != -1)
		lvz->Toggle(target, arenaData->lvzs.JAV_1s, 1);

	if (arenaData->lvzs.timerDisplay)
		SetLVZs(arena, target, PB_TIMER, 1, 0, NULL);

	ResetGameTimerLVZs(arenaData, 1, target);
}


/****************************************************************
 *			GAME START				*
 ****************************************************************/
local void GameStart(Arena *arena)
{
	lm->Log(L_INFO, "<pblvzs> Game Start" );

	PbLVZsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->isLeagueGame)
		SetLVZs(arena, &arenaData->arenaTarget, PB_GAMESTART, 0, 0, &arenaData->lvzs);

	if (!arenaData->roundStart)
		RoundStart(arena, arenaData);
}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local int AnnounceGameOver(void *vp);
local void CleanUpGameOver(void *vp);
local int AnnounceWinner(void *vp);
local void CleanUpWinner(void *vp);

local void GameOver(Arena *arena)
{
	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	lm->Log(L_INFO, "<pblvzs> Game Over %d", arenaData->lastScoringFreq);

	//Target target;
	//target.type=T_ARENA;
	//target.u.arena=arena;

	chat->SendArenaSoundMessage(arena, BONG_LOCKDOWN, "");

	SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEOVER, 0, 0, &arenaData->lvzs);

	SetLVZs(arena, &arenaData->arenaTarget, PB_TIMER, 0, 0, &arenaData->lvzs);
	ml->ClearTimer(GameDisplayTimer, arena);
	ResetGameTimerLVZs(arenaData, 0, &arenaData->arenaTarget);

	arenaData->timerPaused	= FALSE;

	GameOverData * god	= amalloc(sizeof(GameOverData));
	god->arena		= arena;
	god->winner		= arenaData->lastScoringFreq;
	god->target		= arenaData->arenaTarget;
	god->lvzs		= &arenaData->lvzs;

	ml->SetTimer(AnnounceGameOver, ANNOUNCE_GAMEOVER, 0, god, arena);

	arenaData->roundStart		= FALSE;
	arenaData->lastScoringFreq	= -1;
}

local int AnnounceGameOver(void *vp)
{
	GameOverData * god	= (GameOverData *)vp;

	//target, id, on
	chat->SendArenaSoundMessage(god->arena, BONG_GAMEOVER, "");

	lm->Log(L_INFO, "<pblvzs> Announce Winner %d", god->winner );

	if (god->winner != -1)
	{
		ml->SetTimer(AnnounceWinner, ANNOUNCE_WINNER, 0, god, god->arena);
	}
	else
	{
		afree(god);
	}
	return 0;
}
local void CleanUpGameOver(void *vp)
{
	GameOverData *gD = (GameOverData *)vp;
	if (gD)
		afree(gD);
}

local int AnnounceWinner(void *vp)
{
	GameOverData * god	= (GameOverData *)vp;

	chat->SendArenaSoundMessage(god->arena, BONG_LOCKDOWN, "");

	SetLVZs(god->arena, &god->target, PB_WIN, god->winner, 0, god->lvzs);

	if (god->winner == 0)
	{
		PlayDelayedSound(god->arena, ANNOUNCE_WIN, BONG_WB_WIN);
	}
	else
	{
		PlayDelayedSound(god->arena, ANNOUNCE_WIN, BONG_JAV_WIN);
	}

	afree(god);
	return 0;
}
local void CleanUpWinner(void *vp)
{
	GameOverData *gD = (GameOverData *)vp;
	if (gD)
		afree(gD);
}

/****************************************************************
 *			TIMER PAUSE				*
 ****************************************************************/
local void TimerPause(Arena *arena, ticks_t time)
{
	lm->Log(L_INFO, "<pblvzs> Timer Pause %d", time );
	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	chat->SendArenaSoundMessage(arena, 2, "Timer Paused!");

	arenaData->timerPaused = TRUE;

	ml->ClearTimer(GameDisplayTimer, arena);

	lvz->Toggle(&arenaData->arenaTarget, LVZ_SEC_COUNTDOWN, 0);
}

/****************************************************************
 *			TIMER RESUME				*
 ****************************************************************/
local void TimerResume(Arena *arena, ticks_t time)
{
	lm->Log(L_INFO, "<pblvzs> Timer Resume: %d", time);
	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	chat->SendArenaSoundMessage(arena, 2, "Timer Resumed!");

	//arenaData->timerSeconds = time;
	//int seconds = (arenaData->timerSeconds % 60) % 10;

	ResetGameTimerLVZs(arenaData, 0, &arenaData->arenaTarget);

	int startDelay = CalculateLVZTimerValues(arenaData, time);

	ml->SetTimer(GameDisplayTimer, startDelay, 1000, arena, arena);

	arenaData->timerPaused = FALSE;
}

/****************************************************************
 *			TIMER SET				*
 ****************************************************************/
local void TimerSet(Arena *arena, ticks_t time)
{
	lm->Log(L_INFO, "<pblvzs> Timer Set: %d", time );
	PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	chat->SendArenaSoundMessage(arena, 2, "Timer Reset to %d:%02d", time / 60, time % 60);

	ml->ClearTimer(GameDisplayTimer, arena);

	ResetGameTimerLVZs(arenaData, 0, &arenaData->arenaTarget);

	int startDelay = CalculateLVZTimerValues(arenaData, time);

	/* timers */
	ml->SetTimer(GameDisplayTimer, startDelay, 1000, arena, arena);
}


/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
//local void ModuleAttached(ModuleData *mod, Arena *arena)
//{
//}
/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
//local void ModuleDetached(ModuleData *mod, Arena *arena)
//{
//}

		/************************************************************************************************
		 *					TIMERS SECTION						*
		 ************************************************************************************************/
/********************************************************
 *			ROUND START			*
 ********************************************************/
local void ResetLVZs(Arena *arena, PbLVZsArenaData *arenaData);

local void RoundStart(Arena *arena, PbLVZsArenaData *arenaData)
{
	ResetLVZs(arena, arenaData);

	arenaData->roundStart		= TRUE;

	arenaData->capturePoints	= cfg->GetInt(arena->cfg, "Soccer", "CapturePoints", 0) * -1;
	arenaData->winBy		= cfg->GetInt(arena->cfg, "Soccer", "WinBy", 0);

	for (int i = 0; i < CFG_SOCCER_MAXFREQ; i++)
		arenaData->hasGamePoint[i] = 0;
}

local void ResetLVZs(Arena *arena, PbLVZsArenaData *arenaData)
{
	lm->Log(L_INFO, "<pblvzs> Reset LVZs %d %d %d", 
													arenaData->lvzs.gameTimeMinutes10s,
													arenaData->lvzs.gameTimeMinutes,
													arenaData->lvzs.gameTimeSeconds10s
	);

	if (arenaData->lvzs.WB_BALL)
		SetLVZs(arena, &arenaData->arenaTarget, PB_LOSTBALL, 0, 0, &arenaData->lvzs);
	if (arenaData->lvzs.JAV_BALL)
		SetLVZs(arena, &arenaData->arenaTarget, PB_LOSTBALL, 1, 0, &arenaData->lvzs);

	if (arenaData->lvzs.WB_LEADING)
		SetLVZs(arena, &arenaData->arenaTarget, PB_LEADLOST, 0, 0, &arenaData->lvzs);
	if (arenaData->lvzs.JAV_LEADING)
		SetLVZs(arena, &arenaData->arenaTarget, PB_LEADLOST, 1, 0, &arenaData->lvzs);

	if (arenaData->lvzs.WB_GAMEPOINT)
		SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINTLOST, 0, 0, &arenaData->lvzs);
	if (arenaData->lvzs.JAV_GAMEPOINT)
		SetLVZs(arena, &arenaData->arenaTarget, PB_GAMEPOINTLOST, 1, 0, &arenaData->lvzs);

	if (arenaData->lvzs.WB_10s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.WB_10s, 0);
		arenaData->lvzs.WB_10s = -1;
	}
	if (arenaData->lvzs.WB_1s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.WB_1s, 0);
		arenaData->lvzs.WB_1s = -1;
	}

	if (arenaData->lvzs.JAV_10s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.JAV_10s, 0);
		arenaData->lvzs.JAV_10s = -1;
	}
	if (arenaData->lvzs.JAV_1s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.JAV_1s, 0);
		arenaData->lvzs.WB_1s = -1;
	}

	for (int i = 0; i < CFG_SOCCER_MAXFREQ; i++)
	{
		arenaData->scores[i]	= 0;
	}

	if (arenaData->lvzs.gameTimeMinutes10s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.gameTimeMinutes10s + LVZ_10MIN_ZERO, 0);
		arenaData->lvzs.gameTimeMinutes10s = -1;
	}
	if (arenaData->lvzs.gameTimeMinutes != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.gameTimeMinutes + LVZ_MIN_ZERO, 0);
		arenaData->lvzs.gameTimeMinutes = -1;
	}
	if (arenaData->lvzs.gameTimeSeconds10s != -1)
	{
		lvz->Toggle(&arenaData->arenaTarget, arenaData->lvzs.gameTimeSeconds10s + LVZ_10SEC_ZERO, 0);
		arenaData->lvzs.gameTimeSeconds10s = -1;
	}

	//if (arenaData->lvzs.timerDisplay)
	//	SetLVZs(arena, arenaData->arenaTarget, PB_TIMER, 0, 0, &arenaData->lvzs);
}

/************************************************
 *		LVZ functions			*
 ************************************************/
local void SetLVZs(Arena *arena, Target *target, PB_lvzAction action, int freq, int score, EnabledLVZs *lvzs)
{
	int g, r;

	switch (action)
	{
		case PB_GOTBALL:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_BALL, 1);
				if (lvzs)
					lvzs->WB_BALL		= TRUE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_BALL, 1);
				if (lvzs)
					lvzs->JAV_BALL		= TRUE;
			}
		break;

		case PB_LOSTBALL:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_BALL, 0);
				if (lvzs)
					lvzs->WB_BALL		= FALSE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_BALL, 0);
				if (lvzs)
					lvzs->JAV_BALL		= FALSE;
			}
		break;

		case PB_LEAD:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_LEADING, 1);
				if (lvzs)
					lvzs->WB_LEADING	= TRUE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_LEADING, 1);
				if (lvzs)
					lvzs->JAV_LEADING	= TRUE;
			}
		break;

		case PB_LEADLOST:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_LEADING, 0);
				if (lvzs)
					lvzs->WB_LEADING	= FALSE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_LEADING, 0);
				if (lvzs)
					lvzs->JAV_LEADING	= FALSE;
			}
		break;

		case PB_GOALGP:
		case PB_GOAL:
			g	= score / 10;
			r	= score % 10;
			PbLVZsArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

			if (freq == 0)
			{
				if (lvzs)
				{
					if (lvzs->WB_10s != -1)
						lvz->Toggle(target, lvzs->WB_10s, 0);
					if (lvzs->WB_1s != -1)
						lvz->Toggle(target, lvzs->WB_1s, 0);
				}

				if (action != PB_GOALGP)
					PlayDelayedSound(arena, ANNOUNCE_WB_SCORE, BONG_WB_SCORE);
				lvz->Toggle(target, LVZ_WB_SCORE_SPARKLE, 1);

				//LVZ  10-19 : 0-9
				lvz->Toggle(target, g+10, 1);
				lvz->Toggle(target, r, 1);

				if (lvzs)
				{
					lvzs->WB_10s	= g+10;
					lvzs->WB_1s	= r;
				}
			}
			else
			{
				if (lvzs)
				{
					if (lvzs->JAV_10s != -1)
						lvz->Toggle(target, lvzs->JAV_10s, 0);
					if (lvzs->JAV_1s != -1)
						lvz->Toggle(target, lvzs->JAV_1s, 0);
				}

				if (action != PB_GOALGP)
					PlayDelayedSound(arena, ANNOUNCE_JAV_SCORE, BONG_JAV_SCORE);
				lvz->Toggle(target, LVZ_JAV_SCORE_SPARKLE, 1);

				//LVZ  30-39 : 20-29
				lvz->Toggle(target, g+30, 1);
				lvz->Toggle(target, r+20, 1);

				if (lvzs)
				{
					lvzs->JAV_10s	= g+30;
					lvzs->JAV_1s	= r+20;
				}
			}
		break;

		case PB_GAMEPOINT:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_GAMEPOINT, 1);
				if (lvzs)
					lvzs->WB_GAMEPOINT	= TRUE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_GAMEPOINT, 1);
				if (lvzs)
					lvzs->JAV_GAMEPOINT	= TRUE;
			}
		break;

		case PB_GAMEPOINTLOST:
			if (freq == 0)
			{
				lvz->Toggle(target, LVZ_WB_GAMEPOINT, 0);
				if (lvzs)
					lvzs->WB_GAMEPOINT	= FALSE;
			}
			else
			{
				lvz->Toggle(target, LVZ_JAV_GAMEPOINT, 0);
				if (lvzs)
					lvzs->JAV_GAMEPOINT	= FALSE;
			}
		break;


		//TIMED - No reset needed
		case PB_WIN:
			if (freq == 0)
				lvz->Toggle(target, LVZ_WB_WIN, 1);
			else
				lvz->Toggle(target, LVZ_JAV_WIN, 1);
		break;

		case PB_GAMEOVER:
			lvz->Toggle(target, LVZ_GAMEOVER, 1);
		break;

		case PB_GAMESTART:
			lvz->Toggle(target, LVZ_NEWGAME, 1);
		break;

		case PB_TIMER:
			if (freq)
			{
				lvz->Toggle(target, LVZ_TIMER, 1);
				if (lvzs)
					lvzs->timerDisplay = 1;
			}
			else
			{
				lvz->Toggle(target, LVZ_TIMER, 0);
				if (lvzs)
					lvzs->timerDisplay = 0;
			}
		break;


		case PB_GAMETIME:

			lm->Log(L_INFO, "<pblvzs> SetLVZs %d %d %d", 
															lvzs->gameTimeMinutes10s,
															lvzs->gameTimeMinutes,
															lvzs->gameTimeSeconds10s
			);

			lvz->Toggle(target, LVZ_SEC_COUNTDOWN, 0);

			lvz->Toggle(target, lvzs->gameTimeSeconds10s + LVZ_10SEC_ZERO, 0);
			lvzs->gameTimeSeconds10s--;

			if (lvzs->gameTimeSeconds10s >= 0)
			{
				lm->Log(L_INFO, "<pblvzs> Timer 1 LVZs %d, %d, %d", lvzs->gameTimeSeconds10s, lvzs->gameTimeMinutes, lvzs->gameTimeMinutes10s );
				lvz->Toggle(target, LVZ_SEC_COUNTDOWN, 1);
				lvz->Toggle(target, lvzs->gameTimeSeconds10s + LVZ_10SEC_ZERO, 1);
				return;
			}
			
			lvz->Toggle(target, lvzs->gameTimeMinutes + LVZ_MIN_ZERO, 0);
			lvzs->gameTimeMinutes--;

			if (lvzs->gameTimeMinutes >= 0)
			{
				lvzs->gameTimeSeconds10s = 5;

				lm->Log(L_INFO, "<pblvzs> Timer 2 LVZs %d, %d, %d", lvzs->gameTimeSeconds10s, lvzs->gameTimeMinutes, lvzs->gameTimeMinutes10s );
				lvz->Toggle(target, LVZ_SEC_COUNTDOWN, 1);
				lvz->Toggle(target, lvzs->gameTimeSeconds10s + LVZ_10SEC_ZERO, 1);
				lvz->Toggle(target, lvzs->gameTimeMinutes + LVZ_MIN_ZERO, 1);
				return;
			}
			
			lvz->Toggle(target, lvzs->gameTimeMinutes10s + LVZ_10MIN_ZERO, 0);
			lvzs->gameTimeMinutes10s--;

			if (lvzs->gameTimeMinutes10s >= 0)
			{
				lvzs->gameTimeSeconds10s = 5;
				lvzs->gameTimeMinutes	= 9;

				lm->Log(L_INFO, "<pblvzs> Timer 3 LVZs %d, %d, %d", lvzs->gameTimeSeconds10s, lvzs->gameTimeMinutes, lvzs->gameTimeMinutes10s );
				lvz->Toggle(target, LVZ_SEC_COUNTDOWN, 1);
				lvz->Toggle(target, lvzs->gameTimeSeconds10s + LVZ_10SEC_ZERO, 1);
				lvz->Toggle(target, lvzs->gameTimeMinutes + LVZ_MIN_ZERO, 1);
				lvz->Toggle(target, lvzs->gameTimeMinutes10s + LVZ_10MIN_ZERO, 1);
				return;
			}

			lvzs->gameTimeSeconds10s	= 0;
			lvzs->gameTimeMinutes		= 0;
			lvzs->gameTimeMinutes10s	= 0;

			lm->Log(L_INFO, "<pblvzs> Timer End LVZs %d, %d, %d", lvzs->gameTimeSeconds10s, lvzs->gameTimeMinutes, lvzs->gameTimeMinutes10s );
			//timer end
			
		break;
	}
}


//settings for other subgame
//Stag Shot> smallpublg


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(pg);
	mm->ReleaseInterface(lvz);
	mm->ReleaseInterface(cfg);
}

/* The entry point: */
EXPORT int MM_pblvzs(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			ml	= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			pg	= mm->GetInterface(I_POINTS_GOALS,	ALLARENAS);
			lvz	= mm->GetInterface(I_OBJECTS,		ALLARENAS);
			cfg	= mm->GetInterface(I_CONFIG,		ALLARENAS);

			if (!chat || !aman || !ml || !cmd || !lm || !pg || !lvz || !cfg)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface aman");
					if (!ml)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface ml");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface cmd");
					if (!pg)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface pg");
					if (!lvz)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface lvz");
					if (!cfg)
						lm->LogA(L_DRIVEL, "pblvzs", arena, "Unable to load interface cfg");
				}

				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey = aman->AllocateArenaData(sizeof(PbLVZsArenaData));

				if (arenaKey == -1)	//check for valid memory allocation
				{
					if (arenaKey != -1)
						aman->FreeArenaData(arenaKey);

					ReleaseInterfaces();

					rv = MM_FAIL;
				}
				else
				{
					mm->RegInterface(&_int, ALLARENAS);

					rv = MM_OK;
				}
			}
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_BALLPICKUP,		BallPickup,		arena);
			mm->RegCallback(CB_BALLFIRE,		BallFire,		arena);
			mm->RegCallback(CB_PGGOAL,			GoalScored,		arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->RegCallback(CB_GAMESTART,		GameStart,		arena);
			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->RegCallback(CB_BALLKILL,		BallHolderKilled,	arena);
			//mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			//mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);
			mm->RegCallback(CB_TIMEPAUSE,		TimerPause,		arena);
			mm->RegCallback(CB_TIMERESUME,		TimerResume,	arena);
			mm->RegCallback(CB_TIMESET,			TimerSet,		arena);


			cmd->AddCommand("pblvzsversion",	CPblvzsVersion,	arena,	pblvzsversion_help);

			if (arenaKey != -1)
				InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("pblvzsversion",	CPblvzsVersion,	arena);

			ml->CleanupTimer(AnnounceGameOver,	arena,	CleanUpGameOver);
			ml->CleanupTimer(AnnounceWinner,	arena,	CleanUpWinner);
			ml->CleanupTimer(SendGamePoint,		arena,	CleanUpGamePoint);
			ml->CleanupTimer(PlayDelayed,		arena,	CleanupDelayed);
			ml->CleanupTimer(GameDisplayTimer,	arena,	CleanupGameDisplayTimer);

			mm->UnregCallback(CB_BALLPICKUP,	BallPickup,		arena);
			mm->UnregCallback(CB_BALLFIRE,		BallFire,		arena);
			mm->UnregCallback(CB_PGGOAL,		GoalScored,		arena);
			mm->UnregCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->UnregCallback(CB_GAMESTART,		GameStart,		arena);
			mm->UnregCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->UnregCallback(CB_BALLKILL,		BallHolderKilled,	arena);

			mm->UnregCallback(CB_TIMEPAUSE,		TimerPause,		arena);
			mm->UnregCallback(CB_TIMERESUME,	TimerResume,		arena);
			mm->UnregCallback(CB_TIMESET,		TimerSet,		arena);
			//mm->UnregCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			//mm->UnregCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			if (arenaKey != -1)
				ReleaseArenaData(arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (arenaKey != -1)
				aman->FreeArenaData(arenaKey);

			if (arenaKey != -1)
				mm->UnregInterface(&_int, ALLARENAS);

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}

