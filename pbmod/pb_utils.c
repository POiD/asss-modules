#include "asss.h"

#ifdef __GNUC__
#define SUPPRESS_NOT_USED_WARN __attribute__ ((unused))
#else
#define SUPPRESS_NOT_USED_WARN
#endif


SUPPRESS_NOT_USED_WARN local int FindPlayerInArenaFuzzy(Ilogman *lm, Iplayerdata *pd, Arena *arena, const char *playerName, struct Player **option)
{
	if (!*playerName)
		return -1;

	lm->Log(L_INFO, "<pb_utils> FindPlayerInArenaFuzzy %s %s", arena->name, playerName);

	int count = 0;
	int pos;

	*option = NULL;

	unsigned int paramLen = strlen(playerName);

	Player * possibles = NULL;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(possibles, arena)
	{
		if (possibles->status != S_PLAYING)
			continue;

		pos = strncasecmp(playerName, possibles->name, paramLen);

		if (!pos)
		{
			//exact name match found
			if (paramLen == strlen(possibles->name))
			{
				count = 1;
				*option = possibles;
				break;
			}

			count++;
			if (!*option)
				*option = possibles;
		}
	}
	pd->Unlock();

	if (count > 1)
		*option = NULL;

	return count;
}

SUPPRESS_NOT_USED_WARN local bool FindPlayerInArenaExact(Ilogman *lm, Iplayerdata *pd, Arena *arena, const char *playerName, struct Player **option)
{
	lm->Log(L_INFO, "<pb_utils> FindPlayerInArenaExact %s %s", arena->name, playerName);

	int pos;

	*option = NULL;

	if (!*playerName)
		return FALSE;

	unsigned int paramLen = strlen(playerName);

	Player * possibles = NULL;
	Link *link;

	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(possibles, arena)
	{
		if (possibles->status != S_PLAYING)
			continue;

		if (paramLen != strlen(possibles->name))
			continue;

		pos = strcasecmp(playerName, possibles->name);

		if (!pos)
		{
			*option = possibles;
			break;
		}
	}
	pd->Unlock();

	if (*option == NULL)
		return FALSE;
	else
		return TRUE;
}

SUPPRESS_NOT_USED_WARN local bool FindPlayerOnlineExact(Ilogman *lm, Iplayerdata *pd, const char *playerName, struct Player **option)
{
	lm->Log(L_INFO, "<pb_utils> FindPlayerOnlineExact %s", playerName);

	*option = NULL;

	if (!*playerName)
		return FALSE;

	unsigned int paramLen = strlen(playerName);

	Player * possibles = NULL;
	Link *link;

	pd->Lock();
	FOR_EACH_PLAYER(possibles)
	{
		if (paramLen != strlen(possibles->name))
			continue;

		if (!strcasecmp(playerName, possibles->name))
		{
			*option = possibles;
			break;
		}
	}
	pd->Unlock();

	if (*option == NULL)
		return FALSE;
	else
		return TRUE;
}

SUPPRESS_NOT_USED_WARN local int FindPlayerOnlineFuzzy(Ilogman *lm, Iplayerdata *pd, const char *playerName, struct Player **option)
{
	lm->Log(L_INFO, "<pb_utils> FindPlayerOnlineFuzzy %s", playerName);

	*option = NULL;

	if (!*playerName)
		return -1;

	unsigned int paramLen = strlen(playerName);

	Player *possibles	= NULL;
	Link *link		= NULL;
	int count = 0;

	pd->Lock();
	FOR_EACH_PLAYER(possibles)
	{
		if (!strncasecmp(playerName, possibles->name, paramLen))
		{
			//exact name match found
			if (paramLen == strlen(possibles->name))
			{
				count = 1;
				*option = possibles;
				break;
			}

			count++;
			if (!*option)
				*option = possibles;
		}
	}
	pd->Unlock();

	if (count > 1)
		*option = NULL;

	return count;
}

SUPPRESS_NOT_USED_WARN local void DisplayCommand(Ichat *chat, Player *player, const char *commandDetail, const char *message)
{
	chat->SendMessage(player, "%-35s - %s", commandDetail, message);
}

SUPPRESS_NOT_USED_WARN local void DisplayModCommand(Ichat *chat, Icapman *capman, Player *player, bool *displayedMod, const char *command, const char *commandDetail, const char *message)
{
	if (capman->HasCapability(player, command))
	{
		if (!*displayedMod)
		{
			*displayedMod = TRUE;
			chat->SendMessage(player, "-=-=-= Moderator Commands =-=-=-");
		}

		chat->SendMessage(player, "%-35s - %s", commandDetail, message);
	}
}

SUPPRESS_NOT_USED_WARN local bool TwoSplitUserInput(const char splitchar, const char *userInput, char **param1, char **param2)
{
	if (!*userInput)
		return FALSE;

	char *split = strchr(userInput, splitchar);

	if (split == NULL)
		return FALSE;

	unsigned int len = split - userInput;

	//ensure string isn't of form "1234:"
	if (len == (strlen(userInput)+1))
		return FALSE;

	*param1 = strndup(userInput, len);

	++split;
	*param2 = strndup(split, strlen(split));

	return TRUE;
}

SUPPRESS_NOT_USED_WARN local void TwoSplitRelease(char **param1, char **param2)
{
	if (*param1)
		afree(*param1);
	if (*param2)
		afree(*param2);

	*param1=NULL;
	*param2=NULL;
}

SUPPRESS_NOT_USED_WARN local int satoi(const char* str)
{
	//apparently atoi() segfaults on null str on linux
	if (!str) return 0;
	else return atoi(str);
}