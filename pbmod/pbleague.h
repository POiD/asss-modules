#ifndef __PB_LEAGUE_H__
#define __PB_LEAGUE_H__


#define LVZ_READY 91
#define LVZ_SET 92
#define LVZ_GO 93


typedef struct PbLeagueArenaDataStruct
{

	Region *	goal0;
	Region *	goal1;
	Region *	start0;
	Region *	start1;

	int		timerSeconds;

	bool		isGoldenGoal;
	int		goldenGoalRule;

} PbLeagueArenaData;


#define I_PBLEAGUE "pbleague-1"

typedef struct Ipbleague
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
} Ipbleague;

#endif
