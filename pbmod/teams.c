#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "asss.h"
#include "teams.h"
#include "signups.h"
#include "reldb.h"
#include "pb_utils.c"
#include "pbstats.h"
#include "pbpub.h"
#include "pbextras.h"
#include "pbleague.h"

// Interfaces
local Imodman *		mm;
local Ichat *		chat;
//arenadata
local Iarenaman *	aman;
local Iplayerdata *	pd;
//timers
local Imainloop * 	ml;
local Igame *		game;
local Icmdman *		cmd;
local Ilogman *		lm;
//config
local Iconfig *		cfg;
//for permissions
local Icapman *		capman;
//for signup list interactions
local Isignups *	signups;
//database access
local Ireldb *		db;
local Istats *		stats;

local Ipbstats *	pbstats;
local Ipbpub *		pbpub;
local Ipbextras *	pbextras;
local Ipbleague *	pbleague;

local Iballs *		balls;

local bool		hasDB		= FALSE;
local int 		arenaKey	= -1;

local void InitiateNewTeams(Arena *arena);
local void HelpCommands(Player *player);
local char *GetActiveEvent();

local void DisplayPickStatus(Player *player);

local Iteams _int =
{
	INTERFACE_HEAD_INIT(I_TEAMS, "teams")
	InitiateNewTeams,
	HelpCommands,
	GetActiveEvent
};

EXPORT const char info_teams[] = CORE_MOD_INFO("teams");

		/****************************************************************************************
		 *			 	INITIALIZATION SECTION					*
		 ****************************************************************************************/
local void ReleaseTeam(TeamsArenaData *arenaData, Arena *arena, Team *team)
{
	Player *removePlayer;
	Link *newLink;
	TeamPlayer *teamPlayer;
	FOR_EACH(team->players, teamPlayer, newLink)
	{
		if ((signups) && (arenaData->activeEvent) && (arenaData->repopulateSignups))
		{
			//void (*IsSignUpPtr)(Player *, bool, void *) = &RemovePlayerCB;
			signups->AddPlayer(NULL, teamPlayer->name, arenaData->activeEvent, NULL, NULL);
		}

		if (FindPlayerInArenaExact(lm, pd, arena, teamPlayer->name, &removePlayer))
		{
			game->SetShipAndFreq(removePlayer, SHIP_SPEC, arena->specfreq);
		}

		afree(teamPlayer->name);
	}
	LLClear(team->players);
	LLFree(team->players);

	if (team->captain)
		afree(team->captain);
	afree(team->teamName);
	//afree(team);

	//Link *link;
	//char *playerName;
	//FOR_EACH(team->borrowList, playerName, link)
	//{
	//	afree(playerName);
	//}
	LLClear(team->borrowList);
	LLFree(team->borrowList);
}

local void ReleaseTeams(TeamsArenaData *arenaData, Arena *arena)
{
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		ReleaseTeam(arenaData, arena, team);
	}
	LLClear(arenaData->teams);
}

local void ResetTeams(TeamsArenaData *arenaData, Arena *arena)
{
	lm->Log(L_INFO, "<teams> Reset Teams");

	ReleaseTeams(arenaData, arena);

	arenaData->numberOfTeams	= 0;
	arenaData->pickingStage		= PICKING_SETUP;
	arenaData->pickingRound		= 0;
	//arenaData->activeEvent
	arenaData->currentPickFreq	= -1;
	arenaData->currentPick		= 1;
	arenaData->pickDirection	= FALSE;
	arenaData->saveTeams		= FALSE;
	arenaData->offlineDrafting	= FALSE;

	arenaData->teamMax		= cfg->GetInt(arena->cfg, "Teams",	"TeamMax",		INT_MAX);
	arenaData->teamInGameMax	= cfg->GetInt(arena->cfg, "Teams",	"TeamInGameMax",	INT_MAX);
	arenaData->pickingType		= cfg->GetInt(arena->cfg, "Teams",	"PickingType",		PICKINGTYPE_NORMAL);
	arenaData->repopulateSignups	= cfg->GetInt(arena->cfg, "Teams",	"RepopulateSignups",	1);
	arenaData->isDraft		= cfg->GetInt(arena->cfg, "Teams",	"IsDraft",		0);

	game->LockArena(arena, 1, 0, 0, 1);

	Player *player;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->p_freq != arena->specfreq)
			game->SetFreq(player, arena->specfreq);
	}
	pd->Unlock();
}

local void InitializeArenaData(Arena *arena)
{
	TeamsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->activeEvent	= NULL;
	arenaData->teams	= LLAlloc();
	ResetTeams(arenaData, arena);

	//balls->EndGame(arena);
	//balls->StopGame(arena);
}

local void ReleaseArenaData(Arena *arena)
{
	TeamsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	ReleaseTeams(arenaData, arena);

	if (arenaData->activeEvent)
		afree(arenaData->activeEvent);

	game->UnlockArena(arena, 1, 0);
}

local void InitializeDB()
{
	if (db->GetStatus())
	{
		db->Query(NULL, NULL, 0, CREATE_TEAMS_TABLE);
		db->Query(NULL, NULL, 0, CREATE_TEAMS_PLAYER_TABLE);
	}
}

		/****************************************************************************************
		 *			 	HELPER COMMAND SECTION					*
		 ****************************************************************************************/
local char * GetActiveEvent(Arena *arena)
{
	if (arenaKey != -1)
	{
		TeamsArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);
		return arenaData->activeEvent;
	}
	return NULL;
}

local bool IsCaptain(TeamsArenaData *arenaData, Player *captain)
{
	lm->Log(L_INFO, "<teams> IsCaptain %s", captain->name);

	//if (!arenaData->teams)
	//	return FALSE;

	Link *link;
	Team *team;
        FOR_EACH(arenaData->teams, team, link)
	{
		if (team->captain)
			if (strcasecmp(captain->name, team->captain) == 0)
				return TRUE;
	}
	return FALSE;
}

local Team * GetCaptainTeam(TeamsArenaData *arenaData, Player *captain)
{
	//if (!arenaData->teams)
	//	return NULL;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->captain)
			if (strcasecmp(captain->name, team->captain) == 0)
				return team;
	}
	return NULL;
}


local int GetCaptainFreq(TeamsArenaData *arenaData, Player *captain)
{
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->captain != NULL)
			if (!strcasecmp(captain->name, team->captain))
				return team->frequency;
	}
	return -1;
}


local bool IsCurrentPick(Player *player, TeamsArenaData *arenaData)
{
	lm->Log(L_INFO, "<teams> Is Current Pick %s", player->name);

	if (arenaData->pickingType == PICKINGTYPE_FREE)
		return TRUE;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->frequency == arenaData->currentPickFreq)
		{
			if ((team->captain) && (!strcasecmp(team->captain, player->name)))
				return TRUE;

			return FALSE;
		}
	}
	return FALSE;
}

local int PickedFrequency(TeamsArenaData *arenaData, Player *player)
{
	lm->Log(L_INFO, "<teams> Picked Frequency %s", player->name);

	Link *link;
	Team *team;
    FOR_EACH(arenaData->teams, team, link)
	{
		if (team->captain)
			if (!strcasecmp(player->name, team->captain))
				return team->frequency;

		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			if (!strcasecmp(player->name, teamPlayer->name))
				return team->frequency;
		}
	}
	return -1;
}

local int PickedPlayerFrequency(TeamsArenaData *arenaData, Player *player)
{
	lm->Log(L_INFO, "<teams> Picked Player Frequency %s", player->name);

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			if (!strcasecmp(player->name, teamPlayer->name))
				return team->frequency;
		}
	}
	return -1;
}

local int FindPlayerInTeamFuzzy(TeamsArenaData *arenaData, const char *partialName, TeamPlayer **foundPlayer, int *frequency)
{
	lm->Log(L_INFO, "<teams> Find player in Team %s %d", partialName, *frequency);

	int count = 0;
	bool specificFreq = *frequency != -1;

	*foundPlayer = NULL;

	bool foundExact = FALSE;
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if ((specificFreq) && (team->frequency != *frequency))
			continue;

		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			unsigned int len = strlen(partialName);
			if (!strncasecmp(partialName, teamPlayer->name, len))
			{
				//exact match check
				if (len == strlen(teamPlayer->name))
				{
					count		= 1;
					*foundPlayer	= teamPlayer;
					*frequency	= team->frequency;
					foundExact	= TRUE;
					break;
				}

				if (!*foundPlayer)
				{
					*foundPlayer	= teamPlayer;
					*frequency	= team->frequency;
				}
				count++;
			}
		}
		if ((specificFreq) || (foundExact))
			break;
	}

	if (count > 1)
	{
		*foundPlayer	= NULL;
		*frequency	= -1;
	}

	return count;
}

local bool FindPlayerInTeamExact(TeamsArenaData *arenaData, const char *fullName, TeamPlayer **foundPlayer, int *frequency)
{
	lm->Log(L_INFO, "<teams> Find player in Team %d", *frequency);

	//if (!arenaData->teams)
	//{
	//	lm->Log(L_INFO, "<teams> Teams not defined");
	//	*foundPlayer = NULL;
	//	*frequency = -1;
	//	return FALSE;
	//}

	bool specificFreq = *frequency != -1;

	*foundPlayer = NULL;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if ((specificFreq) && (team->frequency != *frequency))
			continue;

		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			if (strlen(fullName) != strlen(teamPlayer->name))
				continue;

			if (strcasecmp(fullName, teamPlayer->name) == 0)
			{
				*frequency = team->frequency;
				*foundPlayer = teamPlayer;
				return TRUE;
			}
		}
		if (specificFreq)
			break;
	}

	*frequency = -1;
	*foundPlayer = NULL;
	return FALSE;
}

local int FindTeamFuzzy(TeamsArenaData *arenaData, const char *partialName, Team **foundTeam)
{
	lm->Log(L_INFO, "<teams> Find Team Fuzzy %s", partialName);

	int count = 0;

	unsigned int nameLen = strlen(partialName);
	*foundTeam = NULL;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (strncasecmp(partialName, team->teamName, nameLen) == 0)
		{
			if (strlen(team->teamName) == nameLen)
			{
				*foundTeam = team;
				count = 1;
				break;
			}
			if (!*foundTeam)
			{
				*foundTeam = team;
			}
			count++;
		}
	}

	if (count > 1)
	{
		*foundTeam = NULL;
	}

	return count;
}

local bool FindTeamExactPlayer(TeamsArenaData *arenaData, const char *playerName, Team **foundTeam)
{
	lm->Log(L_INFO, "<teams> Find Team Exact Player %s", playerName);

	unsigned int nameLen = strlen(playerName);
	*foundTeam	= NULL;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			if (nameLen != strlen(teamPlayer->name))
				continue;

			if (!strcasecmp(playerName, teamPlayer->name))
			{
				*foundTeam	= team;
				return TRUE;
			}
		}
	}
	return FALSE;
}

local bool FindTeamFreq(TeamsArenaData *arenaData, int frequency, Team **foundTeam)
{
	lm->Log(L_INFO, "<teams> Find Team Freq %d", frequency);

	*foundTeam = NULL;

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->frequency == frequency)
		{
			*foundTeam = team;
			return TRUE;
		}
	}
	return FALSE;
}

local bool FindTeam(TeamsArenaData *arenaData, Player *player, const char *teamString, Team **foundTeam)
{
	*foundTeam = NULL;
	int freq = satoi(teamString);

	if (	(freq > 0) ||
		((freq == 0) && (teamString[0] == '0'))
	   )
	{
		if (!FindTeamFreq(arenaData, freq, foundTeam))
		{
			int count = FindTeamFuzzy(arenaData, teamString, foundTeam);
			if (count > 1)
			{
				chat->SendMessage(player, "Found %d teams matching %s", count, teamString);
				return FALSE;
			}
			if (count < 1)
			{
				chat->SendMessage(player, "No team found matching %s", teamString);
				return FALSE;
			}
		}
	}
	else
	{
		int count = FindTeamFuzzy(arenaData, teamString, foundTeam);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d teams matching %s", count, teamString);
			return FALSE;
		}
		if (count < 1)
		{
			chat->SendMessage(player, "No team found matching %s", teamString);
			return FALSE;
		}
	}
	return TRUE;
}

		/****************************************************************************************
		 *			 		COMMANDS SECTION				*
		 ****************************************************************************************/

/********************************************************
 *			HELP COMMANDS			*
 ********************************************************/
local helptext_t teamshelp_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display Teams Module available commands.";

local void HelpCommands(Player *player);

local void CTeamsHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);
}

local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	chat->SendMessage(player, "--------------------------------------------------");
	chat->SendMessage(player, "The following Teams Module commands are available:");
	chat->SendMessage(player, "--------------------------------------------------");

	DisplayCommand(chat, player, "?teams",			"Show the currently defined teams");
	DisplayCommand(chat, player, "?caps",			"Show the current captains");
	DisplayCommand(chat, player, "?lagout",			"Allows you to re-enter the game after lagging out");
	DisplayCommand(chat, player, "?teamfreq",		"Places you in spec on your team's freq");
	DisplayCommand(chat, player, "?listborrows [freq]",	"List all the borrows for this game or for [freq]");
	DisplayCommand(chat, player, "?currentpick",		"Display which captain / team currently has the pick.");

	if (IsCaptain(arenaData, player))
	{
		chat->SendMessage(player, "-=-=-= Captain Commands =-=-=-");
		DisplayCommand(chat, player, "?pick <name>",			"Draft player <name> to your team");
		DisplayCommand(chat, player, "?add <name>",			"Add player <name> to your team");
		DisplayCommand(chat, player, "?remove <name>",			"Remove player <name> from your team");
		DisplayCommand(chat, player, "?ready",				"Indicate your team is ready to begin.");
		DisplayCommand(chat, player, "?sub <name1>:<name2>",		"Substitute in game player <name1> with spec player <name2>");
		DisplayCommand(chat, player, "?borrow <name>",			"Request to borrow player <name> for this game");
		DisplayCommand(chat, player, "?unborrow <name>",		"Remove request to borrow player <name> for this game");
		DisplayCommand(chat, player, "?approve <name>",			"Approve the request for another team to borrow player <name>");
	}

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_newteams",		"?newteams [-s]",			"Reset all teams. if -s is specified teams will be saved");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_newteams",		"",					"-s: Specified teams should be saved to the teams database");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addteam",		"?addteam <freq>[:name]",		"Add team <name> using freq <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_removeteam",	"?removeteam <freq>",			"Remove team using <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_removeteam",	"?removeteam <name>",			"Remove team <name>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_saveteams",		"?saveteams [ON/OFF]",			"Turn on or off saving teams to the teams database");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setteamship",	"?setteamship <freq>:<1-8>",		"Set the default ship for team <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setteamship",	"?setteamship <team name>:<1-8>",	"Set the default ship for team <team name>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_draftmode",		"?draftmode [ON/OFF]",			"Adds player to spec not in game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_offlinedrafting",	"?offlinedrafting [ON/OFF]",		"Allow captains to draft offline players that are on the signup list");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_usesignups",	"?usesignups [event name/OFF]",		"Use the <event name> for the eligible players or OFF to remove");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addcaptain",	"?addcap <freq>:<name>",		"Add <name> as captain of <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addcaptain",	"?addcap <team name>:<name>",		"Add <name> as captain of <team name>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_removecaptain",	"?removecap <freq>:<name>",		"Remove <name> as captain of <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_removecaptain",	"?removecap <team name>:<name>",	"Remove <name> as captain of <team name>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_startpicking",	"?startpicking",			"Start team picking");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_picktype",		"?picktype [opt]",			"Display the picking type or set it to [opt]");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_picktype",		"",					"<opt> = 1 (Not Controlled)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_picktype",		"",					"<opt> = 2 (One After Another)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_picktype",		"",					"<opt> = 3 (Snake mode)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_picktype",		"",					"<opt> = 4 (Random order)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_nextpick",		"?nextpick",				"Force move picking to the next team");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_freezeteams",	"?freezeteams",				"Stop any captains from changing teams");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_unfreezeteams",	"?unfreezeteams",			"Allow captains to change teams");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forceadd",		"?add <freq>:<name>",			"Add player <name> to <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forceremove",	"?remove <freq>:<name>",		"Remove player <name> from <freq>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forcesub",		"?sub <name1>:<name2>",			"Substitute in game player <name1> with spec player <name2>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forceready",	"?ready <freq>",			"Set <freq> as ready to begin");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forcelagout",	"?lagout <name>",			"Puts lagged out player <name> back into the game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forceteamfreq",	"?teamfreq <name>",			"Places <name> in spec on their team's freq");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_teammax",		"?teammax [maximum]",			"Show or set the current team maximum players");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_teamingamemax",	"?teamingamemax [maximum]",		"Show or set the current team maximum players");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_pickstatus",	"?pickstatus",				"Show the current status of team picking");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"?setpickingstage [opt]",		"Show or set the current Picking Stage");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"",					"<opt> = 1 (Setup)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"",					"<opt> = 2 (Picking)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"",					"<opt> = 3 (Completed)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"",					"<opt> = 4 (Game Start)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_setpickingstage",	"",					"<opt> = 5 (Game Over)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_sameteams",		"?sameteams",				"Reset for a new game using the same teams. Can only be used after a game has ended.");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_savedteams",	"?savedteams",				"Show the list of database saved teams.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_listsavedteams",	"?listsavedteams",			"Show the list of database saved teams.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_listsavedteam",	"?listsavedteam <name>",		"Show the list of players on database saved team <name>.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_delsavedteam",	"?delsavedteam <name>",			"Delete the database saved team <name>.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_loadteam",		"?loadteam <freq>:<name>",		"Load a saved team <name> from database using <freq>.");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addborrow",		"?addborrow <freq>:<name>",		"Add borrow <name> to team <freq>.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addborrow",		"?addborrow <team name>:<name>",	"Add borrow <name> to team <team name>.");
}

/********************************************************
 *			VERSION				*
 ********************************************************/
local helptext_t teamsversion_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display Teams Module version information.";

local void CTeamsVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS Teams Module -- Version: 1.1    Date: 16 October 2021    Author: POiD");
}

/********************************************************
 *			ADD TEAM			*
 ********************************************************/
local helptext_t addteam_help =
"Module: teams\n"
"Targets: none\n"
"Args: frequency and team name\n"
"Adds team using frequency.";

local Team * AddTeam(TeamsArenaData *arenaData, Player *player, int frequency, char *teamName, bool isLoaded);

local void CAddTeam(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Add Team %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_SETUP)
	{
		chat->SendMessage(player, "Cannot add a team after having started picking.");
		return;
	}

	int freq = 0;
	char *teamName;
	char *freqString;

	if (!*params)
	{
		chat->SendMessage(player, "No frequency specified");
		return;
	}

	if (TwoSplitUserInput(':', params, &freqString, &teamName))
	{
		freq = satoi(freqString);

		if (((freq == 0) && (params[0] != '0')) || (freq < 0))
		{
			chat->SendMessage(player, "No frequency specified");
			TwoSplitRelease(&freqString, &teamName);
			return;
		}
		afree(freqString);
	}
	else
	{
		freq = satoi(params);

		if (((freq == 0) && (params[0] != '0')) || (freq < 0))
		{
			chat->SendMessage(player, "No frequency specified.");
			return;
		}

		teamName = amalloc(strlen(params) + 1 + 5);	// "team " + freq
		sprintf(teamName, "Team %s", params);
	}

	//ok so we have a player and freq
	AddTeam(arenaData, player, freq, teamName, FALSE);
}

local Team * AddTeam(TeamsArenaData *arenaData, Player *player, int frequency, char *teamName, bool isLoaded)
{
	lm->Log(L_INFO, "<teams> AddTeam %s %d %s", player->name, frequency, teamName);

	int teamCheck[] = { -1, -1, -1, -1 };
	int mode = cfg->GetInt(player->arena->cfg, "Soccer", "Mode", 0);

	int teams = 4;
	if (mode < 3)
		teams = 2;

	Link *link;
	Team *team;
        FOR_EACH(arenaData->teams, team, link)
	{
		if (team->frequency == frequency)
		{
			chat->SendMessage(player, "Team already exists for frequency %d", frequency);
			afree(teamName);
			return NULL;
		}
		if (strcasecmp(team->teamName, teamName) == 0)
		{
			chat->SendMessage(player, "Team already exists with name %s", teamName);
			afree(teamName);
			return NULL;
		}
		teamCheck[team->frequency % teams]	= team->frequency;
	}

	if ((!arenaData->isDraft) && (mode))
	{
		if (teamCheck[frequency % teams] != -1)
		{
			chat->SendMessage(player, "Same goal in use by team on frequency %d", teamCheck[frequency % teams]);
			afree(teamName);
			return NULL;
		}
	}

	//so we don't have the freq already
	Team * newTeam		= amalloc(sizeof(Team));
	newTeam->frequency	= frequency;
	int len			= strlen(teamName)+1;
	newTeam->teamName	= amalloc(len);
	strncpy(newTeam->teamName, teamName, len-1);

	afree(teamName);
	newTeam->ready		= FALSE;
	newTeam->captain	= NULL;
	newTeam->wasLoaded	= isLoaded;
	newTeam->players	= LLAlloc();
	newTeam->freqShip	= -1;
	newTeam->playersInGame	= 0;
	newTeam->pickedCount	= 0;

	newTeam->borrowList	= LLAlloc();

	LLAdd(arenaData->teams, newTeam);

	arenaData->numberOfTeams++;

	chat->SendArenaSoundMessage(player->arena, 1, "New team %s added using frequency %d", newTeam->teamName, frequency);

	if ((hasDB) && (db->GetStatus()) && (arenaData->saveTeams) && (!isLoaded))
	{
		db->Query(NULL, NULL, 0, ADD_TEAM_QUERY, newTeam->teamName, "");
	}
	return newTeam;
}

/********************************************************
 *			REMOVE TEAM			*
 ********************************************************/
local helptext_t removeteam_help =
"Module: teams\n"
"Targets: none\n"
"Args: frequency or team name\n"
"Remove team usins frequency or matching team name.";

local bool RemoveTeamFreq(TeamsArenaData *arenaData, Player *player, int frequency);
local bool RemoveTeamName(TeamsArenaData *arenaData, Player *player, const char *teamName);
local void RemoveTeam(TeamsArenaData *arenaData, Player *player, Team *team);

local void CRemoveTeam(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Remove Team %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_SETUP)
	{
		chat->SendMessage(player, "Cannot remove a team after having started picking.");
		return;
	}

	int freq = 0;

	if (!*params)
	{
		chat->SendMessage(player, "No frequency or team name specified");
		return;
	}

	freq = satoi(params);

	if (	(freq > 0) ||
		((freq == 0) && (params[0] == '0'))
	   )
	{
		if (RemoveTeamFreq(arenaData, player, freq))
		{
			return;
		}
	}

	RemoveTeamName(arenaData, player, params);
}

local bool RemoveTeamFreq(TeamsArenaData *arenaData, Player *player, int frequency)
{
	lm->Log(L_INFO, "<teams> Remove Team Freq %s %d", player->name, frequency);

	//if (!arenaData->teams)
	//{
	//	lm->Log(L_INFO, "<teams> Teams not defined");
	//	chat->SendMessage(player, "Unable to remove team as teams not defined.");
	//	return FALSE;
	//}

	Team *foundTeam;
	if (FindTeamFreq(arenaData, frequency, &foundTeam))
	{
		RemoveTeam(arenaData, player, foundTeam);
		return TRUE;
	}

	return FALSE;
}

local bool RemoveTeamName(TeamsArenaData *arenaData, Player *player, const char *teamName)
{
	lm->Log(L_INFO, "<teams> Remove Team Name %s %s", player->name, teamName);

	//check matches for teamName
	Team *team;
	int count = FindTeamFuzzy(arenaData, teamName, &team);

	if (count == 1)
	{
		RemoveTeam(arenaData, player, team);
		return TRUE;
	}
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d teams matching %s", count, teamName);
		return FALSE;
	}

	chat->SendMessage(player, "No teams matching %s", teamName);
	return FALSE;
}

local void RemoveTeam(TeamsArenaData *arenaData, Player *player, Team *team)
{
	lm->Log(L_INFO, "<teams> Remove Team %s", team->teamName);

	ReleaseTeam(arenaData, player->arena, team);

	LLRemove(arenaData->teams, team);
	arenaData->numberOfTeams--;

	afree(team);
	chat->SendArenaSoundMessage(player->arena, 1, "Team %s removed", team->teamName);
}

/********************************************************
 *			ADD CAPTAIN			*
 ********************************************************/
local helptext_t addcaptain_help =
"Module: teams\n"
"Targets: player or none\n"
"Args: none or player name and frequency\n"
"Adds player as captain of frequency.";

local void SetCaptain(TeamsArenaData *arenaData, Player *player, Team *team, Player *captain);

local void CAddCaptain(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Add Captain %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Player *targetPlayer = NULL;
	Team *foundTeam;

	if (target->type == T_PLAYER)
	{
		//we have the player
		targetPlayer = target->u.p;

		if (!*params)
		{
			chat->SendMessage(player, "No frequency or team name specified");
			return;
		}

		if (!FindTeam(arenaData, player, params, &foundTeam))
			return;
	}
	else
	{
		char *teamString;
		char *playerString;

		if (!TwoSplitUserInput(':', params, &teamString, &playerString))
		{
			lm->Log(L_INFO, "<teams> Add Captain FALSE");
			chat->SendMessage(player, "You must specify the team name or frequency and the captain. Ex: ?addcap 0:John.");
			return;
		}

		if (!FindTeam(arenaData, player, teamString, &foundTeam))
		{
			TwoSplitRelease(&teamString, &playerString);
			return;
		}
		afree(teamString);

		//find player
		int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, playerString, &targetPlayer);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, playerString);
			afree(playerString);
			return;
		}
		if (count < 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, playerString);
			afree(playerString);
			return;
		}
		afree(playerString);
	}

	//ok so we have a player and freq
	SetCaptain(arenaData, player, foundTeam, targetPlayer);
}

local void SetCaptain(TeamsArenaData *arenaData, Player *player, Team *team, Player *captain)
{
	lm->Log(L_INFO, "<teams> SetCaptain %s %d %s", player->name, team->frequency, captain->name);

	if (team->captain)
	{
		chat->SendArenaSoundMessage(player->arena, 1, "%s replaced with %s as captain of team %d", team->captain, captain->name, team->frequency);
		afree(team->captain);
	}
	else
	{
		chat->SendArenaSoundMessage(player->arena, 1, "%s set as captain of team %d", captain->name, team->frequency);
	}

	int len		= strlen(captain->name) + 1;
	team->captain	= amalloc(len);
	strncpy(team->captain, captain->name, len-1);

	if ((hasDB) && (db->GetStatus()) && (arenaData->saveTeams))
	{
		db->Query(NULL, NULL, 0, CHG_TEAMS_CAPTAIN_QUERY, team->captain, team->teamName);
	}
}

/********************************************************
 *			REMOVE CAPTAIN			*
 ********************************************************/
local helptext_t removecaptain_help =
"Module: teams\n"
"Targets: player or none\n"
"Args: none or player name and frequency\n"
"Removes player as captain of frequency.";

local void RemoveCaptain(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *captain);

local void CRemoveCaptain(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Remove Captain %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	char *targetPlayer = NULL;
	Team *foundTeam;

	if (target->type == T_PLAYER)
	{
		if (!*params)
		{
			chat->SendMessage(player, "No frequency or team name specified");
			return;
		}

		if (!FindTeam(arenaData, player, params, &foundTeam))
			return;

		//we have the player
		targetPlayer = amalloc(strlen(target->u.p->name) + 1);
		strcpy(targetPlayer, target->u.p->name);
	}
	else
	{
		char *teamString;

		if (!TwoSplitUserInput(':', params, &teamString, &targetPlayer))
		{
			lm->Log(L_INFO, "<teams> Add Captain FALSE");
			chat->SendMessage(player, "You must specify the team name or frequency and the captain. Ex: ?removecap 0:John.");
			return;
		}

		if (!FindTeam(arenaData, player, teamString, &foundTeam))
		{
			TwoSplitRelease(&teamString, &targetPlayer);
			return;
		}
	}

	//ok so we have a player and freq
	RemoveCaptain(arenaData, player, foundTeam, targetPlayer);
}

local void RemoveCaptain(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *captain)
{
	lm->Log(L_INFO, "<teams> RemoveCaptain %s %d %s", player->name, foundTeam->frequency, captain);

	if (!foundTeam->captain)
	{
		chat->SendMessage(player, "There is no captain set for team %s", foundTeam->teamName);
		afree(captain);
		return;
	}
	if (!strncasecmp(foundTeam->captain, captain, strlen(captain)))
	{
		chat->SendArenaSoundMessage(player->arena, 1, "%s removed from captain of team %s by %s", foundTeam->captain, foundTeam->teamName, player->name);
		afree(foundTeam->captain);
		foundTeam->captain = NULL;
	}
	else
	{
		chat->SendMessage(player, "%s is the captain of team %s, not %s.", foundTeam->captain, foundTeam->teamName, captain);
	}

	if ((hasDB) && (db->GetStatus()) && (arenaData->saveTeams))
	{
		db->Query(NULL, NULL, 0, CHG_TEAMS_CAPTAIN_QUERY, "", foundTeam->teamName);
	}

	afree(captain);
}

/********************************************************
 *			PICK				*
 ********************************************************/
local helptext_t startpicking_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Start team Picking!";

local int SortTeamsByFreq(const void *a, const void *b);
local int SortTeamsRandomly(const void *a, const void *b);
local void NormalPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip);
local void SnakePick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip);
local void RandomPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip);

local void CStartPicking(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Pick %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->numberOfTeams < 2)
	{
		chat->SendMessage(player, "Less than 2 teams setup. Please set teams first.");
		return;
	}

	//if (!arenaData->teams)
	//{
	//	lm->Log(L_INFO, "<teams> Teams not defined");
	//	chat->SendMessage(player, "Unable to start as teams not defined.");
	//	return;
	//}

	Link *link;
	Team *team;
	bool issues = FALSE;
	int count = 0;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (!team->captain)
		{
			chat->SendMessage(player, "No captain set for Team %d", team->frequency);
			issues = TRUE;
		}
		count++;
	}
	if ((issues) || (count < 2))
		return;

	if (arenaData->pickingType == PICKINGTYPE_RANDOM)
	{
		time_t t;
		srand((unsigned) time(&t));
		LLSort(arenaData->teams, SortTeamsRandomly);
	}
	else
		LLSort(arenaData->teams, SortTeamsByFreq);

	arenaData->pickingStage = PICKING_PICKING;
	arenaData->currentPick	= 1;
	arenaData->pickingRound	= 1;
	arenaData->pickDirection = FALSE;

	game->LockArena(player->arena, 1, 0, 0, 1);

	DisplayPickStatus(player);

	switch (arenaData->pickingType)
	{
		case PICKINGTYPE_NORMAL:
			chat->SendArenaSoundMessage(player->arena, 1, "Picking of teams has started! Picking Type set to One After Another");
			NormalPick(arenaData, player, TRUE, FALSE);
		break;
		case PICKINGTYPE_SNAKE:
			chat->SendArenaSoundMessage(player->arena, 1, "Picking of teams has started! Picking Type set to Snake mode");
			SnakePick(arenaData, player, TRUE, FALSE);
		break;
		case PICKINGTYPE_RANDOM:
			chat->SendArenaSoundMessage(player->arena, 1, "Picking of teams has started! Picking Type set to Random order");
			RandomPick(arenaData, player, TRUE, FALSE);
		break;
		default:
		case PICKINGTYPE_FREE:
			chat->SendArenaSoundMessage(player->arena, 1, "Picking of teams has started! Picking Type set to not controlled");
			chat->SendArenaMessage(player->arena, "Captains pick at will.");
		break;
	}
}

local int SortTeamsByFreq(const void *a, const void *b)
{
	const Team *teamA = (Team *)a;
	const Team *teamB = (Team *)b;

	return teamA->frequency < teamB->frequency;
}

local int SortTeamsRandomly(const void *a, const void *b)
{
	return rand() % 2;
}

/********************************************************
 *			NEXT PICK			*
 ********************************************************/
local helptext_t nextpick_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Force move to the next team to pick.";

local void NextPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip);
local bool FindMissingPicks(TeamsArenaData *arenaData, Player *player, bool skip);
local bool SetPickingRound(TeamsArenaData *arenaData);

local void CNextPick(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Next Pick %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	NextPick(arenaData, player, TRUE, TRUE);
}

local void NextPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip)
{
	lm->Log(L_INFO, "<teams> NextPick %s", player->name);

	if (FindMissingPicks(arenaData, player, skip))
		return;

	if (SetPickingRound(arenaData))
	{
		if ((notify) && (arenaData->pickingStage == PICKING_PICKING))
			chat->SendArenaSoundMessage(player->arena, 2, "Teams have picked the maximum number of players. Captains to ?ready when ready.");
		return;
	}

	switch (arenaData->pickingType)
	{
		case PICKINGTYPE_NORMAL:
			NormalPick(arenaData, player, notify, skip);
		break;
		case PICKINGTYPE_SNAKE:
			SnakePick(arenaData, player, notify, skip);
		break;
		case PICKINGTYPE_RANDOM:
			RandomPick(arenaData, player, notify, skip);
		break;
		case PICKINGTYPE_FREE:
		default:
			if (notify)
				chat->SendMessage(player, "Picking is currently not controlled so no option to force the Next Pick.");
		break;
	}
}

local bool FindMissingPicks(TeamsArenaData *arenaData, Player *player, bool skip)
{
	Link *link;
	Team *team;

	int lowestFreq = -1;
	char *lowestCap = NULL;
	int lowestCount	= -1;
	char *lowestTeam = NULL;

	FOR_EACH(arenaData->teams, team, link)
	{
		if (arenaData->isDraft)
		{
			//only set as missing picks if picking is less than the previous round
			//this is to ensure order is proper for "this" round still
			if ((team->pickedCount < (arenaData->pickingRound -1)))
			{
				if (	(lowestCount == -1) ||
					(team->pickedCount < lowestCount) ||
					(	(lowestCount == team->pickedCount) && 
						(team->frequency == arenaData->currentPickFreq)
					)
				   )
				{
					if (!skip || (skip && (arenaData->currentPickFreq != team->frequency)))
					{
						lowestCount	= team->pickedCount;
						lowestFreq	= team->frequency;
						lowestCap	= team->captain;
						lowestTeam	= team->teamName;
					}
				}
			}
			continue;
		}

		//not draft but a loaded team
		if (team->wasLoaded)
		{
			//only set as missing picks if picking is less than the previous round
			//this is to ensure order is proper for "this" round still
			if (	(team->playersInGame < (arenaData->pickingRound - 1)) &&
				(team->playersInGame < arenaData->teamInGameMax)
			   )
			{
				if (	(lowestCount == -1) ||
					(team->playersInGame < lowestCount) ||
					(	(lowestCount == team->playersInGame) && 
						(team->frequency == arenaData->currentPickFreq)
					)
				   )
				{
					if (!skip || (skip && (arenaData->currentPickFreq != team->frequency)))
					{
						lowestCount	= team->playersInGame;
						lowestFreq	= team->frequency;
						lowestCap	= team->captain;
						lowestTeam	= team->teamName;
					}
				}
			}
			continue;
		}

		//not draft and not loaded team
		if (team->pickedCount < (arenaData->pickingRound - 1))
		{
			if (	(lowestCount == -1) ||
				(team->pickedCount < lowestCount) ||
				(	(lowestCount == team->pickedCount) && 
					(team->frequency == arenaData->currentPickFreq)
				)
			   )
			{
				if (!skip || (skip && (arenaData->currentPickFreq != team->frequency)))
				{
					lowestCount	= team->pickedCount;
					lowestFreq	= team->frequency;
					lowestCap	= team->captain;
					lowestTeam	= team->teamName;
				}
			}
		}
	}

	if (lowestFreq == -1)
		return FALSE;

	arenaData->currentPickFreq = lowestFreq;
	if (lowestCap)
		chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s", lowestCap);
	else
		chat->SendArenaSoundMessage(player->arena, 1, "%s team pick, but no captain set!", lowestTeam);
	return TRUE;
}


local bool IsTeamMissingPicks(TeamsArenaData *arenaData, int frequency)
{
	Link *link;
	Team *team;

	//if (!arenaData->teams)
	//{
	//	lm->Log(L_INFO, "<teams> IsTeamMissingPicks Teams not defined");
	//	return FALSE;
	//}

	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->frequency == frequency)
		{
			if (team->wasLoaded)
			{
				if (arenaData->pickingStage == PICKING_COMPLETED)
				{
					if (	(arenaData->teamInGameMax != -1) && 
						(team->playersInGame < arenaData->teamInGameMax)
					   )
						return TRUE;
					return FALSE;
				}

				if (	(arenaData->teamInGameMax != -1) && 
					(team->playersInGame < arenaData->teamInGameMax) &&
					(team->playersInGame < arenaData->pickingRound)
				   )
					return TRUE;
				return FALSE;
			}
			if (arenaData->pickingStage == PICKING_COMPLETED)
			{
				if (team->pickedCount < arenaData->teamMax)
					return TRUE;
				return FALSE;
			}

			if (team->pickedCount < arenaData->pickingRound)
				return TRUE;

			return FALSE;
		}
	}
	return FALSE;
}

//returning TRUE means picking rounds are done
//returning FALSE means more picks available
local bool SetPickingRound(TeamsArenaData *arenaData)
{
	lm->Log(L_INFO, "<teams> Set Picking Round : %d", arenaData->pickingRound);

	bool nonLoadedTeam = FALSE;
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		//if ((team->wasLoaded) && (team->playersInGame < arenaData->teamInGameMax))
		if ((team->wasLoaded) && (team->playersInGame < arenaData->pickingRound))
			return FALSE;

		//if (team->pickedCount < arenaData->pickingRound)
		//	return FALSE;

		if (!team->wasLoaded)
		{
			nonLoadedTeam = TRUE;
			if (team->pickedCount < arenaData->pickingRound)
				return FALSE;
		}
	}

	arenaData->pickingRound++;
	if (nonLoadedTeam)
	{
		if ((arenaData->teamMax != -1) && (arenaData->pickingRound > arenaData->teamMax))
		{
			arenaData->pickingRound = arenaData->teamMax;
			return TRUE;
		}
	}
	else
	{
		if ((arenaData->teamInGameMax != -1) && (arenaData->pickingRound > arenaData->teamInGameMax))
		{
			arenaData->pickingRound = arenaData->teamInGameMax;
			return TRUE;
		}
	}

	return FALSE;
}

local void NormalPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip)
{
	lm->Log(L_INFO, "<teams> NormalPick, %d %d - %d", notify, skip, arenaData->currentPick);

	if (arenaData->currentPick > arenaData->numberOfTeams)
		arenaData->currentPick = arenaData->numberOfTeams;

	bool found = FALSE;
	int loopCount = 0;

	while (!found)
	{
		Link *link	= NULL;
		Team *team	= NULL;
		int count	= 0;
		FOR_EACH(arenaData->teams, team, link)
		{
			count++;
			if (count < arenaData->currentPick)
				continue;

			if ((team->wasLoaded) && (team->playersInGame >= arenaData->teamInGameMax))
				break;

			arenaData->currentPickFreq = team->frequency;
			if (team->captain)
				chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s", team->captain);
			else
				chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s (No Captain assigned)", team->teamName);
			found = TRUE;

			break;
		}
		if (!found)
			loopCount++;
		
		if (loopCount >= 2)
			chat->SendArenaSoundMessage(player->arena, 2, "Picking complete. Captains to ?ready when ready.");

		arenaData->currentPick++;
		if (arenaData->currentPick > arenaData->numberOfTeams)
			arenaData->currentPick = 1;
	}
}

local void SnakePick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip)
{
	lm->Log(L_INFO, "<teams> SnakePick, %d %d : %d %s", notify, skip, arenaData->currentPick, arenaData->pickDirection ? "BACK" : "FORWARD" );

	if (arenaData->currentPick > arenaData->numberOfTeams)
		arenaData->currentPick = arenaData->numberOfTeams;

	bool found = FALSE;
	int loopCount = 0;

	while (!found)
	{
		int count = 0;

		Link *link;
		Team *team;
		FOR_EACH(arenaData->teams, team, link)
		{
			count++;
			if (count != arenaData->currentPick)
				continue;
			
			if ((team->wasLoaded) && (team->playersInGame >= arenaData->teamInGameMax))
				break;

			if (!skip || (skip && (arenaData->currentPickFreq != team->frequency)))
			{
				found = TRUE;
				arenaData->currentPickFreq = team->frequency;
				if (team->captain)
					chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s", team->captain);
				else
					chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s (No Captain Assigned)", team->teamName);
			}

			break;
		}

		if (!arenaData->pickDirection)
		{
			arenaData->currentPick++;
			if (arenaData->currentPick > arenaData->numberOfTeams)
			{
				arenaData->currentPick--;
				arenaData->pickDirection = TRUE;
				loopCount++;
			}
		}
		else
		{
			arenaData->currentPick--;
			if (arenaData->currentPick < 1)
			{
				arenaData->currentPick++;
				arenaData->pickDirection = FALSE;
				loopCount++;
			}
		}

		if (loopCount >= 2)
			chat->SendArenaSoundMessage(player->arena, 2, "Picking complete. Captains to ?ready when ready.");
	}
}

local void RandomPick(TeamsArenaData *arenaData, Player *player, bool notify, bool skip)
{
	//if (!arenaData->teams)
	//{
	//	lm->Log(L_INFO, "<teams> RandomPick, Teams not defined");
	//	chat->SendMessage(player, "Unable to determine next pick, teams not defined.");
	//	return;
	//}

	time_t t;
	Link *link;
	Team *team;
	int pickCount = 0;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->wasLoaded)
		{
			if (	(team->playersInGame < arenaData->teamInGameMax) &&
				(team->playersInGame < arenaData->pickingRound))
			{
				pickCount++;
			}
			continue;
		}

		if (team->pickedCount < arenaData->pickingRound)
		{
			pickCount++;
		}
	}

	srand((unsigned) time(&t));

	if (!pickCount)
	{
		arenaData->pickingRound++;
		pickCount = rand() % arenaData->numberOfTeams;
	}
	else
		pickCount = rand() % pickCount;

	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->pickedCount >= arenaData->pickingRound)
			continue;

		if (pickCount)
		{
			pickCount--;
			continue;
		}

		arenaData->currentPickFreq = team->frequency;
		if (team->captain)
			chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s", team->captain);
		else
			chat->SendArenaSoundMessage(player->arena, 1, "Your pick %s (No Captain Assigned)", team->teamName);
		return;
	}
}

/********************************************************
 *			CURRENT PICK			*
 ********************************************************/
local helptext_t currentpick_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display which team has the current pick.";

local void CCurrentPick(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Current Pick %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_PICKING)
	{
		chat->SendMessage(player, "Picking is not currently available.");
		return;
	}

	if (arenaData->pickingType == PICKINGTYPE_FREE)
	{
		chat->SendMessage(player, "Picking is unmanaged. Any captain can currently pick.");
		return;
	}

	if (arenaData->currentPickFreq == -1)
	{
		chat->SendMessage(player, "No team currently has the pick.");
		return;
	}

	Team *foundTeam;
	if (!FindTeamFreq(arenaData, arenaData->currentPickFreq, &foundTeam))
	{
		chat->SendMessage(player, "No team found with the current pick for frequency %d", arenaData->currentPickFreq);
		return;
	}

	if (foundTeam->captain)
		chat->SendMessage(player, "%s has the current pick for %s.", foundTeam->captain, foundTeam->teamName);
	else
		chat->SendMessage(player, "%s has the current pick (No Captain Assigned)", foundTeam->teamName);
}

/********************************************************
 *			PICK TYPE			*
 ********************************************************/
local helptext_t picktype_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or new picking type\n"
"Displays or sets the current picking type.";

local void DisplayPickType(Player *player, TeamsArenaData *arenaData);

local void CPickType(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Picking Type %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		DisplayPickType(player, arenaData);
		return;
	}

	switch (params[0])
	{
		case '1':
			arenaData->pickingType = PICKINGTYPE_FREE;
			chat->SendMessage(player, "Picking Type set to Not Controlled");
		break;
		case '2':
			arenaData->pickingType = PICKINGTYPE_NORMAL;
			chat->SendMessage(player, "Picking Type set to One After Another");
		break;
		case '3':
			arenaData->pickingType = PICKINGTYPE_SNAKE;
			chat->SendMessage(player, "Picking Type set to Snake mode");
		break;
		case '4':
			arenaData->pickingType = PICKINGTYPE_RANDOM;
			chat->SendMessage(player, "Picking Type set to Random Order");
		break;
		default:
			chat->SendMessage(player, "Invalid option specified.");
	}
}

local void DisplayPickType(Player *player, TeamsArenaData *arenaData)
{
	switch (arenaData->pickingType)
	{
		case PICKINGTYPE_NORMAL:
			chat->SendMessage(player, "Picking Type currently set to: One After Another");
		break;
		case PICKINGTYPE_SNAKE:
			chat->SendMessage(player, "Picking Type currently set to: Snake mode");
		break;
		case PICKINGTYPE_RANDOM:
			chat->SendMessage(player, "Picking Type currently set to: Random Order");
		break;
		case PICKINGTYPE_FREE:
		default:
			chat->SendMessage(player, "Picking Type currently set to: Not Controlled");
		break;
	}
}

/********************************************************
 *			PICK				*
 ********************************************************/
local helptext_t pick_help =
"Module: teams\n"
"Targets: player or none\n"
"Args: none or player name and frequency\n"
"Adds player to frequency.";

local void CAddHandleInput(TeamsArenaData *arenaData, const char *command, const char *params, Player *player, const Target *target);

local void CPick(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Add Player %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->isDraft)
	{
		chat->SendMessage(player, "Currently in team adding mode. Pick is only used for drafting.");
		return;
	}
	CAddHandleInput(arenaData, command, params, player, target);
}

/********************************************************
 *			ADD				*
 ********************************************************/
local helptext_t add_help =
"Module: teams\n"
"Targets: player or none\n"
"Args: none or player name and frequency\n"
"Adds player to frequency.";

typedef struct SignUpCheckStruct
{
	TeamsArenaData *	arenaData;
	char *			targetPlayerName;
	Team *			foundTeam;
	Player *		targetPlayer;

} SignUpCheckStruct;

local void CheckPlayerAvailable(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *targetPlayerName, Player *targetPlayer);
local bool CheckCaptainPick(Player *player, TeamsArenaData *arenaData, int freq);
local void NotUsingSignupList(TeamsArenaData *arenaData, Player *player, char *targetPlayerName, Team *foundTeam, Player *targetPlayer);
local void UsingSignupList(TeamsArenaData *arenaData, Player *player, char *targetPlayerName, Team *foundTeam, Player *targetPlayer);
local void HandleFreqAdd(TeamsArenaData *arenaData, Player *player, Team *foundTeam, Player *target);
local void SignUpCheckCallback(Player *player, int resultCount, LinkedList *options, void *data);
local void AddPlayer(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *targetPlayerName, Player *target, bool isLoaded);
local TeamPlayer * AddPlayerToList(Player *player, const char *playerName, int ship, Team *team, char *activeEvent, bool wasLoaded, bool wasBorrowed);
local void RemovePlayerFromSignupCB(Player *player, bool wasRemoved, void *data);

local void CAdd(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Add Player %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->isDraft)
	{
		chat->SendMessage(player, "Currently in team drafting mode. Add is only used for team picking.");
		return;
	}

	CAddHandleInput(arenaData, command, params, player, target);
}

local void CAddHandleInput(TeamsArenaData *arenaData, const char *command, const char *params, Player *player, const Target *target)
{
	char * targetPlayerName	= NULL;
	char * freqString	= NULL;
	Team * foundTeam	= NULL;

	Player *targetPlayer	= NULL;

	bool isStaff = capman->HasCapability(player, "cmd_forceadd");
	bool isCaptain = IsCaptain(arenaData, player);

	if ((!isStaff) && (!isCaptain))
	{
		chat->SendMessage(player, "Only captains or Moderators can add players.");
		return;
	}

	if (	((arenaData->pickingStage == PICKING_SETUP) ||
		(arenaData->pickingStage == PICKING_PAUSED) ||
		(arenaData->pickingStage == PICKING_GAMEOVER))
		&& (!isStaff))
	{
		chat->SendMessage(player, "Picking currently not available.");
		return;
	}

	if (target->type == T_PLAYER)
	{
		if (!*params)
		{
			if (isCaptain)
			{
				foundTeam = GetCaptainTeam(arenaData, player);
				if (!foundTeam)
				{
					chat->SendMessage(player, "Unable to determine your team");
					return;
				}

				if ((arenaData->pickingStage != PICKING_GAMESTART) && (arenaData->pickingStage != PICKING_COMPLETED))
					if (!CheckCaptainPick(player, arenaData, foundTeam->frequency))
						return;

				targetPlayerName = amalloc(strlen(target->u.p->name) + 1);
				strcpy(targetPlayerName, target->u.p->name);

				targetPlayer = target->u.p;
			}
			else
			{
				chat->SendMessage(player, "No frequency or team Name specified.");
				return;
			}
		}
		else
		{
			if (isStaff)
			{
				if (!FindTeam(arenaData, player, params, &foundTeam))
					return;

				targetPlayerName = amalloc(strlen(target->u.p->name) + 1);
				strcpy(targetPlayerName, target->u.p->name);

				targetPlayer = target->u.p;
			}
			else
			{
				chat->SendMessage(player, "No parameters needed if selecting a player as captain.");
				return;
			}
		}
	}
	else
	{
		if (!*params)
		{
			if (isCaptain)
				chat->SendMessage(player, "No player name specified.");
			else
				chat->SendMessage(player, "No player and no freq nor team name specified");
			return;
		}
		else
		{
			if (isStaff)
			{
				if (TwoSplitUserInput(':', params, &freqString, &targetPlayerName))
				{
					if (!FindTeam(arenaData, player, freqString, &foundTeam))
					{
						TwoSplitRelease(&freqString, &targetPlayerName);
						return;
					}
					afree(freqString);
				}
				else if (isCaptain)
				{
					foundTeam = GetCaptainTeam(arenaData, player);
					if (!foundTeam)
					{
						chat->SendMessage(player, "Unable to determine your team");
						return;
					}

					if ((arenaData->pickingStage != PICKING_GAMESTART) && (arenaData->pickingStage != PICKING_COMPLETED))
						if (!CheckCaptainPick(player, arenaData, foundTeam->frequency))
							return;

					int len			= strlen(params) + 1;
					targetPlayerName	= amalloc(len);
					strncpy(targetPlayerName, params, len-1);
				}
				else
				{
					chat->SendMessage(player, "No player and freq specified");
					return;
				}
			}
			else
			{
				foundTeam = GetCaptainTeam(arenaData, player);
				if (!foundTeam)
				{
					chat->SendMessage(player, "Unable to determine your team");
					return;
				}

				if ((arenaData->pickingStage != PICKING_GAMESTART) && (arenaData->pickingStage != PICKING_COMPLETED))
					if (!CheckCaptainPick(player, arenaData, foundTeam->frequency))
						return;

				int len			= strlen(params) + 1;
				targetPlayerName	= amalloc(len);
				strncpy(targetPlayerName, params, len-1);
			}
		}
	}

	CheckPlayerAvailable(arenaData, player, foundTeam, targetPlayerName, targetPlayer);
}

local bool CheckCaptainPick(Player *player, TeamsArenaData *arenaData, int freq)
{
	//handle situation where adding needed after game start
	if (arenaData->pickingStage == PICKING_COMPLETED)
	{
		if (!IsTeamMissingPicks(arenaData, freq))
		{
			chat->SendMessage(player, "Your team is at the team max. No further picking allowed.");
			return FALSE;
		}
	}
	else
	{
		if (!IsCurrentPick(player, arenaData))
		{
			chat->SendMessage(player, "It is not currently your turn to pick. Please wait.");
			return FALSE;
		}
	}
	return TRUE;
}

local void CheckPlayerAvailable(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *targetPlayerName, Player *targetPlayer)
{
	lm->Log(L_INFO, "<teams> CheckPlayerAvailable %s %d %s", player->name, foundTeam->frequency, targetPlayerName);

	if ((!foundTeam->wasLoaded) && (arenaData->activeEvent))
		UsingSignupList(arenaData, player, targetPlayerName, foundTeam, targetPlayer);
	else
		NotUsingSignupList(arenaData, player, targetPlayerName, foundTeam, targetPlayer);
}

local void NotUsingSignupList(TeamsArenaData *arenaData, Player *player, char *targetPlayerName, Team *foundTeam, Player *targetPlayer)
{
	lm->Log(L_INFO, "<teams> NotUsingSignupList %s %d %s", player->name, foundTeam->frequency, targetPlayerName);

	//not using signup list
	if (!targetPlayer)
	{
		//firstly try and find the player in the arena.
		int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, targetPlayerName, &targetPlayer);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayerName);
			afree(targetPlayerName);
			return;
		}
		if (count < 1)
		{
			//could not find a player in the arena, so check in the zone.
			int count = FindPlayerOnlineFuzzy(lm, pd, targetPlayerName, &targetPlayer);
			if ((count < 1) || (count > 1))
			{
				chat->SendMessage(player, "No players matching in the arena and found %d players matching to %s online.", count, targetPlayerName);
				afree(targetPlayerName);
				return;
			}
			//so we have 1 player in targetPlayer
			//fall through
		}
	}

	afree(targetPlayerName);

	int freq = PickedPlayerFrequency(arenaData, targetPlayer);
	if (foundTeam->wasLoaded)
	{
		if (freq == foundTeam->frequency)
		{
			HandleFreqAdd(arenaData, player, foundTeam, targetPlayer);
			return;
		}
		chat->SendMessage(player, "%s isn't part of team %s so cannot be added.", targetPlayer->name, foundTeam->teamName);
		return;
	}
	else
	{
		if (freq != -1)
		{
			chat->SendMessage(player, "%s is already on a team!", targetPlayer->name);
			return;
		}
		freq = GetCaptainFreq(arenaData, targetPlayer);
		if ((freq != -1) && (freq != foundTeam->frequency))
		{
			chat->SendMessage(player, "%s is the other captain!", targetPlayer->name);
			return;
		}
	}

	AddPlayer(arenaData, player, foundTeam, targetPlayer->name, targetPlayer, FALSE);
}


local int DetermineShip(TeamsArenaData *arenaData, Team *team)
{
	int ship;
	if (arenaData->isDraft)
		ship = SHIP_SPEC;
	else
	{
		if (team->playersInGame >= arenaData->teamInGameMax)
			ship = SHIP_SPEC;
		else
		{
			//base it on Freq
			if (team->freqShip == -1)
				ship = (team->frequency % arenaData->numberOfTeams) % SHIP_SPEC;
			else
				ship = team->freqShip;
		}
	}
	return ship;
}

local void HandleFreqAdd(TeamsArenaData *arenaData, Player *player, Team *foundTeam, Player *target)
{
	TeamPlayer *teamPlayer;
	Link *link;

	FOR_EACH(foundTeam->players, teamPlayer, link)
	{
		if (strlen(target->name) != strlen(teamPlayer->name))
			continue;

		if (!strcasecmp(target->name, teamPlayer->name))
		{
			if (teamPlayer->ship != SHIP_SPEC)
			{
				chat->SendMessage(player, "Cannot add %s as they are already not in spec!", target->name);
				return;
			}

			int ship		= DetermineShip(arenaData, foundTeam);
			teamPlayer->ship	= ship;
			game->SetShipAndFreq(target, ship, foundTeam->frequency);
			stats->ScoreReset(target, INTERVAL_RESET);
			stats->SendUpdates(NULL);
			chat->SendArenaSoundMessage(player->arena, 1, "%s picked for team %s", target->name, foundTeam->teamName);
			if (ship != SHIP_SPEC)
				foundTeam->playersInGame++;
			NextPick(arenaData, player, TRUE, FALSE);
			return;
		}
	}
	chat->SendMessage(player, "Cannot find player in the team!");
}


local void UsingSignupList(TeamsArenaData *arenaData, Player *player, char *targetPlayerName, Team *foundTeam, Player *targetPlayer)
{
	lm->Log(L_INFO, "<teams> UsingSignupList %s %d %s", player->name, foundTeam->frequency, targetPlayerName);

	SignUpCheckStruct *signUpCheckStruct		= amalloc(sizeof(SignUpCheckStruct));
	signUpCheckStruct->arenaData			= arenaData;
	signUpCheckStruct->targetPlayerName		= targetPlayerName;
	signUpCheckStruct->targetPlayer			= targetPlayer;
	signUpCheckStruct->foundTeam			= foundTeam;

	void (*IsSignUpPtr)(Player *, int, LinkedList *, void *) = &SignUpCheckCallback;
	signups->IsPlayerSignedUp(player, targetPlayerName, arenaData->activeEvent, (void *)signUpCheckStruct, IsSignUpPtr);
}

local void SignUpCheckCallback(Player *player, int resultCount, LinkedList *options, void *data)
{
	SignUpCheckStruct *signUpCheckStruct = (SignUpCheckStruct *)data;

	//error situation
	if (resultCount == -1)
	{
		afree(signUpCheckStruct->targetPlayerName);
		afree(signUpCheckStruct);
		return;
	}
	if (!resultCount)
	{
		chat->SendMessage(player, "%s is not available. Check ?listsignups for available players.", signUpCheckStruct->targetPlayerName);
		afree(signUpCheckStruct->targetPlayerName);
		afree(signUpCheckStruct);
		return;
	}

	Link *link;
	//have 1 match we can look for.
	if (resultCount == 1)
	{
		link			= LLGetHead(options);
		char *playerName	= (char *)(link->data);

		if (!signUpCheckStruct->targetPlayer)
		{
			//we don't have targetPlayer, so try and find in the arena
			if (!FindPlayerOnlineExact(lm, pd, playerName, &signUpCheckStruct->targetPlayer))
			{

				if (	(!signUpCheckStruct->arenaData->isDraft)	||
					(!signUpCheckStruct->arenaData->offlineDrafting))
				{
					chat->SendMessage(player, "Player %s is not online so cannot be picked.", playerName);
					afree(signUpCheckStruct->targetPlayerName);
					afree(signUpCheckStruct);
					return;
				}
				signUpCheckStruct->targetPlayer = NULL;
			}
		}

		AddPlayer(signUpCheckStruct->arenaData, player, signUpCheckStruct->foundTeam, playerName, signUpCheckStruct->targetPlayer, FALSE);
	}
	else
	{
		//we have multiple. Exact matching is handled in signups->IsPlayerSignedUp
		chat->SendMessage(player, "Found %d matches on the signup list for %s.", resultCount, signUpCheckStruct->targetPlayerName);
		afree(signUpCheckStruct->targetPlayerName);
		afree(signUpCheckStruct);
		return;
	}
}

local void AddPlayer(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *targetPlayerName, Player *target, bool isLoaded)
{
	if (	(!isLoaded) &&
		(arenaData->teamMax != -1) &&
		(foundTeam->pickedCount >= arenaData->teamMax)
	   )
	{
		chat->SendMessage(player, "Team maximum has been reached. No more players can be selected.");
		return;
	}

	if (!isLoaded)
		chat->SendArenaSoundMessage(player->arena, 1, "%s picked for team %s", targetPlayerName, foundTeam->teamName);

	int ship = DetermineShip(arenaData, foundTeam);

	AddPlayerToList(player, targetPlayerName, ship, foundTeam, arenaData->activeEvent, isLoaded, FALSE);

	if ((target) && (target->arena == player->arena))
	{
		game->SetShipAndFreq(target, ship, foundTeam->frequency);
		stats->ScoreReset(target, INTERVAL_RESET);
		stats->SendUpdates(NULL);
	}

	if ((hasDB) && (db->GetStatus()) && (arenaData->saveTeams) && (!isLoaded))
	{
		db->Query(NULL, NULL, 0, ADD_TEAM_PLAYER_QUERY, targetPlayerName, foundTeam->teamName);
	}

	if (!isLoaded)
		NextPick(arenaData, player, TRUE, FALSE);
}

local TeamPlayer * AddPlayerToList(Player *player, const char *playerName, int ship, Team *team, char *activeEvent, bool wasLoaded, bool wasBorrowed)
{
	TeamPlayer *teamPlayer	= amalloc(sizeof(TeamPlayer));
	unsigned int nameLen	= strlen(playerName) + 1;
	teamPlayer->name	= amalloc(nameLen);
	strncpy(teamPlayer->name, playerName, nameLen-1);
	teamPlayer->ship	= ship;
	teamPlayer->wasLoaded	= wasLoaded;
	teamPlayer->wasBorrowed	= wasBorrowed;
	teamPlayer->laggedOut	= TRUE;

	team->pickedCount++;
	if (ship != SHIP_SPEC)
	{
		teamPlayer->laggedOut	= FALSE;
		team->playersInGame++;
	}

	LLAdd(team->players, teamPlayer);

	if (activeEvent)
	{
		void (*IsSignUpPtr)(Player *, bool, void *) = &RemovePlayerFromSignupCB;
		bool *ignore = amalloc(sizeof(bool));
		*ignore = wasLoaded || wasBorrowed;
		signups->RemovePlayer(player, playerName, activeEvent, ignore, IsSignUpPtr);
	}
	return teamPlayer;
}

local void RemovePlayerFromSignupCB(Player *player, bool wasRemoved, void *data)
{
	bool *ignore = (bool *)data;
	if ((!wasRemoved) && (!*ignore))
	{
		chat->SendMessage(player, "Unable to remove player from signed up list.");
	}
	afree(ignore);
}

/********************************************************
 *			REMOVE				*
 ********************************************************/
local helptext_t remove_help =
"Module: teams\n"
"Targets: player or none\n"
"Args: none or player name and frequency\n"
"Remove player from frequency.";

local void RemovePlayer(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *target);

local void CRemove(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Remove Player %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	char *targetPlayer	= NULL;
	char *freqString	= NULL;
	Team *foundTeam		= NULL;

	bool isStaff		= capman->HasCapability(player, "cmd_forceremove");
	bool isCaptain		= IsCaptain(arenaData, player);

	if ((!isStaff) && (!isCaptain))
	{
		chat->SendMessage(player, "Only captains or Moderators can remove players.");
		return;
	}


	if (	((arenaData->pickingStage == PICKING_SETUP) ||
		(arenaData->pickingStage == PICKING_PAUSED) ||
		(arenaData->pickingStage == PICKING_GAMEOVER))
		&& (!isStaff))
	{
		chat->SendMessage(player, "Picking currently not available.");
		return;
	}

	if (target->type == T_PLAYER)
	{
		if (!*params)
		{
			if (isCaptain)
			{
				foundTeam = GetCaptainTeam(arenaData, player);
				if (!foundTeam)
				{
					chat->SendMessage(player, "Unable to determine your team");
					return;
				}

				targetPlayer = amalloc(strlen(target->u.p->name) + 1);
				strcpy(targetPlayer, target->u.p->name);
			}
			else
			{
				chat->SendMessage(player, "No frequency or team name specified.");
				return;
			}
		}
		else
		{
			if (isStaff)
			{
				if (!FindTeam(arenaData, player, params, &foundTeam))
				{
					return;
				}

				targetPlayer = amalloc(strlen(target->u.p->name) + 1);
				strcpy(targetPlayer, target->u.p->name);
			}
			else
			{
				chat->SendMessage(player, "No parameters needed if selecting a player.");
				return;
			}
		}
	}
	else
	{
		if (!*params)
		{
			if (isCaptain)
				chat->SendMessage(player, "No player name specified.");
			else
				chat->SendMessage(player, "No player and frequency or team name specified");
			return;
		}
		else
		{
			if (isStaff)
			{
				if (TwoSplitUserInput(':', params, &freqString, &targetPlayer))
				{
					if (!FindTeam(arenaData, player, freqString, &foundTeam))
					{
						TwoSplitRelease(&freqString, &targetPlayer);
						return;
					}
					afree(freqString);
				}
				else if (isCaptain)
				{
					foundTeam = GetCaptainTeam(arenaData, player);
					if (!foundTeam)
					{
						chat->SendMessage(player, "Unable to determine your team");
						return;
					}

					int len		= strlen(params) + 1;
					targetPlayer	= amalloc(len);
					strncpy(targetPlayer, params, len-1);
				}
				else
				{
					chat->SendMessage(player, "No player and frequency or team name specified");
					return;
				}
			}
			else
			{
				foundTeam = GetCaptainTeam(arenaData, player);
				if (!foundTeam)
				{
					chat->SendMessage(player, "Unable to determine your team");
					return;
				}

				int len		= strlen(params) + 1;
				targetPlayer	= amalloc(len);
				strncpy(targetPlayer, params, len-1);
			}
		}
	}

	TeamPlayer *teamPlayer;
	int freq = foundTeam->frequency;
	int count = FindPlayerInTeamFuzzy(arenaData, targetPlayer, &teamPlayer, &freq);
	if (count > 1)
	{
		if (isCaptain)
			chat->SendMessage(player, "Found %d players matching to %s on your team", count, targetPlayer);
		else
			chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayer);
		afree(targetPlayer);
		return;
	}
	if (count < 1)
	{
		if (isCaptain)
			chat->SendMessage(player, "Found %d players matching to %s on your team", count, targetPlayer);
		else
			chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayer);
		afree(targetPlayer);
		return;
	}

	RemovePlayer(arenaData, player, foundTeam, teamPlayer->name);
}

local void RemovePlayer(TeamsArenaData *arenaData, Player *player, Team *foundTeam, char *target)
{
	lm->Log(L_INFO, "<teams> RemovePlayer %s %d %s", player->name, foundTeam->frequency, target);

	Player *removePlayer;
	TeamPlayer *teamPlayer;
	Link *newLink;

	FOR_EACH(foundTeam->players, teamPlayer, newLink)
	{
		if (!strcasecmp(target, teamPlayer->name))
		{
			chat->SendArenaMessage(player->arena, "Player %s removed from team %s by %s", teamPlayer->name, foundTeam->teamName, player->name);

			if (teamPlayer->ship != SHIP_SPEC)
				foundTeam->playersInGame--;

			if (FindPlayerInArenaExact(lm, pd, player->arena, teamPlayer->name, &removePlayer))
			{
				if (teamPlayer->wasLoaded)
					game->SetShipAndFreq(removePlayer, SHIP_SPEC, foundTeam->frequency);
				else
					game->SetShipAndFreq(removePlayer, SHIP_SPEC, player->arena->specfreq);
			}

			if (!teamPlayer->wasLoaded)
			{
				if ((signups) && (arenaData->activeEvent) && (arenaData->repopulateSignups))
				{
					signups->AddPlayer(NULL, teamPlayer->name, arenaData->activeEvent, NULL, NULL);
				}
			}

			if (!teamPlayer->wasLoaded)
			{
				LLRemove(foundTeam->players, teamPlayer);

				if ((hasDB) && (db->GetStatus()) && (arenaData->saveTeams))
				{
					db->Query(NULL, NULL, 0, DEL_TEAM_PLAYER_QUERY, foundTeam->teamName, teamPlayer->name);
				}

				afree(teamPlayer->name);
				afree(teamPlayer);
				foundTeam->pickedCount--;
			}
			else
				teamPlayer->ship = SHIP_SPEC;

			if (arenaData->pickingStage == PICKING_COMPLETED)
				NextPick(arenaData, player, TRUE, FALSE);

			return;
		}
	}

	chat->SendMessage(player, "Player not found matching %s on team %s", target, foundTeam->teamName);
}


/********************************************************
 *			CAPTAINS			*
 ********************************************************/
local helptext_t captains_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"List the team captains.";

local void CCaptains(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> List Captains");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	//if (!arenaData->teams)
	//{
	//	chat->SendMessage(player, "No teams setup");
	//	return;
	//}

	bool hasTeams = FALSE;
	Link *link;
	Team *team;
        FOR_EACH(arenaData->teams, team, link)
	{
		hasTeams = TRUE;
		chat->SendMessage(player, "Team %s [%d] - Captain: %s", team->teamName, team->frequency, team->captain);
	}
	if (!hasTeams)
		chat->SendMessage(player, "No teams setup");
}
/********************************************************
 *			TEAMS				*
 ********************************************************/
local helptext_t teams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"List the teams.";

const char * ShipDescriptions[] = {
	"warbird",
	"javelin",
	"spider",
	"leviathon",
	"terrier",
	"weasel",
	"lancaster",
	"shark",
	"spectator"
};

const char * ShipDescriptionsShort[] = {
	"WB",
	"JAV",
	"SPID",
	"LEV",
	"TERR",
	"WSL",
	"LANC",
	"SHRK",
	"SPEC"
};

local void CTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> List Teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	bool hasTeams = FALSE;
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		hasTeams = TRUE;
		chat->SendMessage(player, "Team %s [%d] - Captain: %s", team->teamName, team->frequency, team->captain);

		TeamPlayer *teamPlayer;
		Link *newLink;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			if ((teamPlayer->laggedOut) && (teamPlayer->ship != SHIP_SPEC))
				chat->SendMessage(player, "+ %-24s [%s] (Lagged Out)", teamPlayer->name, ShipDescriptions[teamPlayer->ship]);
			else
				chat->SendMessage(player, "+ %-24s [%s]", teamPlayer->name, ShipDescriptions[teamPlayer->ship]);
		}
	}
	if (!hasTeams)
		chat->SendMessage(player, "No teams setup.");
}

/********************************************************
 *			FREEZE TEAMS			*
 ********************************************************/
local helptext_t freezeteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Freeze selection of players by captains.";

local void CFreezeTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Freeze Teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_PICKING)
	{
		chat->SendMessage(player, "Picking is not currently taking place.");
		return;
	}

	arenaData->pickingStage = PICKING_PAUSED;
	chat->SendArenaMessage(player->arena, "Team Picking Frozen by %s", player->name);
}

/********************************************************
 *			UNFREEZE TEAMS			*
 ********************************************************/
local helptext_t unfreezeteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"UnFreeze selection of players by captains.";

local void CUnFreezeTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> UnFreeze Teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_PAUSED)
	{
		chat->SendMessage(player, "Team choosing is not currently paused.");
		return;
	}

	arenaData->pickingStage = PICKING_PICKING;
	chat->SendArenaMessage(player->arena, "Team Picking Restarted by %s", player->name);
}

/********************************************************
 *			NEW TEAMS			*
 ********************************************************/
local helptext_t newteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Reset all teams.";

local void CNewTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> New Teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage == PICKING_GAMESTART)
	{
		chat->SendMessage(player, "Game has started. Cannot create new teams until game is ended.");
		return;
	}

	ResetTeams(arenaData, player->arena);

	balls->StopGame(player->arena);
	pbstats->ResetStats(player->arena);

	if (strstr(params, "-s"))
	{
		if (hasDB)
		{
			arenaData->saveTeams = TRUE;
			chat->SendMessage(player, "Saving teams to database.");
		}
		else
			chat->SendMessage(player, "No DB connection available. Cannot Save teams.");
	}

	chat->SendArenaMessage(player->arena, "Picking of New Teams initiated by %s", player->name);
}

/********************************************************
 *			SAVE TEAMS			*
 ********************************************************/
local helptext_t saveteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or ON or OFF\n"
"Displays or Sets the saving of teams option";

local void SaveCurrentTeams(TeamsArenaData *arenaData, Player *player);
local void CancelCurrentTeams(TeamsArenaData *arenaData, Player *player);
local void ExistingTeamsCheckCB(int status, db_res *res, void *clos);

typedef struct CheckTeamsStruct
{
	char *teamList;
	Player *player;
	TeamsArenaData *arenaData;

} CheckTeamsStruct;


local void CSaveTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Save teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Save Teams currently set as %s", arenaData->saveTeams ? "ON" : "OFF");
		return;
	}

	if (!strncasecmp("ON",params,2))
	{
		if (arenaData->saveTeams)
		{
			chat->SendMessage(player, "Save teams is already set to ON");
			return;
		}
		if ((hasDB) && (db->GetStatus()))
		{
			SaveCurrentTeams(arenaData, player);
		}
		else
		{
			arenaData->saveTeams = FALSE;
			chat->SendMessage(player, "Database connection not available. Save teams currently set as OFF");
		}
	}
	else if (!strncasecmp("OFF",params,3))
	{
		if (!arenaData->saveTeams)
		{
			chat->SendMessage(player, "Save teams is already set to OFF");
			return;
		}
		if ((hasDB) && (db->GetStatus()))
			CancelCurrentTeams(arenaData, player);

		arenaData->saveTeams = FALSE;
		chat->SendMessage(player, "Save Teams currently set as OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

local void CancelCurrentTeams(TeamsArenaData *arenaData, Player *player)
{
	if ((!arenaData->teams) || (LLIsEmpty(arenaData->teams)))
		return;

	Link *link;
	Team *team;

	FOR_EACH(arenaData->teams, team, link)
	{
		db->Query(NULL, NULL, 0, DEL_TEAM_QUERY, team->teamName);
	}
}

local void SaveCurrentTeams(TeamsArenaData *arenaData, Player *player)
{
	lm->Log(L_INFO, "<teams> Save Current Teams");

	Link *link;
	Team *team;

	bool first=TRUE;
	unsigned int curSize = 250;

	if ((!arenaData->teams) || (LLIsEmpty(arenaData->teams)))
	{
		arenaData->saveTeams = TRUE;
		chat->SendMessage(player, "Save Teams currently set as ON");
		return;
	}

	CheckTeamsStruct *checkTeamsStruct	= amalloc(sizeof(CheckTeamsStruct));
	checkTeamsStruct->teamList		= amalloc(curSize);
	checkTeamsStruct->player		= player;
	checkTeamsStruct->arenaData		= arenaData;

	FOR_EACH(arenaData->teams, team, link)
	{
		int teamNameLen = strlen(team->teamName);
		if ((strlen(checkTeamsStruct->teamList) + teamNameLen + 1 + 2) > curSize)
		{
			if (teamNameLen < 100)
				curSize += 100;
			else
				curSize += teamNameLen + 100;
			
			checkTeamsStruct->teamList = arealloc(checkTeamsStruct->teamList, curSize);
		}
		if (first)
		{
			sprintf(checkTeamsStruct->teamList, "%s", team->teamName);
			first = FALSE;
		}
		else
			sprintf(checkTeamsStruct->teamList, "%s, %s", checkTeamsStruct->teamList, team->teamName);
	}

	player->flags.during_query = 1;

	db->Query(ExistingTeamsCheckCB, checkTeamsStruct, 1, CHECK_TEAMS_QUERY, checkTeamsStruct->teamList);
}

local void ExistingTeamsCheckCB(int status, db_res *res, void *clos)
{
	db_row *row;

	CheckTeamsStruct *checkTeamsStruct = (CheckTeamsStruct *)clos;
	afree(checkTeamsStruct->teamList);

	TeamsArenaData *arenaData = checkTeamsStruct->arenaData;

	if (!checkTeamsStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Existing Teams Check player didn't request query. %s", checkTeamsStruct->player->name);
		afree(checkTeamsStruct);
		return;
	}

	checkTeamsStruct->player->flags.during_query = 0;

	int count = db->GetRowCount(res);
	if (count)
	{
		while ((row = db->GetRow(res)))
		{
			chat->SendMessage(checkTeamsStruct->player, "%2s.%s  already exists.", db->GetField(row, 0), db->GetField(row, 1));
		}
		chat->SendMessage(checkTeamsStruct->player, "Cannot save teams until the new team names are unique.");
		arenaData->saveTeams = FALSE;

		afree(checkTeamsStruct);
		return;
	}

	arenaData->saveTeams = TRUE;
	chat->SendMessage(checkTeamsStruct->player, "Save Teams currently set as ON and current teams saved.");

	afree(checkTeamsStruct);

	Link *link;
	Team *team;

	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->captain)
			db->Query(NULL, NULL, 0, ADD_TEAM_QUERY, team->teamName, team->captain);
		else
			db->Query(NULL, NULL, 0, ADD_TEAM_QUERY, team->teamName, "");
		Link *newLink;
		TeamPlayer *teamPlayer;
		FOR_EACH(team->players, teamPlayer, newLink)
		{
			db->Query(NULL, NULL, 0, ADD_TEAM_PLAYER_QUERY, teamPlayer->name , team->teamName);
		}
	}
}

/********************************************************
 *			READY				*
 ********************************************************/
local helptext_t ready_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or freq\n"
"Toggle ready selection for freq.";

local void SetReady(TeamsArenaData *arenaData, Player *captain, Team *foundTeam);

local void CReady(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Team Ready");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Team *foundTeam;

	if (IsCaptain(arenaData, player))
	{
		if (arenaData->pickingStage == PICKING_COMPLETED)
		{
			chat->SendMessage(player, "Both teams have readied, Cannot un-ready.");
			return;
		}
		else if (arenaData->pickingStage != PICKING_PICKING)
		{
			chat->SendMessage(player, "Not currently in Picking portion of teams. Cannot ready.");
			return;
		}

		foundTeam = GetCaptainTeam(arenaData, player);
		if (!foundTeam)
		{
			chat->SendMessage(player, "Cannot find your team.");
			return;
		}

		SetReady(arenaData, player, foundTeam);
		return;
	}
	if (capman->HasCapability(player, "cmd_forceready"))
	{
		if (arenaData->pickingStage == PICKING_COMPLETED)
		{
			chat->SendMessage(player, "Both teams have readied, Cannot un-ready.");
			return;
		}
		else if (arenaData->pickingStage != PICKING_PICKING)
		{
			chat->SendMessage(player, "Not currently in Picking portion of teams. Cannot ready.");
			return;
		}

		if (!*params)
		{
			chat->SendMessage(player, "No frequency or Team name specified");
			return;
		}

		if (!FindTeam(arenaData, player, params, &foundTeam))
			return;

		SetReady(arenaData, player, foundTeam);
		return;
	}

	chat->SendMessage(player, "Only captains or staff may ready for a team.");
}

local void CheckReady(Arena *arena, TeamsArenaData *arenaData, Player *player)
{
	lm->Log(L_INFO, "<teams> CheckReady");

	//if (!arenaData->teams)
	//{
	//	chat->SendMessage(player, "No teams setup.");
	//	return;
	//}

	bool ready = TRUE;
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		ready &= team->ready;
	}

	if (!ready)
	{
		if (IsCurrentPick(player, arenaData))
		{
			NextPick(arenaData, player, TRUE, FALSE);
		}
	}
	else
	{
		chat->SendArenaSoundMessage(arena, 1, "All teams are ready to begin!");
		if (!arenaData->isDraft)
		{
			//chat->SendArenaSoundMessage(arena, 1, "Game Starting soon ... ");
			DO_CBS(CB_TEAMSREADY, arena, TeamsReadyFunc, (arena));

			//balls->StartGame(arena);
			arenaData->pickingStage = PICKING_GAMESTART;
		}
		else
			arenaData->pickingStage = PICKING_COMPLETED;
	}
}

local void SetReady(TeamsArenaData *arenaData, Player *captain, Team *foundTeam)
{
	lm->Log(L_INFO, "<teams> SetReady %s", captain->name);

	foundTeam->ready = !foundTeam->ready;
	chat->SendArenaMessage(captain->arena, "Team %s ready status set to %s by %s", foundTeam->teamName, foundTeam->ready ? "READY":"NOT READY", captain->name);

	CheckReady(captain->arena, arenaData, captain);
}


/********************************************************
 *			TEAM MAX			*
 ********************************************************/
local helptext_t teammax_help =
"Module: teams\n"
"Targets: none\n"
"Args: maximum players\n"
"Set the maximum number of players per team.";

local void CTeamMax(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Team Maximum");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		if (arenaData->teamMax == INT_MAX)
			chat->SendMessage(player, "Current team maximum not set.");
		else
			chat->SendMessage(player, "Current team maximum is %d.", arenaData->teamMax);
		return;
	}

	int max = satoi(params);

	if (!max)
	{
		chat->SendMessage(player, "Invalid number of players entered");
		return;
	}

	if (max < 0)
	{
		arenaData->teamMax = INT_MAX;
		chat->SendMessage(player, "Team Maximum removed!");
		return;
	}

	arenaData->teamMax = max;
	chat->SendMessage(player, "Team Maximum set to %d!", max);
}

/********************************************************
 *			TEAM IN GAME MAX		*
 ********************************************************/
local helptext_t teamingamemax_help =
"Module: teams\n"
"Targets: none\n"
"Args: maximum players\n"
"Set the maximum number of players allowed in game per team.";

local void CTeamInGameMax(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Team In Game Maximum");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		if (arenaData->teamInGameMax == INT_MAX)
			chat->SendMessage(player, "Current team maximum not set.");
		else
			chat->SendMessage(player, "Current team maximum is %d.", arenaData->teamInGameMax);
		return;
	}

	int max = satoi(params);

	if (!max)
	{
		chat->SendMessage(player, "Invalid number of players entered");
		return;
	}

	if (max < 0)
	{
		arenaData->teamInGameMax = INT_MAX;
		chat->SendMessage(player, "Team In Game Maximum removed!");
		return;
	}

	arenaData->teamInGameMax = max;
	chat->SendMessage(player, "Team In Game Maximum set to %d!", max);
}

/********************************************************
 *			DRAFT MODE			*
 ********************************************************/
local helptext_t draftmode_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or ON or OFF\n"
"Displays or Sets the draft mode option";

local void CDraftMode(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Signup");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Draft Mode currently set as %s", arenaData->isDraft ? "ON" : "OFF");
		return;
	}

	if (strncasecmp("ON",params,2) == 0)
	{
		arenaData->isDraft = TRUE;
		chat->SendMessage(player, "Draft Mode currently set as ON");
	}
	else if (strncasecmp("OFF",params,3) == 0)
	{
		arenaData->isDraft = FALSE;
		chat->SendMessage(player, "Draft Mode currently set as OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

/********************************************************
 *			USE SIGN UPS			*
 ********************************************************/
local helptext_t usesignups_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or ON or OFF\n"
"Displays or Sets the use signups option";

typedef struct IsEventCheckStruct
{
	TeamsArenaData *	arenaData;
	char *			eventName;

} IsEventCheck;


local void IsEventCheckCallback(Player *player, bool isEvent, void * data);

local void CUseSignUps(const char *command, const char *params, Player *player, const Target *target)
{
	if (!signups)
	{
		chat->SendMessage(player, "Cannot access signup module thus cannot use signups.");
		return;
	}

	lm->Log(L_INFO, "<teams> UseSignUps");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		if (!arenaData->activeEvent)
			chat->SendMessage(player, "Not currently using event signup lists.");
		else
			chat->SendMessage(player, "Currently using signup up lists from event %s", arenaData->activeEvent);
		return;
	}

	if ((strlen(params)==3) && (strcasecmp("OFF",params) == 0))
	{
		if (arenaData->activeEvent)
			afree(arenaData->activeEvent);
		arenaData->activeEvent = NULL;
		chat->SendMessage(player, "Using event signup list disabled.");
		return;
	}

	IsEventCheck *isEventCheck	= amalloc(sizeof(IsEventCheck));
	isEventCheck->arenaData		= arenaData;
	int len				= strlen(params) + 1;
	isEventCheck->eventName		= amalloc(len);
	strncpy(isEventCheck->eventName, params, len-1);

	void (*IsEventPtr)(Player *, bool, void *) = &IsEventCheckCallback;

	signups->IsValidEvent(player, params, (void *)isEventCheck, IsEventPtr);
}

local void IsEventCheckCallback(Player *player, bool isEvent, void * data)
{
	IsEventCheck *isEventCheck = (IsEventCheck *)data;
	if (!isEvent)
	{
		chat->SendMessage(player, "%s is not a valid sign up event.", isEventCheck->eventName);
		afree(isEventCheck->eventName);
		afree(isEventCheck);
		return;
	}

	if (isEventCheck->arenaData->activeEvent)
		afree(isEventCheck->arenaData->activeEvent);

	isEventCheck->arenaData->activeEvent	= isEventCheck->eventName;

	chat->SendMessage(player, "Active event set to %s", isEventCheck->arenaData->activeEvent);

	//memory being used by arenaData->activeEvent
	//afree(signUpCheckStruct->eventName);
	afree(isEventCheck);
}

/********************************************************
 *			LAGOUT				*
 ********************************************************/
local helptext_t lagout_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Allows a picked player to re-enter the game if they've lagged out";

local void CLagOut(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Lag Out");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (target->type == T_PLAYER)
	{
		if (!capman->HasCapability(player, "cmd_forcelagout"))
		{
			chat->SendMessage(player, "You cannot lagout a player, only yourself.");
			return;
		}

		//frequency = PickedFrequency(arenaData, target->u.p);
		//if (frequency == -1)
		//{
		//	chat->SendMessage(player, "No team found for %s", target->u.p->name);
		//	return;
		//}

		TeamPlayer *teamPlayer	= NULL;
		int teamFreq		= -1;
		if (!FindPlayerInTeamExact(arenaData, target->u.p->name, &teamPlayer, &teamFreq))
		{
			chat->SendMessage(player, "%s is not in a team.", target->u.p->name);
			return;
		}

		if (arenaData->isDraft)
			game->SetShipAndFreq(target->u.p, SHIP_SPEC, teamFreq);
		else
			game->SetShipAndFreq(target->u.p, teamPlayer->ship, teamFreq);
	}
	else
	{
		if (!*params)
		{
			//use this player
			TeamPlayer *teamPlayer	= NULL;
			int teamFreq		= -1;
			if (!FindPlayerInTeamExact(arenaData, player->name, &teamPlayer, &teamFreq))
			{
				chat->SendMessage(player, "%s is not in a team.", player->name);
				return;
			}

			if (arenaData->isDraft)
				game->SetShipAndFreq(player, SHIP_SPEC, teamFreq);
			else
				game->SetShipAndFreq(player, teamPlayer->ship, teamFreq);

			return;
		}

		bool isStaff	= capman->HasCapability(player, "cmd_forcelagout");
		if (!isStaff)
		{
			chat->SendMessage(player, "You cannot lagout a player, only yourself.");
			return;
		}

		TeamPlayer *teamPlayer	= NULL;
		int teamFreq		= -1;
		int count = FindPlayerInTeamFuzzy(arenaData, params, &teamPlayer, &teamFreq);
		if (count != 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, params);
			return;
		}

		Player *targetPlayer = NULL;
		if (!FindPlayerInArenaExact(lm, pd, player->arena, teamPlayer->name, &targetPlayer))
		{
			chat->SendMessage(player, "%s is not in the arena.", teamPlayer->name);
			return;
		}

		if (arenaData->isDraft)
			game->SetShipAndFreq(targetPlayer, SHIP_SPEC, teamFreq);
		else
			game->SetShipAndFreq(targetPlayer, teamPlayer->ship, teamFreq);
	}
}

/********************************************************
 *			TEAM FREQ			*
 ********************************************************/
local helptext_t teamfreq_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Allows a picked player to join their freq in spec";

local void CTeamFreq(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Team Freq");
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	int frequency;
	if (target->type == T_PLAYER)
	{
		if (!capman->HasCapability(player, "cmd_forceteamfreq"))
		{
			chat->SendMessage(player, "You cannot set a player to their team frequency, only yourself.");
			return;
		}
		frequency = PickedFrequency(arenaData, target->u.p);
		if (frequency == -1)
		{
			chat->SendMessage(player, "No team found for %s", target->u.p->name);
			return;
		}

		game->SetShipAndFreq(target->u.p, SHIP_SPEC, frequency);
	}
	else
	{
		if ((*params) && (capman->HasCapability(player, "cmd_forceteamfreq")))
		{
			Player *targetPlayer;

			int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, params, &targetPlayer);
			if (count > 1)
			{
				chat->SendMessage(player, "Found %d players matching to %s", count, params);
				return;
			}
			if (count < 1)
			{
				chat->SendMessage(player, "Found %d players matching to %s", count, params);
				return;
			}
			frequency = PickedFrequency(arenaData, targetPlayer);
			if (frequency == -1)
			{
				chat->SendMessage(player, "No team found for %s", targetPlayer->name);
				return;
			}

			game->SetShipAndFreq(targetPlayer, SHIP_SPEC, frequency);
			return;
		}

		frequency = PickedFrequency(arenaData, player);
		if (frequency == -1)
		{
			chat->SendMessage(player, "No team found for %s", player->name);
			return;
		}

		game->SetShipAndFreq(player, SHIP_SPEC, frequency);
	}
}

/********************************************************
 *			SUBSTITUTE			*
 ********************************************************/
local helptext_t substitute_help =
"Module: teams\n"
"Targets: none or player\n"
"Args: in game player:spec player or player\n"
"Substitute an in-game player with another team mate from spec.";

local void ProcessSubPlayers(	TeamsArenaData *arenaData, Player *player, bool isCaptain, bool isStaff, Team *captainsTeam,
								bool sub1Found, bool sub2Found,	int player1Freq, int player2Freq,
								TeamPlayer *teamPlayer1, TeamPlayer *teamPlayer2, Player *subPlayer1, Player *subPlayer2);
local void PerformSubstitution(Player *player, TeamPlayer *inGameTeamPlayer, TeamPlayer *subTeamPlayer, Player *inGamePlayer, Player *subPlayer, int teamFreq);

local void CSubstitute(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Substitute %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	bool isStaff = capman->HasCapability(player, "cmd_forcesub");
	bool isCaptain = IsCaptain(arenaData, player);

	if ((!isStaff) && (!isCaptain))
	{
		chat->SendMessage(player, "Only captains or staff may sub a player.");
		return;
	}

	if ((arenaData->pickingStage != PICKING_COMPLETED) && (arenaData->pickingStage != PICKING_GAMESTART))
	{
		chat->SendMessage(player, "Can only substitute players after picking completed or while game is underway.");
		return;
	}
	if (arenaData->isDraft)
	{
		chat->SendMessage(player, "Cannot substitute players during draft mode.");
		return;
	}

	int				count;
	char *			targetPlayerName1;
	char *			targetPlayerName2;
	TeamPlayer *	teamPlayer1		= NULL;
	TeamPlayer *	teamPlayer2		= NULL;
	int				player1Freq		= -1;
	int				player2Freq		= -1;
	Team *			captainsTeam	= NULL;
	Player *		subPlayer1		= NULL;
	Player *		subPlayer2		= NULL;
	bool			sub1Found		= FALSE;
	bool			sub2Found		= FALSE;

	if ((isCaptain) && (!isStaff))
	{
		captainsTeam	= GetCaptainTeam(arenaData, player);
		if (!captainsTeam)
		{
			chat->SendMessage(player, "Cannot determine your team frequency.");
			return;
		}
		player1Freq = captainsTeam->frequency;
		player2Freq = captainsTeam->frequency;
	}

	if (target->type == T_PLAYER)
	{
		if (!*params)
		{
			chat->SendMessage(player, "Must specify the second player taking part in the substitution.");
			return;
		}

		subPlayer1		= target->u.p;
		sub1Found		= FindPlayerInTeamExact(arenaData, target->u.p->name, &teamPlayer1, &player1Freq);

		count = FindPlayerInTeamFuzzy(arenaData, params, &teamPlayer2, &player2Freq);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, params);
			return;
		}
		sub2Found = (count);

		count = FindPlayerInArenaFuzzy(lm, pd, player->arena, params, &subPlayer2);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, params);
			return;
		}

		ProcessSubPlayers(	arenaData, player, isCaptain, isStaff, captainsTeam,
							sub1Found, sub2Found, player1Freq, player2Freq,
							teamPlayer1, teamPlayer2, subPlayer1, subPlayer2);
		return;
	}


	if (!*params)
	{
		chat->SendMessage(player, "You must specify the 2 players as part of the substitution.");
		return;
	}

	if (!TwoSplitUserInput(':', params, &targetPlayerName1, &targetPlayerName2))
	{
		chat->SendMessage(player, "Ex: ?sub Paul:John");
		return;
	}

	count = FindPlayerInTeamFuzzy(arenaData, targetPlayerName1, &teamPlayer1, &player1Freq);
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayerName1);
		TwoSplitRelease(&targetPlayerName1, &targetPlayerName2);
		return;
	}
	sub1Found = (count);

	count = FindPlayerInTeamFuzzy(arenaData, targetPlayerName2, &teamPlayer2, &player2Freq);
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayerName2);
		TwoSplitRelease(&targetPlayerName1, &targetPlayerName2);
		return;
	}
	sub2Found = (count);

	count = FindPlayerInArenaFuzzy(lm, pd, player->arena, targetPlayerName1, &subPlayer1);
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayerName1);
		TwoSplitRelease(&targetPlayerName1, &targetPlayerName2);
		return;
	}

	count = FindPlayerInArenaFuzzy(lm, pd, player->arena, targetPlayerName2, &subPlayer2);
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, targetPlayerName2);
		TwoSplitRelease(&targetPlayerName1, &targetPlayerName2);
		return;
	}

	TwoSplitRelease(&targetPlayerName1, &targetPlayerName2);

	ProcessSubPlayers(	arenaData, player, isCaptain, isStaff, captainsTeam,
						sub1Found, sub2Found, player1Freq, player2Freq,
						teamPlayer1, teamPlayer2, subPlayer1, subPlayer2);

}

local void ProcessSubPlayers(	TeamsArenaData *arenaData, Player *player, bool isCaptain, bool isStaff, Team *captainsTeam,
								bool sub1Found, bool sub2Found,	int player1Freq, int player2Freq,
								TeamPlayer *teamPlayer1, TeamPlayer *teamPlayer2, Player *subPlayer1, Player *subPlayer2)
{
	if ((!sub1Found) && (!sub2Found))
	{
		//no team found for either player
		chat->SendMessage(player, "Neither player found in a team");
		return;
	}
	//both players found on a team
	if ((sub1Found) && (sub2Found))
	{
		if (player1Freq != player2Freq)
		{
			chat->SendMessage(player, "Players %s and %s are not on the same team.", teamPlayer1->name, teamPlayer2->name);
			return;
		}
		//same team so perform sub.
		if (teamPlayer1->ship == SHIP_SPEC)
		{
			if (teamPlayer2->ship == SHIP_SPEC)
			{
				chat->SendMessage(player, "Neither player is in game to be subbed.");
				return;
			}
			//subbing in target player for non-target player
			//so try and find non-target player, but they may not be in the arena.
			//FindPlayerInArenaExact(lm, pd, player->arena, teamPlayer2->name, &subPlayer2);
			if (!subPlayer1)
			{
				chat->SendMessage(player, "%s isn't in the arena so cannot be subbed in.", teamPlayer2->name);
				return;
			}

			PerformSubstitution(player, teamPlayer2, teamPlayer1, subPlayer2, subPlayer1, player1Freq);
		}
		else
		{
			if (teamPlayer2->ship != SHIP_SPEC)
			{
				chat->SendMessage(player, "Both players are already in game!");
				return;
			}
			//subbing target player with non-target player
			//so find non-target player and throw error if they aren't in the arena.
			//if (!FindPlayerInArenaExact(lm, pd, player->arena, teamPlayer2->name, &subPlayer2 ))
			if (!subPlayer2)
			{
				chat->SendMessage(player, "%s isn't in the arena so cannot be subbed in.", teamPlayer2->name);
				return;
			}

			PerformSubstitution(player, teamPlayer1, teamPlayer2, subPlayer1, subPlayer2, player1Freq);
		}
		return;
	}

	//working with target player being found in Team and not non-target
	if (sub1Found)
	{
		if ((captainsTeam) && (captainsTeam->wasLoaded))
		{
			chat->SendMessage(player, "Player %s is not on team %s.", teamPlayer1->name, captainsTeam->teamName);
			return;
		}

		if (teamPlayer1->ship == SHIP_SPEC)
		{
			chat->SendMessage(player, "%s isn't in game to be substituted!", teamPlayer1->name);
			return;
		}

		if (!subPlayer2)
		{
			chat->SendMessage(player, "%s isn't in the arena so cannot be subbed in.", teamPlayer2->name);
			return;
		}

		Team *team = NULL;
		if ((isCaptain) && (!isStaff))
		{
			team = captainsTeam;
		}
		else
		{
			if (!FindTeamExactPlayer(arenaData, teamPlayer1->name, &team))
			{
				chat->SendMessage(player, "Unable to find team for %s", teamPlayer1->name);
				return;
			}
		}

		teamPlayer2 = AddPlayerToList(player, subPlayer2->name, SHIP_SPEC, team, NULL, FALSE, FALSE);

		PerformSubstitution(player, teamPlayer1, teamPlayer2, subPlayer1, subPlayer2, player1Freq);
		return;
	}

	//working with non-target player being found in Team
	if (sub2Found)
	{
		if ((captainsTeam) && (captainsTeam->wasLoaded))
		{
			chat->SendMessage(player, "Player %s is not on team %s.", teamPlayer2->name, captainsTeam->teamName);
			return;
		}

		if (teamPlayer2->ship == SHIP_SPEC)
		{
			chat->SendMessage(player, "%s isn't in game to be substituted!", teamPlayer2->name);
			return;
		}

		if (!subPlayer1)
		{
			chat->SendMessage(player, "%s isn't in the arena so cannot be subbed in.", teamPlayer1->name);
			return;
		}

		Team *team = NULL;
		if ((isCaptain) && (!isStaff))
		{
			team = captainsTeam;
		}
		else
		{
			if (!FindTeamExactPlayer(arenaData, teamPlayer2->name, &team))
			{
				chat->SendMessage(player, "Unable to find team for %s", teamPlayer2->name);
				return;
			}
		}

		teamPlayer1 = AddPlayerToList(player, subPlayer1->name, SHIP_SPEC, team, NULL, FALSE, FALSE);

		PerformSubstitution(player, teamPlayer2, teamPlayer1, subPlayer2, subPlayer1, player2Freq);
		return;
	}
}

local void PerformSubstitution(Player *player, TeamPlayer *inGameTeamPlayer, TeamPlayer *subTeamPlayer, Player *inGamePlayer, Player *subPlayer, int teamFreq)
{
	subTeamPlayer->ship		= inGameTeamPlayer->ship;
	inGameTeamPlayer->ship	= SHIP_SPEC;

	game->SetShipAndFreq(subPlayer, subTeamPlayer->ship, teamFreq);
	stats->ScoreReset(subPlayer, INTERVAL_RESET);
	stats->SendUpdates(NULL);

	if (inGamePlayer)
	{
		game->SetShipAndFreq(inGamePlayer, inGameTeamPlayer->ship, teamFreq);
	}

	chat->SendArenaMessage(player->arena, "%s substituted with %s by %s", inGameTeamPlayer->name, subTeamPlayer->name, player->name);
	return;
}

/********************************************************
 *			PICK STATUS			*
 ********************************************************/
local helptext_t pickstatus_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display the current status of picking.";

local void CPickStatus(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> pick status %s", params);

	DisplayPickStatus(player);
}

local void DisplayPickStatus(Player *player)
{
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	//if (!arenaData->teams)
	//{
	//	chat->SendMessage(player, "No teams setup");
	//	return;
	//}

	switch (arenaData->pickingStage)
	{
		case PICKING_SETUP:
			chat->SendMessage(player, "Picking Stage:         Setup");
		break;
		case PICKING_PICKING:
			chat->SendMessage(player, "Picking Stage:         Picking");
		break;
		case PICKING_PAUSED:
			chat->SendMessage(player, "Picking Stage:         Paused");
		break;
		case PICKING_COMPLETED:
			chat->SendMessage(player, "Picking Stage:         Completed");
		break;
		case PICKING_GAMESTART:
			chat->SendMessage(player, "Picking Stage:         Game Started");
		break;
		case PICKING_GAMEOVER:
			chat->SendMessage(player, "Picking Stage:         Game Over");
		break;
	}
	chat->SendMessage(player, "Is draft:              %s", arenaData->isDraft ? "YES" : "NO");
	chat->SendMessage(player, "Offline drafting:      %s", arenaData->offlineDrafting ? "YES" : "NO");

	if (arenaData->activeEvent)
	{
		chat->SendMessage(player, "Active Sign up Event:  %s", arenaData->activeEvent);
		chat->SendMessage(player, "Repopulate Signups:    %s", arenaData->repopulateSignups ? "YES" : "NO");
	}
	if (arenaData->teamMax == INT_MAX)
		chat->SendMessage(player, "Team Max:              Unlimited");
	else
		chat->SendMessage(player, "Team Max:              %d", arenaData->teamMax);
	if (arenaData->teamInGameMax == INT_MAX)
		chat->SendMessage(player, "Team In Game Max:      Unlimited");
	else
		chat->SendMessage(player, "Team In Game Max:      %d", arenaData->teamInGameMax);

	chat->SendMessage(player, "Number of teams:       %d", arenaData->numberOfTeams);
	chat->SendMessage(player, "Picking Round:         %d", arenaData->pickingRound);
	chat->SendMessage(player, "Current Picking Freq:  %d", arenaData->currentPickFreq);

	DisplayPickType(player, arenaData);
	if (arenaData->pickingType == PICKINGTYPE_SNAKE)
	{
		chat->SendMessage(player, "Picking Direction:     %s", arenaData->pickDirection ? "FORWARDS" : "BACKWARDS");
	}

	Link *link;
	Team *team;
	int count = 1;
	FOR_EACH(arenaData->teams, team, link)
	{
		if (team->freqShip == -1)
			chat->SendMessage(player, "%2d. %-32s %4d %2d [%-24s] : %-9s %s", 
						count,
						team->teamName,
						team->frequency,
						team->pickedCount,
						team->captain,
						team->ready ? "READY" : "NOT READY",
						team->wasLoaded ? "(LOADED)" : "");
		else
			chat->SendMessage(player, "%2d. %-32s %4d (%-4s) %2d [%-24s] : %-9s %s",
						count,
						team->teamName,
						team->frequency,
						ShipDescriptionsShort[team->freqShip % 9],
						team->pickedCount,
						team->captain,
						team->ready ? "READY" : "NOT READY",
						team->wasLoaded ? "(LOADED)" : "");
		count++;
	}
}


/********************************************************
 *			SAVED TEAMS			*
 ********************************************************/
local helptext_t savedteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display the list of saved teams.";

local void SavedTeamsCB(int status, db_res *res, void *clos);

local void CSavedTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Saved Teams %s", params);
	//TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if ((hasDB) && (db->GetStatus()))
	{
		player->flags.during_query = 1;

		db->Query(SavedTeamsCB, player, 1, GET_TEAMS_QUERY);
	}
	else
	{
		chat->SendMessage(player, "Database connection is unavailable.");
	}
}

local void SavedTeamsCB(int status, db_res *res, void *clos)
{
	db_row *row;

	Player *player = (Player *)clos;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Saved Teams player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	int count = db->GetRowCount(res);
	if (count)
	{
		while ((row = db->GetRow(res)))
		{
			chat->SendMessage(player, "%2s.%-32s  [%s]", db->GetField(row, 0), db->GetField(row, 1), db->GetField(row, 2));
		}
	}
	else
	{
		chat->SendMessage(player, "No teams currently saved.");
	}
}

/********************************************************
 *			LIST SAVED TEAM			*
 ********************************************************/
local helptext_t listsavedteam_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Display the current status of picking.";

typedef struct SavedTeamsStruct
{
	Player *player;
	char *teamName;

} SavedTeamsStruct;

local void FindSavedTeamsCB(int status, db_res *res, void *clos);
local void ListSavedTeamCB(int status, db_res *res, void *clos);

local void CListSavedTeam(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> List Saved Team %s", params);
	//TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "You must specify a team to list the players for.");
		return;
	}

	if ((hasDB) && (db->GetStatus()))
	{
		player->flags.during_query = 1;

		SavedTeamsStruct *savedTeamsStruct	= amalloc(sizeof(SavedTeamsStruct));
		savedTeamsStruct->player		= player;
		int len					= strlen(params) + 1;
		savedTeamsStruct->teamName		= amalloc(len);
		strncpy(savedTeamsStruct->teamName, params, len-1);

		db->Query(FindSavedTeamsCB, savedTeamsStruct, 1, GET_TEAMS_FUZZY_QUERY, params);
	}
	else
	{
		chat->SendMessage(player, "Database connection is unavailable.");
	}
}

local void FindSavedTeamsCB(int status, db_res *res, void *clos)
{
	db_row *row;

	SavedTeamsStruct *savedTeamsStruct = (SavedTeamsStruct *)clos;

	if (!savedTeamsStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Saved Teams player didn't request query. %s", savedTeamsStruct->player->name);
		afree(savedTeamsStruct->teamName);
		afree(savedTeamsStruct);
		return;
	}

	if (db->GetRowCount(res))
	{
		while ((row = db->GetRow(res)))
		{
			const char *teamName = db->GetField(row, 1);
			chat->SendMessage(savedTeamsStruct->player, "Team: %2s. %-32s   Captain: %s", db->GetField(row,0), teamName, db->GetField(row, 2));
			db->Query(ListSavedTeamCB, savedTeamsStruct->player, 1, GET_TEAM_PLAYER_QUERY, teamName);
		}

		afree(savedTeamsStruct->teamName);
		afree(savedTeamsStruct);
	}
	else
	{
		chat->SendMessage(savedTeamsStruct->player, "No teams found matching %s", savedTeamsStruct->teamName);
		savedTeamsStruct->player->flags.during_query = 0;
		afree(savedTeamsStruct->teamName);
		afree(savedTeamsStruct);
	}
}

local void ListSavedTeamCB(int status, db_res *res, void *clos)
{
	db_row *row;

	Player *player = (Player *)clos;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Saved Teams player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	int count = db->GetRowCount(res);
	if (count)
	{
		while ((row = db->GetRow(res)))
		{
			chat->SendMessage(player, "%-32s", db->GetField(row, 0));
		}
	}
	else
	{
		chat->SendMessage(player, "No players found for team.");
	}
}

/********************************************************
 *			DELETE SAVED TEAMS		*
 ********************************************************/
local helptext_t delsavedteam_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Delete a saved team.";

local void DelSavedTeamCB(int status, db_res *res, void *clos);

local void CDelSavedTeam(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Del Saved Teams %s", params);
	//TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "You must specify a team to delete");
		return;
	}

	if ((hasDB) && (db->GetStatus()))
	{
		player->flags.during_query = 1;

		SavedTeamsStruct *savedTeamsStruct	= amalloc(sizeof(SavedTeamsStruct));
		savedTeamsStruct->player		= player;
		int len					= strlen(params) + 1;
		savedTeamsStruct->teamName		= amalloc(len);
		strncpy(savedTeamsStruct->teamName, params, len-1);

		db->Query(DelSavedTeamCB, savedTeamsStruct, 1, DEL_TEAM_QUERY, params);
	}
	else
	{
		chat->SendMessage(player, "Database connection is unavailable.");
	}
}

local void DelSavedTeamCB(int status, db_res *res, void *clos)
{
	SavedTeamsStruct *savedTeamsStruct = (SavedTeamsStruct *)clos;

	if (!savedTeamsStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Del Saved Teams player didn't request query. %s", savedTeamsStruct->player->name);
		afree(savedTeamsStruct->teamName);
		afree(savedTeamsStruct);
		return;
	}

	savedTeamsStruct->player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(savedTeamsStruct->player, "Unable to delete a team matching to %s.", savedTeamsStruct->teamName);
		afree(savedTeamsStruct->teamName);
		afree(savedTeamsStruct);
		return;
	}

	chat->SendMessage(savedTeamsStruct->player, "Team deleted!");

	afree(savedTeamsStruct->teamName);
	afree(savedTeamsStruct);
}

/********************************************************
 *			LOAD TEAM			*
 ********************************************************/
local helptext_t loadteam_help =
"Module: teams\n"
"Targets: none\n"
"Args: <freq>:<team name>\n"
"Load a saved team from the database.";

typedef struct LoadTeamsStruct
{
	Player		*player;
	char		*teamName;
	int		frequency;
	TeamsArenaData	*arenaData;
	Team		*team;

} LoadTeamsStruct;

local void CheckSavedTeamsCB(int status, db_res *res, void *clos);
local void LoadSavedTeamPlayersCB(int status, db_res *res, void *clos);
local void LoadSpecificTeam(LoadTeamsStruct *loadTeamsStruct, const char *teamID, const char *teamName, const char *captainName);

local void CLoadTeam(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Load Team %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	char *teamName;
	char *freqString;

	if (!*params)
	{
		chat->SendMessage(player, "You must specify a team name and frequency.");
		return;
	}

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Database currently not available.");
		return;
	}

	if (!TwoSplitUserInput(':', params, &freqString, &teamName))
	{
		chat->SendMessage(player, "You must specify a team name and frequency. ie ?loadteam 100:Best Team");
		return;
	}

	int freq = satoi(freqString);

	if (((freq == 0) && (params[0] != '0')) || (freq < 0))
	{
		chat->SendMessage(player, "No frequency specified");
		TwoSplitRelease(&freqString, &teamName);
		return;
	}
	afree(freqString);

	player->flags.during_query = 1;

	LoadTeamsStruct *loadTeamsStruct	= amalloc(sizeof(LoadTeamsStruct));
	loadTeamsStruct->player			= player;
	loadTeamsStruct->teamName		= teamName;
	loadTeamsStruct->frequency		= freq;
	loadTeamsStruct->arenaData		= arenaData;

	db->Query(CheckSavedTeamsCB, loadTeamsStruct, 1, GET_TEAMS_FUZZY_QUERY, teamName);
}

local void CheckSavedTeamsCB(int status, db_res *res, void *clos)
{
	db_row *row;

	LoadTeamsStruct *loadTeamsStruct = (LoadTeamsStruct *)clos;

	if (!loadTeamsStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Load Teams player didn't request query. %s", loadTeamsStruct->player->name);
		afree(loadTeamsStruct->teamName);
		afree(loadTeamsStruct);
		return;
	}

	loadTeamsStruct->player->flags.during_query = 0;

	int count = db->GetRowCount(res);

	if (count == 0)
	{
		chat->SendMessage(loadTeamsStruct->player, "No teams found matching to %s", loadTeamsStruct->teamName);
		afree(loadTeamsStruct->teamName);
		afree(loadTeamsStruct);
		return;
	}

	if (count == 1)
	{
		//so we have 1 team! Let's load it up
		row			= db->GetRow(res);
		const char *teamID	= db->GetField(row, 0);
		const char *teamName	= db->GetField(row, 1);
		const char *captainName	= db->GetField(row, 2);

		LoadSpecificTeam(loadTeamsStruct, teamID, teamName, captainName);
		return;
	}

	if (count > 1)
	{
		row			= db->GetRow(res);
		const char *teamID		= NULL;
		const char *exactTeamName	= NULL;
		const char *captainName		= NULL;

		bool exactMatch	= FALSE;
		bool first	= TRUE;
		char *line	= amalloc(150);
		while ((row = db->GetRow(res)))
		{
			teamID		= db->GetField(row, 0);
			exactTeamName	= db->GetField(row, 1);
			captainName	= db->GetField(row, 2);

			//exact match found
			if (!strcasecmp(exactTeamName, loadTeamsStruct->teamName))
			{
				exactMatch	= TRUE;
				break;
			}

			if ((strlen(line) + strlen(exactTeamName) + 1 + 2) > 150)
			{
				chat->SendMessage(loadTeamsStruct->player, "%s", line);
				memset(line, '\0', 150);
			}
			if (first)
			{
				sprintf(line, "%s", exactTeamName);
				first = FALSE;
			}
			else
				sprintf(line, "%s, %s", line, exactTeamName);
		}

		if (!exactMatch)
		{
			chat->SendMessage(loadTeamsStruct->player, "Found %d teams matching to %s", count, loadTeamsStruct->teamName);
			chat->SendMessage(loadTeamsStruct->player, "%s", line);

			afree(line);
			afree(loadTeamsStruct->teamName);
			afree(loadTeamsStruct);
			return;
		}

		afree(line);

		LoadSpecificTeam(loadTeamsStruct, teamID, exactTeamName, captainName);
	}
}

local void LoadSpecificTeam(LoadTeamsStruct *loadTeamsStruct, const char *teamID, const char *teamName, const char *captainName)
{
	chat->SendMessage(loadTeamsStruct->player, "Team: %2s. %-32s   Captain: %s", teamID, teamName, captainName);

	int len				= strlen(teamName) + 1;
	char *teamNameParam		= amalloc(len);
	strncpy(teamNameParam, teamName, len-1);

	Team *team			= AddTeam(loadTeamsStruct->arenaData, loadTeamsStruct->player, loadTeamsStruct->frequency, teamNameParam, TRUE);
	if (!team)
	{
		afree(loadTeamsStruct->teamName);
		afree(loadTeamsStruct);
		return;
	}
	loadTeamsStruct->team		= team;

	len				= strlen(captainName) + 1;
	loadTeamsStruct->team->captain	= amalloc(len);
	strncpy(loadTeamsStruct->team->captain, captainName, len-1);

	loadTeamsStruct->player->flags.during_query = 1;

	db->Query(LoadSavedTeamPlayersCB, loadTeamsStruct, 1, GET_TEAM_PLAYER_QUERY, teamName);
}

local void LoadSavedTeamPlayersCB(int status, db_res *res, void *clos)
{
	db_row *row;

	LoadTeamsStruct *loadTeamsStruct = (LoadTeamsStruct *)clos;

	if (!loadTeamsStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<teams> Load Saved Teams player didn't request query. %s", loadTeamsStruct->player->name);
		afree(loadTeamsStruct->teamName);
		afree(loadTeamsStruct);
		return;
	}

	loadTeamsStruct->player->flags.during_query = 0;

	int count = db->GetRowCount(res);
	if (count)
	{
		while ((row = db->GetRow(res)))
		{
			const char *playerName = db->GetField(row, 0);
			Player *teamPlayer;

			if (FindPlayerInArenaExact(lm, pd, loadTeamsStruct->player->arena, playerName, &teamPlayer))
			{
				chat->SendMessage(loadTeamsStruct->player, "Added %32s", playerName);
				game->SetShipAndFreq(teamPlayer, SHIP_SPEC, loadTeamsStruct->team->frequency);
			}
			else
				chat->SendMessage(loadTeamsStruct->player, "%s not available in arena.", playerName);

			AddPlayerToList(loadTeamsStruct->player, playerName, SHIP_SPEC, loadTeamsStruct->team, loadTeamsStruct->arenaData->activeEvent, TRUE, FALSE);
		}
		//players are loaded into the team but not actually into the game yet.
		loadTeamsStruct->team->pickedCount = 0;
	}
	else
	{
		chat->SendMessage(loadTeamsStruct->player, "No players found for team.");
	}

	afree(loadTeamsStruct->teamName);
	afree(loadTeamsStruct);
}

/********************************************************
 *			BORROWED FUNCTIONS		*
 ********************************************************/
local Team * GetBorrowedTeam(TeamsArenaData *arenaData, char *targetName)
{
	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		Link *borrowLink;
		BorrowedPlayer *borrowedPlayer;
		FOR_EACH(team->borrowList, borrowedPlayer, borrowLink)
		{
			if (!strcasecmp(borrowedPlayer->name, targetName))
				return team;
		}
	}
	return NULL;
}

local int FindBorrowedPlayerInTeam(Team *team, const char *partialName, BorrowedPlayer **borrowedPlayer)
{
	Link *borrowLink;
	BorrowedPlayer *potentialBorrow;

	int count		= 0;
	*borrowedPlayer		= NULL;
	unsigned int len	= strlen(partialName);
	FOR_EACH(team->borrowList, potentialBorrow, borrowLink)
	{
		if (!strncasecmp(potentialBorrow->name, partialName, len))
		{
			if (len == strlen(potentialBorrow->name))
			{
				*borrowedPlayer	= potentialBorrow;
				return 1;
			}

			if (!*borrowedPlayer)
				*borrowedPlayer = potentialBorrow;
			count++;
		}
	}
	return count;
}

local int FindBorrowedPlayerInTeams(TeamsArenaData *arenaData, const char *partialName, BorrowedPlayer **borrowedPlayer, Team **borrowedTeam)
{
	int count		= 0;
	*borrowedPlayer		= NULL;
	*borrowedTeam		= NULL;
	unsigned int len	= strlen(partialName);

	Link *link;
	Team *team;
	FOR_EACH(arenaData->teams, team, link)
	{
		Link *borrowLink = NULL;
		BorrowedPlayer *potentialBorrow = NULL;

		FOR_EACH(team->borrowList, potentialBorrow, borrowLink)
		{
			if (!strncasecmp(potentialBorrow->name, partialName, len))
			{
				if (len == strlen(potentialBorrow->name))
				{
					*borrowedPlayer	= potentialBorrow;
					*borrowedTeam	= team;
					return 1;
				}

				if (!*borrowedPlayer)
				{
					*borrowedPlayer = potentialBorrow;
					*borrowedTeam	= team;
				}
				count++;
			}
		}
	}
	return count;
}

/********************************************************
 *			BORROW				*
 ********************************************************/
local helptext_t borrow_help =
"Module: teams\n"
"Targets: none\n"
"Args: <player name>\n"
"Request to borrow <player>";

local BorrowedPlayer * AddBorrow(Team *team, Player *targetPlayer);

local void CBorrow(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Borrow %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Team *team = GetCaptainTeam(arenaData, player);
	if (!team)
	{
		chat->SendMessage(player, "Only captains may request to borrow players.");
		return;
	}

	if (!team->wasLoaded)
	{
		chat->SendMessage(player, "Borrowing only applies to pre-defined teams.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "In order to request a borrow, you must provide the player name.");
		return;
	}

	Player *targetPlayer = NULL;
	int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, params, &targetPlayer);

	if ((count > 1) || (count < 1))
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, params);
		return;
	}
	int frequency = PickedFrequency(arenaData, targetPlayer);
	if (frequency != -1)
	{
		chat->SendMessage(player, "Player is already on a team.");
		return;
	}

	Team *borrowedTeam = GetBorrowedTeam(arenaData, targetPlayer->name);
	if (borrowedTeam)
	{
		chat->SendMessage(player, "%s is already on the borrow list for %s", targetPlayer->name, borrowedTeam->teamName);
		return;
	}

	AddBorrow(team, targetPlayer);

	chat->SendArenaSoundMessage(player->arena, 2, "%s is requesting to borrow %s for this game.", team->teamName, targetPlayer->name);
}

local BorrowedPlayer * AddBorrow(Team *team, Player *targetPlayer)
{
	BorrowedPlayer *borrowedPlayer	= amalloc(sizeof(BorrowedPlayer));
	borrowedPlayer->approved	= FALSE;
	borrowedPlayer->approvedBy	= NULL;
	int len	= strlen(targetPlayer->name) + 1;
	borrowedPlayer->name		= amalloc(len);
	strncpy(borrowedPlayer->name, targetPlayer->name, len-1);

	LLAdd(team->borrowList, borrowedPlayer);

	return borrowedPlayer;
}

/********************************************************
 *			UNBORROW			*
 ********************************************************/
local helptext_t unborrow_help =
"Module: teams\n"
"Targets: none\n"
"Args: <player name>\n"
"Remove request to borrow <player>";

local void RemoveBorrow(Team *team, BorrowedPlayer *borrowedPlayer);

local void CUnBorrow(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Unborrow %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Team *team = GetCaptainTeam(arenaData, player);
	if (!team)
	{
		chat->SendMessage(player, "Only captains may unborrow players.");
		return;
	}

	if (!team->wasLoaded)
	{
		chat->SendMessage(player, "Borrowing only applies to pre-defined teams.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "In order to unborrow someone, you must provide the player name.");
		return;
	}

	BorrowedPlayer *borrowedPlayer;
	int count = FindBorrowedPlayerInTeam(team, params, &borrowedPlayer);
	if ((count < 1) || (count > 1))
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, params);
		return;
	}

	chat->SendArenaSoundMessage(player->arena, 2, "%s removed request to borrow %s", team->teamName, borrowedPlayer->name);

	RemoveBorrow(team, borrowedPlayer);
}

local void RemoveBorrow(Team *team, BorrowedPlayer *borrowedPlayer)
{
	afree(borrowedPlayer->name);
	if (borrowedPlayer->approved)
		afree(borrowedPlayer->approvedBy);
	
	LLRemove(team->borrowList, borrowedPlayer);
}

/********************************************************
 *			APPROVE				*
 ********************************************************/
local helptext_t approve_help =
"Module: teams\n"
"Targets: none\n"
"Args: <freq>:<team name>\n"
"Load a saved team from the database.";

local void ApproveBorrowedPlayer(TeamsArenaData *arenaData, Player *player, BorrowedPlayer *borrowedPlayer, Team *team);

local void CApprove(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Approve %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Team *team = GetCaptainTeam(arenaData, player);
	if (!team)
	{
		chat->SendMessage(player, "Only captains may approve borrows.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "In order to approve someone, you must provide the player name.");
		return;
	}

	BorrowedPlayer *borrowedPlayer = NULL;
	Team *borrowedTeam = NULL;
	int count = FindBorrowedPlayerInTeams(arenaData, params, &borrowedPlayer, &borrowedTeam);
	if ((count < 1) || (count > 1))
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, params);
		return;
	}

	if (borrowedTeam == team)
	{
		chat->SendMessage(player, "You cannot approve your own borrow requests");
		return;
	}

	ApproveBorrowedPlayer(arenaData, player, borrowedPlayer, borrowedTeam);
}

local void ApproveBorrowedPlayer(TeamsArenaData *arenaData, Player *player, BorrowedPlayer *borrowedPlayer, Team *team)
{
	lm->Log(L_INFO, "<teams> Approve Borrow %s", borrowedPlayer->name);

	int len	= strlen(player->name) + 1;
	borrowedPlayer->approvedBy	= amalloc(len);
	strncpy(borrowedPlayer->approvedBy, player->name, len-1);
	borrowedPlayer->approved	= TRUE;

	chat->SendArenaSoundMessage(player->arena, 2, "%s approved for being borrowed by %s.", borrowedPlayer->name, player->name);

	//Add player
	Player *targetPlayer = NULL;
	if (FindPlayerInArenaExact(lm, pd, player->arena, borrowedPlayer->name, &targetPlayer))
	{
		//chat->SendMessage(loadTeamsStruct->player, "Added %32s", playerName);
		game->SetShipAndFreq(targetPlayer, SHIP_SPEC, team->frequency);
	}

	AddPlayerToList(player, borrowedPlayer->name, SHIP_SPEC, team, arenaData->activeEvent, FALSE, TRUE);
}

/********************************************************
 *			ADD BORROW			*
 ********************************************************/
local helptext_t addborrow_help =
"Module: teams\n"
"Targets: none\n"
"Args: <freq>:<player>\n"
"Add borrow <player> to <freq> / <team>";

local void CAddBorrow(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Add Borrow %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	Player *targetPlayer = NULL;
	Team *foundTeam;

	if (target->type == T_PLAYER)
	{
		//we have the player
		targetPlayer = target->u.p;

		if (!*params)
		{
			chat->SendMessage(player, "No frequency or team name specified");
			return;
		}

		if (!FindTeam(arenaData, player, params, &foundTeam))
			return;
	}
	else
	{
		char *teamString;
		char *playerString;

		if (!TwoSplitUserInput(':', params, &teamString, &playerString))
		{
			lm->Log(L_INFO, "<teams> Add Captain FALSE");
			chat->SendMessage(player, "You must specify the team name or frequency and the player to borrow");
			return;
		}

		if (!FindTeam(arenaData, player, teamString, &foundTeam))
		{
			TwoSplitRelease(&teamString, &playerString);
			return;
		}
		afree(teamString);

		//find player
		int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, playerString, &targetPlayer);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, playerString);
			afree(playerString);
			return;
		}
		if (count < 1)
		{
			chat->SendMessage(player, "Found %d players matching to %s", count, playerString);
			afree(playerString);
			return;
		}
		afree(playerString);
	}

	//ok so we have a player and freq
	BorrowedPlayer *borrowedPlayer = AddBorrow(foundTeam, targetPlayer);

	ApproveBorrowedPlayer(arenaData, player, borrowedPlayer, foundTeam);
}

/********************************************************
 *			LIST BORROWS			*
 ********************************************************/
local helptext_t listborrows_help =
"Module: teams\n"
"Targets: none\n"
"Args: <freq>:<team name>\n"
"Load a saved team from the database.";

local bool DisplayTeamBorrows(TeamsArenaData *arenaData, Player *player, Team *team);

local void CListBorrows(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> List Borrows %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	bool found = FALSE;

	if (!*params)
	{
		Link *link;
		Team *team;
		FOR_EACH(arenaData->teams, team, link)
		{
			found = found || DisplayTeamBorrows(arenaData, player, team);
		}
	}
	else
	{
		Team *team = NULL;
		int count = FindTeamFuzzy(arenaData, params, &team);
		if (count > 1)
		{
			chat->SendMessage(player, "Found %d teams matching %s", count, params);
			return;
		}
		if (count < 1)
		{
			chat->SendMessage(player, "No team found matching %s", params);
			return;
		}

		found = DisplayTeamBorrows(arenaData, player, team);
	}
	if (!found)
		chat->SendMessage(player, "There are no borrows currently.");
}

local bool DisplayTeamBorrows(TeamsArenaData *arenaData, Player *player, Team *team)
{
	bool found = FALSE;
	Link *borrowLink;
	BorrowedPlayer *borrowedPlayer;
	FOR_EACH(team->borrowList, borrowedPlayer, borrowLink)
	{
		found = TRUE;
		if (borrowedPlayer->approved)
			chat->SendMessage(player, "[%s] %-32s [APPROVED by: %s", team->teamName, borrowedPlayer->name, borrowedPlayer->approvedBy);
		else
			chat->SendMessage(player, "[%s] %-32s [PENDING]", team->teamName, borrowedPlayer->name);
	}
	return found;
}

/********************************************************
 *			OFFLINE DRAFTING		*
 ********************************************************/
local helptext_t offlinedrafting_help =
"Module: teams\n"
"Targets: none\n"
"Args: none or OFF/ON\n"
"Allow captains to draft offline people on the signup list";

local void COfflineDrafting(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Offline Drafting %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Offline Drafting currently set as %s", arenaData->offlineDrafting ? "ON" : "OFF");
		return;
	}

	if (strncasecmp("ON",params,2) == 0)
	{
		if (arenaData->offlineDrafting)
		{
			chat->SendMessage(player, "Save teams is already set to ON");
			return;
		}

		arenaData->offlineDrafting = TRUE;
		chat->SendMessage(player, "Offline drafting set to ON");
	}
	else if (strncasecmp("OFF",params,3) == 0)
	{
		if (!arenaData->offlineDrafting)
		{
			chat->SendMessage(player, "Offline drafting is already set to OFF");
			return;
		}

		arenaData->offlineDrafting = FALSE;
		chat->SendMessage(player, "Offline drafting set to OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

/********************************************************
 *			SET TEAM SHIP			*
 ********************************************************/
local helptext_t setteamship_help =
"Module: teams\n"
"Targets: none\n"
"Args: <team>:<1-8>\n"
"Set the ship to be used by the team";

local void CSetTeamShip(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Set Team Ship %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "You must specify the team and ship number to use.");
		return;
	}

	char *teamString;
	char *shipString;
	Team *foundTeam;

	if (!TwoSplitUserInput(':', params, &teamString, &shipString))
	{
		lm->Log(L_INFO, "<teams> Set Team Ship FALSE");
		chat->SendMessage(player, "You must specify the team and ship number to use.");
		TwoSplitRelease(&teamString, &shipString);
		return;
	}

	if (!FindTeam(arenaData, player, teamString, &foundTeam))
	{
		TwoSplitRelease(&teamString, &shipString);
		return;
	}
	afree(teamString);

	int ship = satoi(shipString);
	if (	(!ship) || 
		((ship<1) || (ship>8))
	   )
	{
		chat->SendMessage(player, "Invalid ship provided: %s", shipString);
		afree(shipString);
		return;
	}
	afree(shipString);

	foundTeam->freqShip = ship-1;
	chat->SendMessage(player, "Default ship for team %s set to %s", foundTeam->teamName, ShipDescriptions[(ship-1)]);

	if (foundTeam->players)
	{
		Link *link;
		TeamPlayer *teamPlayer;
		FOR_EACH(foundTeam->players, teamPlayer, link)
		{
			if (teamPlayer->ship != SHIP_SPEC)
			{
				teamPlayer->ship = foundTeam->freqShip;
				Player *foundPlayer = NULL;
				if (FindPlayerInArenaExact(lm, pd, player->arena, teamPlayer->name, &foundPlayer))
				{
					game->SetShipAndFreq(foundPlayer, teamPlayer->ship, foundTeam->frequency);
				}
			}
		}
	}
}

/********************************************************
 *			SET PICKING STAGE		*
 ********************************************************/
local helptext_t setpickingstage_help =
"Module: teams\n"
"Targets: none\n"
"Args: <1-5>\n"
"Display or Set the current picking stage. 1-Setup, 2-Picking, 3-Completed, 4-GameStart, 5-GameOver";

local void CSetPickingStage(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Set Picking Stage %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		DisplayPickStatus(player);
		return;
	}
	switch (params[0])
	{
		case '1':
			arenaData->pickingStage = PICKING_SETUP;
			chat->SendMessage(player, "Picking Stage reset to Setup");
		break;
		case '2':
			arenaData->pickingStage = PICKING_PICKING;
			chat->SendMessage(player, "Picking Stage reset to Picking");
		break;
		case '3':
			arenaData->pickingStage = PICKING_COMPLETED;
			chat->SendMessage(player, "Picking Stage reset to Setup");
		break;
		case '4':
			arenaData->pickingStage = PICKING_GAMESTART;
			chat->SendMessage(player, "Picking Stage reset to Game Start");
		break;
		case '5':
			arenaData->pickingStage = PICKING_GAMEOVER;
			chat->SendMessage(player, "Picking Stage reset to Game Over");
		break;
		default:
			chat->SendMessage(player, "Unrecognized stage %c, please specify 1-5 as the option", params[0]);
	}
}

/********************************************************
 *			SAME TEAMS			*
 ********************************************************/
local helptext_t sameteams_help =
"Module: teams\n"
"Targets: none\n"
"Args: none\n"
"Play another round using the same teams. Captains will need to ?ready to begin.";

local void CSameTeams(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<teams> Same Teams %s", params);
	TeamsArenaData * arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (arenaData->pickingStage != PICKING_GAMEOVER)
	{
		chat->SendMessage(player, "Same teams can only be used once a game is over with teams already defined.");
		DisplayPickStatus(player);
		return;
	}

	arenaData->pickingStage = PICKING_PICKING;

	Team *team;
	Link *link;
	FOR_EACH(arenaData->teams, team, link)
	{
		team->ready = FALSE;
	}

	chat->SendArenaSoundMessage(player->arena, 1, "A new game using the same teams has been initiated by %s", player->name);
	chat->SendArenaMessage(player->arena, "Captains use ?ready when you are ready to begin!");
}

		/****************************************************************
		 *			CallBack functions			*
		 ****************************************************************/
/************************************************
 * 		PLAYER ACTION SECTION		*
 ************************************************/
local void ChangePlayerStatus(TeamsArenaData *arenaData, Player *player, bool isLeaving)
{
	TeamPlayer *foundPlayer;
	int frequency = -1;
	if (FindPlayerInTeamExact(arenaData, player->name, &foundPlayer, &frequency))
	{
		if (isLeaving)
			foundPlayer->laggedOut	= TRUE;
		else
			foundPlayer->laggedOut = (player->p_ship == SHIP_SPEC);
	}
}

local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<teams> %s Player Action %d", player->name, action );
	TeamsArenaData * arenaData;

	switch (action)
	{
		case PA_ENTERARENA:
			arenaData	= P_ARENA_DATA(arena, arenaKey);

			switch (arenaData->pickingStage)
			{
				case PICKING_SETUP:
				case PICKING_PAUSED:
				case PICKING_PICKING:

					if (arenaData->pickingStage == PICKING_SETUP)
						chat->SendMessage(player, "Picking of teams has yet to begin.");
					else
						chat->SendMessage(player, "Teams are currently being picked.");

					if (!arenaData->activeEvent)
					{
						chat->SendMessage(player, "Consider sending a message in public chat so captains know you are available to be picked.");
					}
					else
					{	
						chat->SendMessage(player, "In order to be picked you must be on the signup list. If you haven't signed up already use  ?signup %s", arenaData->activeEvent);
					}
				break;
				case PICKING_COMPLETED:
					chat->SendMessage(player, "Picking of teams is completed. Waiting on captains to ready and start the game.");
				break;
				case PICKING_GAMESTART:
					chat->SendMessage(player, "The game in this arena is already started.");
				break;
				case PICKING_GAMEOVER:
					chat->SendMessage(player, "The game in this arena has finished.");
				break;
			}
			chat->SendMessage(player, "Type ?pbhelp for available commands.");
			ChangePlayerStatus(arenaData, player, FALSE);
		break;

		case PA_LEAVEARENA:
			arenaData	= P_ARENA_DATA(arena, arenaKey);

			ChangePlayerStatus(arenaData, player, TRUE);
		break;
	}
}
/************************************************
 * 		INITIATE NEW TEAMS		*
 ************************************************/
local void InitiateNewTeams(Arena *arena)
{
	lm->Log(L_INFO, "<teams> Initiate New Teams");
	TeamsArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

	if (arenaData->pickingStage == PICKING_GAMESTART)
	{
		//chat->SendMessage(player, "Game has started. Cannot create new teams until game is ended.");
		return;
	}

	ResetTeams(arenaData, arena);

	balls->StopGame(arena);
	pbstats->ResetStats(arena);

	chat->SendArenaSoundMessage(arena, 1, "Picking of New Teams Started!");
}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local void GameOver(Arena *arena)
{
	TeamsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	lm->Log(L_INFO, "<teams> Game Over");

	arenaData->pickingStage = PICKING_GAMEOVER;

	balls->StopGame(arena);
}

/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups = mm->GetInterface(I_SIGNUPS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras   = mm->GetInterface(I_PBEXTRAS,             ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats = mm->GetInterface(I_PBSTATS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbpub"))
	{
		pbpub = mm->GetInterface(I_PBPUB,		ALLARENAS);
		//cmd->RemoveCommand("pbhelp",		CTeamsHelp,	arena);
		return;
	}
}

/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups = NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras = NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats = NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbpub"))
	{
		pbpub = NULL;
		//cmd->AddCommand("pbhelp",	CTeamsHelp,	arena,	teamshelp_help);
		return;
	}
}
/****************************************************************
 *			SHIP FREQ CHANGE			*
 ****************************************************************/
local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq)
{
	lm->Log(L_INFO, "<pbpub> %s freq ship change %d:%d %d:%d", player->name, oldShip, newShip, oldFreq, newFreq );

	TeamsArenaData * arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	TeamPlayer *foundPlayer;
	int frequency = -1;
	if (FindPlayerInTeamExact(arenaData, player->name, &foundPlayer, &frequency))
	{
		foundPlayer->laggedOut = (newShip == SHIP_SPEC);
	}
}

///////-------------------------------------------------------------------------------------

/*
local void CCrash(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "Please crash");
	Team *team = NULL;

	if (team->borrowList)
		chat->SendMessage(player, "Crashing hopefully.");
}
*/

local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(stats);

	mm->ReleaseInterface(signups);
	mm->ReleaseInterface(db);
	mm->ReleaseInterface(pbstats);
	mm->ReleaseInterface(pbpub);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(pbextras);
	mm->ReleaseInterface(pbleague);

}

/* The entry point: */
EXPORT int MM_teams(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			pd	= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			ml	= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			game	= mm->GetInterface(I_GAME,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			cfg	= mm->GetInterface(I_CONFIG,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			stats	= mm->GetInterface(I_STATS,		ALLARENAS);

			signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			db	= mm->GetInterface(I_RELDB,		ALLARENAS);
			pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			pbpub	= mm->GetInterface(I_PBPUB,		ALLARENAS);
			balls	= mm->GetInterface(I_BALLS,		ALLARENAS);
			pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);
			pbleague= mm->GetInterface(I_PBLEAGUE,		ALLARENAS);

			if (!db)
				hasDB	= FALSE;
			else
			{
				hasDB	= TRUE;
				InitializeDB();
			}

			if (!chat || !aman || !pd || !ml || !game || !cmd || !lm || !cfg || !capman || !balls || !stats)
			{
				ReleaseInterfaces();
				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey = aman->AllocateArenaData(sizeof(TeamsArenaData));

				if (arenaKey == -1)	//check for valid memory allocation
				{
					aman->FreeArenaData(arenaKey);

					ReleaseInterfaces();

					rv = MM_FAIL;
				}
				else
				{
					mm->RegInterface(&_int, ALLARENAS);

					rv = MM_OK;
					if (!signups)
						lm->Log(L_INFO, "<teams> signups not loaded.");
				}
			}
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);

			if (!pbstats)
				pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			if (!pbpub)
				pbpub	= mm->GetInterface(I_PBPUB,		ALLARENAS);
			if (!signups)
				signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			if (!pbextras)
				pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);
			if (!pbleague)
				pbleague= mm->GetInterface(I_PBLEAGUE,		ALLARENAS);

			mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->RegCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);

			cmd->AddCommand("teamshelp",		CTeamsHelp,		arena,	teamshelp_help);

			cmd->AddCommand("teamsversion",		CTeamsVersion,		arena,	teamsversion_help);
			cmd->AddCommand("addteam",		CAddTeam,		arena,	addteam_help);
			cmd->AddCommand("removeteam",		CRemoveTeam,		arena,	removeteam_help);
			cmd->AddCommand("saveteams",		CSaveTeams,		arena,	saveteams_help);
			cmd->AddCommand("setteamship",		CSetTeamShip,		arena,	setteamship_help);
			cmd->AddCommand("addcap",		CAddCaptain,		arena,	addcaptain_help);
			cmd->AddCommand("addcaptain",		CAddCaptain,		arena,	addcaptain_help);
			cmd->AddCommand("removecap",		CRemoveCaptain,		arena,	removecaptain_help);
			cmd->AddCommand("removecaptain",	CRemoveCaptain,		arena,	removecaptain_help);
			
			cmd->AddCommand("pick",			CPick,			arena,	pick_help);
			cmd->AddCommand("add",			CAdd,			arena,	add_help);
			cmd->AddCommand("remove",		CRemove,		arena,	remove_help);
			cmd->AddCommand("captains",		CCaptains,		arena,	captains_help);
			cmd->AddCommand("caps",			CCaptains,		arena,	captains_help);
			cmd->AddCommand("teams",		CTeams,			arena,	teams_help);
			cmd->AddCommand("freezeteams",		CFreezeTeams,		arena,	freezeteams_help);
			cmd->AddCommand("unfreezeteams",	CUnFreezeTeams,		arena,	unfreezeteams_help);
			cmd->AddCommand("newteams",		CNewTeams,		arena,	newteams_help);
			cmd->AddCommand("ready",		CReady,			arena,	ready_help);
			cmd->AddCommand("teammax",		CTeamMax,		arena,	teammax_help);
			cmd->AddCommand("teamingamemax",	CTeamInGameMax,		arena,	teamingamemax_help);
			cmd->AddCommand("nextpick",		CNextPick,		arena,	nextpick_help);
			cmd->AddCommand("currentpick",		CCurrentPick,		arena,	currentpick_help);
			cmd->AddCommand("startpicking",		CStartPicking,		arena,	startpicking_help);
			cmd->AddCommand("pickstatus",		CPickStatus,		arena,	pickstatus_help);
			cmd->AddCommand("picktype",		CPickType,		arena,	picktype_help);
			cmd->AddCommand("draftmode",		CDraftMode,		arena,	draftmode_help);
			cmd->AddCommand("usesignups",		CUseSignUps,		arena,	usesignups_help);
			cmd->AddCommand("teamfreq",		CTeamFreq,		arena,	teamfreq_help);
			cmd->AddCommand("lagout",		CLagOut,		arena,	lagout_help);
			cmd->AddCommand("sub",			CSubstitute,		arena,	substitute_help);

			cmd->AddCommand("forceadd",		CAdd,			arena,	add_help);
			cmd->AddCommand("forceremove",		CRemove,		arena,	remove_help);
			cmd->AddCommand("forceteamfreq",	CTeamFreq,		arena,	teamfreq_help);
			cmd->AddCommand("forcelagout",		CLagOut,		arena,	lagout_help);
			cmd->AddCommand("forceready",		CReady,			arena,	ready_help);
			cmd->AddCommand("forcesub",		CSubstitute,		arena,	substitute_help);

			cmd->AddCommand("savedteams",		CSavedTeams,		arena,	savedteams_help);
			cmd->AddCommand("listsavedteams",	CSavedTeams,		arena,	savedteams_help);
			cmd->AddCommand("delsavedteam",		CDelSavedTeam,		arena,	delsavedteam_help);
			cmd->AddCommand("listsavedteam",	CListSavedTeam,		arena,	listsavedteam_help);
			cmd->AddCommand("loadteam",		CLoadTeam,		arena,	loadteam_help);

			cmd->AddCommand("borrow",		CBorrow,		arena,	borrow_help);
			cmd->AddCommand("unborrow",		CUnBorrow,		arena,	unborrow_help);
			cmd->AddCommand("approve",		CApprove,		arena,	approve_help);
			cmd->AddCommand("listborrows",		CListBorrows,		arena,	listborrows_help);
			cmd->AddCommand("addborrow",		CAddBorrow,		arena,	addborrow_help);

			//cmd->AddCommand("forcecrash",		CCrash,			arena,	addborrow_help);
			cmd->AddCommand("offlinedrafting",	COfflineDrafting,	arena,	offlinedrafting_help);
			cmd->AddCommand("setpickingstage",	CSetPickingStage,	arena,	setpickingstage_help);
			cmd->AddCommand("sameteams",		CSameTeams,		arena,	sameteams_help);

			InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("teamshelp",		CTeamsHelp,		arena);

			cmd->RemoveCommand("teamsversion",	CTeamsVersion,		arena);
			cmd->RemoveCommand("addteam",		CAddTeam,		arena);
			cmd->RemoveCommand("removeteam",	CRemoveTeam,		arena);
			cmd->RemoveCommand("saveteams",		CSaveTeams,		arena);
			cmd->RemoveCommand("setteamship",	CSetTeamShip,		arena);
			cmd->RemoveCommand("addcap",		CAddCaptain,		arena);
			cmd->RemoveCommand("addcaptain",	CAddCaptain,		arena);
			cmd->RemoveCommand("removecap",		CRemoveCaptain,		arena);
			cmd->RemoveCommand("removecaptain",	CRemoveCaptain,		arena);

			cmd->RemoveCommand("pick",		CPick,			arena);
			cmd->RemoveCommand("add",		CAdd,			arena);
			cmd->RemoveCommand("remove",		CRemove,		arena);
			cmd->RemoveCommand("caps",		CCaptains,		arena);
			cmd->RemoveCommand("captains",		CCaptains,		arena);
			cmd->RemoveCommand("teams",		CTeams,			arena);
			cmd->RemoveCommand("freezeteams",	CFreezeTeams,		arena);
			cmd->RemoveCommand("unfreezeteams",	CUnFreezeTeams,		arena);
			cmd->RemoveCommand("newteams",		CNewTeams,		arena);
			cmd->RemoveCommand("ready",		CReady,			arena);
			cmd->RemoveCommand("teammax",		CTeamMax,		arena);
			cmd->RemoveCommand("teamingamemax",	CTeamInGameMax,		arena);
			cmd->RemoveCommand("nextpick",		CNextPick,		arena);
			cmd->RemoveCommand("currentpick",	CCurrentPick,		arena);
			cmd->RemoveCommand("startpicking",	CStartPicking,		arena);
			cmd->RemoveCommand("pickstatus",	CPickStatus,		arena);
			cmd->RemoveCommand("picktype",		CPickType,		arena);
			cmd->RemoveCommand("draftmode",		CDraftMode,		arena);
			cmd->RemoveCommand("usesignups",	CUseSignUps,		arena);
			cmd->RemoveCommand("teamfreq",		CTeamFreq,		arena);
			cmd->RemoveCommand("lagout",		CLagOut,		arena);
			cmd->RemoveCommand("sub",		CSubstitute,		arena);

			cmd->RemoveCommand("forceadd",		CAdd,			arena);
			cmd->RemoveCommand("forceremove",	CRemove,		arena);
			cmd->RemoveCommand("forceteamfreq",	CTeamFreq,		arena);
			cmd->RemoveCommand("forcelagout",	CLagOut,		arena);
			cmd->RemoveCommand("forceready",	CReady,			arena);
			cmd->RemoveCommand("forcesub",		CSubstitute,		arena);

			cmd->RemoveCommand("savedteams",	CSavedTeams,		arena);
			cmd->RemoveCommand("listsavedteams",	CSavedTeams,		arena);
			cmd->RemoveCommand("delsavedteam",	CDelSavedTeam,		arena);
			cmd->RemoveCommand("listsavedteam",	CListSavedTeam,		arena);
			cmd->RemoveCommand("loadteam",		CLoadTeam,		arena);

			cmd->RemoveCommand("borrow",		CBorrow,		arena);
			cmd->RemoveCommand("unborrow",		CUnBorrow,		arena);
			cmd->RemoveCommand("approve",		CApprove,		arena);
			cmd->RemoveCommand("listborrows",	CListBorrows,		arena);
			cmd->RemoveCommand("addborrow",		CAddBorrow,		arena);

			//cmd->RemoveCommand("forcecrash",	CCrash,			arena);
			cmd->RemoveCommand("offlinedrafting",	COfflineDrafting,	arena);
			cmd->RemoveCommand("setpickingstage",	CSetPickingStage,	arena);
			cmd->RemoveCommand("sameteams",		CSameTeams,		arena);

			mm->UnregCallback(CB_MODULEATTACHED,    ModuleAttached,         arena);
			mm->UnregCallback(CB_MODULEDETACHED,    ModuleDetached,         arena);
			mm->UnregCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->UnregCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);

			mm->UnregCallback(CB_PLAYERACTION,	PlayerAction,		arena);

			if (arenaKey != -1)
				ReleaseArenaData(arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (arenaKey != -1)
			{
				aman->FreeArenaData(arenaKey);
				mm->UnregInterface(&_int, ALLARENAS);
			}

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}
