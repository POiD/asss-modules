#ifndef __SIGNUPS_MOD_H__
#define __SIGNUPS_MOD_H__

#define CFG_LOG_MYSQL_QUERIES 1

#define EVENTS_TABLE "EVENTS"
#define SIGNUPS_TABLE "SIGNUPS"
#define EVENT_NAME_LEN 32
#define EVENT_DESC_LEN 250
#define SIGNUP_NAME_LEN 32

#define CREATE_EVENTS_TABLE \
"CREATE TABLE IF NOT EXISTS " EVENTS_TABLE " (" \
"  id int AUTO_INCREMENT PRIMARY KEY," \
"  name char(32) NOT NULL," \
"  description char(250) DEFAULT ''," \
"  active tinyint(1) DEFAULT 0" \
")"

#define CREATE_SIGNUPS_TABLE \
"CREATE TABLE IF NOT EXISTS " SIGNUPS_TABLE " (" \
"  id int NOT NULL," \
"  name char(32) NOT NULL," \
"  PRIMARY KEY(id,name)," \
"  FOREIGN KEY (id) " \
"    REFERENCES " EVENTS_TABLE "(id)" \
"    ON DELETE CASCADE" \
")"

#define ADD_EVENT_QUERY			"INSERT INTO " EVENTS_TABLE " (name, description) VALUES (UCASE(?), ?)"
#define DEL_EVENT_QUERY			"DELETE FROM " EVENTS_TABLE " WHERE name=UCASE(?)"
#define CHG_EVENT_QUERY			"UPDATE " EVENTS_TABLE " SET description=? WHERE name=UCASE(?)"
#define GET_EVENTS_QUERY		"SELECT name, active, description FROM " EVENTS_TABLE
#define GET_EVENTID_QUERY		"SELECT id FROM " EVENTS_TABLE " WHERE name=UCASE(?)"
#define ACTIVATE_EVENTS_QUERY		"UPDATE " EVENTS_TABLE " SET active=# WHERE name=UCASE(?)"
#define CHECK_ACTIVE_EVENT_QUERY	"SELECT active FROM " EVENTS_TABLE " WHERE name=UCASE(?)"

#define ADD_SIGNUP_QUERY	"INSERT INTO " SIGNUPS_TABLE " (id, name) SELECT E.id, UCASE(?) FROM " EVENTS_TABLE " E WHERE E.name=UCASE(?)"
#define DEL_SIGNUP_QUERY	"DELETE S.* FROM " SIGNUPS_TABLE " S INNER JOIN " EVENTS_TABLE " E ON S.id=E.id WHERE E.name=UCASE(?) AND S.name=UCASE(?)"
#define GET_SIGNUPS_QUERY	"SELECT S.name FROM " EVENTS_TABLE " E INNER JOIN " SIGNUPS_TABLE " S ON S.id=E.id WHERE E.name=UCASE(?)"
#define CLR_SIGNUPS_QUERY	"DELETE S.* FROM " SIGNUPS_TABLE " S INNER JOIN " EVENTS_TABLE " E ON S.id=E.id WHERE E.name=UCASE(?)"
#define CHECK_SIGNUP_QUERY	"SELECT S.name FROM " EVENTS_TABLE " E INNER JOIN " SIGNUPS_TABLE " S ON S.id=E.id WHERE E.name=UCASE(?) AND S.name LIKE CONCAT(UCASE(?),'%')"

typedef struct PlayerEventStruct
{
	Player *	player;
	char *		playerName;
	char *		event;
	char *		description;
} PlayerEvent;


#define I_SIGNUPS "signups-1"

typedef struct Isignups
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
	void (*IsPlayerSignedUp)(Player *player, char *playerName, const char *eventName, void *passThroughData, void (*IsSignUpPtr)(Player *, int, LinkedList *, void *));
	void (*RemovePlayer)(Player *player, const char *playerName, const char *eventName, void *data, void (*IsSignUpPtr)(Player *, bool, void *));
	void (*AddPlayer)(Player *player, char *playerName, const char *eventName, void *data, void (*IsSignUpPtr)(Player *, bool, void *));
	void (*IsValidEvent)(Player *player, const char *eventName, void *data, void (*IsEventPtr)(Player *, bool, void *));

} Isignups;


#endif
