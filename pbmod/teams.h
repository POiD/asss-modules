#ifndef __TEAMS_MOD_H__
#define __TEAMS_MOD_H__

#define CFG_LOG_MYSQL_QUERIES 1

#define TEAMS_TABLE "TEAMS"
#define TEAMS_PLAYERS_TABLE "TEAMS_PLAYERS"
#define TEAM_NAME_LEN 64

#define CREATE_TEAMS_TABLE \
"CREATE TABLE IF NOT EXISTS " TEAMS_TABLE " (" \
"  id int AUTO_INCREMENT PRIMARY KEY," \
"  name char(64) NOT NULL UNIQUE," \
"  captain char(32) NOT NULL" \
")"

#define CREATE_TEAMS_PLAYER_TABLE \
"CREATE TABLE IF NOT EXISTS " TEAMS_PLAYERS_TABLE " (" \
"  team_id int NOT NULL," \
"  name char(32) NOT NULL," \
"  PRIMARY KEY(team_id,name)," \
"  FOREIGN KEY(team_id) " \
"    REFERENCES " TEAMS_TABLE "(id)" \
"    ON DELETE CASCADE" \
")"

#define ADD_TEAM_QUERY		"INSERT INTO " TEAMS_TABLE " (name, captain) VALUES (UCASE(?), UCASE(?))"
#define DEL_TEAM_QUERY		"DELETE FROM " TEAMS_TABLE " WHERE name=UCASE(?)"
#define GET_TEAMS_QUERY		"SELECT id, name, captain FROM " TEAMS_TABLE
#define GET_TEAMS_FUZZY_QUERY	"SELECT id,name,captain FROM " TEAMS_TABLE " WHERE name LIKE CONCAT(UCASE(?),'%')"
#define CHECK_TEAMS_QUERY	"SELECT id,name FROM " TEAMS_TABLE " WHERE name in (UCASE(?))"
#define CHG_TEAMS_CAPTAIN_QUERY	"UPDATE " TEAMS_TABLE " SET captain=UCASE(?) WHERE name=UCASE(?)"

#define ADD_TEAM_PLAYER_QUERY	"INSERT INTO " TEAMS_PLAYERS_TABLE " (team_id, name) SELECT T.id, UCASE(?) FROM " TEAMS_TABLE " T WHERE T.name=UCASE(?)"
#define DEL_TEAM_PLAYER_QUERY	"DELETE TP.* FROM " TEAMS_PLAYERS_TABLE " TP INNER JOIN " TEAMS_TABLE " T ON TP.team_id=T.id WHERE T.name=UCASE(?) AND TP.name=UCASE(?)"
#define CLEAR_TEAM_PLAYER_QUERY	"DELETE TP.* FROM " TEAMS_PLAYERS_TABLE " TP INNER JOIN " TEAMS_TABLE " T ON TP.team_id=T.id WHERE T.name=UCASE(?)"
#define GET_TEAM_PLAYER_QUERY	"SELECT TP.name FROM " TEAMS_PLAYERS_TABLE " TP INNER JOIN " TEAMS_TABLE " T ON TP.team_id=T.id WHERE T.name=UCASE(?)"

typedef enum
{
	PICKINGTYPE_FREE = 1,
	PICKINGTYPE_NORMAL,
	PICKINGTYPE_SNAKE,
	PICKINGTYPE_RANDOM
} PickingType;

typedef enum
{
	PICKING_SETUP = 1,
	PICKING_PICKING,
	PICKING_PAUSED,
	PICKING_COMPLETED,
	PICKING_GAMESTART,
	PICKING_GAMEOVER
} PickingStage;

typedef struct TeamsArenaData
{
	int		numberOfTeams;
	PickingStage	pickingStage;
	int		pickingRound;
	char *		activeEvent;

	int		currentPickFreq;
	int		currentPick;
	bool		pickDirection;
	bool		saveTeams;
	bool		offlineDrafting;

	//set by config
	int		teamMax;
	int		teamInGameMax;
	PickingType	pickingType;
	bool		repopulateSignups;
	bool		isDraft;

	LinkedList *	teams;

} TeamsArenaData;


typedef struct Team
{
	int		frequency;
	char *		teamName;
	bool		ready;
	char *		captain;
	LinkedList *	players;

	int		pickedCount;
	bool		wasLoaded;
	int		freqShip;
	int		playersInGame;

	LinkedList *	borrowList;

} Team;

typedef struct TeamPlayer
{
	char *		name;
	int		ship;
	bool		laggedOut;
	bool		wasLoaded;
	bool		wasBorrowed;
} TeamPlayer;

typedef struct BorrowedPlayerStruct
{
	char *		name;
	bool		approved;
	char *		approvedBy;
} BorrowedPlayer;

/* called when teams are ready to start
 * @threading called from main
 */
#define CB_TEAMSREADY "teamsready"
typedef void (*TeamsReadyFunc)(Arena *arena);
/* pycb: arena_not_none */


#define I_TEAMS "teams-1"

typedef struct Iteams
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	void (*InitiateNewTeams)(Arena *arena);
	/* 	*/
	/* pyint: arena_not_none -> void */
	void (*HelpCommands)(Player *player);
	char * (*GetActiveEvent)(Arena *arena);
} Iteams;


#endif

