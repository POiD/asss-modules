/* dist: public */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "asss.h"
#include "scramble.h"
//#include "game_timer.h"
#include "../scoring/points_goal.h"
#include "pbpub.h"

/* prototypes */
void MyAA(Arena *arena, int action);
void MyGoal(Arena *arena, Player *p, int bid, int x, int y);
void MyCatch(Arena *arena, Player *p, int bid);
void MyThrow(Arena *arena, Player *p, int bid);
void MyShip(Player *, int, int, int, int);
local int pre_game(void *);
local int upgoalvalue (void *param);
local int dump_stats(void *param);
void Cstartgm(const char *tc, const char *params, Player *p, const Target *target);
void Cstopgm(const char *tc, const char *params, Player *p, const Target *target);
void Crules(const char *tc, const char *params, Player *p, const Target *target);

//local helptext_t;

/* global data */
local int scrdkey, mtxkey;
local helptext_t startgm_help, rules_help, stopgm_help;

local Imodman *mm;
local Iplayerdata *pd;
local Iballs *balls;
local Iarenaman *aman;
local Iconfig *cfg;
local Ichat *chat;
local Icmdman *cmd;
local Istats *stats;
local Imainloop *ml;
local Igame *game;
local Iobjects *obj;
//local Igmtimer *gmtimer;
local Ipointsgoals *ptsgoal;
local Ipbpub *pbpub;

local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(stats);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(obj);
	//mm->ReleaseInterface(gmtimer);
	mm->ReleaseInterface(ptsgoal);
	mm->ReleaseInterface(pbpub);
}

EXPORT int MM_scramble (int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm	= mm_;
		pd	= mm->GetInterface(I_PLAYERDATA, arena);
		balls	= mm->GetInterface(I_BALLS, arena);
		aman	= mm->GetInterface(I_ARENAMAN, arena);
		cfg	= mm->GetInterface(I_CONFIG, arena);
		chat	= mm->GetInterface(I_CHAT, arena);
		cmd	= mm->GetInterface(I_CMDMAN, arena);
		stats	= mm->GetInterface(I_STATS, arena);
		ml	= mm->GetInterface(I_MAINLOOP, arena);
		game	= mm->GetInterface(I_GAME, arena);
		obj	= mm->GetInterface(I_OBJECTS, arena);
		//gmtimer = mm->GetInterface(I_GMTIMER, arena);
		//ptsgoal = mm->GetInterface(I_PTSGOAL, arena);
		ptsgoal	= mm->GetInterface(I_POINTS_GOALS, arena);
		pbpub	= mm->GetInterface(I_PBPUB, arena);

		if (!pbpub || !chat || !cfg || !aman || !balls || !pd || !cmd || !stats || !ml || !game || !obj || !ptsgoal)
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}

		scrdkey = aman->AllocateArenaData(sizeof(ScrambleData));
		mtxkey = aman->AllocateArenaData(sizeof(pthread_mutex_t));

		if ((scrdkey == -1) || (mtxkey == -1))
		{
			if (mtxkey != -1)
				aman->FreeArenaData(mtxkey);
			if (scrdkey != -1)
				aman->FreeArenaData(scrdkey);

			ReleaseInterfaces();

			return MM_FAIL;
		}

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		ReleaseInterfaces();

		if (mtxkey != -1)
			aman->FreeArenaData(mtxkey);
		if (scrdkey != -1)
			aman->FreeArenaData(scrdkey);

		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		mm->RegCallback(CB_ARENAACTION, MyAA, arena);
		mm->RegCallback(CB_GOAL, MyGoal, arena);
		mm->RegCallback(CB_BALLPICKUP, MyCatch, arena);
		//mm->RegCallback(CB_BALLFIRE, MyThrow, arena);
		//mm->RegCallback(CB_SHIPCHANGE, MyShip, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, MyShip, arena);

		cmd->AddCommand("startgm",Cstartgm, arena, startgm_help);
		cmd->AddCommand("stopgm",Cstopgm, arena, stopgm_help);
		cmd->AddCommand("rules",Crules, arena, rules_help);

		pbpub->SetTypeAndLock(arena, PB_GAME_SCRAMBLE);

		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		mm->UnregCallback(CB_ARENAACTION, MyAA, arena);
		mm->UnregCallback(CB_GOAL, MyGoal, arena);
		mm->UnregCallback(CB_BALLPICKUP, MyCatch, arena);
		//mm->UnregCallback(CB_BALLFIRE, MyThrow, arena);
		//mm->UnregCallback(CB_SHIPCHANGE, MyShip, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, MyShip, arena);

		cmd->RemoveCommand("startgm", Cstartgm, arena);
		cmd->RemoveCommand("stopgm", Cstopgm, arena);
		cmd->RemoveCommand("rules", Crules, arena);

		ml->ClearTimer(upgoalvalue, arena);
		ml->ClearTimer(pre_game, arena);

		return MM_OK;
	}
	return MM_FAIL;
}

void MyAA(Arena *arena, int action)
{
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	ConfigHandle c = arena->cfg;

	/* create the mutex if necessary */
	if (action == AA_PRECREATE)
	{
		pthread_mutexattr_t attr;

		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init((pthread_mutex_t*)P_ARENA_DATA(arena, mtxkey), &attr);
		pthread_mutexattr_destroy(&attr);
	}
	LOCK_STATUS(arena);
	if (action == AA_CREATE)
	{
		scrd->gamestate = 0;
		scrd->ballfreq = -1;
		scrd->goalvalue = 0;
		scrd->leaguearena = cfg->GetInt(c, "Misc", "LeagueArena", 0);
	}
	else if (action == AA_DESTROY)
	{
		ml->ClearTimer(upgoalvalue, arena);
		ml->ClearTimer(pre_game, arena);
		ml->ClearTimer(dump_stats, arena);
	}
	else if (action == AA_CONFCHANGED)
	{
		/* reload settings */
		scrd->leaguearena = cfg->GetInt(c, "Misc", "LeagueArena", 0);
		//   chat->SendMessage(p, "leaguearena: %d:", scrd->leaguearena);
	}
	UNLOCK_STATUS(arena);
}

local int dump_stats(void *param)
{
	Arena *arena = param;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	int scores[8];

	ptsgoal->GetScores(arena, scores, 8);

	LOCK_STATUS(arena);

	if (!scrd->gamestate)
	{
		UNLOCK_STATUS(arena);
		return FALSE;
	}

	if (scores[0] == scores[1])
	{
		chat->SendArenaMessage(arena, "Sudden Death Overtime. Next goal wins!");
		scrd->gamestate = 11;
	}
	else
	{
		chat->SendArenaMessage(arena,
		"SCORE: Warbirds:%d Javelins:%d", scores[0], scores[1]);
		chat->SendArenaMessage(arena, "Soccer game over.");
		scrd->gamestate = 0;
	}

	UNLOCK_STATUS(arena);
	return FALSE;
}

void DepleteShip(Player *p)
{
	int i;
	Target target;
	target.type = T_PLAYER;
	target.u.p = p;

	for(i = 1; i < 29; i++)
		game->GivePrize(&target, -i, 5);

	game->WarpTo(&target, p->p_ship == SHIP_WARBIRD ? 404 : 619, 512);
}

local int pre_game(void *param)
{
	Arena *arena = param;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	struct BallData newpos;
	Link *link;
	Player *p;
	Target target;
	int i, scores[8];

	LOCK_STATUS(arena);

	if (scrd->leaguearena)
	{
		scrd->ballfreq = -1;
		scrd->gamestate = 10;
	}

	if (scrd->gamestate == 1)
	{
		//chat->SendArenaMessage(arena, "Game will begin in 5 seconds.");

		// fix any settings, so clients have enough time to receive changes
		scrd->gamestate = 2;
		ml->SetTimer(pre_game, 200, 0, arena, arena);
	}
	else if (scrd->gamestate == 2)
	{
		// set next timer, start animation
		scrd->gamestate = 3;

		//chat->SendArenaMessage(arena, "SCORE: Warbirds:0  Javelins:0");
		for(i=0;i<8;i++)
			scores[i] = 0;

		UNLOCK_STATUS(arena);
		ptsgoal->SetScores(arena, (int *)&scores[0]);
		LOCK_STATUS(arena);

		scrd->ballfreq = -1;

		ml->SetTimer(pre_game, 100, 0, arena, arena);
	}
	else if (scrd->gamestate == 3)
	{
		target.type = T_ARENA;
		target.u.arena = arena;
		obj->Toggle(&target, 91, TRUE);
		scrd->gamestate = 4;
		chat->SendArenaSoundMessage(arena, 91, "READY");

		ml->SetTimer(pre_game, 100, 0, arena, arena);
	}
	else if (scrd->gamestate == 4)
	{
		target.type = T_ARENA;
		target.u.arena = arena;
		obj->Toggle(&target, 91, FALSE);
		obj->Toggle(&target, 92, TRUE);
		scrd->gamestate = 5;
		chat->SendArenaSoundMessage(arena, 92, "SET");

		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena && (p->p_ship ==  SHIP_WARBIRD || p->p_ship == SHIP_JAVELIN))
			{
				DepleteShip(p);
 			}
		}
		pd->Unlock();

		// center ball, neut ball
		newpos.state = BALL_ONMAP;
		newpos.x = 8200;
		newpos.xspeed = 0;
		newpos.y = 8200;
		newpos.yspeed = 0;
		newpos.freq = 0;
		newpos.carrier = NULL;
		newpos.time = current_ticks();
		UNLOCK_STATUS(arena);
		balls->PlaceBall(arena, 0, &newpos);
		LOCK_STATUS(arena);

		ml->SetTimer(pre_game, 100, 0, arena, arena);
	}
	else if (scrd->gamestate == 5)
	{
		pd->Lock();
		FOR_EACH_PLAYER(p)
		{
			if (p->arena == arena && (p->p_ship ==  SHIP_WARBIRD || p->p_ship == SHIP_JAVELIN))
			{
				target.type = T_PLAYER;
				target.u.p = p;

				game->ShipReset(&target);
			}
		}
		pd->Unlock();

		target.type = T_ARENA;
		target.u.arena = arena;
		obj->Toggle(&target, 92, FALSE);
		obj->Toggle(&target, 93, TRUE);
		scrd->gamestate = 10;
		chat->SendArenaSoundMessage(arena, 93, "GO!!!");

		UNLOCK_STATUS(arena);

		//gmtimer->SetGmTimer(arena, "15");

		scrd->timerStartTick = current_ticks();
		LOCK_STATUS(arena);

		ml->SetTimer(dump_stats, 90100, 0, arena, arena);

		ml->SetTimer(pre_game, 100, 0, arena, arena);
	}
	else if (scrd->gamestate == 10)
	{
		target.type = T_ARENA;
		target.u.arena = arena;
		obj->Toggle(&target, 93, FALSE);
	}

	UNLOCK_STATUS(arena);
	return FALSE;
}

void MyShip(Player *p, int newship, int oldShip, int newfreq, int oldFreq)
{
	Arena *arena = p->arena;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	int  num_birds = 0, num_javs = 0;
	Link *link;
	Player *t;

	LOCK_STATUS(arena);
	if (scrd->gamestate < 10 && (newship == SHIP_WARBIRD || newship == SHIP_JAVELIN))
		DepleteShip(p);

	// check if 3v3 or 4v4 and not a league arena and start countdown if so
	if (!scrd->gamestate && !scrd->leaguearena)
	{
		pd->Lock();
		FOR_EACH_PLAYER(t)
		{
			if (t->arena == p->arena && t->p_ship ==  SHIP_WARBIRD)
				num_birds++;
			else if (t->arena == p->arena && t->p_ship == SHIP_JAVELIN)
				num_javs++;
		}
		pd->Unlock();

		if ( ((num_birds == 4 && num_javs == 4) || (num_birds == 3 && num_javs == 3)))
		{
			UNLOCK_STATUS(arena);
			Cstartgm(NULL, NULL, p, NULL);
			pd->Lock();
			FOR_EACH_PLAYER(p)
			{
				if (p->arena == arena && (p->p_ship ==  SHIP_WARBIRD || p->p_ship == SHIP_JAVELIN))
				{
					DepleteShip(p);
				}
			}
			pd->Unlock();
			return;
		}
		else if (!num_birds && !num_javs)
		{
			scrd->gamestate = 0;
		}
	}
	UNLOCK_STATUS(arena);
}

void MyGoal(Arena *arena, Player *p, int bid, int x, int y)
{
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	Link *link;
	Player *j;
	LinkedList teamset = LL_INITIALIZER, nmeset = LL_INITIALIZER;
	int scores[8];

	ptsgoal->GetScores(arena, scores, 8);

	LOCK_STATUS(arena);

	if (!scrd->gamestate)
	{
		UNLOCK_STATUS(arena);
		return;
	}

	pd->Lock();
	FOR_EACH_PLAYER(j)
		if (j->status == S_PLAYING &&
		    j->arena == arena)
		{
			if (j->p_freq == p->p_freq)
				LLAdd(&teamset, j);
			else
				LLAdd(&nmeset, j);
		}
	pd->Unlock();

	chat->SendSetSoundMessage(&teamset, SOUND_GOAL,
					"Team Goal! by %s: %u point%s",
					p->name, scrd->goalvalue, scrd->goalvalue == 1 ? "" : "s");
	chat->SendSetSoundMessage(&nmeset, SOUND_GOAL,
					"Enemy Goal! by %s: %u point%s",
					p->name, scrd->goalvalue, scrd->goalvalue == 1 ? "" : "s");

	LLEmpty(&teamset); LLEmpty(&nmeset);

	if (p->p_ship == SHIP_WARBIRD)
		scores[0] += scrd->goalvalue;
	else
		scores[1] += scrd->goalvalue;

	ml->ClearTimer(upgoalvalue, arena);

	UNLOCK_STATUS(arena);
	ptsgoal->SetScores(arena, (int *)&scores[0]);

	DO_CBS(CB_PGGOAL, arena, PgGoalFunc, (arena, p, bid, x, y));

	LOCK_STATUS(arena);

	if (scores[0] > 64 || scores[1] > 64 || scrd->gamestate == 11)
	{
		ml->ClearTimer(dump_stats, arena);
		chat->SendArenaSoundMessage(arena, SOUND_DING, "Soccer game over.");
		scrd->gamestate = 0;
		balls->EndGame(arena);
	}

	scrd->ballfreq = -1;

	UNLOCK_STATUS(arena);
}

local int upgoalvalue (void *param)
{
	Arena *arena = param;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);

	LOCK_STATUS(arena);
	if (!scrd->gamestate)
	{
		UNLOCK_STATUS(arena);
		return FALSE;
	}

	if (scrd->goalvalue != 32)
	{
		scrd->goalvalue *= 2;
		chat->SendArenaMessage(arena, "Goal: %u", scrd->goalvalue);
	}
	else
	{
		scrd->goalvalue = 1;
		chat->SendArenaSoundMessage(arena, SOUND_AWW, "Goal: %u", scrd->goalvalue);
	}

	UNLOCK_STATUS(arena);
	return TRUE;
}

void MyCatch(Arena *arena, Player *p, int bid)
{
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	int secsleft;
	int scores[8];

	//gmtimer->GetGmTimer(arena, (int *)&secsleft);
	secsleft= 15 - (TICK_DIFF(current_ticks(), scrd->timerStartTick) / 100);
	ptsgoal->	GetScores(arena, scores, 8);

	LOCK_STATUS(arena);

	// handle catch, only adjust timer on t/o
	if (scrd->gamestate >= 10 && scrd->ballfreq != (p->p_ship == SHIP_WARBIRD ? 0 : 1))
	{
		chat->SendArenaMessage(arena, "Goal: 1");
		scrd->goalvalue = 1;
		scrd->ballfreq = (p->p_ship == SHIP_WARBIRD ? 0 : 1);
		ml->ClearTimer(upgoalvalue, arena);
		ml->SetTimer(upgoalvalue, 1200, 1200, arena, arena);

		// check if not enough time remaining to catch up
		// technically starts at 1:48 if a team is down 64-0
		if (!scrd->leaguearena && scrd->gamestate == 10 && secsleft < 108)
		{
		}
	}

	UNLOCK_STATUS(arena);
}

void MyThrow(Arena *arena, Player *p, int bid)
{
    //     chat->SendMessage(p, "Throw from %s", p->name);
}


local helptext_t startgm_help =
"Targets: arena\n"
"Args: none\n"
"Starts Scramble game.\n";
void Cstartgm(const char *tc, const char *params, Player *p, const Target *target)
{
	Arena *arena = p->arena;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);
	int scores[8];
	//ConfigHandle c = arena->cfg;

	scores[0] = scores[1] = 0;
	ptsgoal->SetScores(arena, (int *)&scores[0]);

	LOCK_STATUS(arena);
	//  scrd->leaguearena = cfg->GetInt(c, "Misc", "LeagueArena", 0);
	//chat->SendMessage(p, "leaguearena: %d:", scrd->leaguearena);
	if (!scrd->leaguearena)
		chat->SendArenaMessage(arena, "Game will begin in 20 seconds.");

	// set game state
	scrd->gamestate = 1;
	scrd->ballfreq = -1;
	scrd->goalvalue = 1;

	ml->ClearTimer(dump_stats, arena);
	ml->ClearTimer(pre_game, arena);
	ml->ClearTimer(upgoalvalue, arena);

	// set timer to warp players to goal at 5 seconds
	// also fix any settings
	ml->SetTimer(pre_game, (scrd->leaguearena ? 1 : 1500), 0, arena, arena);

	UNLOCK_STATUS(arena);
}

local helptext_t stopgm_help =
"Targets: arena\n"
"Args: none\n"
"Stops Scramble game.\n";
void Cstopgm(const char *tc, const char *params, Player *p, const Target *target)
{
	Arena *arena = p->arena;
	ScrambleData *scrd = P_ARENA_DATA(arena, scrdkey);

	LOCK_STATUS(arena);

	scrd->gamestate = 0;

	ml->ClearTimer(dump_stats, arena);
	ml->ClearTimer(pre_game, arena);
	ml->ClearTimer(upgoalvalue, arena);

	chat->SendMessage(p, "Game stopped.");

	UNLOCK_STATUS(arena);
}

local helptext_t rules_help =
"Targets: arena\n"
"Args: none\n"
"Describes rules for scramble arena.\n";
void Crules(const char *tc, const char *params, Player *p, const Target *target)
{
	chat->SendMessage(p, "Rules:");
	chat->SendMessage(p, "The first team to score 65 points wins. Games are timed at 15 mins max.");
	chat->SendMessage(p, "Goal value starts at 1 point and doubles every 12 seconds while your team possesses the ball.");
	chat->SendMessage(p, "A turnover causes the goal value to be reset to 1 point.");
	chat->SendMessage(p, "A goal can reach up to 32 points, then resets back to 1 point after 12 seconds.");
	chat->SendMessage(p, "Wall-passing is allowed.");
}
