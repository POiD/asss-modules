#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "asss.h"
#include "pbstats.h"
#include "balls.h"
//CB_PGGOAL
#include "../scoring/points_goal.h"
#include "pb_utils.c"


// Interfaces
local Imodman *		mm;
local Ichat *		chat;
//arenadata
local Iarenaman *	aman;
local Iplayerdata *	pd;
//timers
local Imainloop * 	ml;
//for permissions
local Icapman *		capman;
//regions
local Imapdata *	mapdata;

local Iballs *		balls;
local Icmdman *		cmd;
local Ilogman *		lm;


local int 		arenaKey	= -1;

local void HelpCommands(Player *player);
local void ResetStats(Arena *arena);


local Ipbstats _int =
{
	INTERFACE_HEAD_INIT(I_PBSTATS, "pbstats")
	HelpCommands,
	ResetStats
};

EXPORT const char info_pbstats[] = CORE_MOD_INFO("pbstats");

/****************************************************************
 *		INITIALIZATION SECTION				*
 ****************************************************************/
local void InitializeArenaData(Arena *arena)
{
	StatsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	for (int i = 0; i < STATS_MAX_TEAMS; i++)
		arenaData->gameStats[i]	= LLAlloc();

	arenaData->playerWithBall	= NULL;
	arenaData->lastWithBall		= NULL;
	arenaData->ballKilledPlayer	= NULL;

	arenaData->roundStart		= FALSE;
	arenaData->goalScored		= TRUE;
	arenaData->activeBallID		= -1;
	arenaData->gameTime		= 0;

	arenaData->lastReleasedTick	= 0;

	arenaData->goal0		= mapdata->FindRegionByName(arena, "Goal0");
	arenaData->goal1		= mapdata->FindRegionByName(arena, "Goal1");

	arenaData->assistPlayers	= LLAlloc();
	arenaData->assistFrequency	= -1;

	if (!arenaData->goal0)
		lm->Log(L_INFO, "<pbstats> goal0 is Not found!!" );
	else
		lm->Log(L_INFO, "<pbstats> goal0 is found!!" );
	if (!arenaData->goal1)
		lm->Log(L_INFO, "<pbstats> goal1 is Not found!!" );
	else
		lm->Log(L_INFO, "<pbstats> goal1 is found!!" );
}

local void ClearAssists(StatsArenaData *arenaData)
{
	lm->Log(L_INFO, "<pbstats> Clearing Assists!!" );

	if (!arenaData->assistPlayers)
		return;

	Link *link;
	AssistPlayer *assistPlayer;
	FOR_EACH(arenaData->assistPlayers, assistPlayer, link)
	{
		afree(assistPlayer->name);
	}
	LLClear(arenaData->assistPlayers);
}

local void ClearGameStats(StatsArenaData *arenaData)
{
	for (int i = 0; i < STATS_MAX_TEAMS; i++)
	{
		if (!arenaData->gameStats[i])
			continue;

		Link *link;
		PlayerStatsData *playerData;
		FOR_EACH(arenaData->gameStats[i], playerData, link)
		{
			afree(playerData->name);
		}
		LLClear(arenaData->gameStats[i]);
	}
}

local void ReleaseArenaData(Arena *arena)
{
	StatsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	ClearGameStats(arenaData);
	for (int i = 0; i < STATS_MAX_TEAMS; i++)
	{
		if (arenaData->gameStats[i])
			LLFree(arenaData->gameStats[i]);
	}

	ClearAssists(arenaData);
	if (arenaData->assistPlayers)
		LLFree(arenaData->assistPlayers);
}

		/************************************************************************************************
		 *					HELPER COMMAND SECTION					*
		 ************************************************************************************************/
local PlayerStatsData * InitPlayerStatsData(StatsArenaData *arenaData, char *playerName, int frequency)
{
	int teamFreq = frequency % STATS_MAX_TEAMS;

	PlayerStatsData *playerData	= amalloc(sizeof(PlayerStatsData));

	int len				= strlen(playerName) + 1;
	playerData->name		= amalloc(len);
	strncpy(playerData->name, playerName, len-1);

	playerData->nearBall		= FALSE;
	playerData->inShip		= TRUE;
	playerData->enteredTick		= current_ticks();
	playerData->pickupTick		= 0;
	playerData->hasBall		= FALSE;

	if (!arenaData->gameStats[teamFreq])
		arenaData->gameStats[teamFreq] = LLAlloc();

	LLAdd( arenaData->gameStats[teamFreq], playerData);

	return playerData;
}

local PlayerStatsData * GetPlayerStatsRecord(StatsArenaData *arenaData, char *playerName, int frequency, bool initialize)
{
	int teamFreq = frequency % STATS_MAX_TEAMS;

	if (arenaData->gameStats[teamFreq])
	{
		Link * next	= LLGetHead(arenaData->gameStats[teamFreq]);
		while (next)
		{
			if ((next->data))
				if (strcasecmp(((PlayerStatsData *)next->data)->name, playerName) == 0)
					return (PlayerStatsData *)next->data;
			next	= next->next;
		}
	}
	if (initialize)
		return InitPlayerStatsData(arenaData, playerName, frequency);
	return NULL;
}

/*
local bool RemovePlayerStats(StatsArenaData *arenaData, char *name, int frequency)
{
	int teamFreq = frequency % STATS_MAX_TEAMS;
	if (arenaData->gameStats[teamFreq] != NULL)
	{
		Link *link;
		PlayerStatsData *playerData;
		FOR_EACH(arenaData->gameStats[teamFreq], playerData, link)
		{
			//find old reference to remove it.
			if (strcasecmp(playerData->name, name) == 0)
			{
				if (LLRemove( arenaData->gameStats[teamFreq], playerData))
					return TRUE;
			}
		}
	}
	return FALSE;
}
*/

local void RoundStart(StatsArenaData *arenaData, Arena *arena)
{
	Player *player		= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->p_ship != SHIP_SPEC)
		{
			//ensure player is setup on this freq for stats and balancing
			GetPlayerStatsRecord(arenaData, player->name, player->p_freq, TRUE);
		}
	}
	pd->Unlock();
}

local void AddAssistPlayer(StatsArenaData *arenaData, char *name, int frequency)
{
	lm->Log(L_INFO, "<pbstats> Add Assists!! %d vs %d - %s", arenaData->assistFrequency, frequency, name );

	if (frequency != arenaData->assistFrequency)
	{
		ClearAssists(arenaData);
		arenaData->assistFrequency = frequency;
	}
	lm->Log(L_INFO, "<pbstats> Add Assists Passed Freq test!! %d - %s", frequency, name );

	//check for existing assist in list
	Link *link;
	AssistPlayer *assistPlayer;
	bool found = FALSE;
	FOR_EACH(arenaData->assistPlayers, assistPlayer, link)
	{
		if (!strcasecmp(assistPlayer->name, name))
		{
			if (assistPlayer->frequency == arenaData->assistFrequency)
			{
				assistPlayer->passedTick	= current_ticks();
				found = TRUE;
			}
			else
				//we're from a different freq .. ie changed teams before
				LLRemove(arenaData->assistPlayers, assistPlayer);
			break;
		}
	}
	if (found)
		return;

	AssistPlayer *newAssistPlayer = amalloc(sizeof(AssistPlayer));
	newAssistPlayer->passedTick	= current_ticks();
	newAssistPlayer->frequency	= frequency;
	int len				= strlen(name)+1;
	newAssistPlayer->name		= amalloc(len);
	strncpy(newAssistPlayer->name, name, len-1);

	LLAdd(arenaData->assistPlayers, newAssistPlayer);
}

local int SortByPassedTick(const void *a, const void *b)
{
	const AssistPlayer *aa = (AssistPlayer *)a;
	const AssistPlayer *bb = (AssistPlayer *)b;

	return TICK_GT(aa->passedTick, bb->passedTick);
}

local char * ProcessAssists(StatsArenaData *arenaData, Player *goalScorer)
{
	lm->Log(L_INFO, "<pbstats> Process Assists!!" );

	char *players = NULL;

	int count = LLCount(arenaData->assistPlayers);
	if (count < 1)
	{
		arenaData->assistFrequency = -1;
		return NULL;
	}
	if (count > 1)
		LLSort(arenaData->assistPlayers, SortByPassedTick);

	bool first = TRUE;
	int currentTick = arenaData->lastReleasedTick;
	Link *link;
	AssistPlayer *assistPlayer;
	arenaData->numAssists = 0;
	FOR_EACH(arenaData->assistPlayers, assistPlayer, link)
	{
		if (!strcasecmp(assistPlayer->name, goalScorer->name))
			continue;

		if (assistPlayer->frequency != goalScorer->p_freq)
			continue;

		PlayerStatsData *playerData = GetPlayerStatsRecord(arenaData, assistPlayer->name, assistPlayer->frequency, FALSE);
		if (!playerData)
		{
			lm->Log(L_INFO, "<pbstats> Process Assists!! %d - %s", assistPlayer->frequency, assistPlayer->name );

			continue;
		}

		int delta = TICK_DIFF(currentTick, assistPlayer->passedTick);

		lm->Log(L_INFO, "<pbstats> Process Assists!! %d vs %d", delta, ASSIST_TIMEOUT );

		if (delta <= ASSIST_TIMEOUT)
		{
			playerData->assists++;

			if (first)
			{
				first = FALSE;
				players = realloc(players, strlen(assistPlayer->name) + 1);
				sprintf(players, "%s", assistPlayer->name);
			}
			else
			{
				players = realloc(players, strlen(players) + strlen(assistPlayer->name) + 1 + 2);
				sprintf(players, "%s, %s", players, assistPlayer->name);
			}
			arenaData->numAssists++;
		}
	}
	ClearAssists(arenaData);

	arenaData->assistFrequency = -1;
	return players;
}

		/************************************************************************************************
		 *					COMMANDS SECTION					*
		 ************************************************************************************************/

/****************************************************************
 *			HELP COMMANDS				*
 ****************************************************************/
local helptext_t pbstatshelp_help =
"Module: pbstats\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module available commands.";

local void CPbStatsHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);
}

local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;

	chat->SendMessage(player, "-----------------------------------------------------");
	chat->SendMessage(player, "The following PB Stats Module commands are available:");
	chat->SendMessage(player, "-----------------------------------------------------");

	DisplayCommand(chat, player, "?chart",		"Display the current Stats Chart");
	DisplayCommand(chat, player, "?charthelp",	"Display details of what the Chart columns mean");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_resetstats",	"?resetstats",	"Resets the stats recording for this game.");
}

/****************************************************************
 *			VERSION					*
 ****************************************************************/
local helptext_t pbstatsversion_help =
"Module: pbstats\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module version information.";

local void CPbStatsVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS PB Stats Module -- Version: 1.0    Date: 26 September 2021    Author: POiD");
}

/****************************************************************
 *			CHART					*
 ****************************************************************/
local helptext_t chart_help =
"Module: pbstats\n"
"Targets: none\n"
"Args: none\n"
"Displays current game Stats Chart.";

local void DisplayChart(StatsArenaData *arenaData, Arena *arena, Player *player);

local void CChart(const char *command, const char *params, Player *player, const Target *target)
{
	StatsArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	DisplayChart(arenaData, player->arena, player);
}

/****************************************************************
 *			CHART HELP				*
 ****************************************************************/
local helptext_t charthelp_help =
"Module: pbstats\n"
"Targets: none\n"
"Args: none\n"
"Display descriptions of pb stats chart fields.";

local void CChartHelp(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "STAT   RATING POINTS   DESCRIPTION");
	chat->SendMessage(player, "----   -------------   -----------");
	chat->SendMessage(player, "  RA                   Current rating");
	chat->SendMessage(player, " RPM                   Rating per minute of play");
	chat->SendMessage(player, "   G         7         Goals scored");
	chat->SendMessage(player, "   A         5         Assists       [last passer before a Goal is scored]");
	chat->SendMessage(player, "   K         2         Kills");
	chat->SendMessage(player, "  BK         3         Ball Kills    [Killing enemy player with the ball]");
	chat->SendMessage(player, "             1         Far Kills     [Killing enemy player far from the ball]");
	chat->SendMessage(player, "  TK        -1         Team Kills    [Killing members of your own team]");
	chat->SendMessage(player, "   D        -1         Deaths");
	chat->SendMessage(player, "   B        -2         Ball Deaths   [Deaths while carrying the ball]");
	chat->SendMessage(player, "   S         4         Steals        [Stealing the ball from opposition]");
	chat->SendMessage(player, "   T        -3         Turn Overs    [Losing the ball to the opposition]");
	chat->SendMessage(player, "   V         5         Saves         [Catching ball from enemy in goal]");
	chat->SendMessage(player, "   W         2         Spawns.       [Gathering ball from center after a goal]");
	chat->SendMessage(player, "  BC         0.1       Ball Carries  [Times with the ball, Capped at Ball Time / 5]");
	chat->SendMessage(player, "  NT         0.1       Near Time     [Time near the ball]");
}

/****************************************************************
 *			RESET STATS				*
 ****************************************************************/
local helptext_t resetstats_help =
"Module: pbstats\n"
"Targets: none\n"
"Args: none\n"
"Reset the Stats for the current game.";

local void CResetStats(const char *command, const char *params, Player *player, const Target *target)
{
	StatsArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	ClearGameStats(arenaData);

	chat->SendArenaSoundMessage(player->arena, 1, "Game Stats reset by %s", player->name);
}

local void ResetStats(Arena *arena)
{
	StatsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	ClearGameStats(arenaData);
}

		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/

/****************************************************************
 *			SHIP FREQ CHANGE			*
 ****************************************************************/
local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq)
{
	lm->Log(L_INFO, "<pbstats> %s freq ship change %d:%d %d:%d", player->name, oldShip, newShip, oldFreq, newFreq );

	StatsArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->roundStart)
		return;

	//player specced so update playingTime
	if (newShip == SHIP_SPEC)
	{
		if (oldShip != SHIP_SPEC)
		{
			PlayerStatsData *playerData	= GetPlayerStatsRecord(arenaData, player->name, oldFreq, FALSE);
			if (playerData)
			{
				if (playerData->inShip)
				{
					playerData->playingTime	+= TICK_DIFF(current_ticks(), playerData->enteredTick);
					playerData->inShip	= FALSE;
				}
				else
				{
					lm->Log(L_WARN, "<pbstats> player previously not spec but not showing 'in ship' %s", player->name);
				}
			}
		}
	}
	else
	{
	/*	New Ship is Not SPEC  */

		PlayerStatsData *playerData;

		if (oldFreq != newFreq)
		{
			playerData = GetPlayerStatsRecord(arenaData, player->name, oldFreq, FALSE);
			if ((playerData) && (playerData->inShip))
			{
				playerData->playingTime	+= TICK_DIFF(current_ticks(), playerData->enteredTick);
				playerData->inShip	= FALSE;
			}

			playerData = GetPlayerStatsRecord(arenaData, player->name, newFreq, TRUE);
			playerData->enteredTick	= current_ticks();
			playerData->inShip	= TRUE;
		}
		else
		{
			playerData = GetPlayerStatsRecord(arenaData, player->name, newFreq, TRUE);
			if ((playerData) && (!playerData->inShip))
			{
				playerData->enteredTick	= current_ticks();
				playerData->inShip	= TRUE;
			}
		}
	}
}

/****************************************************************
 *			BALL PICKUP				*
 ****************************************************************/
local void BallPickup(Arena *arena, Player *player, int ballID)
{
	lm->Log(L_INFO, "<pbstats> %s ball pickup", player->name );

	StatsArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
	{
		arenaData->activeBallID	= ballID;
		RoundStart(arenaData, arena);
	}

	PlayerStatsData *playerData	= GetPlayerStatsRecord(arenaData, player->name, player->p_freq, TRUE);
	playerData->carries++;
	arenaData->roundStart	= TRUE;

	if (arenaData->goalScored)
	{
		arenaData->goalScored	= FALSE;
		playerData->spawns++;
	}

	//shouldn't happen but just in case
	if (arenaData->playerWithBall)
	{
		//Turn over checks
		if (arenaData->playerWithBall != player)
		{
			PlayerStatsData *last	= GetPlayerStatsRecord(arenaData, arenaData->playerWithBall->name, arenaData->playerWithBall->p_freq, FALSE);
			if ((last) && (last->hasBall))
			{
				unsigned int carriedTime= TICK_DIFF(current_ticks(), playerData->pickupTick);
				playerData->carriesTime	+= carriedTime;
				lm->Log(L_INFO, "<pbstats> ball Fire: %s : %d (%d) %d - %d", playerData->name, carriedTime, playerData->hasBall, playerData->pickupTick, current_ticks());

				if (arenaData->lastWithBall != arenaData->playerWithBall)
					last->turnover++;
				last->hasBall		= FALSE;
			}
		}
	}

	//assist list
	//lm->Log(L_INFO, "<pbstats> Ball Pickup!! %d - %s - %p", player->p_freq, player->name, arenaData->lastWithBall );

	if (arenaData->lastWithBall)
	{
		if (arenaData->lastWithBall != player)
		{
			AddAssistPlayer(arenaData, arenaData->lastWithBall->name, player->p_freq);

			if (arenaData->lastWithBall->p_freq != player->p_freq)
			{
				if (TICK_DIFF(current_ticks(), arenaData->lastReleasedTick) < STEAL_TIMEOUT)
				{
					bool isSave = FALSE;

					if ((arenaData->goal0) && (arenaData->goal1))
					{
						if (player->p_freq % STATS_MAX_TEAMS == 0)
						{
							if (mapdata->Contains(arenaData->goal0, player->position.x / 16, player->position.y / 16))
							{
								isSave = TRUE;
								playerData->saves++;
								chat->SendArenaMessage(arena, "Save by %s!", player->name);
								if (arenaData->possibleChoke)
									chat->SendArenaMessage(arena, "Choke by %s.", arenaData->lastWithBall->name);
							}
						}
						else
						{
							if (mapdata->Contains(arenaData->goal1, player->position.x / 16, player->position.y / 16))
							{
								isSave = TRUE;
								playerData->saves++;
								chat->SendArenaMessage(arena, "Save by %s!", player->name);
								if (arenaData->possibleChoke)
									chat->SendArenaMessage(arena, "Choke by %s.", arenaData->lastWithBall->name);
							}
						}
					}

					if (!isSave)
					{
						playerData->steals++;

						PlayerStatsData *last	= GetPlayerStatsRecord(arenaData, arenaData->lastWithBall->name, arenaData->lastWithBall->p_freq, FALSE);
						if (last)
						{
							chat->SendArenaMessage(arena, "%s by %s.", arenaData->possibleChoke ? "Choke" : "Turnover", last->name);
							last->turnover++;
						}
						chat->SendArenaMessage(arena, "Steal by %s!", player->name);
					}
				}
			}
		}
	}
	arenaData->playerWithBall	= player;
	arenaData->lastWithBall		= player;
	playerData->hasBall		= TRUE;
	playerData->pickupTick		= current_ticks();
}

/****************************************************************
 *			BALL FIRE				*
 ****************************************************************/
local void BallFire(Arena *arena, Player *player, int ballID)
{
	lm->Log(L_INFO, "<pbstats> %s fired ball", player->name );

	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	arenaData->lastReleasedTick	= current_ticks();
	arenaData->lastWithBall		= player;

	if (arenaData->playerWithBall != player)
	{
		arenaData->playerWithBall	= NULL;
		return;
	}

	arenaData->playerWithBall	= NULL;

	PlayerStatsData * playerData	= GetPlayerStatsRecord(arenaData, player->name, player->p_freq, FALSE);
	if (!playerData)
		return;

	if (playerData->hasBall)
	{
		unsigned int carriedTime= TICK_DIFF(current_ticks(), playerData->pickupTick);
		playerData->carriesTime	+= carriedTime;
		lm->Log(L_INFO, "<pbstats> ball Fire: %s : %d (%d) %d - %d", playerData->name, carriedTime, playerData->hasBall, playerData->pickupTick, current_ticks());
		playerData->hasBall	= FALSE;

		bool isChoke = FALSE;

		if ((arenaData->goal0) && (arenaData->goal1))
		{
			if (player->p_freq % STATS_MAX_TEAMS == 0)
			{
				if (mapdata->Contains(arenaData->goal1, player->position.x / 16, player->position.y / 16))
				{
					isChoke = TRUE;
				}
			}
			else
			{
				if (mapdata->Contains(arenaData->goal0, player->position.x / 16, player->position.y / 16))
				{
					isChoke = TRUE;
				}
			}
		}

		arenaData->possibleChoke = isChoke;
	}
}

/****************************************************************
 *			BALL HOLDER KILLED			*
 ****************************************************************/
local void BallHolderKilled(Arena *arena, Player *killer, Player *killed, int ballID)
{
	lm->Log(L_INFO, "<pbstats> %s ball holder killed by %s", killed->name, killer->name );

	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
		return;

	PlayerStatsData * killerData	= GetPlayerStatsRecord(arenaData, killer->name, killer->p_freq, TRUE);
	if (!killerData)
	{
		lm->LogA(L_DRIVEL, "pbstats", arena, "Killer Data null.");
		return;
	}
	PlayerStatsData * killedData 	= GetPlayerStatsRecord(arenaData, killed->name, killed->p_freq, TRUE);
	if (!killedData)
	{
		lm->LogA(L_DRIVEL, "pbstats", arena, "Killed Data null.");
		return;
	}

	if (arenaData->playerWithBall != killed)
		return;

	if (killedData->hasBall)
	{
		unsigned int carriedTime= TICK_DIFF(current_ticks(), killedData->pickupTick);
		killedData->carriesTime	+= carriedTime;
		lm->Log(L_INFO, "<pbstats> ball holder killed: %s : %d (%d) %d - %d", killedData->name, carriedTime, killedData->hasBall, killedData->pickupTick, current_ticks());
		killedData->hasBall	= FALSE;
	}

	arenaData->lastReleasedTick	= current_ticks();
	arenaData->lastWithBall		= killed;
	arenaData->ballKilledPlayer	= killed;
	arenaData->playerWithBall	= NULL;

	bool isChoke = FALSE;
        if ((arenaData->goal0) && (arenaData->goal1))
        {
        	if (killed->p_freq % STATS_MAX_TEAMS == 0)
                {
        	        if (mapdata->Contains(arenaData->goal1, killed->position.x / 16, killed->position.y / 16))
                        {
                	        isChoke = TRUE;
                        }
                }
                else
                {
                        if (mapdata->Contains(arenaData->goal0, killed->position.x / 16, killed->position.y / 16))
                        {
                                isChoke = TRUE;
                        }
                }
        }
        arenaData->possibleChoke = isChoke;

	if (killer->p_freq != killed->p_freq)
	{
		killerData->ballKills++;
		killedData->ballDeaths++;
	}
	else
	{
		killerData->teamKills++;
		killedData->deaths++;
	}
}

/****************************************************************
 *			PLAYER KILLED				*
 ****************************************************************/
local long GetSqrDistance(Player *player, struct BallData *ball);

local void PlayerKilled(Arena *arena, Player *killer, Player *killed, int bounty, int flags, int *pts, int *green)
{
	lm->Log(L_INFO, "<pbstats> %s killed by %s", killed->name, killer->name );

	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
		return;

	//already processed
	if (arenaData->ballKilledPlayer == killed)
	{
		arenaData->ballKilledPlayer = NULL;
		return;
	}

	PlayerStatsData * killerData	= GetPlayerStatsRecord(arenaData, killer->name, killer->p_freq, TRUE);
	if (!killerData)
	{
		lm->LogA(L_DRIVEL, "pbstats", arena, "Killer Data null.");
		return;
	}
	PlayerStatsData * killedData 	= GetPlayerStatsRecord(arenaData, killed->name, killed->p_freq, TRUE);
	if (!killedData)
	{
		lm->LogA(L_DRIVEL, "pbstats", arena, "Killed Data null.");
		return;
	}

	if (killer->p_freq == killed->p_freq)
		killerData->teamKills++;
	else
	{
		ArenaBallData *abd = balls->GetBallData(arena);
		struct BallData *ball = &abd->balls[arenaData->activeBallID];

		if (GetSqrDistance(killed, ball) > NEARBALLSQRDISTANCE)
			killerData->farKills++;
		else
			killerData->kills++;

		balls->ReleaseBallData(arena);
	}
	killedData->deaths++;
}

/****************************************************************
 *			GOAL SCORED				*
 ****************************************************************/
local void GoalScored(Arena *arena, Player *player, int ballID, int x, int y)
{
	lm->Log(L_INFO, "<pbstats> %s scored goal", player->name );

	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	Player *j;
	LinkedList teamset = LL_INITIALIZER;
	Link *link;

	arenaData->goalScored	= TRUE;

	char * assists = ProcessAssists(arenaData, player);
	if (assists)
	{
		pd->Lock();
		FOR_EACH_PLAYER(j)
		{
			if (j->status == S_PLAYING &&
				    j->arena == arena)
			{
				LLAdd(&teamset, j);
			}
		}
		pd->Unlock();

		chat->SendSetMessage(&teamset, "Assist%s By: %s", arenaData->numAssists > 1 ? "s" : "", assists);
		LLEmpty(&teamset);

		afree(assists);
	}
	arenaData->playerWithBall	= NULL;
	arenaData->lastWithBall		= NULL;
	arenaData->ballKilledPlayer	= NULL;

	PlayerStatsData * playerData 	= GetPlayerStatsRecord(arenaData, player->name, player->p_freq, TRUE);
	if (!playerData)
		return;
	playerData->goals++;
}

/****************************************************************
 *			PLAYER ACTION				*
 ****************************************************************/
local void RemovePlayer(Arena *arena, Player *player);

local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<pbstats> %s Player Action %d", player->name, action );

	switch (action)
	{
		case PA_DISCONNECT:
		case PA_LEAVEARENA:
			RemovePlayer(arena, player);
		break;
	}
}

local void RemovePlayer(Arena *arena, Player *player)
{
	StatsArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

	if (arenaData->playerWithBall == player)
	{
		arenaData->playerWithBall = NULL;
		arenaData->lastReleasedTick = current_ticks();
	}

	if (arenaData->lastWithBall == player)
		arenaData->lastWithBall = NULL;
	if (arenaData->ballKilledPlayer == player)
		arenaData->ballKilledPlayer = NULL;
}


/****************************************************************
 *			NEW PLAYER				*
 ****************************************************************/
local void NewPlayer(Player *player, int isnew)
{
	lm->Log(L_INFO, "<pbstats> %s new player %d", player->name, isnew);

	if (!isnew)
	{
		RemovePlayer(player->arena, player);
	}
}

/****************************************************************
 *			GAME START				*
 ****************************************************************/
local void GameStart(Arena *arena)
{
	lm->Log(L_INFO, "<pbstats> Game Start" );

	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	RoundStart(arenaData, arena);
}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local void GameOver(Arena *arena)
{
	StatsArenaData * arenaData	= P_ARENA_DATA(arena, arenaKey);

	lm->Log(L_INFO, "<pbstats> Game Over");

	DisplayChart(arenaData, arena, NULL);
	ClearGameStats(arenaData);

	arenaData->playerWithBall	= NULL;
	arenaData->lastWithBall		= NULL;
	arenaData->ballKilledPlayer	= NULL;

	arenaData->roundStart		= FALSE;
	arenaData->goalScored		= TRUE;
	arenaData->activeBallID		= -1;
	arenaData->lastReleasedTick	= 0;
	arenaData->gameTime		= 0;
}


		/************************************************************************************************
		 *					CHART DISPLAY SECTION					*
		 ************************************************************************************************/

/********************************************************
 *			ADD MVP NAME			*
 ********************************************************/
local void AddMVPName(int *max, int cur, char *mvpname, char *player)
{
	if (cur > *max)
	{
		memset(mvpname, 0, MVPNAMELEN);
		strcpy(mvpname, player);
		*max = cur;
	}
	else if ((cur == *max) && (cur != 0))
	{
		int len = strlen(mvpname);
		int lennew = strlen(player);

		if ((len+lennew+2+1) <= MVPNAMELEN)
		{
			if (len > 0)
				strcat(mvpname, ", ");
			strcat(mvpname, player);
		}
	}
}

/********************************************************
 *			CALCULATE RATING		*
 ********************************************************/
local void CalculateRating(PlayerStatsData *playerData)
{
	int rating =  	(playerData->goals * 7) +
			(playerData->assists * 5) +
			(playerData->kills * 2) +
			(playerData->ballKills * 3) +
			(playerData->farKills * 1) +
			(playerData->teamKills * -1) +
			(playerData->deaths * -1) +
			(playerData->ballDeaths * -2) +
			(playerData->steals * 4) +
			(playerData->turnover * -3) +
			(playerData->saves * 5) +
			(playerData->spawns * 2) +
			(playerData->carriesTime / 100 * 0.1) +
			(playerData->nearTime * 0.1);		//already in seconds

	int bc = (playerData->carries * 0.5f);
	int carTime = (int)playerData->carriesTime / 100 / 5;

	if (bc > carTime)
		bc = carTime;

	rating	+= bc;

	playerData->rating = rating;

	int playingTime		= playerData->playingTime;
	if (playerData->inShip)
		playingTime	+= TICK_DIFF(current_ticks(), playerData->enteredTick);

	//RPM, per minute not seconds
	float playing = playingTime / 100.0f / 60.0f;
	if (playing > 0)
		playerData->rpm	= rating / playing;
}

/********************************************************
 *			FREQ TOTALS			*
 ********************************************************/
local void FreqTotals(FreqStats *freqTotal, PlayerStatsData *data, GameMVPs *MVP)
{
	int kills = data->kills + data->farKills + data->ballKills;
	int deaths = data->deaths + data->ballDeaths;

	CalculateRating(data);
	freqTotal->rating	+= data->rating;
	freqTotal->goals	+= data->goals;
	freqTotal->assists	+= data->assists;
	freqTotal->kills	+= kills;
	freqTotal->ballKills	+= data->ballKills;
	freqTotal->teamKills	+= data->teamKills;
	freqTotal->deaths	+= deaths;
	freqTotal->ballDeaths	+= data->ballDeaths;
	freqTotal->steals	+= data->steals;
	freqTotal->turnover	+= data->turnover;
	freqTotal->saves	+= data->saves;
	freqTotal->spawns	+= data->spawns;
	freqTotal->carries	+= data->carries;

	AddMVPName(&MVP->goals,		data->goals,		MVP->goalsNames,	data->name);
	AddMVPName(&MVP->assists,	data->assists,		MVP->assistsNames,	data->name);
	AddMVPName(&MVP->kills,		kills,			MVP->killsNames,	data->name);
	AddMVPName(&MVP->deaths,	deaths,			MVP->deathsNames,	data->name);
	AddMVPName(&MVP->steals,	data->steals,		MVP->stealsNames,	data->name);
	AddMVPName(&MVP->spawns,	data->spawns,		MVP->spawnsNames,	data->name);
	AddMVPName(&MVP->carries,	data->carries,		MVP->carriesNames,	data->name);
	AddMVPName(&MVP->time,		data->carriesTime,	MVP->timeNames,		data->name);
	AddMVPName(&MVP->near,		data->nearTime,		MVP->nearNames,		data->name);
	AddMVPName(&MVP->rating,	data->rating,		MVP->ratingNames,	data->name);
}

/********************************************************
 *			PLAYER STATS DISPLAY		*
 ********************************************************/
local void PlayerStatsDisplay(bool first, char *line, PlayerStatsData *data)
{
	if (first)
	{
		if (!data)
			sprintf(line, "%78s", "");
		else
		{
				//24 	         +4   +5  +3   +3  +4  +4  +4  +4  +4  +4  +4  +4  +3  +4 = 78
				//"WARBIRDS      RA  RPM   G    A   K  BK  TK   D  BD   S   T   V   W  BC
			sprintf(line, "%-24s %3d %4.1f %2d %2d %3d %3d %3d %3d %3d %3d %3d %3d %2d %3d",
					data->name,
					data->rating,
					data->rpm,
					data->goals,
					data->assists,
					data->kills + data->farKills + data->ballKills,
					data->ballKills,
					data->teamKills,
					data->deaths + data->ballDeaths,
					data->ballDeaths,
					data->steals,
					data->turnover,
					data->saves,
					data->spawns,
					data->carries
				);
		}
		return;
	}

	if (data)
	{
			//24 	         +4   +5  +3   +3  +4  +4  +4  +4  +4  +4  +4  +4  +3  +4 = 78
			//"WARBIRDS      RA  RPM   G    A   K  BK  TK   D  BD   S   T   V   W  BC
		sprintf(line, "%s  %-24s %3d %4.1f %2d %2d %3d %3d %3d %3d %3d %3d %3d %3d %2d %3d",
				line,
				data->name,
				data->rating,
				data->rpm,
				data->goals,
				data->assists,
				data->kills + data->farKills + data->ballKills,
				data->ballKills,
				data->teamKills,
				data->deaths + data->ballDeaths,
				data->ballDeaths,
				data->steals,
				data->turnover,
				data->saves,
				data->spawns,
				data->carries
			);
	}
}

/********************************************************
 *			FREQ STATS DISPLAY		*
 ********************************************************/
local void FreqStatsDisplay(bool first, char *line, FreqStats *data)
{
	if (first)
		sprintf(line, "%-24s %3d %4s %2d %2d %3d %3d %3d %3d %3d %3d %3d %3d %2d %3d",
				"TOTAL",
				data->rating,
				"",
				data->goals,
				data->assists,
				data->kills,
				data->ballKills,
				data->teamKills,
				data->deaths,
				data->ballDeaths,
				data->steals,
				data->turnover,
				data->saves,
				data->spawns,
				data->carries
			);
	else
		sprintf(line, "%s  %-24s %3d %4s %2d %2d %3d %3d %3d %3d %3d %3d %3d %3d %2d %3d",
				line,
				"TOTAL",
				data->rating,
				"",
				data->goals,
				data->assists,
				data->kills,
				data->ballKills,
				data->teamKills,
				data->deaths,
				data->ballDeaths,
				data->steals,
				data->turnover,
				data->saves,
				data->spawns,
				data->carries
			);

}

/********************************************************
 *		SORT STATS BY RATING			*
 ********************************************************/
local int SortStatsByRating(const void *a, const void *b)
{
	const PlayerStatsData *aa = (PlayerStatsData *)a;
	const PlayerStatsData *bb = (PlayerStatsData *)b;

	return aa->rating >= bb->rating;
}

/********************************************************
 *			DISPLAY CHART			*
 ********************************************************/
local void DisplayChart(StatsArenaData *arenaData, Arena *arena, Player *player)
{
	if ((!arenaData->gameStats[0]) || (!arenaData->gameStats[1]))
	{
		if (player)
			chat->SendMessage(player, "Stats Chart is currently unavailable.");
		return;
	}

	FreqStats freqTotal[STATS_MAX_TEAMS];
	for (int i = 0; i < STATS_MAX_TEAMS; i++)
	{
		freqTotal[i].rating	= 0;
		freqTotal[i].goals	= 0;
		freqTotal[i].assists	= 0;
		freqTotal[i].kills	= 0;
		freqTotal[i].ballKills	= 0;
		freqTotal[i].teamKills	= 0;
		freqTotal[i].deaths	= 0;
		freqTotal[i].ballDeaths	= 0;
		freqTotal[i].steals	= 0;
		freqTotal[i].turnover	= 0;
		freqTotal[i].saves	= 0;
		freqTotal[i].spawns	= 0;
		freqTotal[i].carries	= 0;
	}
	GameMVPs MVP;
	memset(&MVP, 0, sizeof(GameMVPs));

	if (player)
	{
		chat->SendMessage(player, "WARBIRDS                  RA  RPM  G  A   K  BK  TK   D  BD   S   T   V  W  BC  JAVS                      RA  RPM  G  A   K  BK  TK   D  BD   S   T   V  W  BC");
		chat->SendMessage(player, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");
	}
	else
	{
		chat->SendArenaMessage(arena, "WARBIRDS                  RA  RPM  G  A   K  BK  TK   D  BD   S   T   V  W  BC  JAVS                      RA  RPM  G  A   K  BK  TK   D  BD   S   T   V  W  BC");
		chat->SendArenaMessage(arena, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");
	}

	Link * nextFreq0	= LLGetHead(arenaData->gameStats[0]);
	Link * nextFreq1	= LLGetHead(arenaData->gameStats[1]);

	while ((nextFreq0) || (nextFreq1))
	{
		PlayerStatsData * freq0		= NULL;
		PlayerStatsData * freq1		= NULL;

		if (nextFreq0)
		{
			freq0	= (PlayerStatsData *)nextFreq0->data;
			FreqTotals(&freqTotal[0], freq0, &MVP);
			nextFreq0	= nextFreq0->next;
		}
		if (nextFreq1)
		{
			freq1	= (PlayerStatsData *)nextFreq1->data;
			FreqTotals(&freqTotal[1], freq1, &MVP);
			nextFreq1	= nextFreq1->next;
		}
	}

	LLSort(arenaData->gameStats[0], SortStatsByRating);
	LLSort(arenaData->gameStats[1], SortStatsByRating);

	nextFreq0	= LLGetHead(arenaData->gameStats[0]);
	nextFreq1	= LLGetHead(arenaData->gameStats[1]);

	char line[200];

	while ((nextFreq0) || (nextFreq1))
	{
		PlayerStatsData * freq0		= NULL;
		PlayerStatsData * freq1		= NULL;

		if (nextFreq0)
		{
			freq0	= (PlayerStatsData *)nextFreq0->data;
			nextFreq0= nextFreq0->next;
		}
		if (nextFreq1)
		{
			freq1	= (PlayerStatsData *)nextFreq1->data;
			nextFreq1= nextFreq1->next;
		}

		memset(line, '\0', 200);

		if (freq0)
			PlayerStatsDisplay(TRUE, line, freq0);
		else
			PlayerStatsDisplay(TRUE, line, NULL);

		if (freq1)
			PlayerStatsDisplay(FALSE, line, freq1);
		else
			PlayerStatsDisplay(FALSE, line, NULL);

		if (player)
			chat->SendMessage(player, "%s", line);
		else
			chat->SendArenaMessage(arena, "%s", line);
	}

	if (player)
		chat->SendMessage(player, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");
	else
		chat->SendArenaMessage(arena, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");

	memset(line, '\0', 200);
	FreqStatsDisplay(TRUE, line, &freqTotal[0]);
	FreqStatsDisplay(FALSE, line, &freqTotal[1]);

	if (player)
		chat->SendMessage(player, "%s", line);
	else
		chat->SendArenaMessage(arena, "%s", line);

	if (player)
		chat->SendMessage(player, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");
	else
		chat->SendArenaMessage(arena, "------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---  ------------------------ --- ---- -- -- --- --- --- --- --- --- --- --- -- ---");


	if (player)
	{
		chat->SendMessage(player, "Most Goals:           %2d   %s",	MVP.goals,	MVP.goalsNames);
		if (MVP.assists > 0)
			chat->SendMessage(player, "Most Assists:         %2d   %s",	MVP.assists,	MVP.assistsNames);
		if (MVP.kills > 0)
			chat->SendMessage(player, "Most Kills:          %3d   %s",	MVP.kills,	MVP.killsNames);
		if (MVP.deaths > 0)
			chat->SendMessage(player, "Most Deaths:         %3d   %s",	MVP.deaths,	MVP.deathsNames);
		if (MVP.steals > 0)
			chat->SendMessage(player, "Most Steals:         %3d   %s",	MVP.steals,	MVP.stealsNames);
		chat->SendMessage(player, "Most Spawns:         %3d   %s",	MVP.spawns,	MVP.spawnsNames);
		chat->SendMessage(player, "Most Ball Carries:   %3d   %s",	MVP.carries,	MVP.carriesNames);
		chat->SendMessage(player, "Most Ball Time:    %2d:%02d   %s",	(MVP.time/100)/60, (MVP.time/100)%60, MVP.timeNames);
		if ((MVP.near > 0) && (arenaData->gameTime > 0))
			chat->SendMessage(player, "Most Near Ball:     %4.1f%%  %s",	((MVP.near / (double)arenaData->gameTime) * 100),	MVP.nearNames);
		chat->SendMessage(player, "Highest Rating:      %3d   %s",	MVP.rating,	MVP.ratingNames);
	}
	else
	{
		chat->SendArenaMessage(arena, "Most Goals:           %2d   %s",	MVP.goals,	MVP.goalsNames);
		if (MVP.assists > 0)
			chat->SendArenaMessage(arena, "Most Assists:         %2d   %s",	MVP.assists,	MVP.assistsNames);
		if (MVP.kills > 0)
			chat->SendArenaMessage(arena, "Most Kills:          %3d   %s",	MVP.kills,	MVP.killsNames);
		if (MVP.deaths > 0)
			chat->SendArenaMessage(arena, "Most Deaths:         %3d   %s",	MVP.deaths,	MVP.deathsNames);
		if (MVP.steals > 0)
			chat->SendArenaMessage(arena, "Most Steals:         %3d   %s",	MVP.steals,	MVP.stealsNames);
		chat->SendArenaMessage(arena, "Most Spawns:         %3d   %s",	MVP.spawns,	MVP.spawnsNames);
		chat->SendArenaMessage(arena, "Most Ball Carries:   %3d   %s",	MVP.carries,	MVP.carriesNames);
		chat->SendArenaMessage(arena, "Most Ball Time:    %2d:%02d   %s",	(MVP.time/100)/60, (MVP.time/100)%60, MVP.timeNames);
		if ((MVP.near > 0) && (arenaData->gameTime > 0))
			chat->SendArenaMessage(arena, "Most Near Ball:     %4.1f%%  %s",	((MVP.near / (double)arenaData->gameTime) * 100),	MVP.nearNames);
		chat->SendArenaMessage(arena, "Highest Rating:      %3d   %s",	MVP.rating,	MVP.ratingNames);
	}
}

/****************************************
 * 	Base Timer SECTION		*
 ****************************************/
local int PBBaseTimer(void *vp)
{
	Arena *arena = (Arena *)vp;
	StatsArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
		return TRUE;

	if (arenaData->activeBallID == -1)
		return TRUE;

	ArenaBallData *abd = balls->GetBallData(arena);
	struct BallData *ball = &abd->balls[arenaData->activeBallID];

	bool inGamePlayer	= FALSE;

	Player *player		= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->p_ship != SHIP_SPEC)
		{
			if (!inGamePlayer)
			{
				inGamePlayer = TRUE;
				arenaData->gameTime++;
			}
			PlayerStatsData * playerData 	= GetPlayerStatsRecord(arenaData, player->name, player->p_freq, TRUE);

			if (GetSqrDistance(player, ball) > NEARBALLSQRDISTANCE)
			{
				playerData->nearBall	= FALSE;
			}
			else
			{
				if (playerData->nearBall)
					playerData->nearTime++;
				else
					playerData->nearBall	= TRUE;
			}
		}
	}
	pd->Unlock();

	balls->ReleaseBallData(arena);

	return TRUE;
}

local long GetSqrDistance(Player *player, struct BallData *ball)
{
	register long dx = player->position.x - ball->x;
	register long dy = player->position.y - ball->y;
	dx *= dx;
	dy *= dy;
	return dx+dy;
}


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(mapdata);
	mm->ReleaseInterface(capman);
}

/* The entry point: */
EXPORT int MM_pbstats(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			pd	= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			balls	= mm->GetInterface(I_BALLS,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			ml	= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			mapdata	= mm->GetInterface(I_MAPDATA,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);

			if (!chat || !aman || !pd || !balls || !cmd || !lm || !capman || !ml)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface aman");
					if (!pd)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface pd");
					if (!ml)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface mainloop");
					if (!balls)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface balls");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface cmd");
					if (!capman)
						lm->LogA(L_DRIVEL, "pbstats", arena, "Unable to load interface capman");
				}

				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey = aman->AllocateArenaData(sizeof(StatsArenaData));

				rv = MM_OK;

				if (arenaKey == -1)	//check for valid memory allocation
				{
					ReleaseInterfaces();

					rv = MM_FAIL;
				}
				else
					mm->RegInterface(&_int, ALLARENAS);
			}
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->RegCallback(CB_BALLPICKUP,		BallPickup,		arena);
			mm->RegCallback(CB_BALLFIRE,		BallFire,		arena);
			mm->RegCallback(CB_PGGOAL,		GoalScored,		arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->RegCallback(CB_GAMESTART,		GameStart,		arena);
			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->RegCallback(CB_BALLKILL,		BallHolderKilled,	arena);
			mm->RegCallback(CB_KILL,		PlayerKilled,		arena);
			mm->RegCallback(CB_NEWPLAYER,		NewPlayer,		arena);

			cmd->AddCommand("pbstatshelp",		CPbStatsHelp,	arena,	pbstatshelp_help);
			cmd->AddCommand("pbstatsversion",	CPbStatsVersion,arena,	pbstatsversion_help);
			cmd->AddCommand("chart",		CChart,		arena,	chart_help);
			cmd->AddCommand("charthelp",		CChartHelp,	arena,	charthelp_help);
			cmd->AddCommand("resetstats",		CResetStats,	arena,	resetstats_help);

			InitializeArenaData(arena);

			/* timers */
			ml->SetTimer(PBBaseTimer, 100, 100, arena, arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("pbstatshelp",	CPbStatsHelp,		arena);
			cmd->RemoveCommand("pbstatsversion",	CPbStatsVersion,	arena);
			cmd->RemoveCommand("chart",		CChart,			arena);
			cmd->RemoveCommand("charthelp",		CChartHelp,		arena);
			cmd->RemoveCommand("resetstats",	CResetStats,		arena);

			ml->ClearTimer(PBBaseTimer,		arena);

			mm->UnregCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->UnregCallback(CB_BALLPICKUP,	BallPickup,		arena);
			mm->UnregCallback(CB_BALLFIRE,		BallFire,		arena);
			mm->UnregCallback(CB_PGGOAL,		GoalScored,		arena);
			mm->UnregCallback(CB_GAMEOVER,		GameOver,		arena);
			mm->UnregCallback(CB_GAMESTART,		GameStart,		arena);
			mm->UnregCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->UnregCallback(CB_BALLKILL,		BallHolderKilled,	arena);
			mm->UnregCallback(CB_KILL,		PlayerKilled,		arena);
			mm->UnregCallback(CB_NEWPLAYER,		NewPlayer,		arena);

			if (arenaKey != -1)
				ReleaseArenaData(arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			ReleaseInterfaces();

			if (arenaKey != -1)
			{
				aman->FreeArenaData(arenaKey);
				mm->UnregInterface(&_int, ALLARENAS);
			}

			rv = MM_OK;
		break;
	}
	return rv;
}

