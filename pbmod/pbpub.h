#ifndef __PB_PUB_H__
#define __PB_PUB_H__

#define PUBSPAWNRADIUS		20
#define PUBSPAWNX		512
#define PUBSPAWNY		512
#define PUBPASSDELAY		10
#define PUBWALLPASS		1
#define PUBBOMBALIVE		6000
#define PUBBULLETDAMAGE		250
#define PUBMINEALIVE		4500
#define PUBBURSTDAMAGE		300
#define PUBWARPRADIUS		125
#define PUBENTERDELAY		500
#define PUBBULLETALIVETIME	250
#define PUBBRICKMAX		1
#define PUBREPELMAX		0
#define PUBREPELPRIZE		0
//
#define PUBBURSTMAX		1
#define PUBFIREENERGY1		20
#define PUBFIREENERGY2		30
#define PUBPORTALMAX		1
#define PUBTHROWTIME		700

#define SMLSPAWNRADIUS		3
#define SMLSPAWNX		512
#define SMLSPAWNY		137
#define SMLPASSDELAY		20
#define SMLWALLPASS		0
#define SMLBOMBALIVE		3000
#define SMLBULLETDAMAGE		266
#define SMLMINEALIVE		4500
#define SMLBURSTDAMAGE		266
#define SMLWARPRADIUS		8
#define SMLENTERDELAY		200
#define SMLBULLETALIVETIME	550
#define SMLBRICKMAX		0
#define SMLREPELMAX		1
#define SMLREPELPRIZE		16
//
#define SMLBURSTMAX		0
#define SMLFIREENERGY1		20
#define SMLFIREENERGY2		30
#define SMLPORTALMAX		2
#define SMLTHROWTIME		775

#define MINISPAWNRADIUS		10
#define MINISPAWNX		512
#define MINISPAWNY		256
#define MINISPAWNRADIUS0	15
#define MINISPAWNX0		449
#define MINISPAWNY0		256
#define MINISPAWNRADIUS1	15
#define MINISPAWNX1		574
#define MINISPAWNY1		256
#define MINIPASSDELAY		20
#define MINIWALLPASS		1
#define MINIBOMBALIVE		3000
#define MINIBULLETDAMAGE	266
#define MINIMINEALIVE		4500
#define MINIBURSTDAMAGE		266
#define MINIWARPRADIUS		8
#define MINIENTERDELAY		400
#define MINIBULLETALIVETIME	250
#define MINIBRICKMAX		1
#define MINIREPELMAX		0
#define MINIREPELPRIZE		0
//
#define MINIBURSTMAX		0
#define MINIFIREENERGY1		20
#define MINIFIREENERGY2		30
#define MINIPORTALMAX		2
#define MINITHROWTIME		775

//Soccer:SpawnRadius      = 20
//Soccer:SpawnX           = 512
//Soccer:SpawnY           = 512
//soccer:spawnradius1     = 3
//soccer:spawnx1          = 512
//soccer:spawny1          = 780
//soccer:spawnradius2     = 3
//soccer:spawnx2          = 512
//soccer:spawny2          = 137
//soccer:spawnradius3     = 3
//soccer:spawnx3          = 512
//soccer:spawny3          = 263


typedef enum
{
	PB_GAME_ANY,
	PB_GAME_PUB,
	PB_GAME_PRO,
	PB_GAME_3H,
	PB_GAME_SCRAMBLE,
	PB_GAME_MINI
} PB_gameType;

typedef struct PBPlayerVotingStruct
{
	bool		randomizeVote;
	PB_gameType	changeGameType;

} PBPlayerVoting;

typedef struct MyArenaDataStruct
{
	Target			arenaTarget;

	PB_gameType		gameType;

	bool			randomizeTeamsOnEnd;
	bool			roundStart;
	bool			pubMode;

} MyArenaData;


#define I_PBPUB "pbpub-1"

typedef struct Ipbpub
{
	INTERFACE_HEAD_DECL

	void (*SetTypeAndLock)(Arena *arena, int gameType);
	void (*HelpCommands)(Player *player);
} Ipbpub;

#endif
