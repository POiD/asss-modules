#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "pbextras.h"
#include "scoring/points_goal.h"
#include "balls.h"
#include "pb_utils.c"
#include "pbstats.h"
#include "pbpub.h"
#include "pbleague.h"
#include "teams.h"
#include "signups.h"

// Interfaces
local Imodman *		mm;
local Ichat *		chat;
local Iarenaman *	aman;
local Iballs *		balls;	//x
//local Igame *		game;
local Icmdman *		cmd;
local Ilogman *		lm;
local Ipointsgoals *	pg;
local Icapman *		capman;
local Iplayerdata *	pd;
local Istats *		stats;

local Ipbstats *	pbstats;
local Ipbpub *		pbpub;
local Ipbleague *	pbleague;
local Iteams *		teams;
local Isignups *	signups;


local void HelpCommands(Player *player);

local Ipbextras _int =
{
	INTERFACE_HEAD_INIT(I_PBEXTRAS, "pbextras")
	HelpCommands
};

EXPORT const char info_pbextras[] = CORE_MOD_INFO("pbextras");



/********************************************************
 *			HELP COMMANDS			*
 ********************************************************/
local helptext_t pbhelp_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none\n"
"Display PB Help Modules available commands.";

local void HelpCommands(Player *player);

local void CPbHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);

	if ((pbpub) && (mm->IsModuleAttached("pbpub", player->arena)))
	{
		pbpub->HelpCommands(player);
	}

	if ((teams) && (mm->IsModuleAttached("teams", player->arena)))
	{
		teams->HelpCommands(player);
	}
	if ((signups) && (mm->IsModuleAttached("signups", player->arena)))
	{
		signups->HelpCommands(player);
	}

	if ((pbstats) && (mm->IsModuleAttached("pbstats", player->arena)))
	{
		pbstats->HelpCommands(player);
        }
	if ((pbleague) && (mm->IsModuleAttached("pbleague", player->arena)))
	{
		pbleague->HelpCommands(player);
	}
}



local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;

	chat->SendMessage(player, "------------------------------------------------------");
	chat->SendMessage(player, "The following PB Extras Module commands are available:");
	chat->SendMessage(player, "------------------------------------------------------");
	DisplayCommand(chat, player, "?authhelp [player]", "Send the auth help text to [player] or yourself if no name provided.");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_centerball",	"?centerball [ballID]",	"Center Ball [ballID] (default 0)");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_endgame",		"?endgame",		"Force end the current ball game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_startgame",		"?startgame",		"Froce start the current ball game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_stopgame",		"?stopgame",		"Force stop the current ball game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_scoreresetall",	"?scoreresetall",	"Reset scores of all players in the arena");
}

/****************************************************************
 *			CENTER BALL				*
 ****************************************************************/
local helptext_t centerball_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none or ballID\n"
"Op Command to center ballID (default of 0)";

local void CCenterBall(const char *command, const char *params, Player *player, const Target *target)
{
	int ballID = 0;

	if (*params)
	{
		ballID = atoi(params);
	}

	struct BallData ballData;

	ballData.state		= BALL_ONMAP;
	ballData.carrier	= NULL;
	ballData.freq		= -1;
	ballData.xspeed		= 0;
	ballData.yspeed		= 0;
	ballData.x		= 8200;
	ballData.y		= 8200;
	ballData.time		= current_ticks();

	balls->PlaceBall(player->arena, ballID, &ballData);

	chat->SendArenaMessage(player->arena, "Ball %d centered by %s!", ballID, player->name);
}

/****************************************************************
 *			END GAME				*
 ****************************************************************/
local helptext_t endgame_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none\n"
"Force End a ball game.";

local void CEndGame(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendArenaMessage(player->arena, "Game Ended by %s", player->name);
	balls->EndGame(player->arena);
}

/****************************************************************
 *			START GAME				*
 ****************************************************************/
local helptext_t startgame_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none\n"
"Force End a ball game.";

local void CStartGame(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendArenaMessage(player->arena, "Game Started by %s", player->name);
	
	balls->StartGame(player->arena);
}

/****************************************************************
 *			STOP GAME				*
 ****************************************************************/
local helptext_t stopgame_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none\n"
"Force End a ball game.";

local void CStopGame(const char *command, const char *params, Player *player, const Target *target)
{
	//chat->SendArenaMessage(player->arena, "Game force ended by %s", player->name);
	pg->ResetGame(player->arena, player);

	//stop game handled in teams.c
	//balls->StopGame(player->arena);
}



/****************************************************************
 *			SCORERESETALL				*
 ****************************************************************/
local helptext_t scoreresetall_help =
"Module: pbextras\n"
"Targets: none\n"
"Args: none\n"
"Reset scores of everyone in the arena.";

local void CScoreResetAll(const char *command, const char *params, Player *player, const Target *target)
{
	Link *link;
	Player *curPlayer;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(curPlayer, player->arena)
	{
		stats->ScoreReset(curPlayer, INTERVAL_RESET);
	}
	pd->Unlock();
	stats->SendUpdates(NULL);

	chat->SendArenaMessage(player->arena, "All player scores reset in this arena.");
}


/****************************************************************
 *			AUTH HELP				*
 ****************************************************************/
local helptext_t authhelp_help =
"Module: pbextras\n"
"Targets: none or player\n"
"Args: none or player\n"
"Send Auth help lines to player";

local void SendAuthHelp(Player *player, Player *target);

local void CAuthHelp(const char *command, const char *params, Player *player, const Target *target)
{
	if (target->type == T_PLAYER)
	{
		SendAuthHelp(player, target->u.p);
		return;
	}
	if (!*params)
	{
		SendAuthHelp(NULL, player);
		return;
	}

	Player *targetPlayer = NULL;
	int count = FindPlayerInArenaFuzzy(lm, pd, player->arena, params, &targetPlayer);
	if (count > 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, params);
		return;
	}
	if (count < 1)
	{
		chat->SendMessage(player, "Found %d players matching to %s", count, params);
		return;
	}
	SendAuthHelp(player, targetPlayer);
}

local void SendAuthHelp(Player *player, Player *target)
{
	chat->SendMessage(target, "This server uses the Isometry 2.0 biller that makes use of email authentication.");
	chat->SendMessage(target, "With this email you will be able to change your password if you forget it, without needing a billing ops help.");
	chat->SendMessage(target, "If you entered a proper email in the registration form, check your email for the auth code and then enter:");
	chat->SendMessage(target, "   ?nick auth <code>                (?bman nick auth - for command details)");
	chat->SendMessage(target, "If you entered an invalid email in the registration form, you can fix it using the following command:");
	chat->SendMessage(target, "   ?nick email set <email>          (?bman nick email set - for command details)");
	chat->SendMessage(target, "This will autosend you a new auth code you can then enter using the same command as above. Welcome to Isometry 2.0! ?bman for all biller commands.");

	if (player)
	{
		chat->SendMessage(target, "These help messages were sent by %s", player->name);
		chat->SendMessage(player, "Authentication help has been sent to %s", target->name);

		chat->SendModMessage("NOTICE: %s sent authhelp to %s", player->name, target->name);
	}
}


		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
		return;
	}

	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbpub"))
	{
		pbpub	= mm->GetInterface(I_PBPUB,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbleague"))
	{
		pbleague= mm->GetInterface(I_PBLEAGUE,		ALLARENAS);
		return;
	}
}
/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= NULL;
		return;
	}

	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbpub"))
	{
		pbpub	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbleague"))
	{
		pbleague= NULL;
		return;
	}
}




local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(balls);
	//mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(pg);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(stats);

	mm->ReleaseInterface(pbstats);
	mm->ReleaseInterface(pbpub);
	mm->ReleaseInterface(pbleague);
	mm->ReleaseInterface(teams);
	mm->ReleaseInterface(signups);
}

/* The entry point: */
EXPORT int MM_pbextras(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			balls	= mm->GetInterface(I_BALLS,		ALLARENAS);
			//game	= mm->GetInterface(I_GAME,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			pg	= mm->GetInterface(I_POINTS_GOALS,	ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			pd	= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			stats	= mm->GetInterface(I_STATS,		ALLARENAS);

			pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			pbpub	= mm->GetInterface(I_PBPUB,		ALLARENAS);
			pbleague= mm->GetInterface(I_PBLEAGUE,		ALLARENAS);
			teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
			signups = mm->GetInterface(I_SIGNUPS,		ALLARENAS);

			if (!chat || !aman || !balls || !cmd || !lm || !pg || !capman || !pd || !stats)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface aman");
					if (!balls)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface balls");
					//if (!game)
					//	lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface game");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface cmd");
					if (!pg)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface pg");
					if (!capman)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface capman");
					if (!pd)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface pd");
					if (!stats)
						lm->LogA(L_DRIVEL, "pbextras", arena, "Unable to load interface stats");
				}

				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				mm->RegInterface(&_int, ALLARENAS);

				rv = MM_OK;
			}
		break;

		case MM_ATTACH:

			if (!pbstats)
				pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			if (!pbpub)
				pbpub	= mm->GetInterface(I_PBPUB,		ALLARENAS);
			if (!signups)
				signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			if (!teams)
				teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
			if (!pbleague)
				pbleague= mm->GetInterface(I_PBLEAGUE,		ALLARENAS);

			//mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			//mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			cmd->AddCommand("centerball",		CCenterBall,	arena,	centerball_help);
			cmd->AddCommand("endgame",		CEndGame,	arena,	endgame_help);
			cmd->AddCommand("authhelp",		CAuthHelp,	arena,	authhelp_help);
			cmd->AddCommand("scoreresetall",	CScoreResetAll,	arena,	scoreresetall_help);
			cmd->AddCommand("startgame",		CStartGame,	arena,	startgame_help);
			cmd->AddCommand("stopgame",		CStopGame,	arena,	stopgame_help);

			cmd->AddCommand("pbhelp",		CPbHelp,	arena,	pbhelp_help);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("centerball",	CCenterBall,	arena);
			cmd->RemoveCommand("endgame",		CEndGame,	arena);
			cmd->RemoveCommand("authhelp",		CAuthHelp,	arena);
			cmd->RemoveCommand("scoreresetall",	CScoreResetAll,	arena);
			cmd->RemoveCommand("startgame",		CStartGame,	arena);
			cmd->RemoveCommand("stopgame",		CStopGame,	arena);

			cmd->RemoveCommand("pbhelp",		CPbHelp,	arena);

			//mm->UnregCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			//mm->UnregCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			mm->UnregInterface(&_int, ALLARENAS);

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}
