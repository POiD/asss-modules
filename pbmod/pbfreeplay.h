#ifndef __PB_FREEPLAY_H__
#define __PB_FREEPLAY_H__

typedef struct PBPlayerVotingStruct
{
	bool		randomizeVote;

} PBPlayerVoting;

typedef struct FreeArenaDataStruct
{
	Target			arenaTarget;

	bool			randomizeTeamsOnEnd;
	bool			roundStart;
	bool			pubMode;

} FreeArenaData;


#define I_PBFREEPLAY "pbfreeplay-1"

typedef struct Ipbfreeplay
{
	INTERFACE_HEAD_DECL

	void (*HelpCommands)(Player *player);
} Ipbfreeplay;

#endif
