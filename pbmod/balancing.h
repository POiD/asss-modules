#ifndef __BALANCING_MOD_H__
#define __BALANCING_MOD_H__

typedef enum
{
        PB_ALLOKAY,
        PB_FIRST_WARNING,
        PB_LAST_WARNING,
        PB_TOOLATE
} PB_warningType;

#define TEAM_WARNING            1000

typedef struct BalanceData
{
	Arena *         arena;
	PB_warningType  warningStage;

	LinkedList *	freqDetails;

} BalanceData;


typedef struct FreqDetailsStruct
{
	int		frequency;
	int		count;
	LinkedList *	players;

} FreqDetails;

#define I_BALANCING "balancing-1"

typedef struct Ibalancing
{
        INTERFACE_HEAD_DECL

        void (*HelpCommands)(Player *player);
} Ibalancing;


#endif
