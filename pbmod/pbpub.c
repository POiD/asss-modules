#include <stdlib.h>
#include <string.h>

#include "asss.h"
#include "pbpub.h"
#include "balls.h"
#include "../scoring/points_goal.h"
#include "teams.h"
#include "pb_utils.c"
#include "signups.h"
#include "pbstats.h"
#include "pbextras.h"

// Interfaces
local Imodman *			mm;
local Ichat *			chat;
local Iarenaman *		aman;
local Iplayerdata *		pd;	//x
local Iballs *			balls;	//x
local Imainloop * 		ml;	//x
local Igame *			game;	//x
local Icmdman *			cmd;
local Ilogman *			lm;
local Ipointsgoals *	pg;
local Iconfig *			cfg;
local Iteams *			teams;
local Icapman *			capman;
local Isignups *		signups;
local Ipbstats *		pbstats;
local Ipbextras *		pbextras;


local int 		arenaKey	= -1;
local int		playerKey	= -1;


local void SetTypeAndLock(Arena *arena, int gameType);
local void HelpCommands(Player *player);

local Ipbpub _int =
{
	INTERFACE_HEAD_INIT(I_PBPUB, "pbpub")
	SetTypeAndLock,
	HelpCommands
};

EXPORT const char info_pbpub[] = CORE_MOD_INFO("pbpub");


/****************************************************************
 *		INITIALIZATION SECTION				*
 ****************************************************************/
local void InitializeArenaData(Arena *arena)
{
	MyArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);

	arenaData->roundStart			= FALSE;
	arenaData->gameType				= PB_GAME_PUB;
	arenaData->randomizeTeamsOnEnd	= TRUE;
	arenaData->pubMode				= TRUE;

	Target target;
	target.type=T_ARENA;
	target.u.arena=arena;

	arenaData->arenaTarget			= target;
}

local void ReleaseArenaData(Arena *arena)
{
	//MyArenaData *arenaData = P_ARENA_DATA(arena, arenaKey);
}

		/************************************************************************************************
		 *					HELPER COMMAND SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			VALID GAME FREQ				*
 ****************************************************************/
local bool ValidGameFreq(PB_gameType gameType, int freq)
{
	switch (gameType)
	{
		case PB_GAME_ANY:
		case PB_GAME_3H:
		case PB_GAME_PUB:
		case PB_GAME_PRO:
		case PB_GAME_SCRAMBLE:
		case PB_GAME_MINI:
			if (freq > 1)
				return FALSE;
		break;
		/*
		case 4 player freqs
			if (freq > 3)
				return FALSE;
		break;
		*/
	}
	return TRUE;
}

		/************************************************************************************************
		 *					COMMANDS SECTION					*
		 ************************************************************************************************/

/****************************************************************
 *			HELP COMMANDS				*
 ****************************************************************/
local helptext_t pbhelp_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module available commands.";

local void CPbHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);
}

local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;
	MyArenaData *arenaData		= P_ARENA_DATA(player->arena, arenaKey);

	chat->SendMessage(player, "----------------------------------------------------");
	chat->SendMessage(player, "The following PB Module commands are available:");
	chat->SendMessage(player, "----------------------------------------------------");

	DisplayCommand(chat, player, "?pbhelp",		"Display this help list");
	DisplayCommand(chat, player, "?pbversion",	"Display the PB Module version information");

	if (arenaData->gameType == PB_GAME_SCRAMBLE)
	{
		chat->SendMessage(player, "---------- SCRAMBLE ARENA Commands ------------------");
		DisplayCommand(chat, player, "?rules",			"Display the rules of scramble");
		DisplayCommand(chat, player, "?startgm",		"Start a scramble game!");
		DisplayCommand(chat, player, "?stopgm",			"Pre-maturely stop a scramble game");
	}
	else
	{
		DisplayCommand(chat, player, "?randomize",		"Log your vote for randomizing the teams");
		DisplayCommand(chat, player, "?changegame <opt>",	"Log your vote for changing the Game Type");
		DisplayCommand(chat, player, "",			"<opt> = 2 or 'pub' (Public)");
		DisplayCommand(chat, player, "",			"<opt> = 3 or '3h' (Smallpub3h)");
		DisplayCommand(chat, player, "",			"<opt> = 4 or 'mini' (MiniPB)");
		DisplayCommand(chat, player, "?cg <opt>",		"Alias for changegame. Log your vote for changing the Game Type");
	}

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_randomnow",		"?randomnow",		"Randomize the teams and restart the game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_changenow",		"?changenow",		"Change Game Type and restart the game");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_togglerandom",	"?togglerandom [ON/OFF]","Display or Enabled/Disable if teams are randomized at the end of a game.");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_pubmode",		"?pubmode [ON/OFF]",	"Display or Enable/Disable Pub Mode.");
}

/****************************************************************
 *			VERSION					*
 ****************************************************************/
local helptext_t pbversion_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Display PB Module version information.";

local void CPbVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS PB Module -- Version: 1.2    Date: 10 October 2021    Author: POiD");
}

/****************************************************************
 *			RANDOMIZE				*
 ****************************************************************/
local helptext_t randomize_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Randomize teams with players in game.";

local void RandomizeTeams(Arena *arena);

local void CRandomize(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		chat->SendMessage(player, "Pub Mode is disabled. Cannot randomize teams.");
		return;
	}

	int total		= 0;
	int yesVote		= 0;
	Player *currentPlayer	= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(currentPlayer, player->arena)
	{
		if (currentPlayer->status != S_PLAYING)
			continue;
		
		if (currentPlayer->p_ship != SHIP_SPEC)
		{
			total++;
			PBPlayerVoting *playerData = PPDATA(currentPlayer, playerKey);

			if (currentPlayer == player)
			{
				playerData->randomizeVote = !playerData->randomizeVote;
				chat->SendMessage(player, "Your vote to random teams has been set to %s", playerData->randomizeVote ? "Yes" : "No" );
			}

			if (playerData->randomizeVote)
				yesVote++;
		}
	}
	pd->Unlock();

	if (yesVote > (total / 2))
	{
		RandomizeTeams(player->arena);
		pg->ResetGame(player->arena, player);
	}
	else
	{
		chat->SendMessage(player, "Current votes for randomizing:  For: %d  Against: %d", yesVote, (total-yesVote));
	}
}

local void RandomizeTeams(Arena *arena)
{
	MyArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

	int freqCounts[4]	= { 0, 0, 0, 0};

	int random		= 0;
	int total		= 0;
	int extra		= 0;

	Target target;
	target.type		= T_PLAYER;

	Player *player	= NULL;
	Link *link		= NULL;

	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		PBPlayerVoting *playerData	= PPDATA(player, playerKey);
		if (playerData)
			playerData->randomizeVote	= FALSE;

		if ((player->status == S_PLAYING) && (player->p_ship != SHIP_SPEC))
		{
			total++;
		}
	}
	pd->Unlock();

	switch (arenaData->gameType)
	{
		case PB_GAME_ANY:
		case PB_GAME_3H:
		case PB_GAME_PUB:
		case PB_GAME_PRO:
		case PB_GAME_SCRAMBLE:
		case PB_GAME_MINI:
			extra	= total % 2;
			total	= total / 2;
		break;
		//4 player freqs
		//	totalExtra	= total % 4;
		//	extra		= 1;
		//	total		= total / 4;
		//break;
	}

	if (total != 0)
	{
		pd->Lock();
		player	= NULL;
		link	= NULL;
		FOR_EACH_PLAYER_IN_ARENA(player, arena)
		{
			if (player->status != S_PLAYING)
				continue;
			
			if (player->p_ship != SHIP_SPEC)
			{
				switch (arenaData->gameType)
				{
					case PB_GAME_ANY:
					case PB_GAME_3H:
					case PB_GAME_PUB:
					case PB_GAME_PRO:
					case PB_GAME_SCRAMBLE:
					case PB_GAME_MINI:
						if (freqCounts[1] == total)
						{
							if (player->p_freq != 0)
								game->SetShipAndFreq(player, 0, 0);
							else
							{
								//warp
								target.u.p	= player;
								game->GivePrize(&target, PRIZE_WARP , 1);
							}
						}
						else if (freqCounts[0] == total + extra)
						{
							if (player->p_freq != 1)
								game->SetShipAndFreq(player, 1, 1);
							else
							{
								//warp
								target.u.p	= player;
								game->GivePrize(&target, PRIZE_WARP , 1);
							}
						}
						else
						{
							random = rand() % 2;
							freqCounts[random]++;

							if (player->p_freq != random)
							{
								game->SetShipAndFreq(player, random, random);
							}
							else
							{
								//warp
								target.u.p	= player;
								game->GivePrize(&target, PRIZE_WARP , 1);
							}
						}
					break;
					//case 4 player freqs
					/*
						random = rand() % 4;

						while (freqCounts[random] >= total + extra)
							random = (random + 1) % 4;

						if (freqCounts[random] == total)
						{
							totalExtra--;
							if (totalExtra >= 0)
								extra=0;
						}

						freqCounts[random]++;
						if (p->p_freq != random)
							game->SetShipAndFreq(p, random, random);
					break;
					*/
				}
			}
		}
		pd->Unlock();
		chat->SendArenaMessage(arena, "Teams Randomized!");
	}
}


/****************************************************************
 *			RANDOM NOW				*
 ****************************************************************/
local helptext_t randomnow_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Op Command to randomize teams and restart the game.";

local void CRandomNow(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		chat->SendMessage(player, "Pub Mode is disabled. Cannot randomize teams.");
		return;
	}

	RandomizeTeams(player->arena);
	pg->ResetGame(player->arena, player);
}

/****************************************************************
 *			CHANGE GAME				*
 ****************************************************************/
local helptext_t changegame_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Register your vote to change game type. Options: 1 - Anything  2/pub - Pub  3/3h - 3H  4/mini - Mini PB";

local void CountVotes(Player *player, MyArenaData *arenaData, PB_gameType vote, bool isCheck);
local void ChangeGameType(MyArenaData *arenaData, Player *player, PB_gameType gameType);

local void CChangeGame(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		chat->SendMessage(player, "Pub Mode is disabled. Cannot randomize teams.");
		return;
	}

	PB_gameType vote	= PB_GAME_ANY;

	//const char *c		= params;
	if (!*params)
	{
		//chat->SendMessage(player, "Your vote has been reset!");
		//PBPlayerVoting *playerData	= PPDATA(player, playerKey);
		//playerData->changeGameType	= vote;
		CountVotes(player, arenaData, vote, TRUE);
		return;
	}

	if (strlen(params) == 1)
	{
		switch (*params)
		{
			case '2':
				vote	= PB_GAME_PUB;
				chat->SendMessage(player, "Your vote has been counted for Small Pub play!");
			break;
			//case '2':
			//	vote	= PB_GAME_PRO;
			//	chat->SendMessage(player, "Your vote has been counted for Proball play!");
			//break;
			case '3':
				vote	= PB_GAME_3H;
				chat->SendMessage(player, "Your vote has been counted for 3H play!");
			break;
			case '4':
				vote	= PB_GAME_MINI;
				chat->SendMessage(player, "Your vote has been counted for Mini PB play!");
			break;
			/*
			case '4':
				vote	= PB_GAME_SCRAMBLE;
				chat->SendMessage(player, "Your vote has been counted for Scramble play!");
			break;
			*/
			default:
				chat->SendMessage(player, "Invalid game option %c.", *params);
				return;
		}
	}
	else
	{
		if (!strcasecmp(params, "mini"))
		{
			vote	= PB_GAME_MINI;
			chat->SendMessage(player, "Your vote has been counted for Mini PB play!");
		}
		else if (!strcasecmp(params, "pub"))
		{
			vote	= PB_GAME_PUB;
			chat->SendMessage(player, "Your vote has been counted for Small Pub play!");
		}
		else if (!strcasecmp(params, "3h"))
		{
			vote	= PB_GAME_3H;
			chat->SendMessage(player, "Your vote has been counted for 3H play!");
		}
		else
		{
			chat->SendMessage(player, "Invalid game option %s.", params);
			return;
		}
	}
	CountVotes(player, arenaData, vote, FALSE);
}

local void CountVotes(Player *player, MyArenaData *arenaData, PB_gameType vote, bool isCheck)
{
	int total		= 0;
	int yesVote		= 0;

	if (isCheck)
		vote = arenaData->gameType;

	Player *currentPlayer	= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(currentPlayer, player->arena)
	{
		if (currentPlayer->status != S_PLAYING)
			continue;

		if (currentPlayer->p_ship != SHIP_SPEC)
		{
			total++;
			PBPlayerVoting *playerData = PPDATA(currentPlayer, playerKey);
			if ((!isCheck) && (currentPlayer == player))
			{
				playerData->changeGameType	= vote;
			}
			if ((isCheck) && ((playerData->changeGameType == vote) || (playerData->changeGameType == PB_GAME_ANY )))
				yesVote++;
			if ((!isCheck) && (playerData->changeGameType == vote))
				yesVote++;
		}
	}
	pd->Unlock();

	if (isCheck)
	{
		chat->SendMessage(player, "Votes To Change Game: %d  Against: %d", (total-yesVote), yesVote);
		return;
	}

	if (yesVote > (total / 2))
	{
		ChangeGameType(arenaData, player, vote);
	}
	else
	{
		chat->SendMessage(player, "Votes For: %d  Against: %d", yesVote, (total-yesVote));
		chat->SendArenaMessage(player->arena, "New vote lodged for game change.");
	}
}

local void ChangeGameType(MyArenaData *arenaData, Player *player, PB_gameType gameType)
{
	if (arenaData->gameType	== gameType)
		return;

	//don't allow changing from Scramble due to use as separate arena.
	if (arenaData->gameType == PB_GAME_SCRAMBLE)
		return;

	int radius,radius0,radius1;
	int spawnx,spawnx0,spawnx1;
	int spawny,spawny0,spawny1;
	//int pRadius;
	int passDelay;
	int wallPass;
	int bombAlive;
	int bulletDamage;
	int mineAlive;
	int burstDamage;
	int warpRadius;
	int enterDelay;
	//int initialGuns;
	//int maxGuns;
	int burstMax;
	int fireEnergy1;
	int fireEnergy2;
	int portalMax;
	int throwTime;
	int bulletAlive;
	int brickMax;
	int repelMax;
	int repelPrize;

	switch(gameType)
	{
		case PB_GAME_PRO:
		case PB_GAME_PUB:
			arenaData->gameType	= gameType;
			radius			= PUBSPAWNRADIUS;
			spawnx			= PUBSPAWNX;
			spawny			= PUBSPAWNY;
			radius0			= PUBSPAWNRADIUS;
			spawnx0			= PUBSPAWNX;
			spawny0			= PUBSPAWNY;
			radius1			= PUBSPAWNRADIUS;
			spawnx1			= PUBSPAWNX;
			spawny1			= PUBSPAWNY;
			//pRadius		= PUBPLAYER;
			passDelay		= PUBPASSDELAY;
			wallPass		= PUBWALLPASS;
			bombAlive		= PUBBOMBALIVE;
			bulletDamage	= PUBBULLETDAMAGE;
			mineAlive		= PUBMINEALIVE;
			burstDamage		= PUBBURSTDAMAGE;
			warpRadius		= PUBWARPRADIUS;
			enterDelay		= PUBENTERDELAY;
			//
			//initialGuns		= PUBINITIALGUNS;
			//maxGuns			= PUBMAXGUNS;
			burstMax		= PUBBURSTMAX;
			fireEnergy1		= PUBFIREENERGY1;
			fireEnergy2		= PUBFIREENERGY2;
			portalMax		= PUBPORTALMAX;
			throwTime		= PUBTHROWTIME;
			bulletAlive		= PUBBULLETALIVETIME;
			brickMax		= PUBBRICKMAX;
			repelMax		= PUBREPELMAX;
			repelPrize		= PUBREPELPRIZE;
		break;
		case PB_GAME_3H:
			arenaData->gameType	= gameType;
			radius			= SMLSPAWNRADIUS;
			spawnx			= SMLSPAWNX;
			spawny			= SMLSPAWNY;
			radius0			= SMLSPAWNRADIUS;
			spawnx0			= SMLSPAWNX;
			spawny0			= SMLSPAWNY;
			radius1			= SMLSPAWNRADIUS;
			spawnx1			= SMLSPAWNX;
			spawny1			= SMLSPAWNY;
			//pRadius			= SMLPLAYER;
			passDelay		= SMLPASSDELAY;
			wallPass		= SMLWALLPASS;
			bombAlive		= SMLBOMBALIVE;
			bulletDamage		= SMLBULLETDAMAGE;
			mineAlive		= SMLMINEALIVE;
			burstDamage		= SMLBURSTDAMAGE;
			warpRadius		= SMLWARPRADIUS;
			enterDelay		= SMLENTERDELAY;
			//
			//initialGuns		= SMLINITIALGUNS;
			//maxGuns			= SMLMAXGUNS;
			burstMax		= SMLBURSTMAX;
			fireEnergy1		= SMLFIREENERGY1;
			fireEnergy2		= SMLFIREENERGY2;
			portalMax		= SMLPORTALMAX;
			throwTime		= SMLTHROWTIME;
			bulletAlive		= SMLBULLETALIVETIME;
			brickMax		= SMLBRICKMAX;
			repelMax		= SMLREPELMAX;
			repelPrize		= SMLREPELPRIZE;
		break;
		case PB_GAME_MINI:
			arenaData->gameType	= gameType;
			radius			= MINISPAWNRADIUS;
			spawnx			= MINISPAWNX;
			spawny			= MINISPAWNY;
			radius0			= MINISPAWNRADIUS0;
			spawnx0			= MINISPAWNX0;
			spawny0			= MINISPAWNY0;
			radius1			= MINISPAWNRADIUS1;
			spawnx1			= MINISPAWNX1;
			spawny1			= MINISPAWNY1;
			//pRadius			= SMLPLAYER;
			passDelay		= MINIPASSDELAY;
			wallPass		= MINIWALLPASS;
			bombAlive		= MINIBOMBALIVE;
			bulletDamage	= MINIBULLETDAMAGE;
			mineAlive		= MINIMINEALIVE;
			burstDamage		= MINIBURSTDAMAGE;
			warpRadius		= MINIWARPRADIUS;
			enterDelay		= MINIENTERDELAY;
			//
			//initialGuns		= SMLINITIALGUNS;
			//maxGuns			= SMLMAXGUNS;
			burstMax		= MINIBURSTMAX;
			fireEnergy1		= MINIFIREENERGY1;
			fireEnergy2		= MINIFIREENERGY2;
			portalMax		= MINIPORTALMAX;
			throwTime		= MINITHROWTIME;
			bulletAlive		= MINIBULLETALIVETIME;
			brickMax		= MINIBRICKMAX;
			repelMax		= MINIREPELMAX;
			repelPrize		= MINIREPELPRIZE;
		break;
		case PB_GAME_ANY:
		default:
			return;
	}

	cfg->SetInt(player->arena->cfg, "Soccer",	"SpawnRadius",		radius,		"", FALSE);
	cfg->SetInt(player->arena->cfg, "Soccer",	"SpawnX",		spawnx,		"", FALSE);
	cfg->SetInt(player->arena->cfg, "Soccer",	"SpawnY",		spawny,		"", FALSE);
	cfg->SetInt(player->arena->cfg, "Soccer",	"PassDelay",		passDelay,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Soccer",	"DisableWallPass",	wallPass,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Spawn",	"Team0-Radius", 	radius0,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Spawn",	"Team0-X",		spawnx0,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Spawn",	"Team0-Y",		spawny0,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Spawn",	"Team1-Radius", 	radius1,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Spawn",	"Team1-X",		spawnx1,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Spawn",	"Team1-Y",		spawny1,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Bomb",		"BombAliveTime",	bombAlive,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Bullet",	"BulletDamageLevel",	bulletDamage,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Bullet",	"BulletAliveTime",	bulletAlive,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Mine",		"MineAliveTime",	mineAlive,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Burst",	"BurstDamageLevel",	burstDamage,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Misc",		"WarpRadiusLimit",	warpRadius,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "Kill",		"EnterDelay",		enterDelay,	"", FALSE);


	//cfg->SetInt(player->arena->cfg, "Warbird",	"InitialGuns",		initialGuns,	"", FALSE);
	//cfg->SetInt(player->arena->cfg, "Warbird",	"MaxGuns",		maxGuns,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"BurstMax",		burstMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"BulletFireEnergy",	fireEnergy1,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"PortalMax",		portalMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"SoccerThrowTime",	throwTime,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"BrickMax",		brickMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Warbird",	"RepelMax",		repelMax,	"", FALSE);

	//cfg->SetInt(player->arena->cfg, "Javelin",	"InitialGuns",		initialGuns,	"", FALSE);
	//cfg->SetInt(player->arena->cfg, "Javelin",	"MaxGuns",		maxGuns,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"BurstMax",		burstMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"BulletFireEnergy",	fireEnergy2,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"PortalMax",		portalMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"SoccerThrowTime",	throwTime,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"BrickMax",		brickMax,	"", FALSE);
	cfg->SetInt(player->arena->cfg, "Javelin",	"RepelMax",		repelMax,	"", FALSE);

	cfg->SetInt(player->arena->cfg, "PrizeWeight",	"Repel",		repelPrize,	"", FALSE);

	cfg->FlushDirtyValues();

	pg->ResetGame(player->arena, player);

	//RandomizeTeams(player->arena);

	game->GivePrize(&arenaData->arenaTarget, PRIZE_WARP , 1);
}

/****************************************************************
 *			CHANGE NOW				*
 ****************************************************************/
local helptext_t changenow_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Op Command to change game type. Options: 1 - Anything  2 - Pub  3 - 3H  4 - Mini PB";

local void CChangeNow(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData	= P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		chat->SendMessage(player, "Pub Mode is disabled. Cannot randomize teams.");
		return;
	}

	PB_gameType vote	= PB_GAME_PUB;

	const char *c		= params;
	if (!c)
	{
		chat->SendMessage(player, "No change option specified.");
		return;
	}

	switch (*c)
	{
		case '2':
			vote	= PB_GAME_PUB;
			if (arenaData->gameType == vote)
			{
				chat->SendMessage(player, "No change needed. Already playing PUB!");
				return;
			}
			else
				chat->SendMessage(player, "Changing to Pub!!");
		break;
			//case '2':
			//	vote	= PB_GAME_PRO;
			//	if (arenaData->gameType == vote)
			//	{
			//		chat->SendMessage(player, "No change needed. Already playing Proball!");
			//		return;
			//	}
			//	else
			//		chat->SendMessage(player, "Changing to Proball!!");
			//break;
		case '3':
			vote	= PB_GAME_3H;
			if (arenaData->gameType == vote)
			{
				chat->SendMessage(player, "No change needed. Already playing 3H!");
				return;
			}
			else
				chat->SendMessage(player, "Changing to 3H!!");
		break;
		case '4':
			vote	= PB_GAME_MINI;
			if (arenaData->gameType == vote)
			{
				chat->SendMessage(player, "No change needed. Already playing Mini PB!");
				return;
			}
			else
				chat->SendMessage(player, "Changing to Mini PB.");
		break;
			/*
			case '4':
				vote	= PB_GAME_SCRAMBLE;
				if (arenaData->gameType == vote)
				{
					chat->SendMessage(player, "No change needed. Already playing Scramble!");
					return;
				}
				else
					chat->SendMessage(player, "Changing to Scramble!!");
			break;
			*/
		default:
			chat->SendMessage(player, "Invalid game option %c.", *c);
			return;
	}

	ChangeGameType(arenaData, player, vote);
}


/****************************************************************
 *			TOGGLE RANDOM				*
 ****************************************************************/
local helptext_t togglerandom_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none\n"
"Toggle whether teams are randomized at the end of a game.";

local void CToggleRandom(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		chat->SendMessage(player, "Pub Mode is disabled. Cannot randomize teams.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Randomize currently set as %s", arenaData->randomizeTeamsOnEnd ? "ON" : "OFF");
		return;
	}

	if (strncasecmp("ON",params,2) == 0)
	{
		if (arenaData->randomizeTeamsOnEnd)
		{
			chat->SendMessage(player, "Randomize is already set to ON");
			return;
		}

		arenaData->randomizeTeamsOnEnd = TRUE;
		chat->SendMessage(player, "Randomize set to ON");
	}
	else if (strncasecmp("OFF",params,3) == 0)
	{
		if (!arenaData->randomizeTeamsOnEnd)
		{
			chat->SendMessage(player, "Randomize is already set to OFF");
			return;
		}

		arenaData->randomizeTeamsOnEnd = FALSE;
		chat->SendMessage(player, "Randomize set to OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

/****************************************************************
 *			PUB MODE				*
 ****************************************************************/
local helptext_t pubmode_help =
"Module: pbpub\n"
"Targets: none\n"
"Args: none or ON/OFF\n"
"Display or enable/disable pub mode";

local void CPubMode(const char *command, const char *params, Player *player, const Target *target)
{
	MyArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!*params)
	{
		chat->SendMessage(player, "Pub Mode currently set as %s", arenaData->pubMode ? "ON" : "OFF");
		return;
	}

	if (strncasecmp("ON",params,2) == 0)
	{
		if (arenaData->pubMode)
		{
			chat->SendMessage(player, "Pub Mode is already set to ON");
			return;
		}

		arenaData->pubMode = TRUE;
		chat->SendMessage(player, "Pub mode set to ON");
	}
	else if (strncasecmp("OFF",params,3) == 0)
	{
		if (!arenaData->pubMode)
		{
			chat->SendMessage(player, "Pub Mode is already set to OFF");
			return;
		}

		arenaData->pubMode = FALSE;
		chat->SendMessage(player, "Pub Mode set to OFF");
	}
	else
	{
		chat->SendMessage(player, "Please specify ON or OFF.");
	}
}

		/************************************************************************************************
		 *					CALLBACKS SECTION					*
		 ************************************************************************************************/
/****************************************************************
 *			SHIP FREQ CHANGE			*
 ****************************************************************/
local void ShipFreqChange(Player *player, int newShip, int oldShip, int newFreq, int oldFreq)
{
	lm->Log(L_INFO, "<pbpub> %s freq ship change %d:%d %d:%d", player->name, oldShip, newShip, oldFreq, newFreq );

	MyArenaData *arenaData = P_ARENA_DATA(player->arena, arenaKey);

	if (!arenaData->pubMode)
	{
		return;
	}

	if (!arenaData->roundStart)
		return;

	int random;
	//player specced so update playingTime
	if (newShip != SHIP_SPEC)
	{
		/********************************
		 *	New Ship is Not SPEC	*
		 ********************************/
		if (!ValidGameFreq(arenaData->gameType, player->p_freq))
		{
			switch (arenaData->gameType)
			{
				case PB_GAME_ANY:
				case PB_GAME_3H:
				case PB_GAME_PUB:
				case PB_GAME_PRO:
				case PB_GAME_MINI:
				case PB_GAME_SCRAMBLE:
					random = rand() % 2;
					game->SetShipAndFreq(player, random, random);
				break;
				//case 4 player freqs
				//	random = rand() % 4;
				//	game->SetShipAndFreq(player, random, random);
				//break;
			}
			return;
		}
		if (player->p_freq != player->p_ship)
		{
			game->SetShipAndFreq(player, player->p_freq, player->p_freq);
			return;
		}
	}

	//check balance if someone specced or someone entered
	//if (arenaData->balanceKey == NULL)
	//	BalanceFreqs(player->arena, arenaData, PB_FIRST_WARNING);
}

/****************************************************************
 *			BALL PICKUP				*
 ****************************************************************/
local void RoundStart(Arena *arena, MyArenaData *arenaData);

local void BallPickup(Arena *arena, Player *player, int ballID)
{
	//lm->Log(L_INFO, "<pbpub> %s ball pickup", player->name );

	MyArenaData * arenaData = P_ARENA_DATA(arena, arenaKey);

	if (!arenaData->roundStart)
	{
		RoundStart(arena, arenaData);
	}

	arenaData->roundStart = TRUE;
}

/****************************************************************
 *			PLAYER ACTION				*
 ****************************************************************/
local void PlayerAction(Player *player, int action, Arena *arena)
{
	lm->Log(L_INFO, "<pbpub> %s Player Action %d", player->name, action );

	switch (action)
	{
		case PA_ENTERARENA:
			chat->SendMessage(player, "Welcome to PowerBall! ");
			chat->SendMessage(player, "This arena is powered by PB Module!");
			chat->SendMessage(player, "Type ?pbhelp for available commands.");

			MyArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

			switch(arenaData->gameType)
			{
				case PB_GAME_PUB:
					chat->SendMessage(player, "Currently playing Small Pub!");
				break;
				case PB_GAME_3H:
					chat->SendMessage(player, "Currently playing Small 3H!");
				break;
				case PB_GAME_PRO:
					chat->SendMessage(player, "Currently playing Proball!");
				break;
				case PB_GAME_SCRAMBLE:
					chat->SendMessage(player, "Currently playing Scramble!");
				break;
				case PB_GAME_MINI:
					chat->SendMessage(player, "Currently playing Mini PB!");
				break;
				case PB_GAME_ANY:
					chat->SendMessage(player, "Currently no game selected!");
				break;
			}
		break;
	}
}

/****************************************************************
 *			GAME START				*
 ****************************************************************/
local void GameStart(Arena *arena)
{
	lm->Log(L_INFO, "<pbpub> Game Start" );

	MyArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	RoundStart(arena, arenaData);
}

/****************************************************************
 *			GAME OVER				*
 ****************************************************************/
local void GameOver(Arena *arena)
{
	MyArenaData * arenaData		= P_ARENA_DATA(arena, arenaKey);

	lm->Log(L_INFO, "<pbpub> Game Over");

	arenaData->roundStart		= FALSE;

	if ((arenaData->gameType == PB_GAME_PRO) || (!arenaData->randomizeTeamsOnEnd))
		return;

	if (!arenaData->pubMode)
		return;

	RandomizeTeams(arena);
}


/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= mm->GetInterface(I_TEAMS,			ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);
		return;
	}
}
/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "signups"))
	{
		signups	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbstats"))
	{
		pbstats	= NULL;
		return;
	}
	if (!strcasecmp(mod->args.name, "pbextras"))
	{
		pbextras= NULL;
		return;
	}
}

/****************************************************************
 *			SET TYPE AND LOCK			*
 ****************************************************************/
local void SetTypeAndLock(Arena *arena, int gameType)
{
	MyArenaData *arenaData	= P_ARENA_DATA(arena, arenaKey);

	if (arenaData->gameType	== (PB_gameType)gameType)
		return;

	arenaData->gameType = gameType;
}

		/************************************************************************************************
		 *					TIMERS SECTION						*
		 ************************************************************************************************/
/********************************************************
 *			ROUND START			*
 ********************************************************/
local void RoundStart(Arena *arena, MyArenaData *arenaData)
{
	if (!arenaData->pubMode)
		return;

	Player *player	= NULL;
	Link *link		= NULL;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(player, arena)
	{
		if (player->status != S_PLAYING)
			continue;

		if (player->p_ship != SHIP_SPEC)
		{
			if (!ValidGameFreq(arenaData->gameType, player->p_freq))
			{
				int random = rand() % 2;
				game->SetShipAndFreq(player, random, random);
			}
			else if (player->p_freq != player->p_ship)
			{
				game->SetShipAndFreq(player, player->p_freq, player->p_freq);
			}
		}
	}
	pd->Unlock();
}

//settings for other subgame
//Stag Shot> smallpublg


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(balls);
	mm->ReleaseInterface(ml);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(pg);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(teams);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(signups);
	mm->ReleaseInterface(pbstats);
	mm->ReleaseInterface(pbextras);
}

/* The entry point: */
EXPORT int MM_pbpub(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm		= mm_;
			chat	= mm->GetInterface(I_CHAT,			ALLARENAS);
			aman	= mm->GetInterface(I_ARENAMAN,		ALLARENAS);
			pd		= mm->GetInterface(I_PLAYERDATA,	ALLARENAS);
			balls	= mm->GetInterface(I_BALLS,			ALLARENAS);
			ml		= mm->GetInterface(I_MAINLOOP,		ALLARENAS);
			game	= mm->GetInterface(I_GAME,			ALLARENAS);
			cmd		= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm		= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			pg		= mm->GetInterface(I_POINTS_GOALS,	ALLARENAS);
			cfg		= mm->GetInterface(I_CONFIG,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			teams	= mm->GetInterface(I_TEAMS,			ALLARENAS);
			signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);
			pbextras= mm->GetInterface(I_PBEXTRAS,		ALLARENAS);

			if (!chat || !aman || !pd || !balls || !ml || !game || !cmd || !lm || !pg || !cfg || !capman)
			{
				if (lm)
				{
					if (!chat)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface chat");
					if (!aman)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface aman");
					if (!pd)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface pd");
					if (!balls)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface balls");
					if (!ml)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface ml");
					if (!game)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface game");
					if (!cmd)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface cmd");
					if (!pg)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface pg");
					if (!cfg)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface cfg");
					if (!capman)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface capman");
				}

				ReleaseInterfaces();

				rv = MM_FAIL;
			}
			else
			{
				//allocate data
				arenaKey	= aman->AllocateArenaData(sizeof(MyArenaData));
				playerKey	= pd->AllocatePlayerData(sizeof(PBPlayerVoting));

				if ((arenaKey == -1) || (playerKey == -1))	//check for valid memory allocation
				{
					if (arenaKey != -1)
						aman->FreeArenaData(arenaKey);
					if (playerKey != -1)
						pd->FreePlayerData(playerKey);

					ReleaseInterfaces();

					rv = MM_FAIL;
				}
				else
				{
					mm->RegInterface(&_int, ALLARENAS);

					rv = MM_OK;

					if (!signups)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface signups");
					if (!teams)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface teams");
					if (!pbstats)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface pbstats");
					if (!pbextras)
						lm->LogA(L_DRIVEL, "pbpub", arena, "Unable to load interface pbextras");
				}
			}
		break;

		case MM_ATTACH:

			if (!signups)
				signups	= mm->GetInterface(I_SIGNUPS,		ALLARENAS);
			if (!teams)
				teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);
			if (!pbstats)
				pbstats	= mm->GetInterface(I_PBSTATS,		ALLARENAS);

			mm->RegCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->RegCallback(CB_BALLPICKUP,		BallPickup,			arena);
			mm->RegCallback(CB_GAMEOVER,		GameOver,			arena);
			mm->RegCallback(CB_GAMESTART,		GameStart,			arena);
			mm->RegCallback(CB_PLAYERACTION,	PlayerAction,		arena);
			mm->RegCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->RegCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			cmd->AddCommand("pbpubhelp",	CPbHelp,		arena,	pbhelp_help);
			cmd->AddCommand("pbversion",	CPbVersion,		arena,	pbversion_help);
			cmd->AddCommand("randomize",	CRandomize,		arena,	randomize_help);
			cmd->AddCommand("randomnow",	CRandomNow,		arena,	randomnow_help);
			cmd->AddCommand("changegame",	CChangeGame,		arena,	changegame_help);
			cmd->AddCommand("cg",		CChangeGame,		arena,	changegame_help);
			cmd->AddCommand("changenow",	CChangeNow,		arena,	changenow_help);
			cmd->AddCommand("togglerandom",	CToggleRandom,		arena,	togglerandom_help);

			cmd->AddCommand("pubmode",		CPubMode,		arena,	pubmode_help);

			InitializeArenaData(arena);

			rv = MM_OK;
		break;

		case MM_DETACH:

			cmd->RemoveCommand("pbpubhelp",		CPbHelp,		arena);
			cmd->RemoveCommand("pbversion",		CPbVersion,		arena);
			cmd->RemoveCommand("randomize",		CRandomize,		arena);
			cmd->RemoveCommand("randomnow",		CRandomNow,		arena);
			cmd->RemoveCommand("changegame",	CChangeGame,		arena);
			cmd->RemoveCommand("cg",		CChangeGame,		arena);
			cmd->RemoveCommand("changenow",		CChangeNow,		arena);
			cmd->RemoveCommand("togglerandom",	CToggleRandom,		arena);

			cmd->RemoveCommand("pubmode",		CPubMode,		arena);

			mm->UnregCallback(CB_SHIPFREQCHANGE,	ShipFreqChange,		arena);
			mm->UnregCallback(CB_BALLPICKUP,		BallPickup,			arena);
			mm->UnregCallback(CB_GAMEOVER,			GameOver,			arena);
			mm->UnregCallback(CB_GAMESTART,			GameStart,			arena);
			mm->UnregCallback(CB_PLAYERACTION,		PlayerAction,		arena);
			mm->UnregCallback(CB_MODULEATTACHED,	ModuleAttached,		arena);
			mm->UnregCallback(CB_MODULEDETACHED,	ModuleDetached,		arena);

			if (arenaKey != -1)
				ReleaseArenaData(arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			if (playerKey != -1)
				pd->FreePlayerData(playerKey);

			if (arenaKey != -1)
				aman->FreeArenaData(arenaKey);

			if ((playerKey != -1) && (arenaKey != -1))
				mm->UnregInterface(&_int, ALLARENAS);

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}

