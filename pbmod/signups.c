#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "asss.h"
#include "reldb.h"
#include "signups.h"
#include "pb_utils.c"
#include "teams.h"

// Interfaces
local Imodman *		mm;
local Ichat *		chat;

//timers
local Icmdman *		cmd;
local Ilogman *		lm;

//for teamMax amd alternatePicking
local Iconfig *		cfg;
local Icapman *		capman;

//Database access
local Ireldb *		db;

local Iteams *		teams;

local bool		hasDB		= FALSE;

local void HelpCommands(Player *player);
local void IsPlayerSignedUp(Player *player, char *playerName, const char *eventName, void *passThroughData, void (*IsSignUpPtr)(Player *, int, LinkedList *, void *));
local void RemovePlayer(Player *player, const char *playerName, const char *eventName, void *data, void (*IsSignUpPtr)(Player *, bool, void *));
local void AddPlayer(Player *player, char *playerName, const char *eventName, void *data, void (*IsSignUpPtr)(Player *, bool, void *));
local void IsValidEvent(Player *player, const char *eventName, void *data, void (*IsEventPtr)(Player *, bool, void *));

local Isignups _int =
{
	INTERFACE_HEAD_INIT(I_SIGNUPS, "signups")
	HelpCommands,
	IsPlayerSignedUp,
	RemovePlayer,
	AddPlayer,
	IsValidEvent
};

EXPORT const char info_signups[] = CORE_MOD_INFO("signups");


/********************************************************
 * 		INITIALIZATION SECTION			*
 ********************************************************/

local void InitializeDB()
{
	if (db->GetStatus())
	{
		db->Query(NULL, NULL, 0, CREATE_EVENTS_TABLE);
		db->Query(NULL, NULL, 0, CREATE_SIGNUPS_TABLE);
	}
}

		/************************************************************************************************
		 * 					COMMANDS SECTION					*
		 ************************************************************************************************/

/********************************************************
 *			HELP COMMANDS			*
 ********************************************************/
local helptext_t signupshelp_help =
"Module: signups\n"
"Targets: none\n"
"Args: none\n"
"Display Signups Module available commands.";

local void CSignUpsHelp(const char *command, const char *params, Player *player, const Target *target)
{
	HelpCommands(player);
}

local void HelpCommands(Player *player)
{
	bool displayedMod = FALSE;

	chat->SendMessage(player, "----------------------------------------------------");
	chat->SendMessage(player, "The following Signups Module commands are available:");
	chat->SendMessage(player, "----------------------------------------------------");

	DisplayCommand(chat, player, "?listevents",		"List current events and their description");
	DisplayCommand(chat, player, "?signup <event>",		"Add yourself to the signup list for <event>");
	DisplayCommand(chat, player, "?removesignup <event>",	"Remove yourself from signup list for <event>");
	DisplayCommand(chat, player, "?listsignups [event]",	"Lists players on the Signup list for [event] or the default arena event");
	DisplayCommand(chat, player, "?signups [event]",	"Lists players on the Signup list for [event] or the default arena event");

	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_addevent",		"?addevent <event>:<desc>",		"Add a new signup eligible event with a small description");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_delevent",		"?delevent <event>",			"Delete an existing event and all associated signups");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_chgevent",		"?chgevent <event>:<new desc>",		"Change the description on an existing event");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_startsignups",	"?startsignups <event>",		"Allow signups to start for <event>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_endsignups",	"?endsignups <event>",			"End the signups for <event>");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forcesignup",	"?signup <event>:<player>",		"Add <player> to the <event> signup list");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_forceremovesignup",	"?removesignup <event>:<player>",	"Remove <player> from the <event> signup");
	DisplayModCommand(chat, capman, player, &displayedMod, "cmd_clearsignups",	"?clearsignups <event>",		"Clears the Signup list");
}

/********************************************************
 *			VERSION				*
 ********************************************************/
local helptext_t signupsversion_help =
"Module: signups\n"
"Targets: none\n"
"Args: none\n"
"Display Signups Module version information.";

local void CSignUpsVersion(const char *command, const char *params, Player *player, const Target *target)
{
	chat->SendMessage(player, "ASSS Signups Module -- Version: 1.0    Date: 07 September 2021    Author: POiD");
}

/********************************************************
 *			ADD EVENT			*
 ********************************************************/
local helptext_t addevent_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name and description\n"
"Adds a new event.";

typedef struct AddEventStruct
{
	char *eventName;
	char *description;
	Player *player;
} AddEventStruct;

local void AddEventCB(int status, db_res *res, void *clos);
local void ActiveEventCBAdd(int status, db_res *res, void *clos);

local void CAddEvent(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Add Event");
	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event name and description. e.g. '?addevent Event1:First Event'");
		return;
	}

	char * description;
	char * eventName;

	if (!TwoSplitUserInput(':', params, &eventName, &description))
	{
		int len		= strlen(params) + 1;
		eventName	= amalloc(len);
		strncpy(eventName, params, len-1);
		description = amalloc(sizeof(char));
		description[0] = '\0';
	}

	if (strlen(eventName) > EVENT_NAME_LEN)
	{
		chat->SendMessage(player, "Event Name cannot be longer than %d characters", EVENT_NAME_LEN);
		TwoSplitRelease(&eventName, &description);
		return;
	}
	if (strlen(description) > EVENT_DESC_LEN)
	{
		chat->SendMessage(player, "Event Description cannot be longer than %d characters", EVENT_DESC_LEN);
		TwoSplitRelease(&eventName, &description);
		return;
	}

	player->flags.during_query = 1;

	AddEventStruct *addEventStruct	= amalloc(sizeof(AddEventStruct));
	addEventStruct->description	= description;
	addEventStruct->eventName	= eventName;
	addEventStruct->player		= player;

	db->Query(ActiveEventCBAdd, addEventStruct, 1, CHECK_ACTIVE_EVENT_QUERY, eventName);
}

local void FreeAddEventStruct(AddEventStruct **addEventStruct)
{
	if ((*addEventStruct)->description)
		afree((*addEventStruct)->description);
	if ((*addEventStruct)->eventName)
		afree((*addEventStruct)->eventName);
	afree(*addEventStruct);
}

local void ActiveEventCBAdd(int status, db_res *res, void *clos)
{
	AddEventStruct *addEventStruct = (AddEventStruct *)clos;

	lm->Log(L_INFO, "<signups> Active Event CB Add %d", status);

	if (!addEventStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> Add Event Callback player didn't request query. %s", addEventStruct->player->name);
		FreeAddEventStruct(&addEventStruct);
		return;
	}
	addEventStruct->player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(addEventStruct->player, "Unable to access database.");
		FreeAddEventStruct(&addEventStruct);
		return;
	}

	if (db->GetRowCount(res))
	{
		chat->SendMessage(addEventStruct->player, "Event already exists.");
		FreeAddEventStruct(&addEventStruct);
		return;
	}

	addEventStruct->player->flags.during_query = 1;

	db->Query(AddEventCB, addEventStruct, 1, ADD_EVENT_QUERY, addEventStruct->eventName, addEventStruct->description);
}

local void AddEventCB(int status, db_res *res, void *clos)
{
	AddEventStruct *addEventStruct = (AddEventStruct *)clos;

	if (!addEventStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> Add Event Callback player didn't request query. %s", addEventStruct->player->name);
		FreeAddEventStruct(&addEventStruct);
		return;
	}

	addEventStruct->player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(addEventStruct->player, "Unable to create event.");
		FreeAddEventStruct(&addEventStruct);
		return;
	}

	chat->SendMessage(addEventStruct->player, "Event created!");

	FreeAddEventStruct(&addEventStruct);
}

/********************************************************
 *			DELETE EVENT			*
 ********************************************************/
local helptext_t delevent_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name\n"
"Delete an existing event.";

local void DelEventCB(int status, db_res *res, void *clos);

local void CDelEvent(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Del Event");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event name. e.g. '?delevent Event1'");
		return;
	}

	if (strchr(params, ':'))
	{
		chat->SendMessage(player, "Event Name cannot contain ':' characters.");
		return;
	}
	if (strlen(params) > EVENT_NAME_LEN)
	{
		chat->SendMessage(player, "Event Name cannot be longer than %d characters", EVENT_NAME_LEN);
		return;
	}

	player->flags.during_query = 1;

	db->Query(DelEventCB, player, 1, DEL_EVENT_QUERY, params);
}

local void DelEventCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> Del Event Callback player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(player, "Unable to delete event.");
		return;
	}

	chat->SendMessage(player, "Event deleted!");
}

/********************************************************
 *			CHANGE EVENT			*
 ********************************************************/
local helptext_t chgevent_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name and new description\n"
"Change the description on an existing event.";

typedef struct ChangeEventStruct
{
	char *event;
	char *description;
	Player *player;
} ChangeEventStruct;

local void ActiveEventCBChg(int status, db_res *res, void *clos);
local void ChgEventCB(int status, db_res *res, void *clos);
local void FreeChangeEventStruct(ChangeEventStruct **changeEventStruct);

local void CChgEvent(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Chg Event");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event name and new description. e.g. '?chgevent Event1:New description here'");
		return;
	}

	char *description;
	char *eventName;

	if (!TwoSplitUserInput(':', params, &eventName, &description))
	{
		int len		= strlen(params) + 1;
		eventName	= amalloc(len);
		strncpy(eventName, params, len-1);
		description	= amalloc(sizeof(char));
		description[0]	= '\0';
	}

	if (strlen(eventName) > EVENT_NAME_LEN)
	{
		chat->SendMessage(player, "Event Name cannot be longer than %d characters", EVENT_NAME_LEN);
		TwoSplitRelease(&eventName, &description);
		return;
	}
	if (strlen(description) > EVENT_DESC_LEN)
	{
		chat->SendMessage(player, "Event Description cannot be longer than %d characters", EVENT_DESC_LEN);
		TwoSplitRelease(&eventName, &description);
		return;
	}

	player->flags.during_query = 1;

	ChangeEventStruct *changeEventStruct = amalloc(sizeof(ChangeEventStruct));
	changeEventStruct->player	= player;
	changeEventStruct->event	= eventName;
	changeEventStruct->description	= description;

	player->flags.during_query = 1;

	db->Query(ActiveEventCBChg, changeEventStruct, 1, CHECK_ACTIVE_EVENT_QUERY, eventName);
}

local void ActiveEventCBChg(int status, db_res *res, void *clos)
{
	ChangeEventStruct *changeEventStruct = (ChangeEventStruct *)clos;

	lm->Log(L_INFO, "<signups> Active Event CB Chg %d", status);

	if (!changeEventStruct->player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> Chg Event Callback player didn't request query. %s", changeEventStruct->player->name);
		FreeChangeEventStruct(&changeEventStruct);
		return;
	}

	lm->Log(L_INFO, "<signups> Active Event CB Chg 2");

	changeEventStruct->player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(changeEventStruct->player, "Unable to access database.");
		FreeChangeEventStruct(&changeEventStruct);
		return;
	}

	if (!db->GetRowCount(res))
	{
		chat->SendMessage(changeEventStruct->player, "Cannot find event.");
		FreeChangeEventStruct(&changeEventStruct);
		return;
	}

	db->Query(ChgEventCB, changeEventStruct, 1, CHG_EVENT_QUERY, changeEventStruct->description, changeEventStruct->event);
}

local void ChgEventCB(int status, db_res *res, void *clos)
{
	ChangeEventStruct *changeEventStruct = (ChangeEventStruct *)clos;

	if (status != 0)
	{
		chat->SendMessage(changeEventStruct->player, "Unable to change event.");
		FreeChangeEventStruct(&changeEventStruct);
		return;
	}

	chat->SendMessage(changeEventStruct->player, "Event description updated!");
	FreeChangeEventStruct(&changeEventStruct);
}

local void FreeChangeEventStruct(ChangeEventStruct **changeEventStruct)
{
	if ((*changeEventStruct)->event)
		afree((*changeEventStruct)->event);
	if ((*changeEventStruct)->description)
		afree((*changeEventStruct)->description);
	afree(*changeEventStruct);
}

/********************************************************
 *			LIST EVENTS			*
 ********************************************************/
local helptext_t listevents_help =
"Module: signups\n"
"Targets: none\n"
"Args: none\n"
"List the available events";

local void ListEventsCB(int status, db_res *res, void *clos);

local void CListEvents(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> List Events");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	player->flags.during_query = 1;

	db->Query(ListEventsCB, player, 1, GET_EVENTS_QUERY);
}

local void ListEventsCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;
	db_row *row;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> List Events Callback player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(player, "Unable to create event.");
		return;
	}

	if (!db->GetRowCount(res))
	{
		chat->SendMessage(player, "There are currently no events.");
		return;
	}

	chat->SendMessage(player, "Event ID                         Status     Description");

	while ((row = db->GetRow(res)))
	{
		chat->SendMessage(player, "%-32s %-8s   %s",
					db->GetField(row, 0),
					satoi(db->GetField(row, 1))==1 ? "Open" : "Closed",
					db->GetField(row, 2));
	}
}


/********************************************************
 *			START SIGNUPS			*
 ********************************************************/
local helptext_t startsignups_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name\n"
"Start the signups of the event";

local void StartSignUpsCB(int status, db_res *res, void *clos);

local void CStartSignUps(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Start Signups");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event to start signups");
		return;
	}

	player->flags.during_query = 1;

	db->Query(StartSignUpsCB, player, 1, ACTIVATE_EVENTS_QUERY, 1, params);
}

local void StartSignUpsCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> Start Signups Callback player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(player, "Unable to update the event.");
		return;
	}

	chat->SendMessage(player, "Event signups started.");
}

/********************************************************
 *			END SIGNUPS			*
 ********************************************************/
local helptext_t endsignups_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name\n"
"End the signups of the event";

local void EndSignUpsCB(int status, db_res *res, void *clos);

local void CEndSignUps(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> End Signup");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event to end signups");
		return;
	}

	player->flags.during_query = 1;

	db->Query(EndSignUpsCB, player, 1, ACTIVATE_EVENTS_QUERY, 0, params);
}

local void EndSignUpsCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> End Signups Callback player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(player, "Unable to update the event.");
		return;
	}

	chat->SendMessage(player, "Event signups ended.");
}

/********************************************************
 *			LIST SIGNUPS			*
 ********************************************************/
local helptext_t listsignups_help =
"Module: signups\n"
"Targets: none\n"
"Args: none\n"
"List the current signups";

local void ListSignUpsCB(int status, db_res *res, void *clos);

local void CListSignUps(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> List Signups");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (*params)
	{
		player->flags.during_query = 1;
		db->Query(ListSignUpsCB, player, 1, GET_SIGNUPS_QUERY, params);
		return;
	}

	if (teams)
	{
		char *eventName = teams->GetActiveEvent(player->arena);

		if (!eventName)
		{
			chat->SendMessage(player, "Please specify the event to list signups");
			return;
		}

		player->flags.during_query = 1;
		db->Query(ListSignUpsCB, player, 1, GET_SIGNUPS_QUERY, eventName);
		return;
	}
}

local void ListSignUpsCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;
	db_row *row;

	lm->Log(L_INFO, "<signups> List SignUps %d", status);

	player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(player, "Unable to retrieve sign ups.");
		return;
	}

	if ((res == NULL) || (!db->GetRowCount(res)))
	{
		chat->SendMessage(player, "There are currently no sign ups.");
		return;
	}

	int count = 0;

	while ((row = db->GetRow(res)))
	{
		count++;
		chat->SendMessage(player, "%2d. %-32s", count, db->GetField(row, 0));
	}

}

/********************************************************
 *			CLEAR SIGNUPS			*
 ********************************************************/
local helptext_t clearsignups_help =
"Module: signups\n"
"Targets: none\n"
"Args: event name\n"
"Clear the signups from the specified event";

local void ClearSignUpsCB(int status, db_res *res, void *clos);

local void CClearSignUps(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Clear Signup");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (!*params)
	{
		chat->SendMessage(player, "Please specify the event to clear signups");
		return;
	}

	player->flags.during_query = 1;

	db->Query(ClearSignUpsCB, player, 1, CLR_SIGNUPS_QUERY, params);
}

local void ClearSignUpsCB(int status, db_res *res, void *clos)
{
	Player *player = (Player *)clos;

	lm->Log(L_INFO, "<signups> Clear SignUps %d", status);

	if (!player->flags.during_query)
	{
		lm->Log(L_WARN, "<signups> End Signups Callback player didn't request query. %s", player->name);
		return;
	}

	player->flags.during_query = 0;

	if (status != 0)
	{
		chat->SendMessage(player, "Unable to clear the event signups.");
		return;
	}

	chat->SendMessage(player, "Sign Up List Cleared");
}

/********************************************************
 *			SIGNUP				*
 ********************************************************/
local helptext_t signup_help =
"Module: signups\n"
"Targets: none\n"
"Args: event\n"
"Add yourself to the signup list for the specified event";

local void ActiveEventCB(int status, db_res *res, void *clos);
local void CheckSignedUpCB(int status, db_res *res, void *clos);
local void AddSignUpCB(int status, db_res *res, void *clos);
local void FreePlayerEvent(PlayerEvent **playerEvent);

local void CSignUp(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Signup");
	char *playerName;
	char *eventName;

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (target->type == T_PLAYER)
	{
		if (capman->HasCapability(player, "cmd_forcesignup"))
		{
			int len		= strlen(params) + 1;
			eventName	= amalloc(len);
			strncpy(eventName, params, len-1);
			len		= strlen(target->u.p->name) + 1;
			playerName	= amalloc(len);
			strncpy(playerName, target->u.p->name, len-1);
		}
		else
		{
			chat->SendMessage(player, "You do not have the authorization to add %s to an event.", target->u.p->name);
			return;
		}
	}
	else if (capman->HasCapability(player, "cmd_forcesignup"))
	{
		if (!TwoSplitUserInput(':', params, &eventName, &playerName))
		{
			int len		= strlen(params) + 1;
			eventName	= amalloc(len);
			strncpy(eventName, params, len-1);
			len		= strlen(player->name) + 1;
			playerName	= amalloc(len);
			strncpy(playerName, player->name, len-1);
		}
	}
	else
	{
		if (!*params)
		{
			chat->SendMessage(player, "Please specify the event you wish to sign up for.");
			return;
		}

		int len		= strlen(params) + 1;
		eventName	= amalloc(len);
		strncpy(eventName, params, len-1);

		len		= strlen(player->name) + 1;
		playerName	= amalloc(len);
		strncpy(playerName, player->name, len-1);
	}


	PlayerEvent *playerEvent	= amalloc(sizeof(PlayerEvent));
	playerEvent->player		= player;
	playerEvent->playerName		= playerName;
	playerEvent->event		= eventName;

	player->flags.during_query = 1;

	db->Query(ActiveEventCB, playerEvent, 1, CHECK_ACTIVE_EVENT_QUERY, eventName);
}

local void ActiveEventCB(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;
	db_row *row;

	lm->Log(L_INFO, "<signups> Active Event CB %d", status);

	playerEvent->player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(playerEvent->player, "Unable to access database.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	if (!db->GetRowCount(res))
	{
		chat->SendMessage(playerEvent->player, "Cannot find event.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	row = db->GetRow(res);

	int active = satoi(db->GetField(row, 0));

	if (!active)
	{
		chat->SendMessage(playerEvent->player, "Event is not currently accepting sign ups.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	db->Query(CheckSignedUpCB, playerEvent, 1, CHECK_SIGNUP_QUERY, playerEvent->event, playerEvent->playerName);
}

local void CheckSignedUpCB(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;

	lm->Log(L_INFO, "<signups> Check Signed Up CB %d", status);

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(playerEvent->player, "Unable to access database.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	if (db->GetRowCount(res))
	{
		chat->SendMessage(playerEvent->player, "You are already signed up for the event.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	db->Query(AddSignUpCB, playerEvent, 1, ADD_SIGNUP_QUERY, playerEvent->playerName, playerEvent->event);
}

local void AddSignUpCB(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;

	lm->Log(L_INFO, "<signups> Add Sign Up CB %d", status);

	if (status != 0)
	{
		chat->SendMessage(playerEvent->player, "Unable to perform signup.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	chat->SendMessage(playerEvent->player, "You have been added to the signup list!");

	FreePlayerEvent(&playerEvent);
}

local void FreePlayerEvent(PlayerEvent **playerEvent)
{
	if ((*playerEvent)->event)
		afree((*playerEvent)->event);
	if ((*playerEvent)->playerName)
		afree((*playerEvent)->playerName);
	afree(*playerEvent);
}

/********************************************************
 *			REMOVE SIGNUP			*
 ********************************************************/
local helptext_t removesignup_help =
"Module: signups\n"
"Targets: player or none\n"
"Args: event or event:player name\n"
"Remove the player from the signups for event";

local void ActiveEventCB2(int status, db_res *res, void *clos);
local void CheckSignedUpCB2(int status, db_res *res, void *clos);
local void RemoveSignUpCB(int status, db_res *res, void *clos);

local void CRemoveSignUp(const char *command, const char *params, Player *player, const Target *target)
{
	lm->Log(L_INFO, "<signups> Remove Signup");

	char *playerName;
	char *eventName;

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");
		return;
	}

	if (target->type == T_PLAYER)
	{
		if (capman->HasCapability(player, "cmd_forceremovesignup"))
		{
			int len		= strlen(params) + 1;
			eventName	= amalloc(len);
			strncpy(eventName, params, len-1);
			len		= strlen(target->u.p->name) + 1;
			playerName	= amalloc(len);
			strncpy(playerName, target->u.p->name, len-1);
		}
		else
		{
			chat->SendMessage(player, "You do not have the authorization to remove %s from an event.", target->u.p->name);
			return;
		}
	}
	else if (capman->HasCapability(player, "cmd_forceremovesignup"))
	{
		if (!TwoSplitUserInput(':', params, &eventName, &playerName))
		{
			//chat->SendMessage(player, "You must specific the event and player name, or target the player and provide the event.");
			//return;
			int len		= strlen(params) + 1;
			eventName	= amalloc(len);
			strncpy(eventName, params, len-1);
			len		= strlen(player->name) + 1;
			playerName	= amalloc(len);
			strncpy(playerName, player->name, len-1);
		}
	}
	else
	{
		if (!*params)
		{
			chat->SendMessage(player, "Please specify the event you wish to be removed from.");
			return;
		}

		int len		= strlen(params) + 1;
		eventName	= amalloc(len);
		strncpy(eventName, params, len-1);
		len		= strlen(player->name) + 1;
		playerName	= amalloc(len);
		strncpy(playerName, player->name, len-1);
	}

	PlayerEvent *playerEvent	= amalloc(sizeof(PlayerEvent));
	playerEvent->player		= player;
	playerEvent->playerName		= playerName;
	playerEvent->event		= eventName;

	playerEvent->player->flags.during_query = 1;

	db->Query(ActiveEventCB2, playerEvent, 1, CHECK_ACTIVE_EVENT_QUERY, eventName);
}

local void ActiveEventCB2(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;
	db_row *row;

	lm->Log(L_INFO, "<signups> Active Event CB %d : %s - %s", status, playerEvent->playerName, playerEvent->event);

	playerEvent->player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(playerEvent->player, "Unable to access database.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	if (!db->GetRowCount(res))
	{
		chat->SendMessage(playerEvent->player, "Cannot find event.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	row = db->GetRow(res);

	int active = satoi(db->GetField(row, 0));

	if (!active)
	{
		chat->SendMessage(playerEvent->player, "Event is not currently active.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	db->Query(CheckSignedUpCB2, playerEvent, 1, CHECK_SIGNUP_QUERY, playerEvent->event, playerEvent->playerName);
}

local void CheckSignedUpCB2(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;

	lm->Log(L_INFO, "<signups> Check Signed Up CB %d : %s - %s", status, playerEvent->playerName, playerEvent->event);

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(playerEvent->player, "Unable to access database.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	if (!db->GetRowCount(res))
	{
		chat->SendMessage(playerEvent->player, "%s is not currently signed up for the event.", playerEvent->playerName);
		FreePlayerEvent(&playerEvent);
		return;
	}

	db->Query(RemoveSignUpCB, playerEvent, 1, DEL_SIGNUP_QUERY, playerEvent->event, playerEvent->playerName);
}

local void RemoveSignUpCB(int status, db_res *res, void *clos)
{
	PlayerEvent *playerEvent = (PlayerEvent *)clos;

	lm->Log(L_INFO, "<signups> Remove Sign Up CB %d : %s - %s", status, playerEvent->playerName, playerEvent->event);

	if (status != 0)
	{
		chat->SendMessage(playerEvent->player, "Unable to perform signup removal.");
		FreePlayerEvent(&playerEvent);
		return;
	}

	chat->SendMessage(playerEvent->player, "%s has been removed from the signup list for %s", playerEvent->playerName, playerEvent->event);

	FreePlayerEvent(&playerEvent);
}

		/************************************************************************************************
		 * 					CALLBACKS SECTION					*
		 ************************************************************************************************/
typedef struct IsSignUpStruct
{
	Player *player;
	void *passThroughData;
	char *playerName;
	void (*IsSignUpPtr)(Player *, int, LinkedList *, void *);

} IsSignUpStruct;

/************************************************
 *		IS PLAYER SIGNED UP?		*
 ************************************************/
local void IsSignUpCB(int status, db_res *res, void *clos);

local void IsPlayerSignedUp(Player *player, char *playerName, const char *eventName, void *passThroughData, void (*IsSignUpPtr)(Player *, int, LinkedList *, void *))
{
	lm->Log(L_INFO, "<signups> Is Player on Signup List");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");

		IsSignUpPtr(player, -1, NULL, passThroughData);
		return;
	}

	if (!*eventName)
	{
		chat->SendMessage(player, "Please specify the event you wish to check against.");

		IsSignUpPtr(player, -1, NULL, passThroughData);
		return;
	}

	if (!*playerName)
	{
		chat->SendMessage(player, "Please specify the playerName you wish to check against.");

		IsSignUpPtr(player, -1, NULL, passThroughData);
		return;
	}

	IsSignUpStruct *isSignUpStruct	= amalloc(sizeof(IsSignUpStruct));
	isSignUpStruct->player		= player;
	isSignUpStruct->IsSignUpPtr	= IsSignUpPtr;
	isSignUpStruct->playerName	= playerName;
	isSignUpStruct->passThroughData	= passThroughData;

	player->flags.during_query = 1;

	db->Query(IsSignUpCB, isSignUpStruct, 1, CHECK_SIGNUP_QUERY, eventName, playerName);
}

local void IsSignUpCB(int status, db_res *res, void *clos)
{
	IsSignUpStruct * isSignUpStruct = (IsSignUpStruct *)clos;

	if (status != 0 || res == NULL)
	{
		chat->SendMessage(isSignUpStruct->player, "Unable to access database.");

		isSignUpStruct->IsSignUpPtr(isSignUpStruct->player, -1, NULL, isSignUpStruct->passThroughData);
		afree(isSignUpStruct);
		return;
	}

	int count = db->GetRowCount(res);

	if (!count)
	{
		//chat->SendMessage(isSignUpStruct->player, "%s is not currently signed up for the event.", isSignUpStruct->playerName);
		isSignUpStruct->IsSignUpPtr(isSignUpStruct->player, count, NULL, isSignUpStruct->passThroughData);
		afree(isSignUpStruct);
		return;
	}

	LinkedList *options	= LLAlloc();
	db_row *row		= NULL;
	while ((row = db->GetRow(res)))
	{
		const char *name = db->GetField(row, 0);
		int len		= strlen(name) + 1;
		char *nextName	= amalloc(len);
		strncpy(nextName, name, len - 1);

		if (!strcasecmp(isSignUpStruct->playerName, nextName))
		{
			//exact match
			LLClear(options);
			LLAdd(options, nextName);
			count = 1;
			break;
		}
		else
			LLAdd(options, nextName);
	}

	isSignUpStruct->IsSignUpPtr(isSignUpStruct->player, count, options, isSignUpStruct->passThroughData);

	LLClear(options);
	LLFree(options);

	afree(isSignUpStruct);
}

/************************************************
 *		REMOVE SIGNED UP PLAYER		*
 ************************************************/
local void RemoveCB(int status, db_res *res, void *clos);

typedef struct RemoveSignUpStruct
{
	Player *player;
	void *passThroughData;
	void (*RemoveSignUpPtr)(Player *, bool, void *);

} RemoveSignUp;


local void RemovePlayer(Player *player, const char *playerName, const char *eventName, void *passThroughData, void (*RemoveSignUpPtr)(Player *, bool, void *))
{
	lm->Log(L_INFO, "<signups> Remove Signed Up Player");

	if ((!hasDB) || (!db->GetStatus()))
	{
		chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");

		RemoveSignUpPtr(player, FALSE, passThroughData);
		return;
	}

	if (!*playerName)
	{
		chat->SendMessage(player, "Please specify the player name you wish you wish to remove.");

		RemoveSignUpPtr(player, FALSE, passThroughData);
		return;
	}

	if (!*eventName)
	{
		chat->SendMessage(player, "Please specify the event you wish to remove a player from.");

		RemoveSignUpPtr(player, FALSE, passThroughData);
		return;
	}

	RemoveSignUp *removeSignUp	= amalloc(sizeof(RemoveSignUp));
	removeSignUp->player		= player;
	removeSignUp->RemoveSignUpPtr	= RemoveSignUpPtr;
	removeSignUp->passThroughData	= passThroughData;

	player->flags.during_query = 1;

	db->Query(RemoveCB, removeSignUp, 1, DEL_SIGNUP_QUERY, eventName, playerName);
}

local void RemoveCB(int status, db_res *res, void *clos)
{
	RemoveSignUp *removeSignUp = (RemoveSignUp *)clos;

	//if (status != 0 || res == NULL)
	if (status != 0)
	{
		chat->SendMessage(removeSignUp->player, "Unable to access database.");

		removeSignUp->RemoveSignUpPtr(removeSignUp->player, FALSE, removeSignUp->passThroughData);
		afree(removeSignUp);
		return;
	}

	/*
	if (!db->GetRowCount(res))
	{
		//chat->SendMessage(isSignUpStruct->player, "%s is not currently signed up for the event.", isSignUpStruct->playerName);

		isSignUpStruct->IsSignUpPtr(isSignUpStruct->player, FALSE, isSignUpStruct->data);
		afree(isSignUpStruct);
		return;
	}
	*/

	removeSignUp->RemoveSignUpPtr(removeSignUp->player, TRUE, removeSignUp->passThroughData);

	afree(removeSignUp);
}

/************************************************
 *		ADD SIGNED UP PLAYER		*
 ************************************************/
typedef struct AddSignUpStruct
{
	Player *player;
	void *passThroughData;
	void (*AddSignUpPtr)(Player *, bool, void *);

} AddSignUp;

local void AddCB(int status, db_res *res, void *clos);

local void AddPlayer(Player *player, char *playerName, const char *eventName, void *passThroughData, void (*IsAddPtr)(Player *, bool, void *))
{
	lm->Log(L_INFO, "<signups> Add Player to Sign Ups");

	if ((!hasDB) || (!db->GetStatus()))
	{
		if (player)
			chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");

		if (IsAddPtr)
			IsAddPtr(player, FALSE, passThroughData);
		return;
	}

	if (!*playerName)
	{
		if (player)
			chat->SendMessage(player, "Please specify the player name you wish you wish to add.");

		if (IsAddPtr)
			IsAddPtr(player, FALSE, passThroughData);
		return;
	}

	if (!*eventName)
	{
		if (player)
			chat->SendMessage(player, "Please specify the event you wish to add a player to.");

		if (IsAddPtr)
			IsAddPtr(player, FALSE, passThroughData);
		return;
	}

	AddSignUp *addSignUp		= amalloc(sizeof(AddSignUp));
	addSignUp->player		= player;
	addSignUp->AddSignUpPtr		= IsAddPtr;
	addSignUp->passThroughData	= passThroughData;

	if (player)
		player->flags.during_query = 1;

	db->Query(AddCB, addSignUp, 1, ADD_SIGNUP_QUERY, playerName, eventName);
}

local void AddCB(int status, db_res *res, void *clos)
{
	AddSignUp * addSignUp = (AddSignUp *)clos;

	if (addSignUp->player)
		addSignUp->player->flags.during_query = 0;

	if (status != 0 || res == NULL)
	{
		if (addSignUp->player)
			chat->SendMessage(addSignUp->player, "Unable to access database.");

		if (addSignUp->AddSignUpPtr)
			addSignUp->AddSignUpPtr(addSignUp->player, FALSE, addSignUp->passThroughData);
		afree(addSignUp);
		return;
	}

	if (!db->GetRowCount(res))
	{
		//chat->SendMessage(isSignUpStruct->player, "%s is not currently signed up for the event.", isSignUpStruct->playerName);

		if (addSignUp->AddSignUpPtr)
			addSignUp->AddSignUpPtr(addSignUp->player, FALSE, addSignUp->passThroughData);
		afree(addSignUp);
		return;
	}

	if (addSignUp->AddSignUpPtr)
		addSignUp->AddSignUpPtr(addSignUp->player, TRUE, addSignUp->passThroughData);

	afree(addSignUp);
}

/************************************************
 *		IS VALID EVENT			*
 ************************************************/
typedef struct IsValidEventStruct
{
	Player *player;
	void *passThroughData;
	void (*IsValidEventPtr)(Player *, bool, void *);

} IsValidEventStruct;

local void IsEventCB(int status, db_res *res, void *clos);

local void IsValidEvent(Player *player, const char *eventName, void *passThroughData, void (*IsValidEventPtr)(Player *, bool, void *))
{
	lm->Log(L_INFO, "<signups> Is Valid Event");

	if ((!hasDB) || (!db->GetStatus()))
	{
		if (player)
			chat->SendMessage(player, "Unfortunately the database is currently not available. Cannot process request.");

		if (IsValidEventPtr)
			IsValidEventPtr(player, FALSE, passThroughData);
		return;
	}

	if (!*eventName)
	{
		if (player)
			chat->SendMessage(player, "Please specify the event you wish to add a player to.");

		if (IsValidEventPtr)
			IsValidEventPtr(player, FALSE, passThroughData);
		return;
	}

	IsValidEventStruct *isValidEvent	= amalloc(sizeof(IsValidEventStruct));
	isValidEvent->player			= player;
	isValidEvent->IsValidEventPtr		= IsValidEventPtr;
	isValidEvent->passThroughData		= passThroughData;

	player->flags.during_query = 1;

	db->Query(IsEventCB, isValidEvent, 1, CHECK_ACTIVE_EVENT_QUERY, eventName);
}

local void IsEventCB(int status, db_res *res, void *clos)
{
	IsValidEventStruct * isValidEvent = (IsValidEventStruct *)clos;
	//db_row *row;

	if (status != 0 || res == NULL)
	{
		if (isValidEvent->player)
			chat->SendMessage(isValidEvent->player, "Unable to access database.");

		if (isValidEvent->IsValidEventPtr)
			isValidEvent->IsValidEventPtr(isValidEvent->player, FALSE, isValidEvent->passThroughData);
		afree(isValidEvent);
		return;
	}

	if (!db->GetRowCount(res))
	{
		//chat->SendMessage(isSignUpStruct->player, "%s is not currently signed up for the event.", isSignUpStruct->playerName);

		if (isValidEvent->IsValidEventPtr)
			isValidEvent->IsValidEventPtr(isValidEvent->player, FALSE, isValidEvent->passThroughData);
		afree(isValidEvent);
		return;
	}

	//row = db->GetRow(res);
	//int active = satoi(db->GetField(row, 0));

	if (isValidEvent->IsValidEventPtr)
		isValidEvent->IsValidEventPtr(isValidEvent->player, TRUE, isValidEvent->passThroughData);

	afree(isValidEvent);
}

/****************************************************************
 *			MODULE ATTACHED				*
 ****************************************************************/
local void ModuleAttached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams   = mm->GetInterface(I_TEAMS,	ALLARENAS);
		return;
	}
}

/****************************************************************
 *			MODULE DETACHED				*
 ****************************************************************/
local void ModuleDetached(ModuleData *mod, Arena *arena)
{
	if (!strcasecmp(mod->args.name, "teams"))
	{
		teams   = NULL;
		return;
	}
}


local void ReleaseInterfaces()
{
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(cmd);
	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(capman);
	mm->ReleaseInterface(db);
	mm->ReleaseInterface(teams);
}

/* The entry point: */
EXPORT int MM_signups(int action, Imodman *mm_, Arena *arena)
{
	int rv = MM_FAIL;

	switch (action)
	{
		case MM_LOAD:
			mm	= mm_;
			chat	= mm->GetInterface(I_CHAT,		ALLARENAS);
			cmd	= mm->GetInterface(I_CMDMAN,		ALLARENAS);
			lm	= mm->GetInterface(I_LOGMAN,		ALLARENAS);
			cfg	= mm->GetInterface(I_CONFIG,		ALLARENAS);
			capman	= mm->GetInterface(I_CAPMAN,		ALLARENAS);
			db	= mm->GetInterface(I_RELDB,		ALLARENAS);
			teams	= mm->GetInterface(I_TEAMS,		ALLARENAS);

			if (!db)
				hasDB	= FALSE;
			else
			{
				hasDB	= TRUE;
				InitializeDB();
			}

			if (!chat || !cmd || !lm || !cfg || !capman)
			{
				ReleaseInterfaces();
				rv = MM_FAIL;
			}
			else
			{
				mm->RegInterface(&_int, ALLARENAS);
				rv = MM_OK;
			}
		break;

		case MM_POSTLOAD:
		break;

		case MM_ATTACH:

			mm->RegCallback(CB_MODULEATTACHED,      ModuleAttached,         arena);
			mm->RegCallback(CB_MODULEDETACHED,      ModuleDetached,         arena);

			cmd->AddCommand("signupshelp",		CSignUpsHelp,		arena,	signupshelp_help);
			cmd->AddCommand("signupsversion",	CSignUpsVersion,	arena,	signupsversion_help);
			cmd->AddCommand("addevent",		CAddEvent,		arena,	addevent_help);
			cmd->AddCommand("delevent",		CDelEvent,		arena,	delevent_help);
			cmd->AddCommand("chgevent",		CChgEvent,		arena,	chgevent_help);
			cmd->AddCommand("listevents",		CListEvents,		arena,	listevents_help);
			cmd->AddCommand("startsignups",		CStartSignUps,		arena,	startsignups_help);
			cmd->AddCommand("endsignups",		CEndSignUps,		arena,	endsignups_help);
			cmd->AddCommand("listsignups",		CListSignUps,		arena,	listsignups_help);
			cmd->AddCommand("signups",		CListSignUps,		arena,	listsignups_help);
			cmd->AddCommand("clearsignups",		CClearSignUps,		arena,	clearsignups_help);
			cmd->AddCommand("signup",		CSignUp,		arena,	signup_help);
			cmd->AddCommand("removesignup",		CRemoveSignUp,		arena,	removesignup_help);
			cmd->AddCommand("forcesignup",		CSignUp,		arena,	signup_help);
			cmd->AddCommand("forceremovesignup",	CRemoveSignUp,		arena,	removesignup_help);

			rv = MM_OK;
		break;

		case MM_DETACH:

			mm->UnregCallback(CB_MODULEATTACHED,    ModuleAttached,         arena);
			mm->UnregCallback(CB_MODULEDETACHED,    ModuleDetached,         arena);

			cmd->RemoveCommand("signupshelp",	CSignUpsHelp,		arena);
			cmd->RemoveCommand("signupsversion",	CSignUpsVersion,	arena);
			cmd->RemoveCommand("addevent",		CAddEvent,		arena);
			cmd->RemoveCommand("delevent",		CDelEvent,		arena);
			cmd->RemoveCommand("chgevent",		CChgEvent,		arena);
			cmd->RemoveCommand("listevents",	CListEvents,		arena);
			cmd->RemoveCommand("startsignups",	CStartSignUps,		arena);
			cmd->RemoveCommand("endsignups",	CEndSignUps,		arena);
			cmd->RemoveCommand("listsignups",	CListSignUps,		arena);
			cmd->RemoveCommand("signups",		CListSignUps,		arena);
			cmd->RemoveCommand("clearsignups",	CClearSignUps,		arena);
			cmd->RemoveCommand("signup",		CSignUp,		arena);
			cmd->RemoveCommand("removesignup",	CRemoveSignUp,		arena);
			cmd->RemoveCommand("forcesignup",	CSignUp,		arena);
			cmd->RemoveCommand("forceremovesignup",	CRemoveSignUp,		arena);

			rv = MM_OK;
		break;

		case MM_UNLOAD:

			mm->UnregInterface(&_int, ALLARENAS);

			ReleaseInterfaces();

			rv = MM_OK;
		break;
	}
	return rv;
}