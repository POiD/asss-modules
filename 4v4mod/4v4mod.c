//INCLUDES#####################################################################################################################
#include "asss.h"
#include "4v4mod.h"
#include <string.h>
#include <stdio.h>
//#############################################################################################################################



//INTERFACES###################################################################################################################
local Imodman* mm;
local Ichat* chat;
local Iarenaman* aman;
local Iplayerdata* pd;
local Igame* game;
local Icmdman* cmd;
//#############################################################################################################################



//GLOBALS######################################################################################################################
local int arenaKey;
//Arena* arena4v4;
//#############################################################################################################################



//CALLBACKS####################################################################################################################
void Action(Player* p, int action, Arena* arena)// - OK?
{
	if (!p)
		return;

	if ((action == PA_ENTERARENA) || (action == PA_CONNECT))
	{//player entered the zone? arena?
		if (p->p_ship != SHIP_SPEC)
			SpecPlayer(p);

		chat->SendMessage(p, "Welcome to 4v4prac. Use ?leaguehelp to get a list of commands. In PB, all commands must start with ? instead of ! or .");
	}
	else if ((action == PA_LEAVEARENA) || (action == PA_DISCONNECT))//captain left, player (also captain) left, player (not captain) left
	{//player left the zone? arena?
		Team4v4Data* td = GetCaptainsTeam(p);
		if (IsValidTeam(td,p->arena)) //player was a captain
			AssignNewCaptain(p, td);//replace the captain with a new one from his team (not p)

		if (strncmp(td->captain, p->name, NAMESIZE) == STRNCMP_MATCH)//team still has no captain, because there aren't any players in the game
		{
			chat->SendArenaMessage(p->arena,"Team %i has no captain or players, ending game.", td->freq);
				EndMatch(p->arena);
		}
		else if (PlayersOnFreq(p->arena, p->p_freq) == 0)//in case the captain is still in arena, but everyone playing specced
		{
			chat->SendArenaMessage(p->arena, "Team %i has no players, ending game.", p->p_freq);
			EndMatch(p->arena);
			return;
		}
	}
}

void ShipChange(Player* p, int newship, int oldship, int newfreq, int oldfreq) // - OK
{

	if (!GameIsRunning(p->arena))//game not running, do nothing
		return;
	//game is running after this point

	Player4v4Data* p4d = GetPlayersData(p);
	if (!IsValidPlayer(p4d, p->arena))//player doesn't have a slot, he cannot enter
	{
		SpecPlayer(p);//doesn't recursively call the shipchange CB
		chat->SendMessage(p, "You cannot enter while a game is in progress.");
	}
	//game is running and player data is found after this point

	else
	{//we found data for this player, he was/is in the game
		if ((oldship != SHIP_SPEC) && (newship == SHIP_SPEC))
		{//this was a player going into spec, attribute a lagout
			p4d->lagouts++;
		}
		else if ((oldship == SHIP_SPEC) && (newship != SHIP_SPEC))
		{//player leaving spec, but was on a team. he should use !return
			SpecPlayer(p);
			chat->SendArenaMessage(p->arena, "Use ?return or ?sub to enter during a game.");
		}
		else if ((oldship != SHIP_SPEC) && (newship != SHIP_SPEC)) 
		{//regular sc, check timer to make sure player can do this

			if (TICK_DIFF(current_ticks(), p->last_death) > 1000)
			{//more than 10 seconds since his death, player cannot sc
				//game->SetShipAndFreq(p, oldship, 8025); //for matches later, maybe. for now do nothing but notify 
				chat->SendArenaMessage(p->arena, "Illegal ship change by %s.", p->name);
				//reset players items (to do)
			}
			else
			{//legal sc, update lastship information
				p4d->lastship = newship;//update this for using !return
				chat->SendArenaMessage(p->arena, "%s has changed ships to %s.", p->name, shipstrings[newship]);
			}
		}
	}
}

void Kill(Arena* arena, Player* killer, Player* killed, int bounty, int flags, int pts, int green) // - OK
{
	if (!GameIsRunning(killer->arena))//game not running, do nothing
		return;
	//game is running after this point

	Player4v4Data* killerdata = GetPlayersData(killer);
	Player4v4Data* killeddata = GetPlayersData(killed);
	if (!IsValidPlayer(killerdata, killer->arena))//killer player doesn't have a slot/data during a game. How did this happen?
	{
		SpecPlayer(killer);//doesn't recursively call the shipchange CB
		chat->SendArenaMessage(killer->arena, "ERROR: Data not found for killer/killed player. Stats not recorded.");
		return;
	}
	else if (!IsValidPlayer(killeddata, killer->arena))//killed player doesn't have a slot/data during a game. How did this happen?
	{
		SpecPlayer(killed);//doesn't recursively call the shipchange CB
		chat->SendArenaMessage(killer->arena, "ERROR: Data not found for killer/killed player. Stats not recorded.");
		return;
	}

	//we have data after this point
	chat->SendArenaMessage(killer->arena, "%s killed by: %s.", killed->name, killer->name);

	killeddata->deaths++;

	if (killer->p_freq == killed->p_freq)
		killerdata->teamkills++;
	else
		killerdata->kills++;

	int lives = LivesLeft(killed);
	if (lives <= 0)
	{
		chat->SendArenaMessage(killer->arena, "%s IS OUT.", killed->name);
		SpecPlayer(killed);//doesn't recursively call the shipchange CB
	}
	else if (lives > 0)
		chat->SendArenaMessage(killer->arena, "%s has %i lives left.", killed->name, lives);
	else
		chat->SendArenaMessage(killer->arena, "ERROR: Negative life count: %i.", lives);

	Team4v4Data* tkillerd = GetPlayersTeam(killer);
	Team4v4Data* tkilledd = GetPlayersTeam(killed);
	if (!IsValidTeam(tkillerd, killer->arena))
	{
		chat->SendArenaMessage(killer->arena, "ERROR: Data not found for killer's team. Team stats not recorded.");
		return;
	}
	else if (!IsValidTeam(tkilledd, killer->arena))
	{
		chat->SendArenaMessage(killer->arena, "ERROR: Data not found for killed's team. Team stats not recorded.");
		return;
	}

	tkillerd->score++;
	if (tkillerd->score >= MAXSCORE)//one team reached max score, end prac
		EndMatch(killer->arena);
	else if (PlayersOnFreq(killer->arena, killer->p_freq) == 0)//one team has nobody left in the game, end prac
		EndMatch(killer->arena);
	else 
		chat->SendArenaMessage(killer->arena, "Score: 100-%i 200-%i", tkillerd->score, tkilledd->score);
}

//#############################################################################################################################



//COMMAND FUNCTIONS############################################################################################################
/*			
			//captain commands
			cmd->AddCommand("cap", Ccap, arena, getplayers_help);
			cmd->AddCommand("ready", Cready, arena, getplayers_help);
			cmd->AddCommand("rdy", Cready, arena, getplayers_help);
			cmd->AddCommand("restart", Crestart, arena, getplayers_help);
			cmd->AddCommand("end", Cend, arena, getplayers_help);
			cmd->AddCommand("cancel", Ccancel, arena, getplayers_help);
			cmd->AddCommand("removecap", Cremovecap, arena, getplayers_help);
			cmd->AddCommand("kick", Ckick, arena, getplayers_help);

			//player commands
			cmd->AddCommand("leaguehelp", Cleaguehelp, arena, getplayers_help);
			cmd->AddCommand("return", Creturn, arena, getplayers_help);
			cmd->AddCommand("sub", Csub, arena, getplayers_help);
			cmd->AddCommand("freqinfo", Cfreqinfo, arena, getplayers_help);
			cmd->AddCommand("gameinfo", Cgameinfo, arena, getplayers_help);
			cmd->AddCommand("teamsize", Cteamsize, arena, getplayers_help);
			cmd->AddCommand("lives", Clives, arena, getplayers_help);

			//ref commands
			cmd->AddCommand("forcereset", Cforcereset, arena, getplayers_help);

*/

void Cforcereset(const char* command, const char* params, Player* p, const Target* target)//agile/poid only, unless poid changes it to ref
{
	if (!p)
		return;

	EndMatch(p->arena);
	InitializeArenaData(p->arena);
}

void Csc(const char* command, const char* params, Player* p, const Target* target)
{
	if (!p)//ERROR CHECKING
		return;
	if (!GameIsRunning(p->arena))
	{
		chat->SendMessage(p, "Game is not running.");
		return;
	}
	if (p->p_ship == SHIP_SPEC)
	{
		chat->SendMessage(p, "Spectators cannot change ship. If you are returning to a game, use ?return.");
		return;
	}
	if (TICK_DIFF(current_ticks(), p->last_death) > 1000)//player died more than 10s ago (tick is 10ms or hundredth of a second)
	{
		chat->SendMessage(p, "You haven't died in the last 10 seconds, and cannot ship change.");
		return;
	}
	//past this point, game is running and player is in the game, and within 10s of a death


	int targShip = SHIP_WARBIRD;
	sscanf(params, "%d", &targShip);
	if ((targShip >= SHIP_WARBIRD) && (targShip <= SHIP_SHARK))
		game->SetShip(p, targShip);
	else
		chat->SendArenaMessage(p->arena, "ERROR: in Csc function, no valid ship type found.");
}

void Citems(const char* command, const char* params, Player* p, const Target* target)
{
	if (p)
		chat->SendMessage(p, "Not implemented yet. Complain to Agile.");
}

void Cleaguehelp(const char* command, const char* params, Player* p, const Target* target)
{
	if (!p)
		return;//error checking

	chat->SendMessage(p, "4v4 commands: ?cap, ?removecap, ?ready, ?end, ?restart, ?cancel, ?kick, ?teamsize, ?lives, ?sc, ?return, ?sub, ?freqinfo, ?gameinfo");
}

void Ckick(const char* command, const char* params, Player* p, const Target* target)
{
	if (!p)
		return;//error checking

	Team4v4Data* td = GetCaptainsTeam(p);
	if (td)//this player was a captain and we have his team
	{
		Player* targPlayer = pd->FindPlayer(params);
		if (targPlayer)
		{
			if (targPlayer->p_freq == td->freq)//this player was on the freq the captain owns
				game->SetShipAndFreq(targPlayer, SHIP_SPEC, 8025);
			else
				chat->SendMessage(p, "Target player is not on your team.");
		}
		else
			chat->SendMessage(p, "Target player was not found.");
	}
}

void Cfreqinfo(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);

	chat->SendMessage(p, "Game running: %i, Players per team: %i, Lives per player: %i", mad->gameRunning, mad->playersSetting, mad->livesSetting);
	FOR_EACH_TEAM(t)
	{//send the calling player the captains of each team and their ready/not ready status
		chat->SendMessage(p, "Frequency: %i, Captain: %s, Ready: %i, End: %i, Restart: %i", mad->team[t].freq, mad->team[t].captain, mad->team[t].ready, mad->team[t].end, mad->team[t].restart);
	}
}

void Crestart(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	if (!GameIsRunning(p->arena))
	{
		if (p)
			chat->SendMessage(p,"Game is not running.");
		return;//game is not running, do nothing
	}
		
	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena)) //player was not a captain, do nothing
		return;

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);

	if (TotalScore(p->arena) > 5)
	{//too many kills to restart
		chat->SendMessage(p, "Too many kills to restart. Please use !end.");
		return;
	}
	//past this point: game is running, player was a valid captain, and there werent too many kills to restart
	
	td->restart = true;
	chat->SendArenaMessage(p->arena, "%s wants to restart the game", p->name);

	FOR_EACH_TEAM(t)
	{//if any team doesn't want to restart, do nothing
		if (mad->team[t].restart == false)
			return;
	}

	//if we got here, all teams wanted a restart. "fake end" here with reinitializing both teams
	chat->SendArenaMessage(p->arena, "All teams have agreed to restart.");
	ShowStats(p->arena);
	mad->gameRunning = false;

	FOR_EACH_TEAM(t)
	{
		InitializeWinnerData(&mad->team[0], p->arena);//resets everything but the captain and freq
	}
}

void Cend(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	if (!GameIsRunning(p->arena))
	{
		if (p)
			chat->SendMessage(p, "Game is not running.");
		return;//game is not running, do nothing
	}

	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena)) //player was not a captain, do nothing
		return;

	td->end = true;
	chat->SendArenaMessage(p->arena, "%s wants to end the game", p->name);
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);

	FOR_EACH_TEAM(t)
	{//if any team doesn't want to end, do nothing
		if (mad->team[t].end == false)
			return;
	}

	//if we got here, all teams want to end
	chat->SendArenaMessage(p->arena, "All teams have agreed to end.");

	//do for all teams somehow******************************************************************************************
	EndMatch(p->arena);
}

void Creturn(const char* command, const char* params, Player* p, const Target* target)//MODIFY FOR PLAYERS WHO GOT/STAYED IN SOMEHOW
{
	//ERROR CHECKING
	if (!p)
		return;
	if (!GameIsRunning(p->arena))
	{
		chat->SendMessage(p, "Game is not running.");
		return;//game is not running, do nothing
	}
	if (p->p_ship != SHIP_SPEC)
	{
		chat->SendMessage(p,"You are already in the game.");
		return;
	}
	Player4v4Data* p4d = GetPlayersData(p);
	Team4v4Data* td = GetPlayersTeam(p);
	if (!IsValidPlayer(p4d, p->arena) || !IsValidTeam(td, p->arena))
	{
		chat->SendMessage(p,"You were not found on either team.");
		return;
	}
	if (LivesLeft(p) < 1)
	{
		chat->SendMessage(p, "You have no lives left.");
		return;
	}
	//ERROR CHECKS OK

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!IsSubSlot(p))
	{//this player wasn't subbed, just put them in
		game->SetShipAndFreq(p, p4d->lastship, td->freq);
		chat->SendArenaMessage(p->arena, "%s returns to the game on %i", p->name, td->freq);
		BurnItems(p);
		return;
	}
	else//this player was in a "sub slot"
	{
		//first we need to know if this guy was the subber or the subbed
		FOR_EACH_TEAM(t)
		{
			FOR_EACH_SLOT(plyr)
			{
				if (&mad->team[t].slots[plyr] == p4d)//this player was subbed by someone else
				{
					chat->SendMessage(p, "You have already been subbed and cannot return.");
					return;//no need to continue searching
				}
				else if (&mad->team[t].subs[plyr] == p4d)//this player was a sub, let him in
				{
					game->SetShipAndFreq(p, p4d->lastship, td->freq);
					chat->SendArenaMessage(p->arena, "%s returns to the game on %i", p->name, td->freq);
					BurnItems(p);
					return;//no need to continue searching
				}
			}
		}
	}

	//shouldn't be possible to reach this part of code
	chat->SendArenaMessage(p->arena,"ERROR: In return command callback. Reached past end of loop.");
}

void Csub(const char* command, const char* params, Player* p, const Target* target)
{
	if (!p)//ERROR CHECKING
		return;
	if (!GameIsRunning(p->arena))//if game is not running, just put the player on 100 or 200
	{
		if (!params || (sizeof(params) < 3))//make sure there are at least 3 characters in params
			game->SetShipAndFreq(p, SHIP_WARBIRD, 200);
		else
		{//player has entered 3 characters after the space, should be a team number
			int targfreq = 0;
			sscanf(params, "%d", &targfreq);
			if ((targfreq == 100) || (targfreq == 200))
				game->SetShipAndFreq(p, SHIP_WARBIRD, targfreq);
			else
				game->SetShipAndFreq(p, SHIP_WARBIRD, 200);

			return;//player was set to either 100 or 200 already
		}
	}
	//past this point, game was running and player must be on a team
	Player4v4Data* p4d = GetPlayersData(p);
	if (IsValidPlayer(p4d, p->arena))//this guy was already in
	{
		chat->SendMessage(p, "You cannot sub if you were already in.");
		return;
	}
	//ERROR CHECKS OK

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	FOR_EACH_TEAM(t)
	{
		FOR_EACH_SLOT(plyr)
		{
			if (strncmp(mad->team[t].subs[plyr].name, INVALID_NAME, NAMESIZE) == STRNCMP_MATCH)//this slot was never subbed
			{
				//but is he still in?
				Player* thisguy = pd->FindPlayer(mad->team[t].slots[plyr].name);
				if (!thisguy || (thisguy->p_ship == SHIP_SPEC))//if player wasn't found (IE, they left) or isn't in the game (in spec)
				{//this slot is available for subbing
					strncpy(mad->team[t].subs[plyr].name, p->name, NAMESIZE);//put this player in on this sub slot
					game->SetShipAndFreq(p, SHIP_WARBIRD, mad->team[t].freq);//put him in the game
					BurnItems(p);//burn the items
					chat->SendArenaMessage(p->arena, "%s subs in for %s on %i.", p->name, mad->team[t].slots[plyr].name, mad->team[t].freq);
					return;
				}
			}
		}
	}
	//we searched all the slots and didn't find one available for subbing

	chat->SendMessage(p, "There are no available slots to sub into.");
}

void Ccap(const char* command, const char* params, Player* p, const Target* target) // - OK
{
	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena))
	{//player is not a captain
		AddCaptain(p);
	}
	else
	{
		chat->SendMessage(p, "You are already the captain of %i", td->freq);
		return;
	}
}

void Cremovecap(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena))
	{//player was not a captain, do nothing
		return;
	}
	//after this point, player was a valid captain

	strncpy(td->captain, INVALID_NAME, NAMESIZE);//reset the captain to "INVALID_NAME"
	chat->SendArenaMessage(p->arena, "%s is no longer the captain of freq %i", p->name, td->freq);
}

void Cgameinfo(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	if (GameIsRunning(p->arena))
	{//send the stats, if the game was running
		ShowStatsPriv(p);
	}
	else
	{
		if (p)
			chat->SendMessage(p, "The game is not running, no info to show.");
	}
}

void Cready(const char* command, const char* params, Player* p, const Target* target) // - OK
{
	if (GameIsRunning(p->arena))
	{
		if (p)
			chat->SendMessage(p, "Game is already running.");
		return;//game is running, do nothing
	}
	//past this point, game is not running

	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena)) //player was not a captain, do nothing
	{
		chat->SendMessage(p, "You aren't a captain.");
		return;
	}
	//past this point, game is not running and player was a valid captain

	//make this captain's team ready
	td->ready = true;
	chat->SendArenaMessage(p->arena, "%s is ready to start", p->name);

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	FOR_EACH_TEAM(t)
	{
		if (mad->team[t].ready == false)//one of the teams isn't ready
			return;//do nothing
	}
	//past this point, all captains say they are ready

	if (FourEachTeam(p->arena))//make sure each team has the right number of players before we start
		StartGame(p->arena);//counts were also ok - start the game
	else
		chat->SendArenaMessage(p->arena,"Some teams don't have the correct number of players. Canceling start.");
}

void Ccancel(const char* command, const char* params, Player* p, const Target* target) // - OK
{
	Team4v4Data* td = GetCaptainsTeam(p);
	if (!IsValidTeam(td, p->arena))
	{//player was not a captain, do nothing
		chat->SendMessage(p, "You aren't a captain.");
		return;
	}
	//after this point, player is a captain and valid team data was found

	if (GameIsRunning(p->arena))//cancel the end/restart request
	{
		td->restart = false;
		td->end = false;
		chat->SendArenaMessage(p->arena, "%s of freq %i cancels request to end/restart", p->name, td->freq);
	}
	else if (!GameIsRunning(p->arena))//cancel the ready request
	{
		td->ready = false;
		chat->SendArenaMessage(p->arena, "%s cancels request to start.", p->name);
	}
}

void Cteamsize(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!params)
	{
		chat->SendMessage(p, "Current teamsize is %i", mad->playersSetting);
		return;
	}
	//after this point, player typed something after the command (params existed)

	Team4v4Data* td = GetCaptainsTeam(p);
	if (GameIsRunning(p->arena) || !IsValidTeam(td, p->arena))
	{//player was not a captain, do nothing
		chat->SendMessage(p,"Cannot set teams during a game or if you aren't a captain.");
		return;
	}
	//after this point, player was a captain and game wasn't running

	int ts = params[0] - '0';//converts a 0-9 char to a number, "in theory", verify in game
	if ((ts < 1) || (ts > 4))
	{//not a valid teamsize, send player the current team size
		chat->SendMessage(p, "%i is not a valid team size. Options: 1, 2, 3, or 4 players", ts);
		return;
	}
	//after this point, valid captain made valid request to change team size to a number ts

	td->desiredPlayers = ts;//set new desired players value
	chat->SendArenaMessage(p->arena, "%s requests team size be set to %i.", p->name, td->desiredPlayers);
	
	int agreedplayers = mad->team[0].desiredPlayers; //set the "agreed" value to first team's desired and check for agreement
	FOR_EACH_TEAM(t)
	{
		if (mad->team[t].desiredPlayers != agreedplayers)
		{//we found a team that doesn't agree
			return;//do nothing
		}
	}
	
	//after this point, captains have agreed to set a new teamsize
	mad->playersSetting = agreedplayers;//set it to the number, which all captains agreed on and is a valid size
	chat->SendArenaMessage(p->arena, "Players agreed upon and set to %i.", mad->playersSetting);
}

void Clives(const char* command, const char* params, Player* p, const Target* target)// - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!params)
	{
		chat->SendMessage(p, "Current lives are %i", mad->livesSetting);
		return;
	}
	//after this point, player typed something after the command (params existed)

	Team4v4Data* td = GetCaptainsTeam(p);
	if (GameIsRunning(p->arena) || !IsValidTeam(td, p->arena))
	{//player was not a captain, do nothing
		chat->SendMessage(p, "Cannot set lives during a game or if you aren't a captain.");
		return;
	}
	//after this point, player was a captain and game wasn't running

	int ls = params[0] - '0';//converts a 0-9 char to a number, "in theory", verify in game
	if ((ls < 1) || (ls > 9))
	{//not a valid life count, send player the current team size
		chat->SendMessage(p, "%i is not a valid team size. Options: 1-9 lives", ls);
		return;
	}
	//after this point, valid captain made valid request to change lives to a number ls

	td->desiredLives = ls;//set new desired lives value
	chat->SendArenaMessage(p->arena, "%s requests team size be set to %i.", p->name, td->desiredLives);

	int agreedLives = mad->team[0].desiredLives; //set the "agreed" value to first team's desired and check for agreement
	FOR_EACH_TEAM(t)
	{
		if (mad->team[t].desiredLives != agreedLives)
		{//we found a team that doesn't agree
			return;//do nothing
		}
	}

	//after this point, captains have agreed to set a new lives value
	mad->livesSetting = agreedLives;//set it to the number, which all captains agreed on and is a valid size
	chat->SendArenaMessage(p->arena, "Lives agreed upon and set to %i.", mad->livesSetting);
}
//#############################################################################################################################



//COMMAND RELATED FUNCTIONS####################################################################################################
void AddCaptain(Player* p)//to any open team - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);

	FOR_EACH_TEAM(t)
	{
		if (strncmp(mad->team[t].captain, INVALID_NAME, NAMESIZE) == STRNCMP_MATCH)
		{//this team had no captain - name INVALID_NAME means no captain
			strncpy(mad->team[t].captain, p->name, NAMESIZE);//make player p the new captain of this freq
			chat->SendArenaMessage(p->arena, "%s is the new captain of freq %i", p->name, mad->team[t].freq);
			return;
		}
	}

	//couldn't add a captain due to no open spots
	chat->SendMessage(p, "No available captain spots.");
}

void ShowStats(Arena* arena) // - OK
{
	if (!arena || !GameIsRunning(arena))
		return;//error checks

	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);

	//send stats in public
	chat->SendArenaMessage(arena, "Name                 |Ki/De|TK LO|");
	FOR_EACH_TEAM(t)
	{
		chat->SendArenaMessage(arena, "Freq %i:", mad->team[t].freq);
		for (int plyr = 0;plyr < 4;plyr++)
		{//no need to check here, this player should always exist
			chat->SendArenaMessage(arena, "%-20s |%2i/%2i|%2i %2i|", mad->team[t].slots[plyr].name, mad->team[t].slots[plyr].kills,
				mad->team[t].slots[plyr].deaths, mad->team[t].slots[plyr].teamkills, mad->team[t].slots[plyr].lagouts);

			if (strncmp(mad->team[t].subs[plyr].name, INVALID_NAME, NAMESIZE) != STRNCMP_MATCH)//if not invalid name
			{
				chat->SendArenaMessage(arena, "%-20s |%2i/%2i|%2i %2i|", mad->team[t].subs[plyr].name, mad->team[t].subs[plyr].kills,
					mad->team[t].subs[plyr].deaths, mad->team[t].subs[plyr].teamkills, mad->team[t].subs[plyr].lagouts);
			}
		}
	}
}

void ShowStatsPriv(Player* p)
{
	if (!p || !GameIsRunning(p->arena))
		return;//error checks

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);

	chat->SendMessage(p, "Name                 |Ki/De|TK-LO|");
	FOR_EACH_TEAM(t)
	{
		chat->SendMessage(p, "Freq %i:", mad->team[t].freq);
		for (int plyr = 0;plyr < 4;plyr++)
		{//no need to check here, this player should always exist
			chat->SendMessage(p, "%-20s |%2i/%2i|%2i %2i|", mad->team[t].slots[plyr].name, mad->team[t].slots[plyr].kills,
				mad->team[t].slots[plyr].deaths, mad->team[t].slots[plyr].teamkills, mad->team[t].slots[plyr].lagouts);

			if (strncmp(mad->team[t].subs[plyr].name, INVALID_NAME, NAMESIZE) != STRNCMP_MATCH)//if not invalid name
			{
				chat->SendMessage(p, "%-20s |%2i/%2i|%2i %2i|", mad->team[t].subs[plyr].name, mad->team[t].subs[plyr].kills,
					mad->team[t].subs[plyr].deaths, mad->team[t].subs[plyr].teamkills, mad->team[t].subs[plyr].lagouts);
			}
		}
	}
}
//#############################################################################################################################



//LOCAL/HELPER FUNCTIONS#######################################################################################################
//asss-related----------------------------------------------------
void ReleaseAllInterfaces(Imodman* mm_, Arena* arena)// gets called in 3 cases. no need to rewrite it all 3 times - OK
{
	if (!mm_)
	{
		chat->SendArenaMessage(arena,"CRITICAL UNLOADING ERROR: mm_ pointer was null pointer");
		//we can't really do anything about this (module is closing), but it's nice to know what happened
	}

	mm->ReleaseInterface(aman);
	mm->ReleaseInterface(chat);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(cmd);
}

void AssignNewCaptain(Player* p, Team4v4Data* td)//p = captain to be removed - OK
{
	if (!p)
		return;
	if (!IsValidTeam(td, p->arena))
	{
		chat->SendArenaMessage(p->arena, "ERROR: in AssignNewCaptain function. Invalid data passed to function. Returning.");
		return;
	}
	
	//p is the leaving captain and td is his (valid) team
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	FOR_EACH_SLOT(plyr)
	{
		if ( (strncmp(td->slots[plyr].name, p->name, NAMESIZE) != STRNCMP_MATCH) //this slot isn't p (the captain leaving)
		&& (pd->FindPlayer(td->slots[plyr].name))) //and this player still exists in this arena
		{//this player is on the captain's team and still exists in the arena
			strncpy(td->captain, td->slots[plyr].name, NAMESIZE);//make him the new captain of this team
			chat->SendArenaMessage(p->arena, "%s has left the arena with cap. %s is the new captain of freq %i", p->name, td->captain, td->freq);
			return;//we replaced the captain, end the search
		}
	}
}

//game-related----------------------------------------------------
void EndMatch(Arena* arena) // - OK
{// show stats, reset/initialize data, retain winning captain
	int topscore = 0;
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	ShowStats(arena);

	Team4v4Data* winner = &mad->INVALID_TEAM;//here we see if there was a winning team, IE, only one team with the top score
	FOR_EACH_TEAM(t)
	{
		if (mad->team[t].score > topscore) //found a score higher than the current highest score in this search
		{
			topscore = mad->team[t].score;
			winner = &mad->team[t];
		}
		else if (mad->team[t].score == topscore) //found a second team with the "top score", remove winner (due to tie)
		{//dont reset the score though. we keep this in case there is a third team with an even higher score (not supported)
			winner = &mad->INVALID_TEAM;//no winner
		}
		//if their score was less than topscore, do nothing
	}
	//done comparing scores, we should have a "winner" and a "top score"

	if (!IsValidTeam(winner, arena))//nobody "won", reset everything
	{
		chat->SendArenaMessage(arena, "No winning team (tie game). Resetting both captains.");
		FOR_EACH_TEAM(t)
		{
			InitializeTeamData(&mad->team[t], mad->team[t].freq, arena);//reset "losing" team's data (including cap)
		}
		return;
	}
	else //somebody won, keep their captain and freq and reset everyone else
	{
		FOR_EACH_TEAM(t)
		{
			if (winner != &mad->team[t])//this is not the winning team, judged by location in memory
				InitializeTeamData(&mad->team[t], mad->team[t].freq, arena);//reset losing team's data (including cap)
		}

		InitializeWinnerData(winner, arena);//reset winner's team data (not cap or freq)
		mad->gameRunning = false;
	}
}

void StartGame(Arena* arena)// TODO: add a countdown - OK
{
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);

	Player* p;
	Link* link;

	pd->Lock();
	FOR_EACH_PLAYER(p)//check each player
	{
		bool foundteam = false;

		FOR_EACH_TEAM(t)//check each team to match freqs to the player freqs
		{
			FOR_EACH_SLOT(plyr)
			{   //if player is on this freq,                and this slot has no player in it                     and we didnt find a slot for them yet
				if ((mad->team[t].freq == p->p_freq) && (strncmp(mad->team[t].slots[plyr].name, INVALID_NAME, NAMESIZE) == STRNCMP_MATCH) && !foundteam)
				{//found empty player slot, and player is on that freq. put him in that slot
					strncpy(mad->team[t].slots[plyr].name, p->name, NAMESIZE);
					foundteam = true;
					continue;//exit the last loop, cant prevent it from going through the other team, but the checks should handle it
				}
			}
			
		}//we have searched all the players that need registration and added them, and specced everyone else
		
		if (!foundteam)
		{//we searched each team and found no team or slot for this player
			SpecPlayer(p);//wasnt on a team so he must be specced
		}
	}
	pd->Unlock();

	WarpTeams(arena);
	mad->gameRunning = true;

	chat->SendArenaMessage(arena, "Game has started. GO!");
}

void WarpTeams(Arena* arena) // - OK
{
	Target tnptr = {.type = T_FREQ, .u.freq = {arena, 100} };
	Target* t = &tnptr;

	game->WarpTo(t,256,256);
	t->u.freq.freq = 200;
	game->WarpTo(t, 768, 768);
}

int PlayersOnFreq(Arena* arena, int freq)
{
	Link* link;
	Player* p;
	int total = 0;
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->p_freq == freq)
			total++;
	}
	pd->Unlock();
	return total;
}

//checks--------------------------------------------------------------------------------------------------------------------
bool GameIsRunning(Arena* arena) // - OK
{
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	return mad->gameRunning;
}

int LivesLeft(Player* p)// MAY CAUSE ISSUES WITH CHAIN SUBBING. MAKE SURE NOBODY SUBS THEIR OWN SLOT - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	Player4v4Data* p4d = GetPlayersData(p);
	if (!IsValidPlayer(p4d, p->arena))
	{//This player isn't in the player data, why did we search for it?
		chat->SendArenaMessage(p->arena, "ERROR: in LivesLeft function, player data was not found");
		return mad->livesSetting; //return 3 instead of 0 so player doesn't get kicked, in event of an error
	}
	//past this point we have a valid player and his valid player data

	int lives = mad->livesSetting;
	lives -= p4d->deaths;
	if (!IsSubSlot(p))//player was never subbed, just return 3-his deaths
		return lives;
	else//he was subbed, or is a sub, so we must add the deaths together
	{
		FOR_EACH_TEAM(t)
		{
			FOR_EACH_SLOT(plyr)
			{
				if (&mad->team[t].slots[plyr] == p4d)//if the supplied p4d pointer actually points to this player data
				{
					return ((lives - mad->team[t].slots[plyr].deaths) - mad->team[t].subs[plyr].deaths);
				}
			}
		}
	}

	chat->SendArenaMessage(p->arena,"ERROR: In LivesLeft function - no valid player slot found");
	return mad->livesSetting;//during an error, choose not to spec the player
}

bool FourEachTeam(Arena* arena) //Also works with any valid playersSetting value - OK
{//count the number of players "physically" on each team[t] and compare to playersSetting
	Player* p;
	Link* link;
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);

	int playercount[MAXTEAMS];//one counter for each team
	FOR_EACH_TEAM(t)
	{//initialize the counts to zero
		playercount[t] = 0;
	}
	
	pd->Lock();
	FOR_EACH_PLAYER(p)//we only want to include valid players in our count
	{// dont include people on 201, 202, etc  
		FOR_EACH_TEAM(t)
		{
			if (mad->team[t].freq == p->p_freq)//this player was on this team's freq
				playercount[t]++;//increment this team's player count
		}
	}
	pd->Unlock();
	//all teams player totals have been counted, should be TEAMSIZE per team

	FOR_EACH_TEAM(t)
	{//compare player size and team size
		if (playercount[t] != mad->playersSetting)
		{//found a team with the wrong number of players
			return false;
		}
	}

	//all teams were counted and had the right number of players
	return true;
}

bool IsValidTeam(Team4v4Data* td, Arena* arena) //used to check return values for GetPlayersTeam or GetCaptainsTeam - OK
{
	if (!td)//check for null pointer first, but this shouldn't ever happen
		return false;

	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	FOR_EACH_TEAM(t)
	{
		if (&mad->team[t] == td)//if the supplied td pointer actually points to this team data
			return true;
	}
	//we checked all team structs and found no matches to this one (invalid pointer)

	return false;
}

bool IsValidPlayer(Player4v4Data* p4d, Arena* arena) //Double check that p4d points to a playerdata object - OK
{
	if (!p4d)
		return false;

	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	FOR_EACH_TEAM(t)
	{
		FOR_EACH_SLOT(plyr)
		{
			if (&mad->team[t].slots[plyr] == p4d)//if the supplied p4d pointer actually points to this player data
				return true;
			else if (&mad->team[t].subs[plyr] == p4d)//also checking sub slots
				return true;
		}
	}
	//we checked all teams and all player structs on each and found no matches

	return false;
}

void SpecPlayer(Player* p) // - OK
{
	if (!p)//null pointer checking
		return;

	if (p->p_ship != SHIP_SPEC)//no need to spec people who are already spectators
		game->SetShipAndFreq(p, SHIP_SPEC, 8025);//doesn't recursively call the shipchange CB
}

Player4v4Data* GetPlayersData(Player* p) //returns INVALID_PLAYER if player wasnt found - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!p)//null pointer checking
		return &mad->INVALID_PLAYER;//instead of returning another null pointer and breaking more things

	FOR_EACH_TEAM(t)
	{
		FOR_EACH_SLOT(plyr)
		{
			if (strncmp(mad->team[t].slots[plyr].name, p->name, NAMESIZE) == STRNCMP_MATCH)
				return &mad->team[t].slots[plyr];//if p is in this slot, return the address of that player's data
			else if (strncmp(mad->team[t].subs[plyr].name, p->name, NAMESIZE) == STRNCMP_MATCH)
				return &mad->team[t].subs[plyr];;//if p subbed this slot, return the address of that player's data
		}
	}
	//we have searched all the players that were registered to a team. past this point means nothing was found

	return &mad->INVALID_PLAYER;
}

Team4v4Data* GetPlayersTeam(Player* p)//returns pointer to INVALID_TEAM if player is not on a team - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!p)//null pointer checking
		return &mad->INVALID_TEAM;//instead of returning another null pointer and breaking more things

	FOR_EACH_TEAM(t)
	{
		FOR_EACH_SLOT(plyr)
		{
			if (strncmp(mad->team[t].slots[plyr].name, p->name, NAMESIZE) == STRNCMP_MATCH)
				return &mad->team[t];//found as player, return the address of that team's data
			else if (strncmp(mad->team[t].subs[plyr].name, p->name, NAMESIZE) == STRNCMP_MATCH)
				return &mad->team[t];//found as sub, return the address of that team's data
		}
	}
	//we have searched all the players that were registered to a team. past this point means nothing was found

	return &mad->INVALID_TEAM;
}

Team4v4Data* GetCaptainsTeam(Player* p)//returns pointer to INVALID_TEAM if player is not a captain - OK
{
	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	if (!p)
		return &mad->INVALID_TEAM;//instead of returning another null pointer and breaking more things

	FOR_EACH_TEAM(t)
	{
		if (strncmp(mad->team[t].captain, p->name, NAMESIZE) == STRNCMP_MATCH)
			return &mad->team[t];//found the team, return the address of that team's data
	}
	//we have searched all the players that were registered to a team. past this point means nothing was found

	return &mad->INVALID_TEAM;
}

void InitializeArenaData(Arena* arena)//always this arena - OK
{
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	mad->gameRunning = false;
	mad->playersSetting = MAXPLAYERS;
	mad->livesSetting = STANDARDLIVES;

	FOR_EACH_TEAM(t)
	{//default freqs are 100 and 200, can add more later
		int freq = ((t + 1) * 100); // team 0 = 100, team 1 = 200, and so on up to 98 teams theoretically(freq 9900)
		InitializeTeamData(&mad->team[t], freq, arena);
	}

	InitializePlayerData(&mad->INVALID_PLAYER);
	InitializeTeamData(&mad->INVALID_TEAM, 0, arena);
	strncpy(mad->INVALID_PLAYER.name, INVALID_NAME, NAMESIZE);
	strncpy(mad->INVALID_TEAM.captain, INVALID_NAME, NAMESIZE);
}

void InitializeTeamData(Team4v4Data* td, int frequency, Arena* arena)//requires a frequency so we know which freq this is
{
	strncpy(td->captain, INVALID_NAME, NAMESIZE);
	td->freq = frequency;
	td->score = false;
	td->ready = false;
	td->end = false;
	td->restart = false;
	td->desiredPlayers = MAXPLAYERS;
	td->desiredLives = STANDARDLIVES;

	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	FOR_EACH_SLOT(plyr)//initialize the player slots on this team 
	{
		InitializePlayerData(&td->slots[plyr]);
		InitializePlayerData(&td->subs[plyr]);
	}
}

void InitializeWinnerData(Team4v4Data* td, Arena* arena)//do not reset captain, freq, desiredlives/players
{
	//not reset captain or freq
	td->score = 0;
	td->ready = false;
	td->end = false;
	td->restart = false;

	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	FOR_EACH_SLOT(plyr) //initialize the player slots on this team 
	{
		InitializePlayerData(&td->slots[plyr]);
		InitializePlayerData(&td->subs[plyr]);
	}
}

void InitializePlayerData(Player4v4Data* p4d)
{
	p4d->kills = 0;
	p4d->deaths = 0;
	p4d->teamkills = 0;
	p4d->lagouts = 0;
	p4d->lastship = SHIP_WARBIRD;
	strncpy(p4d->name, INVALID_NAME, NAMESIZE);
	strncpy(p4d->subbed, INVALID_NAME, NAMESIZE);
	strncpy(p4d->subbedby, INVALID_NAME, NAMESIZE);
}

void BurnItems(Player* p)
{
	if (!p)
		return;

	Target tnptr = { .type = T_PLAYER, .u.p = p };
	Target* t = &tnptr;

	game->GivePrize(t, -PRIZE_REPEL, 2);
	game->GivePrize(t, -PRIZE_DECOY, 2);
	game->GivePrize(t, -PRIZE_THOR, 1);
	game->GivePrize(t, -PRIZE_BRICK, 1);
	game->GivePrize(t, -PRIZE_ROCKET, 3);
	game->GivePrize(t, -PRIZE_PORTAL, 2);
}

int TotalScore(Arena* arena)
{
	MyArenaData* mad = P_ARENA_DATA(arena, arenaKey);
	int totalscore = 0;

	FOR_EACH_TEAM(t)
	{
		totalscore += (mad->team[t].score);
	}

	return totalscore;
}

bool IsSubSlot(Player* p)
{
	Player4v4Data* p4d = GetPlayersData(p);
	if (!IsValidPlayer(p4d,p->arena))//error checking
		return true;//better to return true in error - else someone could use !sub and break the game
	//past this point we have a valid player and his valid information

	MyArenaData* mad = P_ARENA_DATA(p->arena, arenaKey);
	FOR_EACH_TEAM(t)//first we find which slot this player was in (which "plyr")
	{
		FOR_EACH_SLOT(plyr)
		{
			if (&mad->team[t].slots[plyr] == p4d) //found the exact location of this player's slot/information
			{
				if (strncmp(mad->team[t].subs[plyr].name, INVALID_NAME, NAMESIZE) == STRNCMP_MATCH)//this slot was never subbed
					return false;
				else
					return true;//sub data was named, meaning somebody subbed already
			}
		}
	}

	chat->SendArenaMessage(p->arena, "ERROR: In IsSubSlot function: No valid player location was found");
	return true;//better to return true in error - else someone could use !sub and break the game
}
//#############################################################################################################################



//HELP TEXT####################################################################################################################
// Command Help Text
local helptext_t getplayers_help =
"Module: template\n"
"Targets: none\n"
"Args: none\n"
"Displays every player in the zone. (Template text)";
//#############################################################################################################################



//THE ENTRY POINT##############################################################################################################
EXPORT int MM_4v4mod(int action, Imodman* mm_, Arena* arena)//this arena
{
	//arena4v4 = arena;//this arena (aka the 4v4 arena)
	int rv = MM_FAIL; // return value

	if (action == MM_LOAD)
	{

		mm = mm_;

		aman = mm->GetInterface(I_ARENAMAN, arena);
		chat = mm->GetInterface(I_CHAT, arena);
		pd = mm->GetInterface(I_PLAYERDATA, arena);
		game = mm->GetInterface(I_GAME, arena);
		cmd = mm->GetInterface(I_CMDMAN, arena);

		if (!aman || !chat || !pd || !game || !cmd) // loading failure
		{
			// release interfaces if loading failed
			ReleaseAllInterfaces(mm,arena);
			rv = MM_FAIL;
		}
		else
			rv = MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		// release interfaces
		ReleaseAllInterfaces(mm, arena);
		rv = MM_OK;
	}

	else if (action == MM_ATTACH)
	{
			// allocate data
		arenaKey = aman->AllocateArenaData(sizeof(MyArenaData));

		if (arenaKey == -1) // check if we ran out of memory
		{
			// release interfaces if loading failed
			ReleaseAllInterfaces(mm, arena);

			// free data if it was allocated
			aman->FreeArenaData(arenaKey);

			rv = MM_FAIL;
		}
		else
		{ // declare callbacks, commands, and timers
			//callbacks
			mm->RegCallback(CB_SHIPFREQCHANGE, ShipChange, arena);
			mm->RegCallback(CB_KILL, Kill, arena);
			mm->RegCallback(CB_PLAYERACTION, Action, arena);

			//captain commands
			cmd->AddCommand("cap", Ccap, arena, getplayers_help);
			cmd->AddCommand("ready", Cready, arena, getplayers_help);
			cmd->AddCommand("rdy", Cready, arena, getplayers_help);
			cmd->AddCommand("restart", Crestart, arena, getplayers_help);
			cmd->AddCommand("end", Cend, arena, getplayers_help);
			cmd->AddCommand("cancel", Ccancel, arena, getplayers_help);
			cmd->AddCommand("removecap", Cremovecap, arena, getplayers_help);
			cmd->AddCommand("kick", Ckick, arena, getplayers_help);
			cmd->AddCommand("teamsize", Cteamsize, arena, getplayers_help);
			cmd->AddCommand("lives", Clives, arena, getplayers_help);

			//player commands
			cmd->AddCommand("leaguehelp", Cleaguehelp, arena, getplayers_help);
			cmd->AddCommand("return", Creturn, arena, getplayers_help);
			cmd->AddCommand("sub", Csub, arena, getplayers_help);
			cmd->AddCommand("freqinfo", Cfreqinfo, arena, getplayers_help);
			cmd->AddCommand("gameinfo", Cgameinfo, arena, getplayers_help);
			cmd->AddCommand("sc", Csc, arena, getplayers_help);
			cmd->AddCommand("items", Citems, arena, getplayers_help);

			//ref commands
			cmd->AddCommand("forcereset", Cforcereset, arena, getplayers_help);

			//no timers yet - add one for gametime

			//initialize the arena data so we dont accidentally start with junk data
			InitializeArenaData(arena);

			rv = MM_OK;
		}
	}
	else if (action == MM_DETACH)//step 5: unregister callbacks, free data, release interfaces
	{
		// unregeister callbacks
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipChange, arena);
		mm->UnregCallback(CB_KILL, Kill, arena);
		mm->UnregCallback(CB_PLAYERACTION, Action, arena);

		// free data we allocated
		aman->FreeArenaData(arenaKey);

		rv = MM_OK;
	}

	return rv;
}

//KNOWN BUGS:
//losing captain didnt lose cap

//TODO:
//NEED-------------------------------------------
//add ?items as a command
//items tracking and resetting for .return or illegal sc. currently just burn them
//timing for the game and players

//WANT-------------------------------------------
//people can spam comands
//config support, find the arenas listed in the config
//nrg view for teammates (OPTION, as a command EG ?teamnrg as a team command)
//!lives 10, 11, and 12 support (currently only does 9)

//#define CB_TIMESUP "timesup"
/** the type of CB_TIMESUP
 * @param arena the arena whose timer is ending
 */
//typedef void (*GameTimerFunc)(Arena* arena);
/* pycb: arena_not_none */

//WANT-------------------------------------------
//VARIABLE LINKEDLIST INSTEAD OF FIXED, -> MORE THAN 4/2 TEAMS/PLAYERS
//DAMAGE TRACKING
//SOMEDAY:DATABASES
//PLAYERS = REF CAN SET