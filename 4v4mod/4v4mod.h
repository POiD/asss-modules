
/* dist: public */

#ifndef FOUR_V_FOUR

#define FOUR_V_FOUR
#define NAMESIZE 21 //20, plus the null terminator
#define MAXPLAYERS 4
#define STANDARDLIVES 3;
#define MAXSCORE 12
#define MAXTEAMS 2
#define INVALID_NAME "INVALID_NAME"
#define STRNCMP_MATCH 0

//requires valid "MyArenaData*" pointer named "mad", from MyArenaData* mad = P_ARENA_DATA(arena4v4, arenaKey);
//use like this: FOR_EACH_TEAM(t)
//{mad->team[t].THINGYOUWANTHERE == false} or whatever
#define FOR_EACH_TEAM(t) for (int t = 0; t < MAXTEAMS; t++)
#define FOR_EACH_SLOT(plyr) for (int plyr = 0; plyr < mad->playersSetting; plyr++) //Requires valid arena data

//SHIP_SPEC = 8, other ships are 0-7
char shipstrings[SHIP_SPEC][10] = { "Warbird", "Javelin", "Spider", "Leviathan", "Terrier", "Weasel", "Lancaster", "Shark" };

typedef struct Player4v4Data//Holds stats for each player in the game - needs an initialize function
{
	char name[NAMESIZE];

	int kills;
	int deaths;
	int teamkills;
	int lagouts;
	int lastship;

	char subbedby[NAMESIZE];
	char subbed[NAMESIZE];
} Player4v4Data;

typedef struct Team4v4Data
{
	char captain[NAMESIZE];

	int freq;
	int score;
	int ready;
	int end;
	int restart;
	int desiredPlayers;
	int desiredLives;

	Player4v4Data slots[MAXPLAYERS];
	Player4v4Data subs[MAXPLAYERS];
} Team4v4Data;

typedef struct MyArenaData
{
	//To find the player's team use GetPlayersTeam(Player* p);, returns a Team4v4Data* pointer to his team
	Team4v4Data team[MAXTEAMS];

	bool gameRunning;
	int playersSetting;
	int livesSetting;

	Player4v4Data INVALID_PLAYER; //used instead of null pointer to indicate searched player was not found
	Team4v4Data INVALID_TEAM; //used instead of null pointer to indicate searched team was not found
	//There should be zero null pointers passed anywhere in this program
} MyArenaData;

void ShipChange(Player* p, int newship, int oldship, int newfreq, int oldfreq);
void Kill(Arena* arena, Player* killer, Player* killed, int bounty, int flags, int pts, int green);
void EndMatch(Arena* arena);
void BurnItems(Player* p);
void StartGame(Arena* arena);
void WarpTeams(Arena* arena);
void ShowStatsPriv(Player* p);//safe to send NULL pointer: NULL sends to arena, p sends to player
void ShowStats(Arena* arena);
void SpecPlayer(Player* p);
bool GameIsRunning();
int LivesLeft(Player* p);
int TotalScore();
void NewPlayer(Player* p, int isnew);
Player4v4Data* GetPlayersData(Player* p);
Team4v4Data* GetCaptainsTeam(Player* p);
Team4v4Data* GetPlayersTeam(Player* p);
void AddCaptain(Player* p);
void AssignNewCaptain(Player* p, Team4v4Data* td);//p = captain to be removed, td - his team
bool IsValidTeam(Team4v4Data* td, Arena* arena);
bool IsValidPlayer(Player4v4Data* p4d, Arena* arena);
bool FourEachTeam();
int PlayersOnFreq(Arena* arena, int freq);
void InitializeArenaData();
void InitializePlayerData(Player4v4Data * p4d);
void InitializeTeamData(Team4v4Data* td, int freq, Arena* arena);
void InitializeWinnerData(Team4v4Data* td, Arena* arena);
bool IsSubSlot(Player* p);

#endif